<!DOCTYPE html>
<html lang="en" style="background-color: #C3CDE6">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">


</head>

<body id="page-top">
    {{--level 2 approver below--}}
    <p>Hi! Good Day, {{$varObj['header'][0]->creator}} </p>

    <p>
        Your Stock Request #{{$varObj['header'][0]->stock_req_no}} has been approved by {{$varObj['approver']}}
    </p>

    <br>

    <p>You may click the link below to
        <br>

{{--        http://vcypvc-cos.test/stock-request/view-details/created_stock_request/{{$varObj['header'][0]->stock_req_no}}<br> --}}
        http://192.168.102.152/stock-request/view-details/created_stock_request/{{$varObj['header'][0]->stock_req_no}}<br>
    </p>

    <br>
    <p>This is an auto-generated message. Please do not reply to this email.</p>
    <p>Thank you.</p>

</body>

</html>
