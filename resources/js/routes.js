/*auth*/

let Login = () => import(/* webpackChunkName: "login" */'./components/authentication/Login');
let Profile = () => import(/* webpackChunkName: "profile" */'./components/profile/Profile');

/*users*/
let Users = () => import(/* webpackChunkName: "users" */'./components/user/Users');
let UsersTable = () => import(/* webpackChunkName: "users-table" */'./components/user/UsersTable');
let UserCreate = () => import(/* webpackChunkName: "users-create" */'./components/user/Create');
let UserView = () => import(/* webpackChunkName: "users-view" */'./components/user/Show');
let UserEdit = () => import(/* webpackChunkName: "users-edit" */'./components/user/Edit');

/*permission*/
let Permission = () => import(/* webpackChunkName: "permission" */'./components/permission/Permission');
let PermissionsTable = () => import(/* webpackChunkName: "permission-table" */'./components/permission/PermissionsTable');
let PermissionCreate = () => import(/* webpackChunkName: "permission-create" */'./components/permission/Create');
let PermissionView = () => import(/* webpackChunkName: "permission-view" */'./components/permission/Show');
let PermissionEdit = () => import(/* webpackChunkName: "permission-edit" */'./components/permission/Edit');

/*role*/
let Role = () => import(/* webpackChunkName: "role" */'./components/role/Role');
let RolesTable = () => import(/* webpackChunkName: "role-table" */'./components/role/RolesTable');
let RoleCreate = () => import(/* webpackChunkName: "role-create" */'./components/role/Create');
let RoleView = () => import(/* webpackChunkName: "role-view" */'./components/role/Show');
let RoleEdit = () => import(/* webpackChunkName: "role-edit" */'./components/role/Edit');

/*Tile Master*/
let TileIndex = () => import(/* webpackChunkName: "tile-table" */'./components/tileMaster/index');
let TileParent = () => import(/* webpackChunkName: "tile" */'./components/tileMaster/parent');

/*Stock Creation || Stock Request Management*/
let stockCreate = () => import(/* webpackChunkName: "stock-create" */'./components/stock_request_management/Create');
let stockParent = () => import(/* webpackChunkName: "stock" */'./components/stock_request_management/stock');

/*Stock Request*/
let stockReqTable = () => import(/* webpackChunkName: "stock-req-table" */'./components/stock_request/stockTable');
let stockReqView = () => import(/* webpackChunkName: "stock-req-view" */'./components/stock_request/view');
let stockReqParent = () => import(/* webpackChunkName: "stock-req" */'./components/stock_request/parent');

/*Drafts*/
let draftsTable = () => import(/* webpackChunkName: "drafts-table" */'./components/drafts/draftsTable');
let draftsView = () => import(/* webpackChunkName: "drafts-view" */'./components/drafts/view');
let draftsParent = () => import(/* webpackChunkName: "drafts" */'./components/drafts/parent');

/*For Approvals*/
let approvalTable = () => import(/* webpackChunkName: "for-approvals-table" */'./components/forApprovals/approvalTable');
let approvalView = () => import(/* webpackChunkName: "for-approvals-view" */'./components/forApprovals/view');
let approvalParent = () => import(/* webpackChunkName: "for-approvals" */'./components/forApprovals/parent');

/*For Printing*/
let forPrinting = () => import(/* webpackChunkName: "printing-view" */'./components/forPrinting/printing');
let forPrintingTable = () => import(/* webpackChunkName: "printing-table" */'./components/forPrinting/printingTable');
let forPrintingParent = () => import(/* webpackChunkName: "printing" */'./components/forPrinting/parent');
/*Sales Order*/
let salesOrderTable = () => import(/* webpackChunkName: "sales-order-table" */'./components/sales_order/salesOrderTable');
let salesOrderView = () => import(/* webpackChunkName: "sales-order-view" */'./components/sales_order/view');
let salesOrderParent = () => import(/* webpackChunkName: "sales-order-parent" */'./components/sales_order/parent');
let salesOrderPost = () => import(/* webpackChunkName: "manual-export" */'./components/sales_order/post_so');
let salesOrderActivityLog = () => import(/* webpackChunkName: "activity-log" */'./components/sales_order/activity_log');

/*For Diser Tablet*/
let diserTabletParent = () => import(/* webpackChunkName: "diser-tablet" */'./components/diser_tablet/parent');
let diserTabletTable = () => import(/* webpackChunkName: "diser-tablet-table" */'./components/diser_tablet/tabletTable');

/*Auth Change Pass*/
// let authParent = () => import(/* webpackChunkName: "auth-parent" */'./components/auth_pass/Auth');
// let authCreate = () => import(/* webpackChunkName: "auth-create" */'./components/auth_pass/Create');

/*Plant*/
let Plant = () => import(/* webpackChunkName: "plant" */'./components/plant/Plant');
let PlantsTable = () => import(/* webpackChunkName: "plant-table" */'./components/plant/PlantsTable');

/*passport*/
let Passport = () => import(/* webpackChunkName: "passport" */'./components/passport/Passport');

/*errors*/
import Error_404 from "./components/errors/404";


export const routes = [
    // {
    //     path: '/',
    //     name: 'home',
    //     component: Home,
    //     meta: {
    //         title: 'Home',
    //         requiresAuth: true,
    //     }
    // },
    {
        path: '/login',
        name: 'login',
        component: Login,
        meta: {
            title: 'Login',

        }
    },
    {
        path: '/profile',
        name: 'profile',
        component: Profile,
        meta: {
            title: 'Profile',
            requiresAuth: true,
        }
    },
    {
        path: '/users',
        component: Users,
        children: [
            {
                path: '/',
                component: UsersTable,
                name: 'users-table',
                meta: {
                    title: 'Users',
                    roles: ['administrator', 'super_administrator', 'admin'],
                    requiresAuth: true,
                }
            },
            {
                path: 'create',
                component: UserCreate,
                name: 'users-create',
                meta: {
                    title: 'Create User',
                    roles: ['administrator', 'super_administrator', 'admin'],
                    requiresAuth: true,
                }
            },
            {
                path: ':id',
                component: UserView,
                name: 'users-view',
                meta: {
                    title: 'View User',
                    roles: ['administrator', 'super_administrator','admin'],
                    requiresAuth: true,
                }
            },
            {
                path: ':id/edit',
                component: UserEdit,
                name: 'users-edit',
                meta: {
                    title: 'Edit User',
                    roles: ['administrator', 'super_administrator', 'admin'],
                    requiresAuth: true,
                }
            }
        ]
    },
    {
        path: '/permissions',
        component: Permission,
        children: [
            {
                path: '/',
                component: PermissionsTable,
                name: 'permission-table',
                meta: {
                    title: 'Permissions',
                    roles: ['administrator', 'super_administrator','admin'],
                    requiresAuth: true,
                }
            },
            {
                path: 'create',
                component: PermissionCreate,
                name: 'permission-create',
                meta: {
                    title: 'Create Permission',
                    // roles: ['administrator'],
                    roles: ['administrator', 'super_administrator','admin'],
                    requiresAuth: true,
                }
            },
            {
                path: ':id',
                component: PermissionView,
                name: 'permission-view',
                meta: {
                    title: 'View Permission',
                    roles: ['administrator', 'super_administrator','admin'],
                    requiresAuth: true,
                }
            },
            {
                path: ':id/edit',
                component: PermissionEdit,
                name: 'permission-edit',
                meta: {
                    title: 'Edit Permission',
                    roles: ['administrator', 'super_administrator','admin'],
                    requiresAuth: true,
                }
            }
        ]
    },
    {
        path: '/roles',
        component: Role,
        children: [
            {
                path: '/',
                component: RolesTable,
                name: 'role-table',
                meta:
                    {
                        title: 'Roles',
                        roles: ['administrator', 'super_administrator', 'tile_admin'],
                        requiresAuth: true,
                    }

            },
            {
                path: 'create',
                component: RoleCreate,
                name: 'role-create',
                meta:

                    {
                        title: 'Create Role',
                        roles: ['administrator', 'super_administrator', 'tile_admin'],
                        requiresAuth: true,
                    }

            },
            {
                path: ':id',
                component: RoleView,
                name: 'role-view',
                meta: {
                    title: 'View Role',
                    roles: ['administrator', 'super_administrator', 'tile_admin'],
                    requiresAuth: true,
                }
            },
            {
                path: ':id/edit',
                component: RoleEdit,
                name: 'role-edit',
                meta: {
                    title: 'Edit Role',
                    roles: ['administrator', 'super_administrator', 'tile_admin'],
                    requiresAuth: true,
                }
            }
        ]
    },
    /*Diser Tablet*/
    {
        path: '/diser/tablet/device-use/table',
        component: diserTabletParent,
        children: [
            {
                path: '/',
                component: diserTabletTable,
                name: 'diser-tablet-table',
                meta:
                    {
                        title: 'Roles',
                        roles: ['super_administrator'],
                        requiresAuth: true,
                    }
            },
        ]
    },
    /*Stock Creation*/
    {
        path: '/stock',
        component: stockParent,
        children: [
            {
                path: '/',
                component: stockCreate,
                name: 'stock-table',
                meta:
                    {
                        title: 'Roles',
                        roles: ['administrator', 'super_administrator','approver_level_one','approver_level_two','merchandiser'],
                        requiresAuth: true,
                    }
            },
        ]
    },
    /*Stock Request*/
    {
        path: '/stock-request',
        component: stockReqParent,
        children: [
            {
                path: '/',
                component: stockReqTable,
                name: 'stock-request-table',
                meta:
                    {
                        title: 'Roles',
                        roles: ['administrator', 'super_administrator','approver_level_one','approver_level_two','merchandiser'],
                        requiresAuth: true,
                    }
            },
            {
                path: 'view-details/created_stock_request/:id',
                component: stockReqView,
                name: 'stock-request-view',
                meta:
                    {
                        title: 'Roles',
                        roles: ['administrator', 'super_administrator','approver_level_one','approver_level_two','merchandiser'],
                        requiresAuth: true,
                    }
            },
        ]
    },
    /*Drafts*/
    {
        path: '/drafts',
        component: draftsParent,
        children: [
            {
                path: '/',
                component: draftsTable,
                name: 'drafts-table',
                meta:
                    {
                        title: 'Roles',
                        roles: ['administrator', 'super_administrator','approver_level_one','approver_level_two','merchandiser'],
                        requiresAuth: true,
                    }
            },
            {
                path: ':id/edit',
                component: draftsView,
                name: 'drafts-view',
                meta:
                    {
                        title: 'Roles',
                        roles: ['administrator', 'super_administrator','approver_level_one','approver_level_two','merchandiser'],
                        requiresAuth: true,
                    }
            },
        ]
    },
    /*For Approvals*/
    {
        path: '/forApproval',
        component: approvalParent,
        children: [
            {
                path: '/',
                component: approvalTable,
                name: 'approval-table',
                meta:
                    {
                        title: 'Roles',
                        roles: ['administrator', 'super_administrator','approver_level_one','approver_level_two'],
                        requiresAuth: true,
                    }
            },
            {
                path: 'view-details/pvc-stock-request/:id',
                component: approvalView,
                name: 'approval-edit',
                meta:
                    {
                        title: 'Roles',
                        roles: ['administrator', 'super_administrator','approver_level_one','approver_level_two'],
                        requiresAuth: true,
                    }
            },
        ]
    },
    {
        path: '/for-printing',
        component: forPrintingParent,
        children: [
            {
                path: '/',
                component: forPrintingTable,
                name: 'for-printing-table',
                meta:
                    {
                        title: 'Roles',
                        roles: ['administrator', 'super_administrator','approver_level_one','approver_level_two'],
                        requiresAuth: true,
                    }
            },
            {
                path: 'printing/view-details/delivery/release/stock_no/:id',
                component: forPrinting,
                name: 'for-printing',
                meta:
                    {
                        title: 'Roles',
                        roles: ['administrator', 'super_administrator','approver_level_one','approver_level_two'],
                        requiresAuth: true,
                    }
            },
        ]
    },
    /*Sales Order*/
    {
        path: '/sales-order',
        component: salesOrderParent,
        children: [
            {
                path: '/',
                component: salesOrderTable,
                name: 'sales-order-table',
                meta:
                    {
                        title: 'Roles',
                        roles: ['administrator', 'super_administrator','approver_level_one','approver_level_two'],
                        requiresAuth: true,
                    }
            },
            {
                path: 'view-details/ready-to-post-to-sap/sales_order_no/:id',
                component: salesOrderView,
                name: 'sales-order-edit',
                meta:
                    {
                        title: 'Roles',
                        roles: ['administrator', 'super_administrator','approver_level_one','approver_level_two'],
                        requiresAuth: true,
                    }
            },
            {
                path: 'post-to-sap',
                component: salesOrderPost,
                name: 'post-sales-order',
                meta:
                    {
                        title: 'Roles',
                        roles: ['administrator', 'super_administrator','approver_level_one','approver_level_two'],
                        requiresAuth: true,
                    }
            },
            {
                path: 'pvc/activity-log',
                component: salesOrderActivityLog,
                name: 'pvc-activity-log',
                meta:
                    {
                        title: 'Roles',
                        roles: ['administrator', 'super_administrator','approver_level_one','approver_level_two'],
                        requiresAuth: true,
                    }
            },
        ]
    },
    //Auth Change Pass
    // {
    //     path: '/authentication/change-password',
    //     component: authParent,
    //     children: [
    //         {
    //             path: '/authentication/change-password/user=:id',
    //             component: authCreate,
    //             name: 'auth-create',
    //             meta: {
    //                 title: 'Change Password',
    //                 roles: ['super_administrator', 'merchandiser', 'approver_level_two','approver_level_one','administrator'],
    //                 requiresAuth: true,
    //             }
    //         },
    //     ]
    // },
    {
        path: '/plants',
        component: Plant,
        children: [
            {
                path: '/',
                component: PlantsTable,
                name: 'plant-table',
                meta: {
                    title: 'Plant',
                    roles: ['administrator'],
                    requiresAuth: true,
                }
            }
        ]
    },
    {
        path: '/passport',
        component: Passport,
        meta: {
            title: 'Passport',
            requiresAuth: true,
            roles: ['administrator','approver_level_one','super_administrator','approver_level_two'],
        }
    },
    // {
    //     path: '/404',
    //     name: 'error_404',
    //     component: Error_404,
    //     meta: {
    //         title: 'Error 404',
    //     }
    // },
    // {path: '*', redirect: '/404'},

];
