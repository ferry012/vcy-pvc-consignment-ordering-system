<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::post('login', 'Api\Auth\AuthController@login')->name('login');
Route::post('oauth/token', 'Api\AccessTokenController@issueToken');
Route::post('refresh', 'Api\Auth\AuthController@refresh')->name('api.refresh');

Route::middleware('auth:api')->group(function () {

    Route::post('logout', 'Api\Auth\AuthController@logout')->name('logout');
    Route::post('register', 'Api\Auth\AuthController@register')->name('register');
    Route::get('auth-user', 'Api\Auth\AuthController@authenticatedUser')->name('auth-user');
    Route::patch('auth-user/update', 'Api\Auth\AuthController@authenticatedUserUpdate')->name('auth-user');
    Route::resource('users', 'Api\UserController');
    Route::resource('permissions', 'Api\PermissionController');
    Route::resource('roles', 'Api\RoleController');

    /*Stock Creation*/
    Route::resource('stocks','PVC\StockRequestController');

    Route::get('get-mat-group','PVC\StockRequestController@getMatGroup');

    Route::get('get-mat-code','PVC\StockRequestController@getMatCode');

    Route::post('get-mat-barcode','PVC\StockRequestController@getBarcode');
    /*Get ERDAT AND KWMENG Sync with customer name*/
    Route::get('send-cust-data','PVC\StockRequestController@sendCustData');

    Route::resource('plants', 'PlantController');

    Route::get('show/{id}/plantUser','Api\UserController@showPlant');

    Route::get('show/{id}/merchCust','Api\UserController@showCust');

    Route::get('get-customer','PVC\MerchandiserCustController@getCust');

    Route::get('get-customer/stock-creation','PVC\MerchandiserCustController@getCustStockCreation');

    /*Stock Request Data*/
    Route::get('get-stock-request/stock-data','PVC\StockDataController@getStockData');
    Route::get('stock-data/{id}/view','PVC\StockDataController@viewStockData');
    Route::get('stock-data/header/{id}','PVC\StockDataController@headerStockData');

    Route::get('all-company-user','PVC\StockDataController@allCompanyUser');

    Route::get('creator-user','PVC\StockDataController@creatorUser');
    Route::get('all-creator-user','PVC\StockDataController@allCreatorUser');

    /*Save as Drafts*/
    Route::resource('drafts','PVC\DraftsController');
    Route::get('drafts-header/{id}','PVC\DraftsController@getDraftsHeader');

    Route::patch('drafts-update/{id}','PVC\DraftsController@updateDrafts');
    Route::post('create-drafts','PVC\DraftsController@createDrafts');

    Route::patch('divert/{id}/divert-to-stocks','PVC\StockDataController@divertDratftToStocks');

    /*For Approvals*/
    Route::get('approvals/{id}/approval-details','PVC\ForApprovalsController@approvalDetails');
    Route::get('approvals/header/{id}','PVC\ForApprovalsController@approvalHeader');

    Route::post('approve/{id}/request','PVC\ForApprovalsController@approveRequest');
    Route::post('rejected/{id}/request','PVC\ForApprovalsController@rejectRequest');

    Route::get('get-stock-request/approval-data','PVC\StockDataController@getApprovalData');
    Route::get('get-stock-request/approval-data/for-printing','PVC\StockDataController@getApprovalDataForPrinting');

    /*Sales Order*/
    Route::get('get-stock-request/sales-data','PVC\SalesOrderController@salesOrder');
    Route::get('sales-data/header/{id}','PVC\SalesOrderController@headerSalesData');
    Route::get('sales-data/{id}/view','PVC\SalesOrderController@viewSalesData');
    Route::post('sales-data/{id}/post-sap','PVC\PostToSapController@postToSap');

    /*Manual Export*/
    Route::get('showPostSO','PVC\PostToSapController@showPostSO');
    Route::get('manualExport','PVC\PostToSapController@manualExport');

    /*Activity Logs*/
    Route::get('activity-logs','PVC\ActivityLogsController@actLogs');

    /*Range*/
    Route::post('range-barcode','PVC\PVCRangeController@rangeBarcode');
    Route::post('range-mat-code','PVC\PVCRangeController@rangeMatCode');

    Route::post('range-barcode-approval','PVC\PVCRangeController@rangeBarcodeApproval');
    Route::post('range-barcode-drafts','PVC\PVCRangeController@rangeBarcodeDrafts');

    /*Approve Check*/
    Route::get('check-user-create','Api\ApproverCheckController@checkUser');
    Route::get('check-user-view','Api\ApproverCheckController@checkUserView');

    /*Get all barcode if mat group is null or empty*/
    Route::get('get-all-barcode','PVC\StockRequestController@getAllBarcode');

    /*For Printing*/
    Route::get('for-printing-all','PVC\ForPrintingController@getPrintNo');
    Route::post('approve/{id}/for-printing','PVC\ForPrintingController@approveForPrinting');

    Route::get('approvals/{id}/approval-details-for-printing','PVC\ForApprovalsController@approvalDetailsPrinting');

    /*Post Only Once*/
    Route::get('{id}/error-post','PVC\PostToSapController@postOnlyOnce');

    /*Create Disers Device Info*/
    Route::post('diser-device-info','DiserTabletController@diserDeviceInfo');
    Route::get('get-diser-device-info','DiserTabletController@getDiserDeviceInfo');
});


