<?php

use App\Brand;
use App\INVENTORY;
use App\MaterialMaster;
use App\Model\Invoice;
use App\TileMaster;
use App\TradeMark;
use App\User;
use App\VBAP;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});
//Route::get('/test111', function () {
//    $usersToAdmins = User::permission('email_admin')->get();
//    dd($usersToAdmins);
//});
//
//Auth::routes();
//
//Route::get('/home', 'HomeController@index')->name('home');

//Route::get('{path}', function () {
//    return view('home');
//})->where('path', '(.*)');

//Route::get('/test123', function () {
//    $db = DB::connection('sqlsrv2')->getDatabaseName();
//
//    $query = DB::table('invoices as a')
//        ->join($db . '.dbo.INVENTORY as b', function ($join) {
//            $join->on('b.MATNR', '=', 'a.invoice_MATNR');
//            $join->on('b.WERKS', '=', 'a.invoice_plant');
//        })
//        ->where('a.invoice_date','=','09/02/2021')
//        ->select('b.MATNR','a.invoice_market_MAKTX',
//            DB::raw("'2311_total_qty'=(SELECT SUM(d.qty) FROM invoices AS d
//		            WHERE b.MATNR = d.invoice_MATNR AND d.invoice_plant = 2311)"),
//            DB::raw("'2311_total_LABST'=(SELECT SUM(LABST) FROM VCYSYS820.dbo.INVENTORY AS c
//		            WHERE b.MATNR = c.MATNR AND c.WERKS = 2311)"),
//
//            DB::raw("'2321_total_qty'=(SELECT SUM(d.qty) FROM invoices AS d
//		            WHERE b.MATNR = d.invoice_MATNR AND d.invoice_plant = 2321)"),
//            DB::raw("'2321_total_LABST'=(SELECT SUM(LABST) FROM VCYSYS820.dbo.INVENTORY AS c
//		            WHERE b.MATNR = c.MATNR AND c.WERKS = 2321)"),
//
//            DB::raw("'2331_total_qty'=(SELECT SUM(d.qty) FROM invoices AS d
//		            WHERE b.MATNR = d.invoice_MATNR AND d.invoice_plant = 2331)"),
//            DB::raw("'2331_total_LABST'=(SELECT SUM(LABST) FROM VCYSYS820.dbo.INVENTORY AS c
//		            WHERE b.MATNR = c.MATNR AND c.WERKS = 2331)"),
//
//            DB::raw("'2341_total_qty'=(SELECT SUM(d.qty) FROM invoices AS d
//		            WHERE b.MATNR = d.invoice_MATNR AND d.invoice_plant = 2341)"),
//            DB::raw("'2341_total_LABST'=(SELECT SUM(LABST) FROM VCYSYS820.dbo.INVENTORY AS c
//		            WHERE b.MATNR = c.MATNR AND c.WERKS = 2341)"),
//
//            DB::raw("'2351_total_qty'=(SELECT SUM(d.qty) FROM invoices AS d
//		            WHERE b.MATNR = d.invoice_MATNR AND d.invoice_plant = 2351)"),
//            DB::raw("'2351_total_LABST'=(SELECT SUM(LABST) FROM VCYSYS820.dbo.INVENTORY AS c
//		            WHERE b.MATNR = c.MATNR AND c.WERKS = 2351)"),
//
//            DB::raw("'2361_total_qty'=(SELECT SUM(d.qty) FROM invoices AS d
//		            WHERE b.MATNR = d.invoice_MATNR AND d.invoice_plant = 2361)"),
//            DB::raw("'2361_total_LABST'=(SELECT SUM(LABST) FROM VCYSYS820.dbo.INVENTORY AS c
//		            WHERE b.MATNR = c.MATNR AND c.WERKS = 2361)")
//        )
//        ->groupBy('b.MATNR','a.invoice_market_MAKTX')
//        ->get()
//        ->toArray();
//
//    dd($query);
//});
//Route::get('/test123', function () {
//    $databaseName = Config::get('database.connections');
//    $valBar = 7755000000019 + 200;
//
//    $range = MaterialMaster::join($databaseName['sqlsrv2']['database'] . '.dbo.MVKE as a', 'MATERIAL_MASTER.MATNR', '=', 'a.MATNR')
//        ->where(function ($query) use ($valBar) {
//            $query->where('MATERIAL_MASTER.EAN11', '>=', '7755000000019')
//                ->where('MATERIAL_MASTER.EAN11','<=',(string)$valBar)
//                ->where('a.VKORG','=','1902');
//        })
//        ->select('MATERIAL_MASTER.EAN11','MATERIAL_MASTER.MATNR','MATERIAL_MASTER.MAKTX')
//        ->get();
//
//    $arr = [];
//
//    foreach ($range as $perItem =>$value){
//        $getMVKE = MaterialMaster::join($databaseName['sqlsrv2']['database'] . '.dbo.MVKE as a', 'MATERIAL_MASTER.MATNR', '=', 'a.MATNR')
//            ->where(function ($query) use ($value) {
//                $query->where('MATERIAL_MASTER.EAN11', '=', $value['EAN11'])
//                    ->where('a.VKORG','=','1902');
//            })
//            ->select('MATERIAL_MASTER.MEINS','a.VRKME')
//            ->first();
//        $string =  preg_replace('/\s+/', '', $getMVKE['VRKME']);
//        if($getMVKE->VRKME === null || $string !== ''){
//            $sales_unit = $getMVKE->VRKME;
//        }
//        else{
//            $sales_unit = $getMVKE->MEINS;
//        }
//        $getDateAndQty = VBAP::join($databaseName['sqlsrv2']['database'] . '.dbo.VBAK as a', 'VBAP.VBELN', '=', 'a.VBELN')
//            ->where(function ($query) {
//                $query->where('a.AUART', '=', 'ZKB')
//                    ->where('VBAP.MATNR', '=', 'P1902A2')
//                    ->where('VBAP.KWMENG', '!=', 0);
//            })
//            ->select('VBAP.ERDAT', 'VBAP.KWMENG')
//            ->orderBy('VBAP.ERDAT', 'desc')
//            ->first();
//        $arr[] = [
//            'MEINS' => $sales_unit,
//            'LABST' => $getDateAndQty->KWMENG,
//        ];
//    }
//    dd($arr);
//});

Route::get('/{any}','AppController@index')->where('any','.*');
