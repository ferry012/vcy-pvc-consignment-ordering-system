<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDraftsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('drafts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('barcode');
            $table->string('mat_code');
            $table->string('mat_des');
            $table->string('sales_unit');
            $table->date('ERDAT');
            $table->decimal('KWMENG',10,2);
            $table->decimal('stocks_left',10,2)->nullable();
            $table->decimal('order_qty',10,2)->nullable();
            $table->decimal('approve_qty',10,2)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('drafts');
    }
}
