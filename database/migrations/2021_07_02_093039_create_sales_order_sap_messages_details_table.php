<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSalesOrderSapMessagesDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales_order_sap_messages_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('stock_no');
            $table->string('transaction')->nullable();
            $table->string('ref_id')->nullable();
            $table->string('lineError')->nullable();
            $table->date('dateError')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales_order_sap_messages_details');
    }
}
