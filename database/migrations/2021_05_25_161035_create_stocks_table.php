<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stocks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('barcode');
            $table->string('mat_code');
            $table->string('mat_des');
            $table->string('sales_unit');
            $table->date('ERDAT');
            $table->decimal('KWMENG',10,2);
            $table->integer('stocks_left')->nullable();
            $table->integer('order_qty');
            $table->integer('approve_qty')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stocks');
    }
}
