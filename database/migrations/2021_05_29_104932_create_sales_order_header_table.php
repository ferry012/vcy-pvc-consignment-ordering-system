<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSalesOrderHeaderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales_order_header', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('stock_req_no');
            $table->date('sales_order_date');
            $table->integer('sales_order_number')->nullable(); // from sap return
            $table->string('company');
            $table->string('status');
            $table->string('request_by');
            $table->string('approved_by');
            $table->integer('po_number')->nullable();
            $table->date('po_date')->nullable();
            $table->integer('net_value')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales_order_header');
    }
}
