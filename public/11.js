(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[11],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/stock_request/Create.vue?vue&type=script&lang=js&":
/*!*******************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/stock_request_management/Create.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _trevoreyre_autocomplete_vue_dist_style_css__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @trevoreyre/autocomplete-vue/dist/style.css */ "./node_modules/@trevoreyre/autocomplete-vue/dist/style.css");
/* harmony import */ var _trevoreyre_autocomplete_vue_dist_style_css__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_trevoreyre_autocomplete_vue_dist_style_css__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var fuse_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! fuse.js */ "./node_modules/fuse.js/dist/fuse.esm.js");
/* harmony import */ var jspdf__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! jspdf */ "./node_modules/jspdf/dist/jspdf.es.min.js");
/* harmony import */ var jspdf_autotable__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! jspdf-autotable */ "./node_modules/jspdf-autotable/dist/jspdf.plugin.autotable.js");
/* harmony import */ var jspdf_autotable__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(jspdf_autotable__WEBPACK_IMPORTED_MODULE_5__);


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//TODO IMPORTS





/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      perPage: ['10', '50', '100'],
      currentPage: '10',
      sortClassActive: {
        'activeDesc': false,
        'activeAsc': true
      },
      meta: {},
      page: {},
      currentSort: 'created_at',
      currentSortDir: 'desc',
      stocks: []
    };
  },
  computed: {
    formatDate: function formatDate() {
      var formatDate = new Date(this.form.valid_from);
      return formatDate;
    },
    currentUser: function currentUser() {
      return this.$store.getters.currentUser;
    }
  },
  created: function created() {
    var _this = this;

    return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              _context.next = 2;
              return _this.index();

            case 2:
              _context.next = 4;
              return Reload.$on('AfterCreated', function () {
                _this.index();

                _this.getBrands();

                _this.getImporter();

                _this.getManu();

                _this.getCountry();

                _this.getNominal();

                _this.getThick();

                _this.getWaterRange();

                _this.getMethod();

                _this.getNature();

                _this.getTotalPack();
              });

            case 4:
              _context.next = 6;
              return _this.getBrands();

            case 6:
              _context.next = 8;
              return _this.getImporter();

            case 8:
              _context.next = 10;
              return _this.getManu();

            case 10:
              _context.next = 12;
              return _this.getCountry();

            case 12:
              _context.next = 14;
              return _this.getNominal();

            case 14:
              _context.next = 16;
              return _this.getThick();

            case 16:
              _context.next = 18;
              return _this.getWaterRange();

            case 18:
              _context.next = 20;
              return _this.getMethod();

            case 20:
              _context.next = 22;
              return _this.getNature();

            case 22:
              _context.next = 24;
              return _this.getTotalPack();

            case 24:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }))();
  },
  methods: {
    addRow: function addRow() {
      var elem = document.createElement('tr');
      var total = this.price * this.qty;
      var product_id = this.product_id ? this.product_id : 0;
      var price = this.price ? this.price : 0;
      var qty = this.qty ? this.qty : 0;
      var item_subtotal = this.item_subtotal ? this.item_subtotal : 0;
      this.stocks.push({
        'product_id': product_id,
        'product_name': this.product_name,
        'price': price,
        'qty': qty,
        'discount': '0',
        'item_subtotal': item_subtotal,
        'total': total
      });
    },
    removeElement: function removeElement(index) {
      this.stocks.splice(index, 1);
    },
    createBrand: function createBrand() {
      var _this2 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2() {
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                console.log(_this2.form);
                _context2.next = 3;
                return axios.post('/api/brand', _this2.form).then(function (response) {
                  console.log(response.data.status);

                  if (response.data.status === "success") {
                    console.log('test');
                    _this2.showEdit = false;
                    _this2.showCreate = true;
                    _this2.form.brand = '';
                    _this2.form.photo = '';
                    _this2.$refs.fileImage.value = '';
                    Reload.$emit('AfterCreated');
                    $('#brandModal').modal("hide");
                    Notification.success();
                  }
                })["catch"](function (error) {
                  // code here when an upload is not valid
                  _this2.uploading = false;
                  _this2.error = error.response.data;

                  _this2.$swal({
                    icon: 'warning',
                    title: 'Please try again',
                    text: 'Make sure to fill all the required input fields and check your image is "JPEG" or "PNG" Format',
                    confirmButtonText: 'Ok'
                  });
                });

              case 3:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2);
      }))();
    },
    createImporters: function createImporters() {
      var _this3 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee3() {
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                console.log(_this3.form);
                _context3.next = 3;
                return axios.post('/api/importers', _this3.form).then(function (response) {
                  console.log(response.data.status);

                  if (response.data.status === "success") {
                    _this3.form.name = '';
                    _this3.form.address = '';
                    $('#importersModal').modal("hide");
                    Reload.$emit('AfterCreated');
                    Notification.success();
                  }
                })["catch"](function (error) {
                  _this3.alertSwal(error, '', 'warning', '');
                });

              case 3:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3);
      }))();
    },
    createManu: function createManu() {
      var _this4 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee4() {
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                console.log(_this4.form);
                _context4.next = 3;
                return axios.post('/api/manu', _this4.form).then(function (response) {
                  console.log(response.data.status);

                  if (response.data.status === "success") {
                    _this4.form.name = '';
                    _this4.form.address = '';
                    Reload.$emit('AfterCreated');
                    $('#manuModal').modal("hide");
                    Notification.success();
                  }
                })["catch"](function (error) {
                  _this4.alertSwal(error, '', 'warning', '');
                });

              case 3:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4);
      }))();
    },
    createCountry: function createCountry() {
      var _this5 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee5() {
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee5$(_context5) {
          while (1) {
            switch (_context5.prev = _context5.next) {
              case 0:
                _context5.next = 2;
                return axios.post('/api/country', _this5.form).then(function (response) {
                  if (response.data.status === "success") {
                    _this5.form.country_name = '';
                    Reload.$emit('AfterCreated');
                    $('#countryModal').modal("hide");
                    Notification.success();
                  }
                })["catch"](function (error) {
                  _this5.alertSwal(error, '', 'warning', '');
                });

              case 2:
              case "end":
                return _context5.stop();
            }
          }
        }, _callee5);
      }))();
    },
    createNominal: function createNominal() {
      var _this6 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee6() {
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee6$(_context6) {
          while (1) {
            switch (_context6.prev = _context6.next) {
              case 0:
                console.log(_this6.form);
                _context6.next = 3;
                return axios.post('/api/nominal', _this6.form).then(function (response) {
                  console.log(response.data.status);

                  if (response.data.status === "success") {
                    console.log('test');
                    $('#brand').modal('hide');
                    _this6.form.size = '';
                    Reload.$emit('AfterCreated');
                    $('#nominalModal').modal("hide");
                    Notification.success();
                  }
                })["catch"](function (error) {
                  _this6.alertSwal(error, '', 'warning', '');
                });

              case 3:
              case "end":
                return _context6.stop();
            }
          }
        }, _callee6);
      }))();
    },
    createThickness: function createThickness() {
      var _this7 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee7() {
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee7$(_context7) {
          while (1) {
            switch (_context7.prev = _context7.next) {
              case 0:
                console.log(_this7.form);
                _context7.next = 3;
                return axios.post('/api/thick', _this7.form).then(function (response) {
                  console.log(response.data.status);

                  if (response.data.status === "success") {
                    console.log('test');
                    $('#brand').modal('hide');
                    _this7.form.density = '';
                    Reload.$emit('AfterCreated');
                    $('#thicklModal').modal("hide");
                    _this7.showEdit = false;
                    Notification.success();
                  }
                })["catch"](function (error) {
                  _this7.alertSwal(error, '', 'warning', '');
                });

              case 3:
              case "end":
                return _context7.stop();
            }
          }
        }, _callee7);
      }))();
    },
    createWater: function createWater() {
      var _this8 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee8() {
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee8$(_context8) {
          while (1) {
            switch (_context8.prev = _context8.next) {
              case 0:
                console.log(_this8.form);
                _context8.next = 3;
                return axios.post('/api/water', _this8.form).then(function (response) {
                  console.log(response.data.status);

                  if (response.data.status === "success") {
                    console.log('test');
                    $('#brand').modal('hide');
                    _this8.form.range = '';
                    Reload.$emit('AfterCreated');
                    $('#waterModal').modal("hide");
                    _this8.showEdit = false;
                    Notification.success();
                  }
                })["catch"](function (error) {
                  _this8.alertSwal(error, '', 'warning', '');
                });

              case 3:
              case "end":
                return _context8.stop();
            }
          }
        }, _callee8);
      }))();
    },
    createMethod: function createMethod() {
      var _this9 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee9() {
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee9$(_context9) {
          while (1) {
            switch (_context9.prev = _context9.next) {
              case 0:
                console.log(_this9.form);
                _context9.next = 3;
                return axios.post('/api/method', _this9.form).then(function (response) {
                  console.log(response.data.status);

                  if (response.data.status === "success") {
                    console.log('test');
                    $('#brand').modal('hide');
                    _this9.form.shaping = '';
                    Reload.$emit('AfterCreated');
                    $('#methodModal').modal("hide");
                    _this9.showEdit = false;
                    Notification.success();
                  }
                })["catch"](function (error) {
                  _this9.alertSwal(error, '', 'warning', '');
                });

              case 3:
              case "end":
                return _context9.stop();
            }
          }
        }, _callee9);
      }))();
    },
    createNature: function createNature() {
      var _this10 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee10() {
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee10$(_context10) {
          while (1) {
            switch (_context10.prev = _context10.next) {
              case 0:
                console.log(_this10.form);
                _context10.next = 3;
                return axios.post('/api/nature', _this10.form).then(function (response) {
                  console.log(response.data.status);

                  if (response.data.status === "success") {
                    console.log('test');
                    $('#brand').modal('hide');
                    _this10.form.surface = '';
                    Reload.$emit('AfterCreated');
                    $('#natureModal').modal("hide");
                    _this10.showEdit = false;
                    Notification.success();
                  }
                })["catch"](function (error) {
                  _this10.alertSwal(error, '', 'warning', '');
                });

              case 3:
              case "end":
                return _context10.stop();
            }
          }
        }, _callee10);
      }))();
    },
    createTotalPerPack: function createTotalPerPack() {
      var _this11 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee11() {
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee11$(_context11) {
          while (1) {
            switch (_context11.prev = _context11.next) {
              case 0:
                console.log(_this11.form);
                _context11.next = 3;
                return axios.post('/api/tile', _this11.form).then(function (response) {
                  console.log(response.data.status);

                  if (response.data.status === "success") {
                    console.log('test');
                    $('#brand').modal('hide');
                    _this11.form.per_pack = '';
                    Reload.$emit('AfterCreated');
                    $('#totalPerPackModal').modal("hide");
                    _this11.showEdit = false;
                    Notification.success();
                  }
                })["catch"](function (error) {
                  _this11.alertSwal(error, '', 'warning', '');
                });

              case 3:
              case "end":
                return _context11.stop();
            }
          }
        }, _callee11);
      }))();
    },
    onFileSelected: function onFileSelected(event) {
      var _this12 = this;

      var file = event.target.files[0];

      if (file.size > 52428800) {
        //50MB MAX https://convertlive.com/u/convert/megabytes/to/bytes#50
        Notification.image_validation();
      } else {
        var reader = new FileReader();

        reader.onload = function (event) {
          _this12.form.photo = event.target.result;
          console.log(event.target.result);
        };

        reader.readAsDataURL(file);
      }
    },
    openSearchModal: function openSearchModal() {
      this.searchEAN11 = '', this.searchMATNR = '', this.searchMAKTX = '', this.searchBrand = '', this.searchImporters_name = '', this.searchManu_name = '', this.searchCountry_origin = '', this.searchNominal_size = '', this.searchThickness = '', this.searchWater_range = '', this.searchMethod_shaping = '', this.searchNature_surface = '', this.searchTiles_per_pack = '', this.searchCode = '', $('#searchModal').modal({
        backdrop: 'static',
        keyboard: false
      });
    },
    getDetailsPdf: function getDetailsPdf(id) {
      var _this13 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee12() {
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee12$(_context12) {
          while (1) {
            switch (_context12.prev = _context12.next) {
              case 0:
                _context12.next = 2;
                return axios.get('/api/getDetailsPdf?id=' + id).then(function (response) {
                  console.log(response.data[0]);
                  _this13.pdfDetails = response.data[0];
                  Swal.fire({
                    title: 'Please enter a batch number',
                    icon: 'info',
                    input: 'number',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Proceed'
                  }).then(function (result) {
                    if (result.value) {
                      console.log(id); // this.printTile();

                      _this13.getBatchNumber = result.value;
                      axios.get('/api/addBatch?batch=' + result.value + '&id=' + id).then(function (response) {
                        // console.log(response.data.success);
                        if (response.data.success === 'success') {
                          _this13.printTile();
                        }
                      })["catch"](function (error) {
                        _this13.alertSwal(error, '', 'warning', '');
                      });
                    }
                  });
                })["catch"](function (error) {
                  _this13.alertSwal(error, '', 'warning', '');
                });

              case 2:
              case "end":
                return _context12.stop();
            }
          }
        }, _callee12);
      }))();
    },
    getTotalPack: function getTotalPack() {
      var _this14 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee13() {
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee13$(_context13) {
          while (1) {
            switch (_context13.prev = _context13.next) {
              case 0:
                _context13.next = 2;
                return axios.get('/api/totals-data').then(function (response) {
                  _this14.totals = response.data;
                });

              case 2:
              case "end":
                return _context13.stop();
            }
          }
        }, _callee13);
      }))();
    },
    getNature: function getNature() {
      var _this15 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee14() {
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee14$(_context14) {
          while (1) {
            switch (_context14.prev = _context14.next) {
              case 0:
                _context14.next = 2;
                return axios.get('/api/nature-data').then(function (response) {
                  _this15.natures = response.data;
                });

              case 2:
              case "end":
                return _context14.stop();
            }
          }
        }, _callee14);
      }))();
    },
    getMethod: function getMethod() {
      var _this16 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee15() {
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee15$(_context15) {
          while (1) {
            switch (_context15.prev = _context15.next) {
              case 0:
                _context15.next = 2;
                return axios.get('/api/method-range-data').then(function (response) {
                  _this16.methodShapings = response.data;
                });

              case 2:
              case "end":
                return _context15.stop();
            }
          }
        }, _callee15);
      }))();
    },
    getWaterRange: function getWaterRange() {
      var _this17 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee16() {
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee16$(_context16) {
          while (1) {
            switch (_context16.prev = _context16.next) {
              case 0:
                _context16.next = 2;
                return axios.get('/api/water-range-data').then(function (response) {
                  _this17.waters = response.data;
                });

              case 2:
              case "end":
                return _context16.stop();
            }
          }
        }, _callee16);
      }))();
    },
    getThick: function getThick() {
      var _this18 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee17() {
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee17$(_context17) {
          while (1) {
            switch (_context17.prev = _context17.next) {
              case 0:
                _context17.next = 2;
                return axios.get('/api/thick-data').then(function (response) {
                  _this18.thicks = response.data;
                });

              case 2:
              case "end":
                return _context17.stop();
            }
          }
        }, _callee17);
      }))();
    },
    getNominal: function getNominal() {
      var _this19 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee18() {
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee18$(_context18) {
          while (1) {
            switch (_context18.prev = _context18.next) {
              case 0:
                _context18.next = 2;
                return axios.get('/api/nominal-data').then(function (response) {
                  _this19.nominals = response.data;
                });

              case 2:
              case "end":
                return _context18.stop();
            }
          }
        }, _callee18);
      }))();
    },
    getCountry: function getCountry() {
      var _this20 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee19() {
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee19$(_context19) {
          while (1) {
            switch (_context19.prev = _context19.next) {
              case 0:
                _context19.next = 2;
                return axios.get('/api/country-data').then(function (response) {
                  _this20.countries = response.data;
                });

              case 2:
              case "end":
                return _context19.stop();
            }
          }
        }, _callee19);
      }))();
    },
    getManu: function getManu() {
      var _this21 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee20() {
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee20$(_context20) {
          while (1) {
            switch (_context20.prev = _context20.next) {
              case 0:
                _context20.next = 2;
                return axios.get('/api/manu-data').then(function (response) {
                  _this21.manus = response.data;
                });

              case 2:
              case "end":
                return _context20.stop();
            }
          }
        }, _callee20);
      }))();
    },
    getImporter: function getImporter() {
      var _this22 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee21() {
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee21$(_context21) {
          while (1) {
            switch (_context21.prev = _context21.next) {
              case 0:
                _context21.next = 2;
                return axios.get('/api/importer-data').then(function (response) {
                  _this22.importers = response.data;
                });

              case 2:
              case "end":
                return _context21.stop();
            }
          }
        }, _callee21);
      }))();
    },
    getBrands: function getBrands() {
      var _this23 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee22() {
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee22$(_context22) {
          while (1) {
            switch (_context22.prev = _context22.next) {
              case 0:
                _context22.next = 2;
                return axios.get('/api/brand-data').then(function (response) {
                  console.log(response.data);
                  _this23.brands = response.data;
                });

              case 2:
              case "end":
                return _context22.stop();
            }
          }
        }, _callee22);
      }))();
    },
    createTileInfo: function createTileInfo() {
      var _this24 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee23() {
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee23$(_context23) {
          while (1) {
            switch (_context23.prev = _context23.next) {
              case 0:
                console.log(_this24.form);
                _context23.next = 3;
                return axios.post('/api/tile-master', _this24.form).then(function (response) {
                  if (response.data.status === "success") {
                    _this24.form.MATNR = '';
                    _this24.form.MAKTX = '';
                    _this24.form.brand = '';
                    _this24.form.importers_id = '';
                    _this24.form.manu_id = '';
                    _this24.form.country_origin = '';
                    _this24.form.nominal_size = '';
                    _this24.form.thickness = '';
                    _this24.form.water_range = '';
                    _this24.form.method_shaping = '';
                    _this24.form.nature_surface = '';
                    _this24.form.tiles_per_pack = '';
                    _this24.$refs.autocomplete_create.value = '', $('#tileCreate').modal("hide");
                    Reload.$emit('AfterCreated');
                    Notification.success();
                  }
                })["catch"](function (error) {
                  _this24.alertSwal(error, '', 'warning', '');
                });

              case 3:
              case "end":
                return _context23.stop();
            }
          }
        }, _callee23);
      }))();
    },
    fuseSearch: function fuseSearch(options, search) {
      var fuse = new fuse_js__WEBPACK_IMPORTED_MODULE_3__["default"](options, {
        keys: ["name", "address"],
        shouldSort: true
      });
      return search.length ? fuse.search(search).map(function (_ref) {
        var item = _ref.item;
        return item;
      }) : fuse.list;
    },
    fuseSearchBrand: function fuseSearchBrand(options, search) {
      var fuse = new fuse_js__WEBPACK_IMPORTED_MODULE_3__["default"](options, {
        keys: ["brand_name"],
        shouldSort: true
      });
      return search.length ? fuse.search(search).map(function (_ref2) {
        var item = _ref2.item;
        return item;
      }) : fuse.list;
    },
    fuseSearchCountry: function fuseSearchCountry(options, search) {
      var fuse = new fuse_js__WEBPACK_IMPORTED_MODULE_3__["default"](options, {
        keys: ["country_name"],
        shouldSort: true
      });
      return search.length ? fuse.search(search).map(function (_ref3) {
        var item = _ref3.item;
        return item;
      }) : fuse.list;
    },
    fuseSearchNominal: function fuseSearchNominal(options, search) {
      var fuse = new fuse_js__WEBPACK_IMPORTED_MODULE_3__["default"](options, {
        keys: ["size"],
        shouldSort: true
      });
      return search.length ? fuse.search(search).map(function (_ref4) {
        var item = _ref4.item;
        return item;
      }) : fuse.list;
    },
    fuseSearchThickness: function fuseSearchThickness(options, search) {
      var fuse = new fuse_js__WEBPACK_IMPORTED_MODULE_3__["default"](options, {
        keys: ["density"],
        shouldSort: true
      });
      return search.length ? fuse.search(search).map(function (_ref5) {
        var item = _ref5.item;
        return item;
      }) : fuse.list;
    },
    fuseSearchWater: function fuseSearchWater(options, search) {
      var fuse = new fuse_js__WEBPACK_IMPORTED_MODULE_3__["default"](options, {
        keys: ["range"],
        shouldSort: true
      });
      return search.length ? fuse.search(search).map(function (_ref6) {
        var item = _ref6.item;
        return item;
      }) : fuse.list;
    },
    fuseSearchMethod: function fuseSearchMethod(options, search) {
      var fuse = new fuse_js__WEBPACK_IMPORTED_MODULE_3__["default"](options, {
        keys: ["shaping"],
        shouldSort: true
      });
      return search.length ? fuse.search(search).map(function (_ref7) {
        var item = _ref7.item;
        return item;
      }) : fuse.list;
    },
    fuseSearchNature: function fuseSearchNature(options, search) {
      var fuse = new fuse_js__WEBPACK_IMPORTED_MODULE_3__["default"](options, {
        keys: ["surface"],
        shouldSort: true
      });
      return search.length ? fuse.search(search).map(function (_ref8) {
        var item = _ref8.item;
        return item;
      }) : fuse.list;
    },
    fuseSearchTotalPerPaCK: function fuseSearchTotalPerPaCK(options, search) {
      var fuse = new fuse_js__WEBPACK_IMPORTED_MODULE_3__["default"](options, {
        keys: ["per_pack"],
        shouldSort: true
      });
      return search.length ? fuse.search(search).map(function (_ref9) {
        var item = _ref9.item;
        return item;
      }) : fuse.list;
    },
    openModal: function openModal() {
      this.form.EAN11 = '';
      this.form.MATNR = '';
      this.form.MAKTX = '';
      this.form.brand = '';
      this.form.importers_id = '';
      this.form.manu_id = '';
      this.form.country_origin = '';
      this.form.nominal_size = '';
      this.form.thickness = '';
      this.form.water_range = '';
      this.form.method_shaping = '';
      this.form.nature_surface = '';
      this.form.tiles_per_pack = '';
      this.form.code = '';
      this.$refs.autocomplete_create.value = '', // $('#tileCreate').modal("show");
      $('#tileCreate').modal({
        backdrop: 'static',
        keyboard: false
      });
    },
    closeModal: function closeModal() {
      $('#tileCreate').modal("hide");
      $('#searchModal').modal("hide");
    },
    closeEdit: function closeEdit() {
      $('#tileEdit').modal("hide");
    },
    index: function index(page) {
      var _this25 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee24() {
        var uri;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee24$(_context24) {
          while (1) {
            switch (_context24.prev = _context24.next) {
              case 0:
                if (typeof page === 'undefined') {
                  _this25.page = 1;
                }

                if (_this25.searchEAN11 !== '' || _this25.searchMATNR !== '' || _this25.searchMAKTX !== '' || _this25.searchBrand !== '' || _this25.searchImporters_name !== '' || _this25.searchManu_name !== '' || _this25.searchCountry_origin !== '' || _this25.searchNominal_size !== '' || _this25.searchThickness !== '' || _this25.searchWater_range !== '' || _this25.searchMethod_shaping !== '' || _this25.searchNature_surface !== '' || _this25.searchTiles_per_pack !== '' || _this25.searchCode !== '') {
                  $('#searchModal').modal("hide");
                }

                uri = '/api/tile-master?page=' + page + '&searchEAN11=' + _this25.searchEAN11 + '&searchMATNR=' + _this25.searchMATNR + '&searchMAKTX=' + _this25.searchMAKTX + '&searchBrand=' + _this25.searchBrand + '&searchImporters_name=' + _this25.searchImporters_name + '&searchManu_name=' + _this25.searchManu_name + '&searchCountry_origin=' + _this25.searchCountry_origin + '&searchNominal_size=' + _this25.searchNominal_size + '&searchThickness=' + _this25.searchThickness + '&searchWater_range=' + _this25.searchWater_range + '&searchMethod_shaping=' + _this25.searchMethod_shaping + '&searchNature_surface=' + _this25.searchNature_surface + '&searchTiles_per_pack=' + _this25.searchTiles_per_pack + '&search=' + _this25.search + '&searchCode=' + _this25.searchCode + '&sortby=' + _this25.currentSort + '&sortdir=' + _this25.currentSortDir + '&currentpage=' + _this25.currentPage + '&filterSearch=' + _this25.filterSearch;
                _this25.uri = uri;
                _this25.page = page;
                _context24.next = 7;
                return axios.get(uri).then(function (response) {
                  _this25.tileMasters = response.data;
                  console.log(_this25.tileMasters);
                  _this25.meta.total = response.data.meta.total;
                  _this25.meta.from = response.data.meta.from;
                  _this25.meta.to = response.data.meta.to;
                });

              case 7:
              case "end":
                return _context24.stop();
            }
          }
        }, _callee24);
      }))();
    },
    sort: function sort(s) {
      if (s === this.currentSort) {
        this.currentSortDir = this.currentSortDir === 'asc' ? 'desc' : 'asc';

        if (this.currentSortDir === 'asc') {
          this.sortClassActive.activeAsc = true;
          this.sortClassActive.activeDesc = false;
        } else {
          this.sortClassActive.activeAsc = false;
          this.sortClassActive.activeDesc = true;
        }
      }

      this.currentSort = s;
      this.index();
    },
    selectPageNumber: function selectPageNumber(selected) {
      this.currentPage = selected;
      this.index();
    },
    material: function material(input) {
      var url = "/api/tile-master-data?code=".concat(encodeURI(input), "&showAll=tile");
      return new Promise(function (resolve) {
        if (input.length < 1) {
          return resolve([]);
        }

        fetch(url).then(function (response) {
          return response.json();
        }).then(function (data) {
          resolve(data);
        });
      });
    },
    getResultValue: function getResultValue(result) {
      var $res = result.EAN11;
      return $res;
    },
    selected: function selected(result) {
      var selected = result;
      this.form.EAN11 = selected.EAN11;
      this.form.MATNR = selected.MATNR;
      this.form.MAKTX = selected.MAKTX;
    },
    updateTile: function updateTile(id) {
      var _this26 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee25() {
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee25$(_context25) {
          while (1) {
            switch (_context25.prev = _context25.next) {
              case 0:
                console.log(id);
                _context25.next = 3;
                return axios.get('/api/tile-master/' + id).then(function (response) {
                  _this26.form = response.data;
                  console.log(response.data.importers_name);
                  _this26.form.manu_id = response.data.manu_name;
                  _this26.form.importers_name = response.data.importers_name;
                  $('#tileEdit').modal({
                    backdrop: 'static',
                    keyboard: false
                  });
                })["catch"](function (error) {
                  _this26.alertSwal(error, '', 'warning', '');
                });

              case 3:
              case "end":
                return _context25.stop();
            }
          }
        }, _callee25);
      }))();
    },
    updateTileWithData: function updateTileWithData() {
      var _this27 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee26() {
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee26$(_context26) {
          while (1) {
            switch (_context26.prev = _context26.next) {
              case 0:
                _context26.next = 2;
                return axios.post('/api/tile-master-update', _this27.form).then(function (response) {
                  _this27.form = response.data;

                  if (response.data.status === "success") {
                    _this27.showEdit = false;
                    Reload.$emit('AfterCreated');
                    $('#tileEdit').modal("hide");
                    Notification.update();
                  }
                })["catch"](function (error) {
                  _this27.alertSwal(error, '', 'warning', '');
                });

              case 2:
              case "end":
                return _context26.stop();
            }
          }
        }, _callee26);
      }))();
    },
    deleteTile: function deleteTile(id) {
      var _this28 = this;

      Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
      }).then(function (result) {
        if (result.value) {
          axios["delete"]('/api/tile-master/' + id).then(function () {
            // console.log(this.tileMasters.data);
            _this28.tileMasters.data = _this28.tileMasters.data.filter(function (market) {
              //  console.log(market.id);
              return market.id != id;
            });
            Reload.$emit('AfterCreated'); // return this.tileMasters.data;
          })["catch"](function () {//  this.$router.push({path: '/marketing-inventory'});
            //  this.$router.push({name: 'marketing-inventory'});
            // this.$router.push({name: 'marketing-inventory-index'}).catch(()=>{});
          });
          Swal.fire('Deleted!', 'Your file has been deleted.', 'success');
        }
      });
    },
    alertSwal: function alertSwal(title, text, icon, timer) {
      this.$swal({
        title: title,
        text: text,
        icon: icon,
        timer: timer
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/stock_request/Create.vue?vue&type=style&index=0&id=061171a0&scoped=true&lang=css&":
/*!**************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/stock_request_management/Create.vue?vue&type=style&index=0&id=061171a0&scoped=true&lang=css& ***!
  \**************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n@media (min-width: 992px) {\n.modal-dialog[data-v-061171a0] {\n        max-width: 30% !important;\n}\n}\n@media (min-width: 992px) {\n.modal_custom[data-v-061171a0] {\n        max-width: 50% !important;\n}\n}\n.sampleClass[data-v-061171a0] {\n    background-color: rgba(0, 0, 0, 0.5) !important;\n    /*opacity: 0.5 !important;*/\n}\n.example-open .modal-backdrop.show[data-v-061171a0]:nth-of-type(even) {\n    opacity: 5 !important;\n    z-index: 1052 !important;\n}\n.filter-asc[data-v-061171a0] {\n    border-bottom: 5px solid #ccc;\n    margin-bottom: 1px\n}\n.filter-asc[data-v-061171a0], .filter-desc[data-v-061171a0] {\n    width: 0;\n    height: 0;\n    border-left: 5px solid transparent;\n    border-right: 5px solid transparent\n}\n.filter-desc[data-v-061171a0] {\n    border-top: 5px solid #ccc;\n    margin-top: 1px\n}\n.inline-block[data-v-061171a0] {\n    display: inline-block\n}\n[data-position='below'] .autocomplete-result-list[data-v-061171a0] {\n    border-bottom: none;\n    border-radius: 8px 8px 0 0;\n    color: forestgreen;\n    font-weight: bold;\n}\n.wiki-result[data-v-061171a0] {\n    border-top: 1px solid #eee;\n    padding: 16px;\n    background: transparent;\n}\n.wiki-title[data-v-061171a0] {\n    font-size: 15px;\n    margin-bottom: 8px;\n    font-weight: 550;\n    color: forestgreen;\n}\n.wiki-snippet[data-v-061171a0] {\n    font-size: 14px;\n    color: rgba(0, 0, 0, 0.54);\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/moment/locale sync recursive ^\\.\\/.*$":
/*!**************************************************!*\
  !*** ./node_modules/moment/locale sync ^\.\/.*$ ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": "./node_modules/moment/locale/af.js",
	"./af.js": "./node_modules/moment/locale/af.js",
	"./ar": "./node_modules/moment/locale/ar.js",
	"./ar-dz": "./node_modules/moment/locale/ar-dz.js",
	"./ar-dz.js": "./node_modules/moment/locale/ar-dz.js",
	"./ar-kw": "./node_modules/moment/locale/ar-kw.js",
	"./ar-kw.js": "./node_modules/moment/locale/ar-kw.js",
	"./ar-ly": "./node_modules/moment/locale/ar-ly.js",
	"./ar-ly.js": "./node_modules/moment/locale/ar-ly.js",
	"./ar-ma": "./node_modules/moment/locale/ar-ma.js",
	"./ar-ma.js": "./node_modules/moment/locale/ar-ma.js",
	"./ar-sa": "./node_modules/moment/locale/ar-sa.js",
	"./ar-sa.js": "./node_modules/moment/locale/ar-sa.js",
	"./ar-tn": "./node_modules/moment/locale/ar-tn.js",
	"./ar-tn.js": "./node_modules/moment/locale/ar-tn.js",
	"./ar.js": "./node_modules/moment/locale/ar.js",
	"./az": "./node_modules/moment/locale/az.js",
	"./az.js": "./node_modules/moment/locale/az.js",
	"./be": "./node_modules/moment/locale/be.js",
	"./be.js": "./node_modules/moment/locale/be.js",
	"./bg": "./node_modules/moment/locale/bg.js",
	"./bg.js": "./node_modules/moment/locale/bg.js",
	"./bm": "./node_modules/moment/locale/bm.js",
	"./bm.js": "./node_modules/moment/locale/bm.js",
	"./bn": "./node_modules/moment/locale/bn.js",
	"./bn-bd": "./node_modules/moment/locale/bn-bd.js",
	"./bn-bd.js": "./node_modules/moment/locale/bn-bd.js",
	"./bn.js": "./node_modules/moment/locale/bn.js",
	"./bo": "./node_modules/moment/locale/bo.js",
	"./bo.js": "./node_modules/moment/locale/bo.js",
	"./br": "./node_modules/moment/locale/br.js",
	"./br.js": "./node_modules/moment/locale/br.js",
	"./bs": "./node_modules/moment/locale/bs.js",
	"./bs.js": "./node_modules/moment/locale/bs.js",
	"./ca": "./node_modules/moment/locale/ca.js",
	"./ca.js": "./node_modules/moment/locale/ca.js",
	"./cs": "./node_modules/moment/locale/cs.js",
	"./cs.js": "./node_modules/moment/locale/cs.js",
	"./cv": "./node_modules/moment/locale/cv.js",
	"./cv.js": "./node_modules/moment/locale/cv.js",
	"./cy": "./node_modules/moment/locale/cy.js",
	"./cy.js": "./node_modules/moment/locale/cy.js",
	"./da": "./node_modules/moment/locale/da.js",
	"./da.js": "./node_modules/moment/locale/da.js",
	"./de": "./node_modules/moment/locale/de.js",
	"./de-at": "./node_modules/moment/locale/de-at.js",
	"./de-at.js": "./node_modules/moment/locale/de-at.js",
	"./de-ch": "./node_modules/moment/locale/de-ch.js",
	"./de-ch.js": "./node_modules/moment/locale/de-ch.js",
	"./de.js": "./node_modules/moment/locale/de.js",
	"./dv": "./node_modules/moment/locale/dv.js",
	"./dv.js": "./node_modules/moment/locale/dv.js",
	"./el": "./node_modules/moment/locale/el.js",
	"./el.js": "./node_modules/moment/locale/el.js",
	"./en-au": "./node_modules/moment/locale/en-au.js",
	"./en-au.js": "./node_modules/moment/locale/en-au.js",
	"./en-ca": "./node_modules/moment/locale/en-ca.js",
	"./en-ca.js": "./node_modules/moment/locale/en-ca.js",
	"./en-gb": "./node_modules/moment/locale/en-gb.js",
	"./en-gb.js": "./node_modules/moment/locale/en-gb.js",
	"./en-ie": "./node_modules/moment/locale/en-ie.js",
	"./en-ie.js": "./node_modules/moment/locale/en-ie.js",
	"./en-il": "./node_modules/moment/locale/en-il.js",
	"./en-il.js": "./node_modules/moment/locale/en-il.js",
	"./en-in": "./node_modules/moment/locale/en-in.js",
	"./en-in.js": "./node_modules/moment/locale/en-in.js",
	"./en-nz": "./node_modules/moment/locale/en-nz.js",
	"./en-nz.js": "./node_modules/moment/locale/en-nz.js",
	"./en-sg": "./node_modules/moment/locale/en-sg.js",
	"./en-sg.js": "./node_modules/moment/locale/en-sg.js",
	"./eo": "./node_modules/moment/locale/eo.js",
	"./eo.js": "./node_modules/moment/locale/eo.js",
	"./es": "./node_modules/moment/locale/es.js",
	"./es-do": "./node_modules/moment/locale/es-do.js",
	"./es-do.js": "./node_modules/moment/locale/es-do.js",
	"./es-mx": "./node_modules/moment/locale/es-mx.js",
	"./es-mx.js": "./node_modules/moment/locale/es-mx.js",
	"./es-us": "./node_modules/moment/locale/es-us.js",
	"./es-us.js": "./node_modules/moment/locale/es-us.js",
	"./es.js": "./node_modules/moment/locale/es.js",
	"./et": "./node_modules/moment/locale/et.js",
	"./et.js": "./node_modules/moment/locale/et.js",
	"./eu": "./node_modules/moment/locale/eu.js",
	"./eu.js": "./node_modules/moment/locale/eu.js",
	"./fa": "./node_modules/moment/locale/fa.js",
	"./fa.js": "./node_modules/moment/locale/fa.js",
	"./fi": "./node_modules/moment/locale/fi.js",
	"./fi.js": "./node_modules/moment/locale/fi.js",
	"./fil": "./node_modules/moment/locale/fil.js",
	"./fil.js": "./node_modules/moment/locale/fil.js",
	"./fo": "./node_modules/moment/locale/fo.js",
	"./fo.js": "./node_modules/moment/locale/fo.js",
	"./fr": "./node_modules/moment/locale/fr.js",
	"./fr-ca": "./node_modules/moment/locale/fr-ca.js",
	"./fr-ca.js": "./node_modules/moment/locale/fr-ca.js",
	"./fr-ch": "./node_modules/moment/locale/fr-ch.js",
	"./fr-ch.js": "./node_modules/moment/locale/fr-ch.js",
	"./fr.js": "./node_modules/moment/locale/fr.js",
	"./fy": "./node_modules/moment/locale/fy.js",
	"./fy.js": "./node_modules/moment/locale/fy.js",
	"./ga": "./node_modules/moment/locale/ga.js",
	"./ga.js": "./node_modules/moment/locale/ga.js",
	"./gd": "./node_modules/moment/locale/gd.js",
	"./gd.js": "./node_modules/moment/locale/gd.js",
	"./gl": "./node_modules/moment/locale/gl.js",
	"./gl.js": "./node_modules/moment/locale/gl.js",
	"./gom-deva": "./node_modules/moment/locale/gom-deva.js",
	"./gom-deva.js": "./node_modules/moment/locale/gom-deva.js",
	"./gom-latn": "./node_modules/moment/locale/gom-latn.js",
	"./gom-latn.js": "./node_modules/moment/locale/gom-latn.js",
	"./gu": "./node_modules/moment/locale/gu.js",
	"./gu.js": "./node_modules/moment/locale/gu.js",
	"./he": "./node_modules/moment/locale/he.js",
	"./he.js": "./node_modules/moment/locale/he.js",
	"./hi": "./node_modules/moment/locale/hi.js",
	"./hi.js": "./node_modules/moment/locale/hi.js",
	"./hr": "./node_modules/moment/locale/hr.js",
	"./hr.js": "./node_modules/moment/locale/hr.js",
	"./hu": "./node_modules/moment/locale/hu.js",
	"./hu.js": "./node_modules/moment/locale/hu.js",
	"./hy-am": "./node_modules/moment/locale/hy-am.js",
	"./hy-am.js": "./node_modules/moment/locale/hy-am.js",
	"./id": "./node_modules/moment/locale/id.js",
	"./id.js": "./node_modules/moment/locale/id.js",
	"./is": "./node_modules/moment/locale/is.js",
	"./is.js": "./node_modules/moment/locale/is.js",
	"./it": "./node_modules/moment/locale/it.js",
	"./it-ch": "./node_modules/moment/locale/it-ch.js",
	"./it-ch.js": "./node_modules/moment/locale/it-ch.js",
	"./it.js": "./node_modules/moment/locale/it.js",
	"./ja": "./node_modules/moment/locale/ja.js",
	"./ja.js": "./node_modules/moment/locale/ja.js",
	"./jv": "./node_modules/moment/locale/jv.js",
	"./jv.js": "./node_modules/moment/locale/jv.js",
	"./ka": "./node_modules/moment/locale/ka.js",
	"./ka.js": "./node_modules/moment/locale/ka.js",
	"./kk": "./node_modules/moment/locale/kk.js",
	"./kk.js": "./node_modules/moment/locale/kk.js",
	"./km": "./node_modules/moment/locale/km.js",
	"./km.js": "./node_modules/moment/locale/km.js",
	"./kn": "./node_modules/moment/locale/kn.js",
	"./kn.js": "./node_modules/moment/locale/kn.js",
	"./ko": "./node_modules/moment/locale/ko.js",
	"./ko.js": "./node_modules/moment/locale/ko.js",
	"./ku": "./node_modules/moment/locale/ku.js",
	"./ku.js": "./node_modules/moment/locale/ku.js",
	"./ky": "./node_modules/moment/locale/ky.js",
	"./ky.js": "./node_modules/moment/locale/ky.js",
	"./lb": "./node_modules/moment/locale/lb.js",
	"./lb.js": "./node_modules/moment/locale/lb.js",
	"./lo": "./node_modules/moment/locale/lo.js",
	"./lo.js": "./node_modules/moment/locale/lo.js",
	"./lt": "./node_modules/moment/locale/lt.js",
	"./lt.js": "./node_modules/moment/locale/lt.js",
	"./lv": "./node_modules/moment/locale/lv.js",
	"./lv.js": "./node_modules/moment/locale/lv.js",
	"./me": "./node_modules/moment/locale/me.js",
	"./me.js": "./node_modules/moment/locale/me.js",
	"./mi": "./node_modules/moment/locale/mi.js",
	"./mi.js": "./node_modules/moment/locale/mi.js",
	"./mk": "./node_modules/moment/locale/mk.js",
	"./mk.js": "./node_modules/moment/locale/mk.js",
	"./ml": "./node_modules/moment/locale/ml.js",
	"./ml.js": "./node_modules/moment/locale/ml.js",
	"./mn": "./node_modules/moment/locale/mn.js",
	"./mn.js": "./node_modules/moment/locale/mn.js",
	"./mr": "./node_modules/moment/locale/mr.js",
	"./mr.js": "./node_modules/moment/locale/mr.js",
	"./ms": "./node_modules/moment/locale/ms.js",
	"./ms-my": "./node_modules/moment/locale/ms-my.js",
	"./ms-my.js": "./node_modules/moment/locale/ms-my.js",
	"./ms.js": "./node_modules/moment/locale/ms.js",
	"./mt": "./node_modules/moment/locale/mt.js",
	"./mt.js": "./node_modules/moment/locale/mt.js",
	"./my": "./node_modules/moment/locale/my.js",
	"./my.js": "./node_modules/moment/locale/my.js",
	"./nb": "./node_modules/moment/locale/nb.js",
	"./nb.js": "./node_modules/moment/locale/nb.js",
	"./ne": "./node_modules/moment/locale/ne.js",
	"./ne.js": "./node_modules/moment/locale/ne.js",
	"./nl": "./node_modules/moment/locale/nl.js",
	"./nl-be": "./node_modules/moment/locale/nl-be.js",
	"./nl-be.js": "./node_modules/moment/locale/nl-be.js",
	"./nl.js": "./node_modules/moment/locale/nl.js",
	"./nn": "./node_modules/moment/locale/nn.js",
	"./nn.js": "./node_modules/moment/locale/nn.js",
	"./oc-lnc": "./node_modules/moment/locale/oc-lnc.js",
	"./oc-lnc.js": "./node_modules/moment/locale/oc-lnc.js",
	"./pa-in": "./node_modules/moment/locale/pa-in.js",
	"./pa-in.js": "./node_modules/moment/locale/pa-in.js",
	"./pl": "./node_modules/moment/locale/pl.js",
	"./pl.js": "./node_modules/moment/locale/pl.js",
	"./pt": "./node_modules/moment/locale/pt.js",
	"./pt-br": "./node_modules/moment/locale/pt-br.js",
	"./pt-br.js": "./node_modules/moment/locale/pt-br.js",
	"./pt.js": "./node_modules/moment/locale/pt.js",
	"./ro": "./node_modules/moment/locale/ro.js",
	"./ro.js": "./node_modules/moment/locale/ro.js",
	"./ru": "./node_modules/moment/locale/ru.js",
	"./ru.js": "./node_modules/moment/locale/ru.js",
	"./sd": "./node_modules/moment/locale/sd.js",
	"./sd.js": "./node_modules/moment/locale/sd.js",
	"./se": "./node_modules/moment/locale/se.js",
	"./se.js": "./node_modules/moment/locale/se.js",
	"./si": "./node_modules/moment/locale/si.js",
	"./si.js": "./node_modules/moment/locale/si.js",
	"./sk": "./node_modules/moment/locale/sk.js",
	"./sk.js": "./node_modules/moment/locale/sk.js",
	"./sl": "./node_modules/moment/locale/sl.js",
	"./sl.js": "./node_modules/moment/locale/sl.js",
	"./sq": "./node_modules/moment/locale/sq.js",
	"./sq.js": "./node_modules/moment/locale/sq.js",
	"./sr": "./node_modules/moment/locale/sr.js",
	"./sr-cyrl": "./node_modules/moment/locale/sr-cyrl.js",
	"./sr-cyrl.js": "./node_modules/moment/locale/sr-cyrl.js",
	"./sr.js": "./node_modules/moment/locale/sr.js",
	"./ss": "./node_modules/moment/locale/ss.js",
	"./ss.js": "./node_modules/moment/locale/ss.js",
	"./sv": "./node_modules/moment/locale/sv.js",
	"./sv.js": "./node_modules/moment/locale/sv.js",
	"./sw": "./node_modules/moment/locale/sw.js",
	"./sw.js": "./node_modules/moment/locale/sw.js",
	"./ta": "./node_modules/moment/locale/ta.js",
	"./ta.js": "./node_modules/moment/locale/ta.js",
	"./te": "./node_modules/moment/locale/te.js",
	"./te.js": "./node_modules/moment/locale/te.js",
	"./tet": "./node_modules/moment/locale/tet.js",
	"./tet.js": "./node_modules/moment/locale/tet.js",
	"./tg": "./node_modules/moment/locale/tg.js",
	"./tg.js": "./node_modules/moment/locale/tg.js",
	"./th": "./node_modules/moment/locale/th.js",
	"./th.js": "./node_modules/moment/locale/th.js",
	"./tk": "./node_modules/moment/locale/tk.js",
	"./tk.js": "./node_modules/moment/locale/tk.js",
	"./tl-ph": "./node_modules/moment/locale/tl-ph.js",
	"./tl-ph.js": "./node_modules/moment/locale/tl-ph.js",
	"./tlh": "./node_modules/moment/locale/tlh.js",
	"./tlh.js": "./node_modules/moment/locale/tlh.js",
	"./tr": "./node_modules/moment/locale/tr.js",
	"./tr.js": "./node_modules/moment/locale/tr.js",
	"./tzl": "./node_modules/moment/locale/tzl.js",
	"./tzl.js": "./node_modules/moment/locale/tzl.js",
	"./tzm": "./node_modules/moment/locale/tzm.js",
	"./tzm-latn": "./node_modules/moment/locale/tzm-latn.js",
	"./tzm-latn.js": "./node_modules/moment/locale/tzm-latn.js",
	"./tzm.js": "./node_modules/moment/locale/tzm.js",
	"./ug-cn": "./node_modules/moment/locale/ug-cn.js",
	"./ug-cn.js": "./node_modules/moment/locale/ug-cn.js",
	"./uk": "./node_modules/moment/locale/uk.js",
	"./uk.js": "./node_modules/moment/locale/uk.js",
	"./ur": "./node_modules/moment/locale/ur.js",
	"./ur.js": "./node_modules/moment/locale/ur.js",
	"./uz": "./node_modules/moment/locale/uz.js",
	"./uz-latn": "./node_modules/moment/locale/uz-latn.js",
	"./uz-latn.js": "./node_modules/moment/locale/uz-latn.js",
	"./uz.js": "./node_modules/moment/locale/uz.js",
	"./vi": "./node_modules/moment/locale/vi.js",
	"./vi.js": "./node_modules/moment/locale/vi.js",
	"./x-pseudo": "./node_modules/moment/locale/x-pseudo.js",
	"./x-pseudo.js": "./node_modules/moment/locale/x-pseudo.js",
	"./yo": "./node_modules/moment/locale/yo.js",
	"./yo.js": "./node_modules/moment/locale/yo.js",
	"./zh-cn": "./node_modules/moment/locale/zh-cn.js",
	"./zh-cn.js": "./node_modules/moment/locale/zh-cn.js",
	"./zh-hk": "./node_modules/moment/locale/zh-hk.js",
	"./zh-hk.js": "./node_modules/moment/locale/zh-hk.js",
	"./zh-mo": "./node_modules/moment/locale/zh-mo.js",
	"./zh-mo.js": "./node_modules/moment/locale/zh-mo.js",
	"./zh-tw": "./node_modules/moment/locale/zh-tw.js",
	"./zh-tw.js": "./node_modules/moment/locale/zh-tw.js"
};


function webpackContext(req) {
	var id = webpackContextResolve(req);
	return __webpack_require__(id);
}
function webpackContextResolve(req) {
	if(!__webpack_require__.o(map, req)) {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	}
	return map[req];
}
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = "./node_modules/moment/locale sync recursive ^\\.\\/.*$";

/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/stock_request/Create.vue?vue&type=style&index=0&id=061171a0&scoped=true&lang=css&":
/*!******************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/stock_request_management/Create.vue?vue&type=style&index=0&id=061171a0&scoped=true&lang=css& ***!
  \******************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../node_modules/css-loader??ref--6-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--6-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./Create.vue?vue&type=style&index=0&id=061171a0&scoped=true&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/stock_request/Create.vue?vue&type=style&index=0&id=061171a0&scoped=true&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/stock_request/Create.vue?vue&type=template&id=061171a0&scoped=true&":
/*!***********************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/stock_request_management/Create.vue?vue&type=template&id=061171a0&scoped=true& ***!
  \***********************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c("div", { staticClass: "row" }, [
      _c("div", { staticClass: "col-lg-9" }, [
        _c("div", { staticClass: "card shadow-sm mb-4" }, [
          _c("div", { staticClass: "input-group mb-3" }, [
            _c("br"),
            _vm._v(" "),
            _c(
              "button",
              {
                staticClass: "btn btn-success",
                staticStyle: {
                  margin: "0px 20px",
                  top: "11px",
                  position: "absolute"
                },
                on: { click: _vm.addRow }
              },
              [
                _vm._v("Add Stock Creation  "),
                _c("i", { staticClass: "fas fa-plus-square fa-lg" })
              ]
            )
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "card-body" }, [
            _c("div", { staticClass: "table-responsive" }, [
              _c(
                "table",
                {
                  staticClass:
                    "table align-items-center table-flush table-hover",
                  attrs: { id: "itemsTable" }
                },
                [
                  _vm._m(0),
                  _vm._v(" "),
                  _c(
                    "tbody",
                    _vm._l(_vm.stocks, function(stock, index) {
                      return _c("tr", [
                        _vm._m(1, true),
                        _vm._v(" "),
                        _vm._m(2, true),
                        _vm._v(" "),
                        _vm._m(3, true),
                        _vm._v(" "),
                        _vm._m(4, true),
                        _vm._v(" "),
                        _vm._m(5, true),
                        _vm._v(" "),
                        _vm._m(6, true),
                        _vm._v(" "),
                        _vm._m(7, true),
                        _vm._v(" "),
                        _vm._m(8, true),
                        _vm._v(" "),
                        _c("td", [
                          _c(
                            "a",
                            {
                              staticStyle: {
                                cursor: "pointer",
                                color: "#E10000"
                              },
                              on: {
                                click: function($event) {
                                  return _vm.removeElement(index)
                                }
                              }
                            },
                            [_c("b", [_vm._v("Remove")])]
                          )
                        ])
                      ])
                    }),
                    0
                  )
                ]
              )
            ])
          ])
        ])
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("thead", { staticClass: "thead-light" }, [
      _c("tr", [
        _c("th", [_vm._v("BARCODE")]),
        _vm._v(" "),
        _c("th", [_vm._v("MATERIAL CODE")]),
        _vm._v(" "),
        _c("th", [_vm._v("MATERIAL DESCRIPTION")]),
        _vm._v(" "),
        _c("th", [_vm._v("UOM")]),
        _vm._v(" "),
        _c("th", [_vm._v("LAST FILL-UP DATE")]),
        _vm._v(" "),
        _c("th", [_vm._v("LAST FILL-UP QTY")]),
        _vm._v(" "),
        _c("th", [_vm._v("STOCKS LEFT")]),
        _vm._v(" "),
        _c("th", [_vm._v("SUGGESTED ORDER QTY")]),
        _vm._v(" "),
        _c("th", [_vm._v("ACTIONS")])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("td", [
      _c("input", {
        staticClass: "form-control",
        attrs: { type: "number", placeholder: "Barcode" }
      })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("td", [
      _c("input", {
        staticClass: "form-control",
        attrs: { type: "text", placeholder: "Material Code", required: "" }
      })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("td", [
      _c("input", {
        staticClass: "form-control",
        attrs: {
          type: "number",
          placeholder: "MATERIAL DESCRIPTION",
          required: ""
        }
      })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("td", [
      _c("input", {
        staticClass: "form-control",
        attrs: { type: "number", placeholder: "UOM", required: "" }
      })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("td", [
      _c("input", {
        staticClass: "form-control",
        attrs: {
          type: "number",
          placeholder: "LAST FILL-UP DATE",
          required: ""
        }
      })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("td", [
      _c("input", {
        staticClass: "form-control",
        attrs: { type: "number", placeholder: "LAST FILL-UP QTY", required: "" }
      })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("td", [
      _c("input", {
        staticClass: "form-control",
        attrs: { type: "number", placeholder: "STOCKS LEFT", required: "" }
      })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("td", [
      _c("input", {
        staticClass: "form-control",
        attrs: {
          type: "number",
          placeholder: "SUGGESTED ORDER QTY",
          required: ""
        }
      })
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/components/stock_request/Create.vue":
/*!**********************************************************!*\
  !*** ./resources/js/components/stock_request_management/Create.vue ***!
  \**********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Create_vue_vue_type_template_id_061171a0_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Create.vue?vue&type=template&id=061171a0&scoped=true& */ "./resources/js/components/stock_request/Create.vue?vue&type=template&id=061171a0&scoped=true&");
/* harmony import */ var _Create_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Create.vue?vue&type=script&lang=js& */ "./resources/js/components/stock_request/Create.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _Create_vue_vue_type_style_index_0_id_061171a0_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Create.vue?vue&type=style&index=0&id=061171a0&scoped=true&lang=css& */ "./resources/js/components/stock_request/Create.vue?vue&type=style&index=0&id=061171a0&scoped=true&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _Create_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Create_vue_vue_type_template_id_061171a0_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Create_vue_vue_type_template_id_061171a0_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "061171a0",
  null

)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/stock_request_management/Create.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/stock_request/Create.vue?vue&type=script&lang=js&":
/*!***********************************************************************************!*\
  !*** ./resources/js/components/stock_request_management/Create.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Create_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./Create.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/stock_request/Create.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Create_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]);

/***/ }),

/***/ "./resources/js/components/stock_request/Create.vue?vue&type=style&index=0&id=061171a0&scoped=true&lang=css&":
/*!*******************************************************************************************************************!*\
  !*** ./resources/js/components/stock_request_management/Create.vue?vue&type=style&index=0&id=061171a0&scoped=true&lang=css& ***!
  \*******************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Create_vue_vue_type_style_index_0_id_061171a0_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/style-loader!../../../../node_modules/css-loader??ref--6-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--6-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./Create.vue?vue&type=style&index=0&id=061171a0&scoped=true&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/stock_request/Create.vue?vue&type=style&index=0&id=061171a0&scoped=true&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Create_vue_vue_type_style_index_0_id_061171a0_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Create_vue_vue_type_style_index_0_id_061171a0_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Create_vue_vue_type_style_index_0_id_061171a0_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Create_vue_vue_type_style_index_0_id_061171a0_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ "./resources/js/components/stock_request/Create.vue?vue&type=template&id=061171a0&scoped=true&":
/*!*****************************************************************************************************!*\
  !*** ./resources/js/components/stock_request_management/Create.vue?vue&type=template&id=061171a0&scoped=true& ***!
  \*****************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Create_vue_vue_type_template_id_061171a0_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./Create.vue?vue&type=template&id=061171a0&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/stock_request/Create.vue?vue&type=template&id=061171a0&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Create_vue_vue_type_template_id_061171a0_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Create_vue_vue_type_template_id_061171a0_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);
