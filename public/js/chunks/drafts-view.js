(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["drafts-view"],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/drafts/view.vue?vue&type=script&lang=js&":
/*!**********************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/drafts/view.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _trevoreyre_autocomplete_vue_dist_style_css__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @trevoreyre/autocomplete-vue/dist/style.css */ "./node_modules/@trevoreyre/autocomplete-vue/dist/style.css");
/* harmony import */ var _trevoreyre_autocomplete_vue_dist_style_css__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_trevoreyre_autocomplete_vue_dist_style_css__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var fuse_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! fuse.js */ "./node_modules/fuse.js/dist/fuse.esm.js");
/* harmony import */ var jspdf__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! jspdf */ "./node_modules/jspdf/dist/jspdf.es.min.js");
/* harmony import */ var jspdf_autotable__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! jspdf-autotable */ "./node_modules/jspdf-autotable/dist/jspdf.plugin.autotable.js");
/* harmony import */ var jspdf_autotable__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(jspdf_autotable__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _rangeModals_RangeBarcode__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../rangeModals/RangeBarcode */ "./resources/js/components/rangeModals/RangeBarcode.vue");


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//TODO IMPORTS






/* harmony default export */ __webpack_exports__["default"] = ({
  name: "viewDrafts",
  components: {
    rangeBarcode: _rangeModals_RangeBarcode__WEBPACK_IMPORTED_MODULE_6__["default"]
  },
  data: function data() {
    return {
      rangeBarcodeModal: false,
      rangeNumber: null,
      arrRange: [],
      form: {
        req_date: '',
        cust_company: null,
        mat_group: null,
        mat_code: null,
        barcode: null,
        mat_des: null,
        uom: null,
        KWMENG: null,
        ERDAT: null
      },
      perPage: ['10', '50', '100'],
      currentPage: '10',
      sortClassActive: {
        'activeDesc': false,
        'activeAsc': true
      },
      meta: {},
      page: {},
      errors: {},
      currentSort: 'created_at',
      currentSortDir: 'desc',
      multiForm: {
        sBarcode: '',
        sMat_code: '',
        sMat_des: '',
        sUom: '',
        sLastFillDate: '',
        sLastFillQty: '',
        sStocksLeft: '',
        sSuggestQty: '',
        sApproveQty: ''
      },
      stocks: [],
      matGroup: [],
      matCode: [],
      matBarcode: [],
      multiBarcode: [],
      sales_unit: [],
      dateTime: '',
      company: [],
      trans_stocks: [],
      trans_header: [],
      drafts: {},
      headerCompany: '',
      headerReqDate: '',
      disApproveQty: true,
      windowWidth: window.innerWidth,
      resWidth: null,
      is962: false,
      is1280: false,
      is1422: false,
      is1600: false,
      is1680: false,
      is1920: false
    };
  },
  watch: {
    'form.mat_group': function formMat_group(newVal, oldVal) {
      if (newVal !== null) {
        this.getBarCode(newVal); // this.getMatCode(newVal);
      }

      if (newVal === null) {
        this.form.mat_code = '';
        this.form.barcode = '';
        this.matCode = []; // this.matBarcode = [];

        this.getAllBarCode();
      }
    },
    'form.barcode': function formBarcode(newVal, oldVal) {
      this.getMatCode(newVal);

      if (newVal !== oldVal) {
        this.form.mat_code = null;
        this.form.uom = null;
        this.sales_unit = [];
      } // console.log(newVal,'barcode');


      this.arrRange.push({
        'barcode': newVal
      });
      var barcode = [];
      this.arrRange.forEach(function (obj) {
        barcode.push(obj.barcode = newVal);
      }); // console.log(barcode);
      // console.log(req_date,'test');

      this.arrRange.push({
        'barcode': barcode
      });
    },
    'rangeNumber': function rangeNumber(newVal, oldVal) {
      var range = [];
      this.arrRange.forEach(function (obj) {
        range.push(obj.range = newVal);
      }); // console.log(req_date,'test');

      this.arrRange.push({
        'range': range
      });
    },
    'form.cust_company': function formCust_company(newVal, oldVal) {
      this.getMatCode();
      this.trans_header[0].cust_company = newVal;
    },
    'form.req_date': function formReq_date(newVal, oldVal) {
      this.trans_header[0].req_date = newVal;
    },
    windowWidth: function windowWidth(newWidth, oldWidth) {
      this.resWidth = "it changed to ".concat(newWidth, " from ").concat(oldWidth);
      this.checkWidthToAdjust(newWidth);
    },
    deep: true
  },
  mounted: function mounted() {
    var _this = this;

    window.addEventListener('resize', function () {
      _this.windowWidth = window.innerWidth;
    });
  },
  computed: {
    formatDate: function formatDate() {
      // var formatDate = new Date(this.form.req_date);
      // return formatDate;
      var moment = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js"); // require


      moment().format();
      var today = moment();
      this.dateTime = today.format('YYYY-MM-DD');
      this.form.req_date = this.dateTime;
      return this.dateTime;
    },
    currentUser: function currentUser() {
      return this.$store.getters.currentUser;
    }
  },
  created: function created() {
    Reload.$on('AfterCreated', function () {});
    this.getMatGroup();
    this.getBarCode();
    this.getCustCompany();
    this.getDraftsDetails();
    this.getDraftsHeader();
    this.getAllBarCode();
    this.checkedIfApprovers();
    this.checkWidthToAdjust();
  },
  methods: {
    checkWidthToAdjust: function checkWidthToAdjust(newWidth) {
      console.log(newWidth, this.windowWidth);

      if (newWidth === 1280 || this.windowWidth === 1280) {
        this.is1280 = true;
        this.is1680 = false;
        this.is1920 = false;
        this.is1422 = false;
        this.is962 = false;
        this.is1600 = false;
      } else if (newWidth === 1422 || this.windowWidth === 1422) {
        this.is1422 = true;
        this.is1280 = false;
        this.is1680 = false;
        this.is1920 = false;
        this.is962 = false;
        this.is1600 = false;
      } else if (newWidth === 1680 || this.windowWidth === 1680) {
        this.is1680 = true;
        this.is1920 = false;
        this.is1280 = false;
        this.is1422 = false;
        this.is962 = false;
      } else if (newWidth === 1920 || this.windowWidth === 1920) {
        this.is1920 = true;
        this.is1680 = false;
        this.is1280 = false;
        this.is1422 = false;
        this.is962 = false;
        this.is1600 = false;
      } else if (newWidth === 962 || this.windowWidth === 962) {
        this.is962 = true;
        this.is1680 = false;
        this.is1280 = false;
        this.is1422 = false;
        this.is1920 = false;
        this.is1600 = false;
      } else if (newWidth === 1600 || this.windowWidth === 1600) {
        this.is1600 = true;
        this.is962 = false;
        this.is1680 = false;
        this.is1280 = false;
        this.is1422 = false;
        this.is1920 = false;
      }
    },
    checkedIfApprovers: function checkedIfApprovers() {
      var _this2 = this;

      axios.get('/api/check-user-create').then(function (response) {
        if (response.data.check === 1) {
          _this2.disApproveQty = false;
        } else if (response.data.check === 0) {
          _this2.disApproveQty = true;
        }
      });
    },
    addItem: function addItem() {
      var _this3 = this;

      console.log(this.stocks);
      var dup = '';
      var good = '';

      if (this.stocks.length !== 0) {
        this.stocks.map(function (value) {
          // console.log(value);
          if (value.barcode === _this3.form.barcode) {
            _this3.form.barcode = null;
            _this3.form.mat_code = null;
            _this3.form.uom = null;
            dup = 0;
          } else if (value.barcode !== _this3.form.barcode) {
            good = 1; //good
          }
        });

        if (good === 1 && dup !== 0) {
          console.log('test');
          var elem = document.createElement('tr');
          this.stocks.push({
            'req_date': this.form.req_date,
            'cust_company': this.form.cust_company,
            'mat_group': this.form.mat_group,
            'mat_code': this.form.mat_code,
            'mat_des': this.form.mat_des,
            'sales_unit': this.form.uom,
            'barcode': this.form.barcode,
            'ERDAT': this.form.ERDAT,
            'KWMENG': this.form.KWMENG,
            'stocks_left': this.multiForm.sStocksLeft,
            'order_qty': this.multiForm.sSuggestQty,
            'approve_qty': this.multiForm.sApproveQty,
            'stockDate': this.dateTime
          });
          this.form.barcode = null;
          this.form.mat_code = null;
          this.form.uom = null;
        } else if (dup === 0) {
          this.alertSwal('Duplicate Data Found', 'Please remove first to continue', 'warning', '');
        }
      } else {
        var elem = document.createElement('tr');
        this.stocks.push({
          'req_date': this.form.req_date,
          'cust_company': this.form.cust_company,
          'mat_group': this.form.mat_group,
          'mat_code': this.form.mat_code,
          'mat_des': this.form.mat_des,
          'sales_unit': this.form.uom,
          'barcode': this.form.barcode,
          'ERDAT': this.form.ERDAT,
          'KWMENG': this.form.KWMENG,
          'stocks_left': this.multiForm.sStocksLeft,
          'order_qty': this.multiForm.sSuggestQty,
          'approve_qty': this.multiForm.sApproveQty,
          'stockDate': this.dateTime
        });
        this.form.barcode = null;
        this.form.mat_code = null;
        this.form.uom = null;
      }
    },
    getDraftsDetails: function getDraftsDetails(page) {
      var _this4 = this;

      if (typeof page === 'undefined') {
        this.page = 1;
      }

      var uri = "/api/drafts/".concat(this.$route.params.id, "/edit?page=") + page + "&search=" + this.search + "&sortby=" + this.currentSort + "&sortdir=" + this.currentSortDir + "&currentpage=" + this.currentPage;
      this.uri = uri;
      this.page = page;
      axios.get(uri).then(function (response) {
        // console.log(response.data.data);
        _this4.stocks = response.data.data;
      });
    },
    getDraftsHeader: function getDraftsHeader(page) {
      var _this5 = this;

      if (typeof page === 'undefined') {
        this.page = 1;
      }

      var uri = "/api/drafts-header/".concat(this.$route.params.id, "?page=") + page + "&search=" + this.search + "&sortby=" + this.currentSort + "&sortdir=" + this.currentSortDir + "&currentpage=" + this.currentPage;
      this.uri = uri;
      this.page = page;
      axios.get(uri).then(function (response) {
        _this5.form.req_date = response.data[0].drafts_req_date;
        _this5.form.cust_company = response.data[0].company;

        _this5.trans_header.push({
          'req_date': _this5.form.req_date,
          'cust_company': _this5.form.cust_company
        });
      });
    },
    stockReqCreate: function stockReqCreate() {
      var _this6 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                if (!(_this6.stocks.length !== 0)) {
                  _context.next = 6;
                  break;
                }

                // console.log(this.stocks);
                _this6.trans_stocks.push({
                  'stocks': _this6.stocks,
                  'header': _this6.trans_header
                });

                _context.next = 4;
                return axios.patch("/api/divert/".concat(_this6.$route.params.id, "/divert-to-stocks"), _this6.trans_stocks).then(function (response) {
                  _this6.$router.push({
                    name: 'stock-request-table',
                    query: {
                      stockRequest: 'DataTable',
                      id: _this6.currentUser.token
                    }
                  });

                  Reload.$emit('AfterCreated');
                  Notification.success();
                })["catch"](function (error) {
                  _this6.alertSwal(error, '', 'warning', '');
                });

              case 4:
                _context.next = 7;
                break;

              case 6:
                _this6.alertSwal('Cannot save empty transactions', '', 'warning', '');

              case 7:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }))();
    },
    stockReqAsDraft: function stockReqAsDraft() {
      var _this7 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2() {
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                if (!(_this7.stocks.length !== 0)) {
                  _context2.next = 6;
                  break;
                }

                // console.log(this.stocks);
                _this7.trans_stocks.push({
                  'stocks': _this7.stocks,
                  'header': _this7.trans_header
                });

                _context2.next = 4;
                return axios.patch("/api/drafts-update/".concat(_this7.$route.params.id), _this7.trans_stocks).then(function (response) {
                  Reload.$emit('AfterCreated');
                  Notification.success();

                  _this7.$router.push({
                    name: 'drafts-table',
                    query: {
                      drafts: 'DataTable',
                      id: _this7.currentUser.token
                    }
                  });
                })["catch"](function (error) {
                  _this7.alertSwal(error, '', 'warning', '');
                });

              case 4:
                _context2.next = 7;
                break;

              case 6:
                _this7.alertSwal('Cannot save empty transactions', '', 'warning', '');

              case 7:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2);
      }))();
    },
    rangeMatCode: function rangeMatCode() {
      alert('Under Construction');
    },
    rangeBarcode: function rangeBarcode() {
      if (this.form.barcode === null || this.form.cust_company === null) {
        if (this.form.barcode === null) {
          this.alertSwal('No Barcode Found!', 'Please select a barcode to continue', 'error');
        } else if (this.form.cust_company === null) {
          this.alertSwal('No Company Found!', 'Please select a company to continue', 'error');
        }
      } else {
        this.rangeBarcodeModal = !this.rangeBarcodeModal;
      }
    },
    dataRangeBarcode: function dataRangeBarcode(num, close) {
      // console.log(num,close);
      this.rangeNumber = num;
      this.rangeBarcodeModal = close;
      this.getRangeBarcode();
    },
    getRangeBarcode: function getRangeBarcode() {
      var _this8 = this;

      this.arrRange.push({
        'range': this.rangeNumber,
        'barcode': this.form.barcode
      });
      axios.post('/api/range-barcode-drafts', this.arrRange).then(function (response) {
        // console.log(response.data,'testrange');
        if (_this8.stocks.length !== 0) {
          var dup = '';
          var good = '';

          _this8.stocks.map(function (value) {
            response.data.map(function (result) {
              // console.log(value.barcode,'value');
              // console.log(result.barcode,'result');
              if (value.barcode === result.barcode) {
                _this8.form.barcode = null;
                _this8.form.mat_code = null;
                _this8.form.uom = null;
                dup = 0; // console.log('sulodkadi123');
              } else if (value.barcode !== result.barcode) {
                //good
                // console.log('goodiesulod');
                good = 1;
              }
            });
          });

          if (good === 1 && dup !== 0) {
            // console.log('range success');
            _this8.stocks = response.data;

            _this8.alertSwal('Range Data Success', '', 'success', '');
          } else if (dup === 0) {
            // console.log('range duplicate');
            _this8.alertSwal('Duplicate Data Found', 'Please remove first to continue', 'warning', '');
          }
        } else {
          _this8.stocks = response.data;
        }
      });
    },
    closeModal: function closeModal(e) {
      this.rangeBarcodeModal = e;
    },
    removeElement: function removeElement(index) {
      this.stocks.splice(index, 1);
    },
    getCustCompany: function getCustCompany() {
      var _this9 = this;

      axios.get('/api/get-customer/stock-creation').then(function (response) {
        _this9.company = response.data;
      });
    },
    multiGetBarCode: function multiGetBarCode(newVal) {
      var _this10 = this;

      if (newVal !== null) {
        var formData = new FormData();
        formData.append('MatGroup', newVal);
        axios.post('/api/get-mat-barcode', formData).then(function (response) {
          _this10.multiBarcode = response.data;
        });
      } else {
        this.multiBarcode = [];
      }
    },
    getBarCode: function getBarCode(newVal) {
      var _this11 = this;

      if (newVal !== null) {
        var formData = new FormData();
        formData.append('MatGroup', newVal);
        axios.post('/api/get-mat-barcode', formData).then(function (response) {
          _this11.matBarcode = response.data;
        });
      } else {
        this.matBarcode = [];
      }
    },
    getAllBarCode: function getAllBarCode() {
      var _this12 = this;

      // get all barcode if mat group is null
      axios.get('/api/get-all-barcode').then(function (response) {
        _this12.matBarcode = response.data;
      });
    },
    getMatCode: function getMatCode(newVal) {
      var _this13 = this;

      if (this.form.barcode !== null && this.form.cust_company !== null) {
        axios.get('/api/get-mat-code?barcode=' + this.form.barcode + '&custCompany=' + this.form.cust_company).then(function (response) {
          _this13.form.mat_code = response.data.MATNR;
          _this13.form.mat_des = response.data.MAKTX;
          _this13.form.uom = response.data.MEINH; // sales_unit

          _this13.form.ERDAT = response.data.ERDAT; // last fill up date

          _this13.form.KWMENG = response.data.KWMENG; // last fill up date
        });
      } else {}
    },
    fuseSearch: function fuseSearch(options, search) {
      var fuse = new fuse_js__WEBPACK_IMPORTED_MODULE_3__["default"](options, {
        keys: ["WGBEZ"],
        shouldSort: true
      });
      return search.length ? fuse.search(search).map(function (_ref) {
        var item = _ref.item;
        return item;
      }) : fuse.list;
    },
    fuseSearchMatCode: function fuseSearchMatCode(options, search) {
      var fuse = new fuse_js__WEBPACK_IMPORTED_MODULE_3__["default"](options, {
        keys: ["MATNR", "MAKTX"],
        shouldSort: true
      });
      return search.length ? fuse.search(search).map(function (_ref2) {
        var item = _ref2.item;
        return item;
      }) : fuse.list;
    },
    fuseSearchCompany: function fuseSearchCompany(options, search) {
      var fuse = new fuse_js__WEBPACK_IMPORTED_MODULE_3__["default"](options, {
        keys: ["NAME1"],
        shouldSort: true
      });
      return search.length ? fuse.search(search).map(function (_ref3) {
        var item = _ref3.item;
        return item;
      }) : fuse.list;
    },
    fuseSearchBarcode: function fuseSearchBarcode(options, search) {
      var fuse = new fuse_js__WEBPACK_IMPORTED_MODULE_3__["default"](options, {
        keys: ["EAN11", "MAKTX", "MATNR"],
        shouldSort: true
      });
      return search.length ? fuse.search(search).map(function (_ref4) {
        var item = _ref4.item;
        return item;
      }) : fuse.list;
    },
    getMatGroup: function getMatGroup() {
      var _this14 = this;

      axios.get('/api/get-mat-group').then(function (response) {
        _this14.matGroup = response.data;
      });
    },
    alertSwal: function alertSwal(title, text, icon, timer) {
      this.$swal({
        title: title,
        text: text,
        icon: icon,
        timer: timer
      });
    },
    formatNumber: function formatNumber(number) {
      return number === '' ? '' : parseFloat(Math.abs(number)).toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
    }
  }
});

/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/drafts/view.vue?vue&type=style&index=0&id=0d350cb2&scoped=true&lang=css&":
/*!*****************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/drafts/view.vue?vue&type=style&index=0&id=0d350cb2&scoped=true&lang=css& ***!
  \*****************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n@media (min-width: 992px) {\n.modal-dialog[data-v-0d350cb2] {\n        max-width: 30% !important;\n}\n}\n@media (min-width: 992px) {\n.modal_custom[data-v-0d350cb2] {\n        max-width: 50% !important;\n}\n}\n@media screen and (max-width: 1680px) and (min-width: 1280px){\n.dateCreate .col-md-4[data-v-0d350cb2]{\n}\n}\n.isTuod1280[data-v-0d350cb2]{\n    margin-left: -8px;\n}\n.isTuod1422[data-v-0d350cb2]{\n    margin-left: -56px;\n}\n.isTuod1600[data-v-0d350cb2]{\n    margin-left: -115px;\n}\n.isTuod1680[data-v-0d350cb2]{\n    margin-left: -143px;\n}\n.isTuod1920[data-v-0d350cb2]{\n    margin-left: -223px;\n}\n.isRangeTuod962[data-v-0d350cb2]{\n    margin-left: -50px;\n}\n.isRangeTuod1280[data-v-0d350cb2]{\n    margin-left: -75px;\n}\n.isRangeTuod1422[data-v-0d350cb2]{\n    margin-left: -145px;\n}\n.isRangeTuod1600[data-v-0d350cb2]{\n    margin-left: -240px;\n}\n.isRangeTuod1680[data-v-0d350cb2]{\n    margin-left: -275px;\n}\n.isRangeTuod1920[data-v-0d350cb2]{\n    margin-left: -395px;\n}\n.input-group-text[data-v-0d350cb2] {\n    background-color: #007bff;\n    color: white;\n}\n.sampleClass[data-v-0d350cb2] {\n    background-color: rgba(0, 0, 0, 0.5) !important;\n    /*opacity: 0.5 !important;*/\n}\n.example-open .modal-backdrop.show[data-v-0d350cb2]:nth-of-type(even) {\n    opacity: 5 !important;\n    z-index: 1052 !important;\n}\n.filter-asc[data-v-0d350cb2] {\n    border-bottom: 5px solid #ccc;\n    margin-bottom: 1px\n}\n.filter-asc[data-v-0d350cb2], .filter-desc[data-v-0d350cb2] {\n    width: 0;\n    height: 0;\n    border-left: 5px solid transparent;\n    border-right: 5px solid transparent\n}\n.filter-desc[data-v-0d350cb2] {\n    border-top: 5px solid #ccc;\n    margin-top: 1px\n}\n.inline-block[data-v-0d350cb2] {\n    display: inline-block\n}\n[data-position='below'] .autocomplete-result-list[data-v-0d350cb2] {\n    border-bottom: none;\n    border-radius: 8px 8px 0 0;\n    color: forestgreen;\n    font-weight: bold;\n}\n.wiki-result[data-v-0d350cb2] {\n    border-top: 1px solid #eee;\n    padding: 16px;\n    background: transparent;\n}\n.wiki-title[data-v-0d350cb2] {\n    font-size: 15px;\n    margin-bottom: 8px;\n    font-weight: 550;\n    color: forestgreen;\n}\n.wiki-snippet[data-v-0d350cb2] {\n    font-size: 14px;\n    color: rgba(0, 0, 0, 0.54);\n}\n.wrapAll[data-v-0d350cb2] {\n    white-space: nowrap;\n    overflow: hidden;\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/drafts/view.vue?vue&type=style&index=1&lang=css&":
/*!*****************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/drafts/view.vue?vue&type=style&index=1&lang=css& ***!
  \*****************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.matGroupHeight .vs__dropdown-toggle {\n    height: 38px;\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/drafts/view.vue?vue&type=style&index=0&id=0d350cb2&scoped=true&lang=css&":
/*!*********************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/drafts/view.vue?vue&type=style&index=0&id=0d350cb2&scoped=true&lang=css& ***!
  \*********************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../node_modules/css-loader??ref--6-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--6-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./view.vue?vue&type=style&index=0&id=0d350cb2&scoped=true&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/drafts/view.vue?vue&type=style&index=0&id=0d350cb2&scoped=true&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/drafts/view.vue?vue&type=style&index=1&lang=css&":
/*!*********************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/drafts/view.vue?vue&type=style&index=1&lang=css& ***!
  \*********************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../node_modules/css-loader??ref--6-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--6-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./view.vue?vue&type=style&index=1&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/drafts/view.vue?vue&type=style&index=1&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/drafts/view.vue?vue&type=template&id=0d350cb2&scoped=true&":
/*!**************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/drafts/view.vue?vue&type=template&id=0d350cb2&scoped=true& ***!
  \**************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c("div", { staticClass: "row" }, [
        _c("div", { staticClass: "col-lg-12" }, [
          _c("div", { staticClass: "card shadow-sm mb-4" }, [
            _vm._m(0),
            _vm._v(" "),
            _c(
              "form",
              {
                directives: [
                  {
                    name: "promise-btn",
                    rawName: "v-promise-btn",
                    value: { action: "submit" },
                    expression: "{action: 'submit'}"
                  }
                ],
                on: {
                  submit: function($event) {
                    $event.preventDefault()
                    return _vm.addItem($event)
                  }
                }
              },
              [
                _c("div", { staticClass: "card-header" }, [
                  _c("div", { staticClass: "row form-group" }, [
                    _c(
                      "div",
                      {
                        staticClass: "dateCreate",
                        staticStyle: { "margin-left": "15px" }
                      },
                      [
                        _c("div", { staticClass: "input-group" }, [
                          _vm._m(1),
                          _vm._v(" "),
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.form.req_date,
                                expression: "form.req_date"
                              }
                            ],
                            staticClass: "form-control",
                            attrs: {
                              type: "date",
                              placeholder: "Request Date",
                              required: ""
                            },
                            domProps: { value: _vm.form.req_date },
                            on: {
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.$set(
                                  _vm.form,
                                  "req_date",
                                  $event.target.value
                                )
                              }
                            }
                          })
                        ])
                      ]
                    ),
                    _vm._v(" "),
                    _vm.windowWidth !== 962 && _vm.windowWidth !== 960
                      ? _c("div", { staticClass: "col-md-2" })
                      : _vm._e(),
                    _vm._v(" "),
                    _vm.windowWidth === 962 || _vm.windowWidth === 960
                      ? _c("div", { staticClass: "col-md-3" }, [
                          _c("div", { staticClass: "input-group" }, [
                            _vm._m(2),
                            _vm._v(" "),
                            _c("input", {
                              directives: [
                                {
                                  name: "model",
                                  rawName: "v-model",
                                  value: _vm.form.uom,
                                  expression: "form.uom"
                                }
                              ],
                              staticClass: "form-control",
                              attrs: { type: "text", disabled: "" },
                              domProps: { value: _vm.form.uom },
                              on: {
                                input: function($event) {
                                  if ($event.target.composing) {
                                    return
                                  }
                                  _vm.$set(_vm.form, "uom", $event.target.value)
                                }
                              }
                            })
                          ])
                        ])
                      : _vm._e(),
                    _vm._v(" "),
                    _vm.windowWidth !== 962 &&
                    _vm.windowWidth !== 960 &&
                    _vm.windowWidth !== 723
                      ? _c("div", { staticClass: "col-md-2" }, [
                          _c("div", { staticClass: "input-group" }, [
                            _vm._m(3),
                            _vm._v(" "),
                            _c("input", {
                              directives: [
                                {
                                  name: "model",
                                  rawName: "v-model",
                                  value: _vm.form.uom,
                                  expression: "form.uom"
                                }
                              ],
                              staticClass: "form-control",
                              attrs: { type: "text", disabled: "" },
                              domProps: { value: _vm.form.uom },
                              on: {
                                input: function($event) {
                                  if ($event.target.composing) {
                                    return
                                  }
                                  _vm.$set(_vm.form, "uom", $event.target.value)
                                }
                              }
                            })
                          ])
                        ])
                      : _vm._e(),
                    _vm._v(" "),
                    _vm.windowWidth === 962 || _vm.windowWidth === 960
                      ? _c("div", { staticClass: "col-md-4" }, [
                          _c("div", { staticClass: "input-group" }, [
                            _vm._m(4),
                            _vm._v(" "),
                            _c("input", {
                              directives: [
                                {
                                  name: "model",
                                  rawName: "v-model",
                                  value: _vm.form.mat_code,
                                  expression: "form.mat_code"
                                }
                              ],
                              staticClass: "form-control",
                              attrs: { type: "text", disabled: "" },
                              domProps: { value: _vm.form.mat_code },
                              on: {
                                input: function($event) {
                                  if ($event.target.composing) {
                                    return
                                  }
                                  _vm.$set(
                                    _vm.form,
                                    "mat_code",
                                    $event.target.value
                                  )
                                }
                              }
                            })
                          ])
                        ])
                      : _vm._e(),
                    _vm._v(" "),
                    _vm.windowWidth !== 962 && _vm.windowWidth !== 960
                      ? _c("div", { staticClass: "col-md-5" })
                      : _vm._e()
                  ]),
                  _vm._v(" "),
                  _vm.windowWidth === 723
                    ? _c("div", { staticClass: "row form-group" }, [
                        _c(
                          "div",
                          {
                            staticClass: "col-md-2",
                            staticStyle: { width: "35%" }
                          },
                          [
                            _c("div", { staticClass: "input-group" }, [
                              _vm._m(5),
                              _vm._v(" "),
                              _c("input", {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value: _vm.form.uom,
                                    expression: "form.uom"
                                  }
                                ],
                                staticClass: "form-control",
                                attrs: { type: "text", disabled: "" },
                                domProps: { value: _vm.form.uom },
                                on: {
                                  input: function($event) {
                                    if ($event.target.composing) {
                                      return
                                    }
                                    _vm.$set(
                                      _vm.form,
                                      "uom",
                                      $event.target.value
                                    )
                                  }
                                }
                              })
                            ])
                          ]
                        )
                      ])
                    : _vm._e(),
                  _vm._v(" "),
                  _c("div", { staticClass: "row form-group" }, [
                    _vm.windowWidth === 962 || _vm.windowWidth === 960
                      ? _c("div", { staticClass: "col-md-8" }, [
                          _c(
                            "div",
                            { staticClass: "input-group" },
                            [
                              _vm._m(6),
                              _vm._v(" "),
                              _c("v-select", {
                                class: { matGroupHeight: true },
                                staticStyle: { width: "380px" },
                                attrs: {
                                  filter: _vm.fuseSearchCompany,
                                  options: _vm.company,
                                  getOptionLabel: function(option) {
                                    return option.NAME1
                                  },
                                  reduce: function(company) {
                                    return "" + company.NAME1
                                  },
                                  required: ""
                                },
                                scopedSlots: _vm._u(
                                  [
                                    {
                                      key: "option",
                                      fn: function(ref) {
                                        var NAME1 = ref.NAME1
                                        return [
                                          _c("strong", [_vm._v(_vm._s(NAME1))])
                                        ]
                                      }
                                    },
                                    {
                                      key: "search",
                                      fn: function(ref) {
                                        var attributes = ref.attributes
                                        var events = ref.events
                                        return [
                                          _c(
                                            "input",
                                            _vm._g(
                                              _vm._b(
                                                {
                                                  staticClass: "vs__search",
                                                  attrs: {
                                                    required: !_vm.form
                                                      .cust_company,
                                                    name: "cust_company"
                                                  }
                                                },
                                                "input",
                                                attributes,
                                                false
                                              ),
                                              events
                                            )
                                          )
                                        ]
                                      }
                                    }
                                  ],
                                  null,
                                  false,
                                  2120773482
                                ),
                                model: {
                                  value: _vm.form.cust_company,
                                  callback: function($$v) {
                                    _vm.$set(_vm.form, "cust_company", $$v)
                                  },
                                  expression: "form.cust_company"
                                }
                              })
                            ],
                            1
                          )
                        ])
                      : _vm._e(),
                    _vm._v(" "),
                    _vm.windowWidth !== 962 && _vm.windowWidth !== 960
                      ? _c("div", { staticClass: "col-md-6" }, [
                          _c(
                            "div",
                            { staticClass: "input-group" },
                            [
                              _vm._m(7),
                              _vm._v(" "),
                              _c("v-select", {
                                class: { matGroupHeight: true },
                                staticStyle: { width: "380px" },
                                attrs: {
                                  filter: _vm.fuseSearchCompany,
                                  options: _vm.company,
                                  getOptionLabel: function(option) {
                                    return option.NAME1
                                  },
                                  reduce: function(company) {
                                    return "" + company.NAME1
                                  },
                                  required: ""
                                },
                                scopedSlots: _vm._u(
                                  [
                                    {
                                      key: "option",
                                      fn: function(ref) {
                                        var NAME1 = ref.NAME1
                                        return [
                                          _c("strong", [_vm._v(_vm._s(NAME1))])
                                        ]
                                      }
                                    },
                                    {
                                      key: "search",
                                      fn: function(ref) {
                                        var attributes = ref.attributes
                                        var events = ref.events
                                        return [
                                          _c(
                                            "input",
                                            _vm._g(
                                              _vm._b(
                                                {
                                                  staticClass: "vs__search",
                                                  attrs: {
                                                    required: !_vm.form
                                                      .cust_company,
                                                    name: "cust_company"
                                                  }
                                                },
                                                "input",
                                                attributes,
                                                false
                                              ),
                                              events
                                            )
                                          )
                                        ]
                                      }
                                    }
                                  ],
                                  null,
                                  false,
                                  2120773482
                                ),
                                model: {
                                  value: _vm.form.cust_company,
                                  callback: function($$v) {
                                    _vm.$set(_vm.form, "cust_company", $$v)
                                  },
                                  expression: "form.cust_company"
                                }
                              })
                            ],
                            1
                          )
                        ])
                      : _vm._e(),
                    _vm._v(" "),
                    _vm.windowWidth === 1280 ||
                    _vm.windowWidth === 1422 ||
                    _vm.windowWidth === 1600
                      ? _c(
                          "div",
                          {
                            staticClass: "col-md-4",
                            class: [
                              { isTuod1280: _vm.is1280 },
                              { isTuod1422: _vm.is1422 },
                              { isTuod1600: _vm.is1600 },
                              { isTuod1680: _vm.is1680 },
                              { isTuod1920: _vm.is1920 }
                            ]
                          },
                          [
                            _c("div", { staticClass: "input-group" }, [
                              _vm._m(8),
                              _vm._v(" "),
                              _c("input", {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value: _vm.form.mat_code,
                                    expression: "form.mat_code"
                                  }
                                ],
                                staticClass: "form-control",
                                attrs: { type: "text", disabled: "" },
                                domProps: { value: _vm.form.mat_code },
                                on: {
                                  input: function($event) {
                                    if ($event.target.composing) {
                                      return
                                    }
                                    _vm.$set(
                                      _vm.form,
                                      "mat_code",
                                      $event.target.value
                                    )
                                  }
                                }
                              })
                            ])
                          ]
                        )
                      : _vm._e(),
                    _vm._v(" "),
                    _vm.windowWidth !== 1280 &&
                    _vm.windowWidth !== 1422 &&
                    _vm.windowWidth !== 962 &&
                    _vm.windowWidth !== 960 &&
                    _vm.windowWidth !== 1600 &&
                    _vm.windowWidth !== 723
                      ? _c(
                          "div",
                          {
                            staticClass: "col-md-3",
                            class: [
                              { isTuod1280: _vm.is1280 },
                              { isTuod1422: _vm.is1422 },
                              { isTuod1600: _vm.is1600 },
                              { isTuod1680: _vm.is1680 },
                              { isTuod1920: _vm.is1920 }
                            ]
                          },
                          [
                            _c("div", { staticClass: "input-group" }, [
                              _vm._m(9),
                              _vm._v(" "),
                              _c("input", {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value: _vm.form.mat_code,
                                    expression: "form.mat_code"
                                  }
                                ],
                                staticClass: "form-control",
                                attrs: { type: "text", disabled: "" },
                                domProps: { value: _vm.form.mat_code },
                                on: {
                                  input: function($event) {
                                    if ($event.target.composing) {
                                      return
                                    }
                                    _vm.$set(
                                      _vm.form,
                                      "mat_code",
                                      $event.target.value
                                    )
                                  }
                                }
                              })
                            ])
                          ]
                        )
                      : _vm._e(),
                    _vm._v(" "),
                    _c("div", { staticClass: "col-md-3" }),
                    _vm._v(" "),
                    _c("div", { staticClass: "col-md-1" })
                  ]),
                  _vm._v(" "),
                  _vm.windowWidth === 723
                    ? _c(
                        "div",
                        {
                          staticClass: "row form-group",
                          staticStyle: { width: "65%" }
                        },
                        [
                          _c("div", { staticClass: "col-md-3" }, [
                            _c("div", { staticClass: "input-group" }, [
                              _vm._m(10),
                              _vm._v(" "),
                              _c("input", {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value: _vm.form.mat_code,
                                    expression: "form.mat_code"
                                  }
                                ],
                                staticClass: "form-control",
                                attrs: { type: "text", disabled: "" },
                                domProps: { value: _vm.form.mat_code },
                                on: {
                                  input: function($event) {
                                    if ($event.target.composing) {
                                      return
                                    }
                                    _vm.$set(
                                      _vm.form,
                                      "mat_code",
                                      $event.target.value
                                    )
                                  }
                                }
                              })
                            ])
                          ])
                        ]
                      )
                    : _vm._e(),
                  _vm._v(" "),
                  _c("div", { staticClass: "row form-group" }, [
                    _vm.windowWidth === 962 || _vm.windowWidth === 960
                      ? _c("div", { staticClass: "col-md-7" }, [
                          _c(
                            "div",
                            { staticClass: "input-group" },
                            [
                              _vm._m(11),
                              _vm._v(" "),
                              _c("v-select", {
                                class: { matGroupHeight: true },
                                staticStyle: { width: "300px" },
                                attrs: {
                                  filter: _vm.fuseSearch,
                                  options: _vm.matGroup,
                                  getOptionLabel: function(option) {
                                    return option.WGBEZ
                                  },
                                  reduce: function(matGroup) {
                                    return "" + matGroup.WGBEZ
                                  },
                                  required: ""
                                },
                                scopedSlots: _vm._u(
                                  [
                                    {
                                      key: "option",
                                      fn: function(ref) {
                                        var WGBEZ = ref.WGBEZ
                                        return [
                                          _c("strong", [_vm._v(_vm._s(WGBEZ))])
                                        ]
                                      }
                                    },
                                    {
                                      key: "search",
                                      fn: function(ref) {
                                        var attributes = ref.attributes
                                        var events = ref.events
                                        return [
                                          _c(
                                            "input",
                                            _vm._g(
                                              _vm._b(
                                                {
                                                  staticClass: "vs__search",
                                                  attrs: { name: "mat_group" }
                                                },
                                                "input",
                                                attributes,
                                                false
                                              ),
                                              events
                                            )
                                          )
                                        ]
                                      }
                                    }
                                  ],
                                  null,
                                  false,
                                  2235762676
                                ),
                                model: {
                                  value: _vm.form.mat_group,
                                  callback: function($$v) {
                                    _vm.$set(_vm.form, "mat_group", $$v)
                                  },
                                  expression: "form.mat_group"
                                }
                              })
                            ],
                            1
                          )
                        ])
                      : _vm._e(),
                    _vm._v(" "),
                    _vm.windowWidth !== 962 && _vm.windowWidth !== 960
                      ? _c("div", { staticClass: "col-md-6" }, [
                          _c(
                            "div",
                            { staticClass: "input-group" },
                            [
                              _vm._m(12),
                              _vm._v(" "),
                              _c("v-select", {
                                class: { matGroupHeight: true },
                                staticStyle: { width: "300px" },
                                attrs: {
                                  filter: _vm.fuseSearch,
                                  options: _vm.matGroup,
                                  getOptionLabel: function(option) {
                                    return option.WGBEZ
                                  },
                                  reduce: function(matGroup) {
                                    return "" + matGroup.WGBEZ
                                  },
                                  required: ""
                                },
                                scopedSlots: _vm._u(
                                  [
                                    {
                                      key: "option",
                                      fn: function(ref) {
                                        var WGBEZ = ref.WGBEZ
                                        return [
                                          _c("strong", [_vm._v(_vm._s(WGBEZ))])
                                        ]
                                      }
                                    },
                                    {
                                      key: "search",
                                      fn: function(ref) {
                                        var attributes = ref.attributes
                                        var events = ref.events
                                        return [
                                          _c(
                                            "input",
                                            _vm._g(
                                              _vm._b(
                                                {
                                                  staticClass: "vs__search",
                                                  attrs: { name: "mat_group" }
                                                },
                                                "input",
                                                attributes,
                                                false
                                              ),
                                              events
                                            )
                                          )
                                        ]
                                      }
                                    }
                                  ],
                                  null,
                                  false,
                                  2235762676
                                ),
                                model: {
                                  value: _vm.form.mat_group,
                                  callback: function($$v) {
                                    _vm.$set(_vm.form, "mat_group", $$v)
                                  },
                                  expression: "form.mat_group"
                                }
                              })
                            ],
                            1
                          )
                        ])
                      : _vm._e()
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "row form-group" }, [
                    _vm.windowWidth === 962 || _vm.windowWidth === 960
                      ? _c("div", { staticClass: "col-md-7" }, [
                          _c(
                            "div",
                            { staticClass: "input-group" },
                            [
                              _vm._m(13),
                              _vm._v(" "),
                              _c("v-select", {
                                class: { matGroupHeight: true },
                                staticStyle: { width: "325px" },
                                attrs: {
                                  filter: _vm.fuseSearchBarcode,
                                  options: _vm.matBarcode,
                                  getOptionLabel: function(option) {
                                    return option.EAN11
                                  },
                                  reduce: function(matBarcode) {
                                    return "" + matBarcode.EAN11
                                  },
                                  required: ""
                                },
                                scopedSlots: _vm._u(
                                  [
                                    {
                                      key: "option",
                                      fn: function(ref) {
                                        var EAN11 = ref.EAN11
                                        var MAKTX = ref.MAKTX
                                        var MATNR = ref.MATNR
                                        return [
                                          _c("strong", [
                                            _vm._v(
                                              "Material Code: " +
                                                _vm._s(MATNR) +
                                                " "
                                            )
                                          ]),
                                          _vm._v(" "),
                                          _c("br"),
                                          _vm._v(" "),
                                          _c("strong", [
                                            _vm._v("Barcode: " + _vm._s(EAN11))
                                          ]),
                                          _vm._v(" "),
                                          _c("br"),
                                          _vm._v(" "),
                                          _c("cite", [
                                            _vm._v("Material: " + _vm._s(MAKTX))
                                          ])
                                        ]
                                      }
                                    },
                                    {
                                      key: "search",
                                      fn: function(ref) {
                                        var attributes = ref.attributes
                                        var events = ref.events
                                        return [
                                          _c(
                                            "input",
                                            _vm._g(
                                              _vm._b(
                                                {
                                                  staticClass: "vs__search",
                                                  attrs: {
                                                    required: !_vm.form.barcode,
                                                    name: "barcode"
                                                  }
                                                },
                                                "input",
                                                attributes,
                                                false
                                              ),
                                              events
                                            )
                                          )
                                        ]
                                      }
                                    }
                                  ],
                                  null,
                                  false,
                                  1570173513
                                ),
                                model: {
                                  value: _vm.form.barcode,
                                  callback: function($$v) {
                                    _vm.$set(_vm.form, "barcode", $$v)
                                  },
                                  expression: "form.barcode"
                                }
                              })
                            ],
                            1
                          )
                        ])
                      : _vm._e(),
                    _vm._v(" "),
                    _vm.windowWidth !== 962 && _vm.windowWidth !== 960
                      ? _c("div", { staticClass: "col-md-6" }, [
                          _c(
                            "div",
                            { staticClass: "input-group" },
                            [
                              _vm._m(14),
                              _vm._v(" "),
                              _c("v-select", {
                                class: { matGroupHeight: true },
                                staticStyle: { width: "325px" },
                                attrs: {
                                  filter: _vm.fuseSearchBarcode,
                                  options: _vm.matBarcode,
                                  getOptionLabel: function(option) {
                                    return option.EAN11
                                  },
                                  reduce: function(matBarcode) {
                                    return "" + matBarcode.EAN11
                                  },
                                  required: ""
                                },
                                scopedSlots: _vm._u(
                                  [
                                    {
                                      key: "option",
                                      fn: function(ref) {
                                        var EAN11 = ref.EAN11
                                        var MAKTX = ref.MAKTX
                                        var MATNR = ref.MATNR
                                        return [
                                          _c("strong", [
                                            _vm._v(
                                              "Material Code: " +
                                                _vm._s(MATNR) +
                                                " "
                                            )
                                          ]),
                                          _vm._v(" "),
                                          _c("br"),
                                          _vm._v(" "),
                                          _c("strong", [
                                            _vm._v("Barcode: " + _vm._s(EAN11))
                                          ]),
                                          _vm._v(" "),
                                          _c("br"),
                                          _vm._v(" "),
                                          _c("cite", [
                                            _vm._v("Material: " + _vm._s(MAKTX))
                                          ])
                                        ]
                                      }
                                    },
                                    {
                                      key: "search",
                                      fn: function(ref) {
                                        var attributes = ref.attributes
                                        var events = ref.events
                                        return [
                                          _c(
                                            "input",
                                            _vm._g(
                                              _vm._b(
                                                {
                                                  staticClass: "vs__search",
                                                  attrs: {
                                                    required: !_vm.form.barcode,
                                                    name: "barcode"
                                                  }
                                                },
                                                "input",
                                                attributes,
                                                false
                                              ),
                                              events
                                            )
                                          )
                                        ]
                                      }
                                    }
                                  ],
                                  null,
                                  false,
                                  1570173513
                                ),
                                model: {
                                  value: _vm.form.barcode,
                                  callback: function($$v) {
                                    _vm.$set(_vm.form, "barcode", $$v)
                                  },
                                  expression: "form.barcode"
                                }
                              })
                            ],
                            1
                          )
                        ])
                      : _vm._e(),
                    _vm._v(" "),
                    _vm.windowWidth !== 723
                      ? _c(
                          "div",
                          {
                            staticClass: "col-md-3",
                            class: [
                              { isRangeTuod1280: _vm.is1280 },
                              { isRangeTuod1422: _vm.is1422 },
                              { isRangeTuod1600: _vm.is1600 },
                              { isRangeTuod1680: _vm.is1680 },
                              { isRangeTuod1920: _vm.is1920 },
                              { isRangeTuod962: _vm.is962 },
                              { isRangeTuod960: _vm.is960 }
                            ]
                          },
                          [
                            _c(
                              "button",
                              {
                                staticClass: "btn btn-warning",
                                attrs: { type: "button" },
                                on: { click: _vm.rangeBarcode }
                              },
                              [
                                _vm._v("Range  "),
                                _c("i", { staticClass: "fas fa-exchange-alt" })
                              ]
                            )
                          ]
                        )
                      : _vm._e()
                  ]),
                  _vm._v(" "),
                  _vm.windowWidth === 723
                    ? _c("div", { staticClass: "row form-group" }, [
                        _c("div", { staticClass: "col-md-3" }, [
                          _c(
                            "button",
                            {
                              staticClass: "btn btn-warning",
                              attrs: { type: "button" },
                              on: { click: _vm.rangeBarcode }
                            },
                            [
                              _vm._v("Range  "),
                              _c("i", { staticClass: "fas fa-exchange-alt" })
                            ]
                          )
                        ])
                      ])
                    : _vm._e(),
                  _vm._v(" "),
                  _vm._m(15)
                ])
              ]
            ),
            _vm._v(" "),
            _c(
              "form",
              {
                directives: [
                  {
                    name: "promise-btn",
                    rawName: "v-promise-btn",
                    value: { action: "submit" },
                    expression: "{action: 'submit'}"
                  }
                ],
                on: {
                  submit: function($event) {
                    $event.preventDefault()
                    return _vm.stockReqCreate($event)
                  }
                }
              },
              [
                _c("div", { staticClass: "input-group row-md-12 mb-3" }, [
                  _c("br"),
                  _vm._v(" "),
                  _c(
                    "button",
                    {
                      staticClass: "btn btn-info",
                      staticStyle: {
                        margin: "0px 20px",
                        top: "11px",
                        position: "absolute"
                      },
                      attrs: { type: "button" },
                      on: {
                        click: function($event) {
                          $event.preventDefault()
                          return _vm.stockReqAsDraft($event)
                        }
                      }
                    },
                    [
                      _vm._v("SAVE AS DRAFT  "),
                      _c("i", { staticClass: "far fa-file" })
                    ]
                  ),
                  _vm._v(" "),
                  _vm._m(16)
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "card-body" }, [
                  _c("div", { staticClass: "table-responsive" }, [
                    _c(
                      "table",
                      {
                        staticClass:
                          "table align-items-center table-flush table-hover",
                        attrs: {
                          id: "dataTable",
                          width: "100%",
                          cellspacing: "0"
                        }
                      },
                      [
                        _vm._m(17),
                        _vm._v(" "),
                        _c(
                          "tbody",
                          _vm._l(_vm.stocks, function(stock, index) {
                            return _c("tr", [
                              _c("td", { staticClass: "wrapAll" }, [
                                _vm._v(_vm._s(stock.barcode))
                              ]),
                              _vm._v(" "),
                              _c("td", { staticClass: "wrapAll" }, [
                                _vm._v(_vm._s(stock.mat_code))
                              ]),
                              _vm._v(" "),
                              _c("td", { staticClass: "wrapAll" }, [
                                _vm._v(_vm._s(stock.mat_des))
                              ]),
                              _vm._v(" "),
                              _c("td", { staticClass: "wrapAll" }, [
                                _vm._v(" " + _vm._s(stock.sales_unit))
                              ]),
                              _vm._v(" "),
                              _c("td", { staticClass: "wrapAll" }, [
                                _vm._v(
                                  _vm._s(
                                    _vm._f("moment")(stock.ERDAT, "MM/DD/YYYY")
                                  )
                                )
                              ]),
                              _vm._v(" "),
                              stock.KWMENG === null
                                ? _c(
                                    "td",
                                    {
                                      staticClass: "wrapAll",
                                      staticStyle: { "text-align": "right" }
                                    },
                                    [
                                      _vm._v(
                                        _vm._s(stock.KWMENG) +
                                          "\n                                    "
                                      )
                                    ]
                                  )
                                : _vm._e(),
                              _vm._v(" "),
                              stock.KWMENG !== null
                                ? _c(
                                    "td",
                                    {
                                      staticClass: "wrapAll",
                                      staticStyle: { "text-align": "right" }
                                    },
                                    [
                                      _vm._v(
                                        _vm._s(_vm.formatNumber(stock.KWMENG)) +
                                          "\n                                    "
                                      )
                                    ]
                                  )
                                : _vm._e(),
                              _vm._v(" "),
                              _c("td", { staticClass: "wrapAll" }, [
                                _c("input", {
                                  directives: [
                                    {
                                      name: "model",
                                      rawName: "v-model",
                                      value: stock.stocks_left,
                                      expression: "stock.stocks_left"
                                    }
                                  ],
                                  staticClass: "form-control",
                                  staticStyle: { "text-align": "center" },
                                  attrs: { type: "number", placeholder: "" },
                                  domProps: { value: stock.stocks_left },
                                  on: {
                                    input: function($event) {
                                      if ($event.target.composing) {
                                        return
                                      }
                                      _vm.$set(
                                        stock,
                                        "stocks_left",
                                        $event.target.value
                                      )
                                    }
                                  }
                                })
                              ]),
                              _vm._v(" "),
                              _c("td", { staticClass: "wrapAll" }, [
                                _c("input", {
                                  directives: [
                                    {
                                      name: "model",
                                      rawName: "v-model",
                                      value: stock.order_qty,
                                      expression: "stock.order_qty"
                                    }
                                  ],
                                  staticClass: "form-control",
                                  staticStyle: { "text-align": "center" },
                                  attrs: {
                                    type: "number",
                                    placeholder: "",
                                    required: ""
                                  },
                                  domProps: { value: stock.order_qty },
                                  on: {
                                    input: function($event) {
                                      if ($event.target.composing) {
                                        return
                                      }
                                      _vm.$set(
                                        stock,
                                        "order_qty",
                                        $event.target.value
                                      )
                                    }
                                  }
                                })
                              ]),
                              _vm._v(" "),
                              _c("td", { staticClass: "wrapAll" }, [
                                _c("input", {
                                  directives: [
                                    {
                                      name: "model",
                                      rawName: "v-model",
                                      value: stock.approve_qty,
                                      expression: "stock.approve_qty"
                                    }
                                  ],
                                  staticClass: "form-control",
                                  staticStyle: { "text-align": "center" },
                                  attrs: {
                                    type: "number",
                                    disabled: _vm.disApproveQty,
                                    placeholder: ""
                                  },
                                  domProps: { value: stock.approve_qty },
                                  on: {
                                    input: function($event) {
                                      if ($event.target.composing) {
                                        return
                                      }
                                      _vm.$set(
                                        stock,
                                        "approve_qty",
                                        $event.target.value
                                      )
                                    }
                                  }
                                })
                              ]),
                              _vm._v(" "),
                              _c("td", [
                                _c(
                                  "a",
                                  {
                                    staticStyle: {
                                      cursor: "pointer",
                                      color: "#E10000"
                                    },
                                    on: {
                                      click: function($event) {
                                        return _vm.removeElement(index)
                                      }
                                    }
                                  },
                                  [_c("b", [_vm._v("Remove")])]
                                )
                              ])
                            ])
                          }),
                          0
                        )
                      ]
                    )
                  ])
                ])
              ]
            )
          ])
        ])
      ]),
      _vm._v(" "),
      _vm.rangeBarcodeModal
        ? _c("rangeBarcode", {
            attrs: {
              form: _vm.form,
              rangeBarcodeModal: _vm.rangeBarcodeModal,
              rangeNumber: _vm.rangeNumber
            }
          })
        : _vm._e()
    ],
    1
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "card-header" }, [
      _c("h4", { staticClass: "m-0 font-weight-bold text-primary" }, [
        _vm._v("STOCK REQUEST DRAFTS EDIT")
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "input-group-prepend" }, [
      _c(
        "span",
        {
          staticClass: "input-group-text input-group-field",
          attrs: { id: "basic-addon1" }
        },
        [_vm._v("Request Date:")]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "input-group-prepend" }, [
      _c(
        "span",
        {
          staticClass: "input-group-text input-group-field",
          attrs: { id: "basic-addon1" }
        },
        [_vm._v("UOM:")]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "input-group-prepend" }, [
      _c(
        "span",
        {
          staticClass: "input-group-text input-group-field",
          attrs: { id: "basic-addon1" }
        },
        [_vm._v("UOM:")]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "input-group-prepend" }, [
      _c(
        "span",
        {
          staticClass: "input-group-text input-group-field",
          attrs: { id: "basic-addon1" }
        },
        [_vm._v("Material Code:")]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "input-group-prepend" }, [
      _c(
        "span",
        {
          staticClass: "input-group-text input-group-field",
          attrs: { id: "basic-addon1" }
        },
        [_vm._v("UOM:")]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "input-group-prepend" }, [
      _c(
        "span",
        {
          staticClass: "input-group-text input-group-field",
          attrs: { id: "basic-addon1" }
        },
        [_vm._v("Company:")]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "input-group-prepend" }, [
      _c(
        "span",
        {
          staticClass: "input-group-text input-group-field",
          attrs: { id: "basic-addon1" }
        },
        [_vm._v("Company:")]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "input-group-prepend" }, [
      _c(
        "span",
        {
          staticClass: "input-group-text input-group-field",
          attrs: { id: "basic-addon1" }
        },
        [_vm._v("Material Code:")]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "input-group-prepend" }, [
      _c(
        "span",
        {
          staticClass: "input-group-text input-group-field",
          attrs: { id: "basic-addon1" }
        },
        [_vm._v("Material Code:")]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "input-group-prepend" }, [
      _c(
        "span",
        {
          staticClass: "input-group-text input-group-field",
          attrs: { id: "basic-addon1" }
        },
        [_vm._v("Material Code:")]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "input-group-prepend" }, [
      _c(
        "span",
        {
          staticClass: "input-group-text input-group-field",
          attrs: { id: "basic-addon1" }
        },
        [_vm._v("Material Group:")]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "input-group-prepend" }, [
      _c(
        "span",
        {
          staticClass: "input-group-text input-group-field",
          attrs: { id: "basic-addon1" }
        },
        [_vm._v("Material Group:")]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "input-group-prepend" }, [
      _c(
        "span",
        {
          staticClass: "input-group-text input-group-field",
          attrs: { id: "basic-addon1" }
        },
        [_vm._v("Barcode:")]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "input-group-prepend" }, [
      _c(
        "span",
        {
          staticClass: "input-group-text input-group-field",
          attrs: { id: "basic-addon1" }
        },
        [_vm._v("Barcode:")]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "row form-group" }, [
      _c("div", { staticClass: "col-md-3" }, [
        _c(
          "button",
          { staticClass: "btn btn-success", attrs: { type: "submit" } },
          [
            _vm._v("Add Item  "),
            _c("i", { staticClass: "fas fa-plus-square fa-lg" })
          ]
        )
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "button",
      {
        staticClass: "btn btn-success",
        staticStyle: { margin: "0px 185px", top: "11px", position: "absolute" },
        attrs: { type: "submit" }
      },
      [
        _vm._v("\n                            CREATE  "),
        _c("i", {
          staticClass: "fas fa-cart-plus",
          staticStyle: { "font-size": "1.25rem" }
        })
      ]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("thead", [
      _c("tr", { staticClass: "bg-primary" }, [
        _c("th", { staticClass: "wrapAll", staticStyle: { color: "white" } }, [
          _vm._v("BARCODE")
        ]),
        _vm._v(" "),
        _c("th", { staticClass: "wrapAll", staticStyle: { color: "white" } }, [
          _vm._v("MATERIAL CODE")
        ]),
        _vm._v(" "),
        _c("th", { staticClass: "wrapAll", staticStyle: { color: "white" } }, [
          _vm._v("MATERIAL DESCRIPTION")
        ]),
        _vm._v(" "),
        _c("th", { staticClass: "wrapAll", staticStyle: { color: "white" } }, [
          _vm._v("UOM")
        ]),
        _vm._v(" "),
        _c("th", { staticClass: "wrapAll", staticStyle: { color: "white" } }, [
          _vm._v("LAST FILL-UP DATE")
        ]),
        _vm._v(" "),
        _c("th", { staticClass: "wrapAll", staticStyle: { color: "white" } }, [
          _vm._v("LAST FILL-UP QTY")
        ]),
        _vm._v(" "),
        _c("th", { staticClass: "wrapAll", staticStyle: { color: "white" } }, [
          _vm._v("STOCKS LEFT")
        ]),
        _vm._v(" "),
        _c("th", { staticClass: "wrapAll", staticStyle: { color: "white" } }, [
          _vm._v("SUGGESTED ORDER QTY")
        ]),
        _vm._v(" "),
        _c("th", { staticClass: "wrapAll", staticStyle: { color: "white" } }, [
          _vm._v("APPROVED QTY")
        ]),
        _vm._v(" "),
        _c("th", { staticClass: "wrapAll", staticStyle: { color: "white" } }, [
          _vm._v("ACTIONS")
        ])
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/components/drafts/view.vue":
/*!*************************************************!*\
  !*** ./resources/js/components/drafts/view.vue ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _view_vue_vue_type_template_id_0d350cb2_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./view.vue?vue&type=template&id=0d350cb2&scoped=true& */ "./resources/js/components/drafts/view.vue?vue&type=template&id=0d350cb2&scoped=true&");
/* harmony import */ var _view_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./view.vue?vue&type=script&lang=js& */ "./resources/js/components/drafts/view.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _view_vue_vue_type_style_index_0_id_0d350cb2_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./view.vue?vue&type=style&index=0&id=0d350cb2&scoped=true&lang=css& */ "./resources/js/components/drafts/view.vue?vue&type=style&index=0&id=0d350cb2&scoped=true&lang=css&");
/* harmony import */ var _view_vue_vue_type_style_index_1_lang_css___WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./view.vue?vue&type=style&index=1&lang=css& */ "./resources/js/components/drafts/view.vue?vue&type=style&index=1&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");







/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_4__["default"])(
  _view_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _view_vue_vue_type_template_id_0d350cb2_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _view_vue_vue_type_template_id_0d350cb2_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "0d350cb2",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/drafts/view.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/drafts/view.vue?vue&type=script&lang=js&":
/*!**************************************************************************!*\
  !*** ./resources/js/components/drafts/view.vue?vue&type=script&lang=js& ***!
  \**************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_view_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./view.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/drafts/view.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_view_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/drafts/view.vue?vue&type=style&index=0&id=0d350cb2&scoped=true&lang=css&":
/*!**********************************************************************************************************!*\
  !*** ./resources/js/components/drafts/view.vue?vue&type=style&index=0&id=0d350cb2&scoped=true&lang=css& ***!
  \**********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_view_vue_vue_type_style_index_0_id_0d350cb2_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/style-loader!../../../../node_modules/css-loader??ref--6-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--6-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./view.vue?vue&type=style&index=0&id=0d350cb2&scoped=true&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/drafts/view.vue?vue&type=style&index=0&id=0d350cb2&scoped=true&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_view_vue_vue_type_style_index_0_id_0d350cb2_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_view_vue_vue_type_style_index_0_id_0d350cb2_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_view_vue_vue_type_style_index_0_id_0d350cb2_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_view_vue_vue_type_style_index_0_id_0d350cb2_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ "./resources/js/components/drafts/view.vue?vue&type=style&index=1&lang=css&":
/*!**********************************************************************************!*\
  !*** ./resources/js/components/drafts/view.vue?vue&type=style&index=1&lang=css& ***!
  \**********************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_view_vue_vue_type_style_index_1_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/style-loader!../../../../node_modules/css-loader??ref--6-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--6-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./view.vue?vue&type=style&index=1&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/drafts/view.vue?vue&type=style&index=1&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_view_vue_vue_type_style_index_1_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_view_vue_vue_type_style_index_1_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_view_vue_vue_type_style_index_1_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_view_vue_vue_type_style_index_1_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ "./resources/js/components/drafts/view.vue?vue&type=template&id=0d350cb2&scoped=true&":
/*!********************************************************************************************!*\
  !*** ./resources/js/components/drafts/view.vue?vue&type=template&id=0d350cb2&scoped=true& ***!
  \********************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_view_vue_vue_type_template_id_0d350cb2_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./view.vue?vue&type=template&id=0d350cb2&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/drafts/view.vue?vue&type=template&id=0d350cb2&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_view_vue_vue_type_template_id_0d350cb2_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_view_vue_vue_type_template_id_0d350cb2_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);