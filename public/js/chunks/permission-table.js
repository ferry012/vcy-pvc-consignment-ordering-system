(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["permission-table"],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/permission/PermissionsTable.vue?vue&type=script&lang=js&":
/*!**************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/permission/PermissionsTable.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  name: "PermissionsTable",
  data: function data() {
    return {
      permissions: {},
      currentSort: 'created_at',
      currentSortDir: 'desc',
      search: '',
      isSearch: false,
      perPage: ['10', '50', '100'],
      currentPage: '10',
      page: {},
      sortClassActive: {
        'activeDesc': false,
        'activeAsc': true
      },
      meta: {}
    };
  },
  created: function created() {
    this.index();
  },
  methods: {
    index: function index(page) {
      var _this = this;

      if (typeof page === 'undefined') {
        this.page = 1;
      }

      var uri = '/api/permissions?page=' + page + '&search=' + this.search + '&sortby=' + this.currentSort + '&sortdir=' + this.currentSortDir + '&currentpage=' + this.currentPage;
      this.uri = uri;
      this.page = page;
      axios.get(uri).then(function (response) {
        _this.permissions = response.data;
        _this.meta.total = response.data.meta.total;
        _this.meta.from = response.data.meta.from;
        _this.meta.to = response.data.meta.to;
      });
    },
    sort: function sort(s) {
      if (s === this.currentSort) {
        this.currentSortDir = this.currentSortDir === 'asc' ? 'desc' : 'asc';

        if (this.currentSortDir === 'asc') {
          this.sortClassActive.activeAsc = true;
          this.sortClassActive.activeDesc = false;
        } else {
          this.sortClassActive.activeAsc = false;
          this.sortClassActive.activeDesc = true;
        }
      }

      this.currentSort = s;
      this.index();
    },
    selectPageNumber: function selectPageNumber(selected) {
      this.currentPage = selected;
      this.index();
    },
    remove: function remove(e, index) {
      var _this2 = this;

      this.$swal({
        title: 'Are you sure to delete this permission?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
      }).then(function (result) {
        if (result.value) {
          var uri = "/api/permissions/".concat(e);
          axios["delete"](uri).then(function (response) {
            _this2.$swal('Deleted!', 'Your file has been deleted.', 'success');

            _this2.permissions.data.splice(index, 1);
          });
        }
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/permission/PermissionsTable.vue?vue&type=style&index=0&id=1318f60f&scoped=true&lang=css&":
/*!*********************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/permission/PermissionsTable.vue?vue&type=style&index=0&id=1318f60f&scoped=true&lang=css& ***!
  \*********************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.filter-asc[data-v-1318f60f] {\n    border-bottom: 5px solid #ccc;\n    margin-bottom: 1px\n}\n.filter-asc[data-v-1318f60f], .filter-desc[data-v-1318f60f] {\n    width: 0;\n    height: 0;\n    border-left: 5px solid transparent;\n    border-right: 5px solid transparent\n}\n.filter-desc[data-v-1318f60f] {\n    border-top: 5px solid #ccc;\n    margin-top: 1px\n}\n.active-filter-asc[data-v-1318f60f] {\n    border-bottom: 5px solid #a3a3a3\n}\n.active-filter-desc[data-v-1318f60f] {\n    border-top: 5px solid #a3a3a3\n}\n.inline-block[data-v-1318f60f] {\n    display: inline-block\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/permission/PermissionsTable.vue?vue&type=style&index=0&id=1318f60f&scoped=true&lang=css&":
/*!*************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/permission/PermissionsTable.vue?vue&type=style&index=0&id=1318f60f&scoped=true&lang=css& ***!
  \*************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../node_modules/css-loader??ref--6-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--6-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./PermissionsTable.vue?vue&type=style&index=0&id=1318f60f&scoped=true&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/permission/PermissionsTable.vue?vue&type=style&index=0&id=1318f60f&scoped=true&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/permission/PermissionsTable.vue?vue&type=template&id=1318f60f&scoped=true&":
/*!******************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/permission/PermissionsTable.vue?vue&type=template&id=1318f60f&scoped=true& ***!
  \******************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "card shadow mb-4" }, [
    _vm._m(0),
    _vm._v(" "),
    _c("div", { staticClass: "card-body" }, [
      _c(
        "p",
        { staticClass: "mb-4" },
        [
          _vm.$can("permission_create")
            ? _c(
                "router-link",
                {
                  staticClass: "btn btn-success",
                  attrs: { to: { name: "permission-create" } }
                },
                [_vm._v("Create Permission\n            ")]
              )
            : _vm._e()
        ],
        1
      ),
      _vm._v(" "),
      _c("hr"),
      _vm._v(" "),
      _c("div", { staticClass: "row form-group" }, [
        _c("div", { staticClass: "col-md-1" }, [
          _c("label", [_vm._v("Per Page")]),
          _vm._v(" "),
          _c(
            "select",
            {
              directives: [
                {
                  name: "model",
                  rawName: "v-model",
                  value: _vm.currentPage,
                  expression: "currentPage"
                }
              ],
              staticClass: "form-control",
              on: {
                change: [
                  function($event) {
                    var $$selectedVal = Array.prototype.filter
                      .call($event.target.options, function(o) {
                        return o.selected
                      })
                      .map(function(o) {
                        var val = "_value" in o ? o._value : o.value
                        return val
                      })
                    _vm.currentPage = $event.target.multiple
                      ? $$selectedVal
                      : $$selectedVal[0]
                  },
                  function($event) {
                    return _vm.selectPageNumber(_vm.currentPage)
                  }
                ]
              }
            },
            _vm._l(_vm.perPage, function(perPage) {
              return _c(
                "option",
                {
                  domProps: {
                    value: perPage,
                    selected: _vm.currentPage === perPage
                  }
                },
                [_vm._v(_vm._s(perPage) + "\n                    ")]
              )
            }),
            0
          )
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "col-md-8" }),
        _vm._v(" "),
        _c("div", { staticClass: "col-md-3" }, [
          _c(
            "div",
            {
              staticClass: "input-group",
              staticStyle: { "margin-top": "30px" }
            },
            [
              _vm._m(1),
              _vm._v(" "),
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.search,
                    expression: "search"
                  }
                ],
                staticClass: "form-control",
                attrs: { placeholder: "Search here" },
                domProps: { value: _vm.search },
                on: {
                  keyup: _vm.index,
                  click: function($event) {
                    _vm.isSearch = true
                  },
                  input: function($event) {
                    if ($event.target.composing) {
                      return
                    }
                    _vm.search = $event.target.value
                  }
                }
              })
            ]
          )
        ])
      ]),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "table-responsive" },
        [
          _c(
            "table",
            {
              staticClass: "table table-bordered",
              attrs: { id: "dataTable", width: "100%", cellspacing: "0" }
            },
            [
              _c("thead", [
                _c("tr", [
                  _c(
                    "th",
                    {
                      staticStyle: {
                        "background-color": "#007bff",
                        color: "white"
                      },
                      on: {
                        click: function($event) {
                          return _vm.sort("name")
                        }
                      }
                    },
                    [
                      _c("div", { staticClass: "inline-block" }, [
                        _c("div", {
                          staticClass: "filter-asc",
                          class: {
                            "active-filter-asc": _vm.sortClassActive.activeAsc
                          }
                        }),
                        _vm._v(" "),
                        _c("div", {
                          staticClass: "filter-desc",
                          class: {
                            "active-filter-desc": _vm.sortClassActive.activeDesc
                          }
                        })
                      ]),
                      _vm._v(
                        "\n                        Name\n                    "
                      )
                    ]
                  ),
                  _vm._v(" "),
                  _c(
                    "th",
                    {
                      staticStyle: {
                        "background-color": "#007bff",
                        color: "white"
                      },
                      on: {
                        click: function($event) {
                          return _vm.sort("created_at")
                        }
                      }
                    },
                    [
                      _c("div", { staticClass: "inline-block" }, [
                        _c("div", {
                          staticClass: "filter-asc",
                          class: {
                            "active-filter-asc": _vm.sortClassActive.activeAsc
                          }
                        }),
                        _vm._v(" "),
                        _c("div", {
                          staticClass: "filter-desc",
                          class: {
                            "active-filter-desc": _vm.sortClassActive.activeDesc
                          }
                        })
                      ]),
                      _vm._v(
                        "\n                        Created at\n                    "
                      )
                    ]
                  ),
                  _vm._v(" "),
                  _c(
                    "th",
                    {
                      staticStyle: {
                        "background-color": "#007bff",
                        color: "white"
                      }
                    },
                    [_vm._v("Options")]
                  )
                ])
              ]),
              _vm._v(" "),
              _c(
                "tbody",
                [
                  _vm._l(_vm.permissions.data, function(permission, index) {
                    return _c(
                      "tr",
                      { key: permission.id, attrs: { index: index } },
                      [
                        _c("td", [_vm._v(_vm._s(permission.name))]),
                        _vm._v(" "),
                        _c("td", [
                          _vm._v(
                            _vm._s(
                              _vm._f("moment")(
                                permission.created_at,
                                "MM/DD/YYYY"
                              )
                            )
                          )
                        ]),
                        _vm._v(" "),
                        _c(
                          "td",
                          [
                            _vm.$can("permission_edit")
                              ? _c(
                                  "router-link",
                                  {
                                    staticClass: "btn btn-success btn-sm",
                                    attrs: {
                                      to:
                                        "/permissions/" +
                                        permission.id +
                                        "/edit"
                                    }
                                  },
                                  [_vm._v("Edit\n                        ")]
                                )
                              : _vm._e(),
                            _vm._v(" "),
                            _vm.$can("permission_delete")
                              ? _c(
                                  "button",
                                  {
                                    staticClass: "btn btn-danger btn-sm",
                                    on: {
                                      click: function($event) {
                                        return _vm.remove(permission.id, index)
                                      }
                                    }
                                  },
                                  [_vm._v("Delete\n                        ")]
                                )
                              : _vm._e()
                          ],
                          1
                        )
                      ]
                    )
                  }),
                  _vm._v(" "),
                  _vm.permissions.data && _vm.permissions.data.length === 0
                    ? _c("tr", [
                        _c("td", { attrs: { colspan: "3" } }, [
                          _vm._v("No Data")
                        ])
                      ])
                    : _vm._e()
                ],
                2
              )
            ]
          ),
          _vm._v(" "),
          _c("p", { staticClass: "float-left" }, [
            _vm._v(
              "Showing " +
                _vm._s(_vm.meta.from || 0) +
                " to " +
                _vm._s(_vm.meta.to || 0) +
                " of " +
                _vm._s(_vm.meta.total || 0) +
                " entries"
            )
          ]),
          _vm._v(" "),
          _c("pagination", {
            staticClass: "float-right",
            attrs: { data: _vm.permissions, limit: 5 },
            on: { "pagination-change-page": _vm.index }
          })
        ],
        1
      )
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "card-header py-3" }, [
      _c("h6", { staticClass: "m-0 font-weight-bold text-primary" }, [
        _vm._v("Table")
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "input-group-prepend" }, [
      _c(
        "span",
        {
          staticClass: "input-group-text input-group-field",
          attrs: { id: "basic-addon1" }
        },
        [_vm._v("Search")]
      )
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/components/permission/PermissionsTable.vue":
/*!*****************************************************************!*\
  !*** ./resources/js/components/permission/PermissionsTable.vue ***!
  \*****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _PermissionsTable_vue_vue_type_template_id_1318f60f_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./PermissionsTable.vue?vue&type=template&id=1318f60f&scoped=true& */ "./resources/js/components/permission/PermissionsTable.vue?vue&type=template&id=1318f60f&scoped=true&");
/* harmony import */ var _PermissionsTable_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./PermissionsTable.vue?vue&type=script&lang=js& */ "./resources/js/components/permission/PermissionsTable.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _PermissionsTable_vue_vue_type_style_index_0_id_1318f60f_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./PermissionsTable.vue?vue&type=style&index=0&id=1318f60f&scoped=true&lang=css& */ "./resources/js/components/permission/PermissionsTable.vue?vue&type=style&index=0&id=1318f60f&scoped=true&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _PermissionsTable_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _PermissionsTable_vue_vue_type_template_id_1318f60f_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _PermissionsTable_vue_vue_type_template_id_1318f60f_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "1318f60f",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/permission/PermissionsTable.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/permission/PermissionsTable.vue?vue&type=script&lang=js&":
/*!******************************************************************************************!*\
  !*** ./resources/js/components/permission/PermissionsTable.vue?vue&type=script&lang=js& ***!
  \******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_PermissionsTable_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./PermissionsTable.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/permission/PermissionsTable.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_PermissionsTable_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/permission/PermissionsTable.vue?vue&type=style&index=0&id=1318f60f&scoped=true&lang=css&":
/*!**************************************************************************************************************************!*\
  !*** ./resources/js/components/permission/PermissionsTable.vue?vue&type=style&index=0&id=1318f60f&scoped=true&lang=css& ***!
  \**************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_PermissionsTable_vue_vue_type_style_index_0_id_1318f60f_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/style-loader!../../../../node_modules/css-loader??ref--6-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--6-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./PermissionsTable.vue?vue&type=style&index=0&id=1318f60f&scoped=true&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/permission/PermissionsTable.vue?vue&type=style&index=0&id=1318f60f&scoped=true&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_PermissionsTable_vue_vue_type_style_index_0_id_1318f60f_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_PermissionsTable_vue_vue_type_style_index_0_id_1318f60f_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_PermissionsTable_vue_vue_type_style_index_0_id_1318f60f_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_PermissionsTable_vue_vue_type_style_index_0_id_1318f60f_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ "./resources/js/components/permission/PermissionsTable.vue?vue&type=template&id=1318f60f&scoped=true&":
/*!************************************************************************************************************!*\
  !*** ./resources/js/components/permission/PermissionsTable.vue?vue&type=template&id=1318f60f&scoped=true& ***!
  \************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_PermissionsTable_vue_vue_type_template_id_1318f60f_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./PermissionsTable.vue?vue&type=template&id=1318f60f&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/permission/PermissionsTable.vue?vue&type=template&id=1318f60f&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_PermissionsTable_vue_vue_type_template_id_1318f60f_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_PermissionsTable_vue_vue_type_template_id_1318f60f_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);