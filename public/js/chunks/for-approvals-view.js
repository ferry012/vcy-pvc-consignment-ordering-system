(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["for-approvals-view"],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/forApprovals/view.vue?vue&type=script&lang=js&":
/*!****************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/forApprovals/view.vue?vue&type=script&lang=js& ***!
  \****************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _trevoreyre_autocomplete_vue_dist_style_css__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @trevoreyre/autocomplete-vue/dist/style.css */ "./node_modules/@trevoreyre/autocomplete-vue/dist/style.css");
/* harmony import */ var _trevoreyre_autocomplete_vue_dist_style_css__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_trevoreyre_autocomplete_vue_dist_style_css__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var fuse_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! fuse.js */ "./node_modules/fuse.js/dist/fuse.esm.js");
/* harmony import */ var jspdf__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! jspdf */ "./node_modules/jspdf/dist/jspdf.es.min.js");
/* harmony import */ var jspdf_autotable__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! jspdf-autotable */ "./node_modules/jspdf-autotable/dist/jspdf.plugin.autotable.js");
/* harmony import */ var jspdf_autotable__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(jspdf_autotable__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _rangeModals_RangeBarcode__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../rangeModals/RangeBarcode */ "./resources/js/components/rangeModals/RangeBarcode.vue");


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//TODO IMPORTS






/* harmony default export */ __webpack_exports__["default"] = ({
  name: "viewApproval",
  components: {
    rangeBarcode: _rangeModals_RangeBarcode__WEBPACK_IMPORTED_MODULE_6__["default"]
  },
  data: function data() {
    return {
      form: {
        req_date: '',
        cust_company: null,
        mat_group: null,
        mat_code: null,
        barcode: null,
        mat_des: null,
        uom: null,
        KWMENG: null,
        ERDAT: null,
        LABST: null
      },
      perPage: ['10', '50', '100'],
      currentPage: '10',
      sortClassActive: {
        'activeDesc': false,
        'activeAsc': true
      },
      meta: {},
      page: {},
      errors: {},
      currentSort: 'created_at',
      currentSortDir: 'desc',
      multiForm: {
        sBarcode: '',
        sMat_code: '',
        sMat_des: '',
        sUom: '',
        sLastFillDate: '',
        sLastFillQty: '',
        sStocksLeft: '',
        sSuggestQty: '',
        sApproveQty: '',
        sBoxNumber: ''
      },
      stocks: [],
      matGroup: [],
      matCode: [],
      matBarcode: [],
      multiBarcode: [],
      sales_unit: [],
      dateTime: '',
      company: [],
      trans_stocks: [],
      trans_header: [],
      drafts: {},
      headerCompany: '',
      headerReqDate: '',
      ReqAppQty: false,
      ReqBoxNo: false,
      arrRange: [],
      rangeBarcodeModal: false,
      rangeNumber: null,
      disApproveQty: true,
      print_release: null,
      print_delivery: null,
      printTrans: [],
      windowWidth: window.innerWidth,
      resWidth: null,
      is1280: false,
      is1422: false,
      is1680: false,
      is1920: false
    };
  },
  watch: {
    'form.mat_group': function formMat_group(newVal, oldVal) {
      if (newVal !== null) {
        this.getBarCode(newVal); // this.multiGetBarCode(newVal);
        // this.getMatCode(newVal);
      }

      if (newVal === null) {
        this.form.mat_code = '';
        this.form.barcode = '';
        this.matCode = []; // this.matBarcode = [];

        this.getAllBarCode();
      }
    },
    'form.barcode': function formBarcode(newVal, oldVal) {
      this.getMatCode(newVal);

      if (newVal !== oldVal) {
        this.form.mat_code = null;
        this.form.uom = null;
        this.sales_unit = [];
      }

      this.arrRange.push({
        'barcode': newVal
      });
      var barcode = [];
      this.arrRange.forEach(function (obj) {
        barcode.push(obj.barcode = newVal);
      });
      console.log(barcode); // console.log(req_date,'test');

      this.arrRange.push({
        'barcode': barcode
      });
    },
    'form.cust_company': function formCust_company(newVal, oldVal) {
      this.getMatCode();
      this.trans_header[0].cust_company = newVal;
    },
    'form.req_date': function formReq_date(newVal, oldVal) {
      this.trans_header[0].req_date = newVal;
    },
    'stocks': function stocks(newVal, oldVal) {
      var _this = this;

      newVal.map(function (value) {
        // console.log(value.approve_qty);
        if (value.approve_qty === null) {
          //if approve qty will be required
          _this.ReqAppQty = true;
        }
      });
    },
    'rangeNumber': function rangeNumber(newVal, oldVal) {
      var range = [];
      this.arrRange.forEach(function (obj) {
        range.push(obj.range = newVal);
      }); // console.log(req_date,'test');

      this.arrRange.push({
        'range': range
      });
    },
    windowWidth: function windowWidth(newWidth, oldWidth) {
      this.resWidth = "it changed to ".concat(newWidth, " from ").concat(oldWidth);
      this.checkWidthToAdjust(newWidth);
    },
    deep: true
  },
  computed: {
    formatDate: function formatDate() {
      // var formatDate = new Date(this.form.req_date);
      // return formatDate;
      var moment = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js"); // require


      moment().format();
      var today = moment();
      this.dateTime = today.format('YYYY-MM-DD');
      this.form.req_date = this.dateTime;
      return this.dateTime;
    },
    currentUser: function currentUser() {
      return this.$store.getters.currentUser;
    }
  },
  mounted: function mounted() {
    var _this2 = this;

    window.addEventListener('resize', function () {
      _this2.windowWidth = window.innerWidth;
    });
  },
  created: function created() {
    Reload.$on('AfterCreated', function () {});
    this.getMatGroup();
    this.getBarCode();
    this.getCustCompany();
    this.getDraftsDetails();
    this.getDraftsHeader();
    this.checkedIfApprovers();
    this.getAllBarCode();
    this.showCurrentPrintingNo();
    this.checkWidthToAdjust();
  },
  methods: {
    checkWidthToAdjust: function checkWidthToAdjust(newWidth) {
      console.log(newWidth, this.windowWidth);

      if (newWidth === 1680 || this.windowWidth === 1680) {
        this.is1680 = true;
        this.is1920 = false;
      } else if (newWidth === 1920 || this.windowWidth === 1920) {
        this.is1920 = true;
        this.is1680 = false;
      }
    },
    showCurrentPrintingNo: function showCurrentPrintingNo() {
      var _this3 = this;

      axios.get('/api/for-printing-all?printID=' + this.$route.params.id).then(function (response) {
        _this3.print_release = response.data.RELEASE;
        _this3.print_delivery = response.data.DELIVERY; // console.log(response);
        // if(response.data.DELIVERY === null && response.data.RELEASE === null){
        //     this.print_release = '000001';
        //     this.print_delivery = '000001';
        // }else{
        //
        // }
      });
    },
    checkedIfApprovers: function checkedIfApprovers() {
      var _this4 = this;

      axios.get('/api/check-user-view').then(function (response) {
        if (response.data.check === 1) {
          _this4.disApproveQty = false;
        } else if (response.data.check === 0) {
          _this4.disApproveQty = true;
        }
      });
    },
    addItem: function addItem() {
      var _this5 = this;

      console.log(this.stocks);
      var dup = '';
      var good = '';

      if (this.stocks.length !== 0) {
        this.stocks.map(function (value) {
          console.log(value.barcode);

          if (value.barcode === _this5.form.barcode) {
            console.log('check12');
            _this5.form.barcode = null;
            _this5.form.mat_code = null;
            _this5.form.uom = null;
            dup = 0;
          } else if (value.barcode !== _this5.form.barcode) {
            console.log('good123');
            good = 1; //good
          }
        });

        if (good === 1 && dup !== 0) {
          var elem = document.createElement('tr');
          this.stocks.push({
            'req_date': this.form.req_date,
            'cust_company': this.form.cust_company,
            'mat_group': this.form.mat_group,
            'mat_code': this.form.mat_code,
            'mat_des': this.form.mat_des,
            'sales_unit': this.form.uom,
            'barcode': this.form.barcode,
            'ERDAT': this.form.ERDAT,
            'KWMENG': this.form.KWMENG,
            'stocks_left': this.multiForm.sStocksLeft,
            'order_qty': this.multiForm.sSuggestQty,
            'approve_qty': this.multiForm.sApproveQty,
            'stockDate': this.dateTime,
            'box_no': this.multiForm.sBoxNumber,
            'LABST': this.form.LABST
          });
          this.form.barcode = null;
          this.form.mat_code = null;
          this.form.uom = null;
        } else if (dup === 0) {
          this.alertSwal('Duplicate Data Found', 'Please remove first to continue', 'warning', '');
        }
      } else {
        var elem = document.createElement('tr');
        this.stocks.push({
          'req_date': this.form.req_date,
          'cust_company': this.form.cust_company,
          'mat_group': this.form.mat_group,
          'mat_code': this.form.mat_code,
          'mat_des': this.form.mat_des,
          'sales_unit': this.form.uom,
          'barcode': this.form.barcode,
          'ERDAT': this.form.ERDAT,
          'KWMENG': this.form.KWMENG,
          'stocks_left': this.multiForm.sStocksLeft,
          'order_qty': this.multiForm.sSuggestQty,
          'approve_qty': this.multiForm.sApproveQty,
          'stockDate': this.dateTime,
          'box_no': this.multiForm.sBoxNumber,
          'LABST': this.form.LABST
        });
        this.form.barcode = null;
        this.form.mat_code = null;
        this.form.uom = null;
      }
    },
    printReleaseForm: function printReleaseForm() {
      this.trans_stocks = [];
      this.trans_stocks.push({
        'stocks': this.stocks,
        'header': this.trans_header
      });
      var today = new Date();
      var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
      var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
      var dateTime = date + ' ' + time;
      var newFormat = moment__WEBPACK_IMPORTED_MODULE_2___default()(date, "YYYY/M/D").format('LL'); // console.log(this.trans_stocks);

      var vm = this; //Format
      // for orientation p h x w
      // for orientation l w x h

      var doc = new jspdf__WEBPACK_IMPORTED_MODULE_4__["jsPDF"]('l', 'pt', [577, 396]); // or 215.9 by 139.7 mm
      // 10.795 width , 27.94 length
      // 13.97 x 20.3553

      var dataArr = [];
      $.each(this.trans_stocks[0].stocks, function (key, value) {
        dataArr.push([value.approve_qty, value.sales_unit, value.barcode, value.mat_des, value.box_no]);
      });
      var yPlus = 5;
      doc.setFontSize(15);
      doc.text("VCY PVC Consignment", 215, 25);
      yPlus = yPlus - 5;
      doc.setFontSize(12);
      doc.text("Release Form", 255, yPlus + 45);
      doc.setFontSize(10);
      doc.text("Form Number:" + " " + this.print_release, 10, yPlus + 70);
      doc.setFontSize(10);
      doc.text("Consignee:" + ' ' + this.trans_stocks[0].header[0].cust_company, 260, yPlus + 70);
      doc.setFontSize(10);
      doc.text("Date:" + ' ' + newFormat, 10, yPlus + 90);
      doc.setFontSize(10);
      doc.text("Release form by:" + ' ' + this.currentUser.name, 260, yPlus + 90);
      doc.autoTable({
        headerStyles: {
          lineWidth: 1,
          lineColor: [0, 0, 0]
        },
        pageBreak: 'auto',
        rowPageBreak: 'auto',
        startY: yPlus + 100,
        theme: 'plain',
        cellWidth: 'wrap',
        margin: {
          right: 10,
          left: 10
        },
        columnStyles: {
          // 3: {halign: 'right', margin: {right: 20}},
          // 4: {halign: 'right', margin: {right: 20}},
          // 5: {halign: 'right', margin: {right: 20}},
          // 6: {halign: 'right', margin: {right: 20}},
          // 7: {halign: 'right', margin: {right: 20}},
          0: {
            fontSize: 8
          },
          1: {
            fontSize: 8
          },
          2: {
            fontSize: 8
          },
          3: {
            fontSize: 8
          },
          4: {
            fontSize: 8
          }
        },
        // margin: {top: 729},
        head: [[{
          content: 'QTY',
          styles: {
            cellWidth: 'wrap'
          }
        }, {
          content: 'UOM',
          styles: {
            cellWidth: 'wrap'
          }
        }, {
          content: 'BARCODE',
          styles: {
            cellWidth: 'wrap'
          }
        }, {
          content: "MATERIAL DESCRIPTION",
          styles: {
            cellWidth: 'wrap'
          }
        }, {
          content: 'BOX NUMBER',
          styles: {
            cellWidth: 'wrap'
          }
        }]],
        body: dataArr
      });

      if (dataArr.length >= 13) {
        var finalYZCSH = doc.lastAutoTable.finalY;
        finalYZCSH = finalYZCSH + 5;
        doc.setFontSize(10);
        doc.text("________________________________", 80, finalYZCSH + 60);
        doc.text("Item Preparer", 135, finalYZCSH + 75);
        doc.text("________________________________", 320, finalYZCSH + 60);
        doc.text("Item Checker", 385, finalYZCSH + 75);
      }

      if (dataArr.length <= 12) {
        var _finalYZCSH = doc.lastAutoTable.finalY;
        _finalYZCSH = _finalYZCSH + 5;
        doc.setFontSize(10);
        doc.text("________________________________", 80, _finalYZCSH + 15);
        doc.text("Item Preparer", 135, _finalYZCSH + 30);
        doc.text("________________________________", 320, _finalYZCSH + 15);
        doc.text("Item Checker", 385, _finalYZCSH + 30);
      } //PAGE NUMBERING
      //Add Page Number at bottom-rigt
      //Get the number of Pages


      var pageCount = doc.internal.getNumberOfPages(); // console.log(pageCount,'page');

      for (var i = 1; i <= pageCount; i++) {
        //Go to page i
        doc.setPage(i); //Print Page 1 of 2 for Example

        doc.text('Page ' + String(i) + ' of ' + String(pageCount), 540, 375, null, null, "right");
      } // doc.autoPrint({variant:'non-conform'})


      window.open(doc.output('bloburl'), '_blank');
      Notification.printPreviewRelease();
    },
    printDeliveryForm: function printDeliveryForm() {
      this.trans_stocks = [];
      this.trans_stocks.push({
        'stocks': this.stocks,
        'header': this.trans_header
      });
      var today = new Date();
      var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
      var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
      var dateTime = date + ' ' + time;
      var newFormat = moment__WEBPACK_IMPORTED_MODULE_2___default()(date, "YYYY/M/D").format('LL'); // console.log(this.trans_stocks);

      var vm = this; //Format
      // for orientation p h x w
      // for orientation l w x h

      var doc = new jspdf__WEBPACK_IMPORTED_MODULE_4__["jsPDF"]('p', 'pt', 'letter');
      var dataArr = [];
      $.each(this.trans_stocks[0].stocks, function (key, value) {
        dataArr.push([value.approve_qty, value.sales_unit, value.barcode, value.mat_des, value.box_no]);
      }); //more than 28rows it should next the page

      var yPlus = 5;
      doc.setFontSize(15);
      doc.text("VCY PVC Consignment", 215, 25);
      yPlus = yPlus - 5;
      doc.setFontSize(12);
      doc.text("Delivery Receipt", 250, yPlus + 45);
      doc.setFontSize(10);
      doc.text("DR Number:" + " " + this.print_delivery, 10, yPlus + 70);
      doc.setFontSize(10);
      doc.text("Consignee:" + ' ' + this.trans_stocks[0].header[0].cust_company, 260, yPlus + 70);
      doc.setFontSize(10);
      doc.text("Date:" + ' ' + newFormat, 10, yPlus + 90); // doc.setFontSize(10);
      // doc.text("Release form by:"+' '+this.currentUser.name, 260, yPlus + 105);

      doc.autoTable({
        headerStyles: {
          lineWidth: 1,
          lineColor: [0, 0, 0]
        },
        pageBreak: 'auto',
        rowPageBreak: 'auto',
        startY: yPlus + 100,
        theme: 'plain',
        cellWidth: 'wrap',
        margin: {
          right: 10,
          left: 10
        },
        columnStyles: {
          // 3: {halign: 'right', margin: {right: 20}},
          // 4: {halign: 'right', margin: {right: 20}},
          // 5: {halign: 'right', margin: {right: 20}},
          // 6: {halign: 'right', margin: {right: 20}},
          // 7: {halign: 'right', margin: {right: 20}},
          0: {
            fontSize: 8
          },
          1: {
            fontSize: 8
          },
          2: {
            fontSize: 8
          },
          3: {
            fontSize: 8
          },
          4: {
            fontSize: 8
          },
          5: {
            fontSize: 8
          }
        },
        // margin: {top: 729},
        head: [[{
          content: 'QUANTITY',
          styles: {
            cellWidth: 'wrap'
          }
        }, {
          content: 'UOM',
          styles: {
            cellWidth: 'wrap'
          }
        }, {
          content: 'BARCODE',
          styles: {
            cellWidth: 'wrap'
          }
        }, {
          content: "MATERIAL DESCRIPTION",
          styles: {
            cellWidth: 'wrap'
          }
        }, {
          content: 'BOX NUMBER',
          styles: {
            cellWidth: 'wrap'
          }
        }]],
        body: dataArr
      }); // if(dataArr.length === 29){
      //     doc.addPage();
      //     doc.setFontSize(10);
      //
      //     // doc.text("Prepared by:", 10, finalYZCSH + 35);
      //     doc.text("Joaquin Pigar Jr", 45,   30);
      //     doc.text("__________________________", 10, 35);
      //     doc.text("Prepared by", 50,  50);
      //
      //     doc.text("RJ Abello/J Pigar", 270, 30);
      //     doc.text("__________________________", 235,  35);
      //     doc.text("Released by", 280,  50);
      //
      //     doc.text("__________________________", 455, 35);
      //     doc.text("Received by (Hablon)", 485,  50);
      //
      //     doc.text("Kara Katrina Caceres", 35, 95);
      //     doc.text("__________________________", 10,  100);
      //     doc.text("Approved by", 50, 115);
      //
      //     doc.text("__________________________", 235,  100);
      //     doc.text("Delivered by", 280,  115);
      //
      //     doc.text("__________________________", 455,  100);
      //     doc.text("Received by (Branch)", 485,  115);
      // }

      if (dataArr.length >= 29) {
        // console.log('testpdf');
        var finalYZCSH = doc.lastAutoTable.finalY;
        finalYZCSH = finalYZCSH + 5; //683
        // console.log(finalYZCSH); //85.7 new page

        if (finalYZCSH > 683) {
          w;
          doc.addPage(); // console.log('greaterthan');

          doc.setFontSize(10);
          doc.text("Joaquin Pigar Jr", 45, finalYZCSH - 653);
          doc.text("__________________________", 10, finalYZCSH - 648);
          doc.text("Prepared by", 50, finalYZCSH - 633);
          doc.text("RJ Abello/J Pigar", 270, finalYZCSH - 653);
          doc.text("__________________________", 235, finalYZCSH - 648);
          doc.text("Released by", 280, finalYZCSH - 633);
          doc.text("__________________________", 455, finalYZCSH - 648);
          doc.text("Received by (Hablon)", 485, finalYZCSH - 633);
          doc.text("Kara Katrina Caceres", 35, finalYZCSH - 588);
          doc.text("__________________________", 10, finalYZCSH - 583);
          doc.text("Approved by", 50, finalYZCSH - 568);
          doc.text("__________________________", 235, finalYZCSH - 583);
          doc.text("Delivered by", 280, finalYZCSH - 568);
          doc.text("__________________________", 455, finalYZCSH - 583);
          doc.text("Received by (Branch)", 485, finalYZCSH - 568);
        } else if (finalYZCSH < 683) {
          if (dataArr.length <= 63 && finalYZCSH < 680) {
            // console.log('lessthan');
            doc.setFontSize(10);
            doc.text("Joaquin Pigar Jr", 45, finalYZCSH + 30);
            doc.text("__________________________", 10, finalYZCSH + 35);
            doc.text("Prepared by", 50, finalYZCSH + 50);
            doc.text("RJ Abello/J Pigar", 270, finalYZCSH + 30);
            doc.text("__________________________", 235, finalYZCSH + 35);
            doc.text("Released by", 280, finalYZCSH + 50);
            doc.text("__________________________", 455, finalYZCSH + 35);
            doc.text("Received by (Hablon)", 485, finalYZCSH + 50);
            doc.text("Kara Katrina Caceres", 35, finalYZCSH + 95);
            doc.text("__________________________", 10, finalYZCSH + 100);
            doc.text("Approved by", 50, finalYZCSH + 115);
            doc.text("__________________________", 235, finalYZCSH + 100);
            doc.text("Delivered by", 280, finalYZCSH + 115);
            doc.text("__________________________", 455, finalYZCSH + 100);
            doc.text("Received by (Branch)", 485, finalYZCSH + 115);
          } else if (dataArr.length >= 64 && finalYZCSH > 680) {
            doc.addPage(); // console.log('greaterthan');

            doc.setFontSize(10);
            doc.text("Joaquin Pigar Jr", 45, finalYZCSH - 653);
            doc.text("__________________________", 10, finalYZCSH - 648);
            doc.text("Prepared by", 50, finalYZCSH - 633);
            doc.text("RJ Abello/J Pigar", 270, finalYZCSH - 653);
            doc.text("__________________________", 235, finalYZCSH - 648);
            doc.text("Released by", 280, finalYZCSH - 633);
            doc.text("__________________________", 455, finalYZCSH - 648);
            doc.text("Received by (Hablon)", 485, finalYZCSH - 633);
            doc.text("Kara Katrina Caceres", 35, finalYZCSH - 588);
            doc.text("__________________________", 10, finalYZCSH - 583);
            doc.text("Approved by", 50, finalYZCSH - 568);
            doc.text("__________________________", 235, finalYZCSH - 583);
            doc.text("Delivered by", 280, finalYZCSH - 568);
            doc.text("__________________________", 455, finalYZCSH - 583);
            doc.text("Received by (Branch)", 485, finalYZCSH - 568);
          } else if (dataArr.length >= 64 && finalYZCSH < 680) {
            // console.log('lessthan');
            doc.setFontSize(10);
            doc.text("Joaquin Pigar Jr", 45, finalYZCSH + 30);
            doc.text("__________________________", 10, finalYZCSH + 35);
            doc.text("Prepared by", 50, finalYZCSH + 50);
            doc.text("RJ Abello/J Pigar", 270, finalYZCSH + 30);
            doc.text("__________________________", 235, finalYZCSH + 35);
            doc.text("Released by", 280, finalYZCSH + 50);
            doc.text("__________________________", 455, finalYZCSH + 35);
            doc.text("Received by (Hablon)", 485, finalYZCSH + 50);
            doc.text("Kara Katrina Caceres", 35, finalYZCSH + 95);
            doc.text("__________________________", 10, finalYZCSH + 100);
            doc.text("Approved by", 50, finalYZCSH + 115);
            doc.text("__________________________", 235, finalYZCSH + 100);
            doc.text("Delivered by", 280, finalYZCSH + 115);
            doc.text("__________________________", 455, finalYZCSH + 100);
            doc.text("Received by (Branch)", 485, finalYZCSH + 115);
          }
        }
      }

      if (dataArr.length <= 28) {
        // console.log('testpdf123');
        var _finalYZCSH2 = doc.lastAutoTable.finalY;
        _finalYZCSH2 = _finalYZCSH2 + 5;
        doc.setFontSize(10); // doc.text("Prepared by:", 10, finalYZCSH + 35);

        doc.text("Joaquin Pigar Jr", 45, _finalYZCSH2 + 30);
        doc.text("__________________________", 10, _finalYZCSH2 + 35);
        doc.text("Prepared by", 50, _finalYZCSH2 + 50);
        doc.text("RJ Abello/J Pigar", 270, _finalYZCSH2 + 30);
        doc.text("__________________________", 235, _finalYZCSH2 + 35);
        doc.text("Released by", 280, _finalYZCSH2 + 50);
        doc.text("__________________________", 455, _finalYZCSH2 + 35);
        doc.text("Received by (Hablon)", 485, _finalYZCSH2 + 50);
        doc.text("Kara Katrina Caceres", 35, _finalYZCSH2 + 95);
        doc.text("__________________________", 10, _finalYZCSH2 + 100);
        doc.text("Approved by", 50, _finalYZCSH2 + 115);
        doc.text("__________________________", 235, _finalYZCSH2 + 100);
        doc.text("Delivered by", 280, _finalYZCSH2 + 115);
        doc.text("__________________________", 455, _finalYZCSH2 + 100);
        doc.text("Received by (Branch)", 485, _finalYZCSH2 + 115); // // doc.text("Approved by:", 320, finalYZCSH + 35);
        // doc.text("__________________________", 390, finalYZCSH + 35);
        // doc.text("Kara Katrina Caceres", 415, finalYZCSH + 50);
        //
        // // doc.text("Released by:", 10, finalYZCSH + 80);
        // doc.text("__________________________", 75, finalYZCSH + 80);
        // doc.text("RJ Abello/J Pigar", 110, finalYZCSH + 95);
        //
        // // doc.text("Delivered by:", 320, finalYZCSH + 80);
        // doc.text("__________________________", 390, finalYZCSH + 80);
        // doc.text("Name and Siganture", 415, finalYZCSH + 95);
        //
        // // doc.text("Received by:", 10, finalYZCSH + 125);
        // doc.text("__________________________", 75, finalYZCSH + 125);
        // doc.text("Name and Siganture (Hablon)", 82, finalYZCSH + 140);
        //
        // // doc.text("Received by:", 320, finalYZCSH + 125);
        // doc.text("__________________________", 390, finalYZCSH + 125);
        // doc.text("Name and Siganture (Branch)", 396, finalYZCSH + 140);
      } //PAGE NUMBERING
      //Add Page Number at bottom-rigt
      //Get the number of Pages


      var pageCount = doc.internal.getNumberOfPages(); // console.log(pageCount,'page');

      for (var i = 1; i <= pageCount; i++) {
        //Go to page i
        doc.setPage(i); //Print Page 1 of 2 for Example

        doc.text('Page ' + String(i) + ' of ' + String(pageCount), 570, 762, null, null, "right");
      }

      window.open(doc.output('bloburl'), '_blank');
      Notification.printPreviewDelivery();
    },
    approveRequest: function approveRequest() {
      var _this6 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                if (!(_this6.stocks.length !== 0)) {
                  _context.next = 5;
                  break;
                }

                _context.next = 3;
                return Swal.fire({
                  title: "Are you sure you want to approve Stock Request <br>No. ".concat(_this6.$route.params.id, "?"),
                  text: "You won't be able to revert this!",
                  icon: 'question',
                  showCancelButton: true,
                  confirmButtonColor: '#3085d6',
                  cancelButtonColor: '#d33',
                  confirmButtonText: 'Yes, Approve it!'
                }).then(function (result) {
                  if (result.value) {
                    _this6.trans_stocks.push({
                      'stocks': _this6.stocks,
                      'header': _this6.trans_header
                    });

                    axios.post("/api/approve/".concat(_this6.$route.params.id, "/request"), _this6.trans_stocks).then(function (response) {// this.printTrans.push({
                      //     'release' : this.print_release,
                      //     'delivery' :this.print_delivery,
                      // });
                      // axios.post(`/api/approve/${this.$route.params.id}/for-printing`, this.printTrans)
                      //     .then(res => {
                      //         // console.log(response.data);
                      //         // this.alertSwal(`Stock Request ${this.$route.params.id} has been Approved`, '', 'success', '3000')
                      //     });
                    });

                    _this6.$swal({
                      icon: 'success',
                      title: 'Approved!',
                      text: "Stock Request ".concat(_this6.$route.params.id, " has been approved."),
                      showConfirmButton: false,
                      timer: 3000
                    });

                    _this6.$router.push({
                      name: 'approval-table',
                      query: {
                        allRequest: 'DataTable',
                        id: _this6.currentUser.token
                      }
                    });
                  }
                });

              case 3:
                _context.next = 6;
                break;

              case 5:
                _this6.$swal({
                  icon: 'error',
                  title: "<span style=\"color:#dc3545\">ERROR!</span>",
                  text: "Can't APPROVE empty transactions!",
                  showConfirmButton: false,
                  timer: 3000
                });

              case 6:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }))();
    },
    rejectRequest: function rejectRequest() {
      var _this7 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2() {
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                if (!(_this7.stocks.length !== 0)) {
                  _context2.next = 5;
                  break;
                }

                _context2.next = 3;
                return Swal.fire({
                  title: "Are you sure you want to reject Stock Request <br>No. ".concat(_this7.$route.params.id, "?"),
                  text: "You won't be able to revert this!",
                  icon: 'warning',
                  showCancelButton: true,
                  confirmButtonColor: '#3085d6',
                  cancelButtonColor: '#d33',
                  confirmButtonText: 'Yes, Reject it!'
                }).then(function (result) {
                  if (result.value) {
                    _this7.trans_stocks.push({
                      'stocks': _this7.stocks,
                      'header': _this7.trans_header
                    });

                    axios.post("/api/rejected/".concat(_this7.$route.params.id, "/request"), _this7.trans_stocks).then(function (response) {});

                    _this7.$swal({
                      icon: 'success',
                      title: 'Rejected!',
                      text: "Stock Request ".concat(_this7.$route.params.id, " has been rejected."),
                      showConfirmButton: false,
                      timer: 3000
                    });

                    _this7.$router.push({
                      name: 'approval-table',
                      query: {
                        allRequest: 'DataTable',
                        id: _this7.currentUser.token
                      }
                    });
                  }
                });

              case 3:
                _context2.next = 6;
                break;

              case 5:
                _this7.$swal({
                  icon: 'error',
                  title: "<span style=\"color:#dc3545\">ERROR!</span>",
                  text: "Can't REJECT empty transactions!",
                  showConfirmButton: false,
                  timer: 3000
                });

              case 6:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2);
      }))();
    },
    getDraftsDetails: function getDraftsDetails(page) {
      var _this8 = this;

      if (typeof page === 'undefined') {
        this.page = 1;
      }

      var uri = "/api/approvals/".concat(this.$route.params.id, "/approval-details?page=") + page + "&search=" + this.search + "&sortby=" + this.currentSort + "&sortdir=" + this.currentSortDir + "&currentpage=" + this.currentPage;
      this.uri = uri;
      this.page = page;
      axios.get(uri).then(function (response) {
        // console.log(response.data.data);
        _this8.stocks = response.data.data;
      });
    },
    getDraftsHeader: function getDraftsHeader(page) {
      var _this9 = this;

      if (typeof page === 'undefined') {
        this.page = 1;
      }

      var uri = "/api/approvals/header/".concat(this.$route.params.id, "?page=") + page + "&search=" + this.search + "&sortby=" + this.currentSort + "&sortdir=" + this.currentSortDir + "&currentpage=" + this.currentPage;
      this.uri = uri;
      this.page = page;
      axios.get(uri).then(function (response) {
        _this9.form.req_date = response.data[0].stock_req_date;
        _this9.form.cust_company = response.data[0].company;

        _this9.trans_header.push({
          'req_date': _this9.form.req_date,
          'cust_company': _this9.form.cust_company
        });
      });
    },
    rangeMatCode: function rangeMatCode() {
      alert('Under Construction');
    },
    closeModal: function closeModal(e) {
      this.rangeBarcodeModal = e;
    },
    rangeBarcode: function rangeBarcode() {
      if (this.form.barcode === null || this.form.cust_company === null) {
        if (this.form.barcode === null) {
          this.alertSwal('No Barcode Found!', 'Please select a barcode to continue', 'error');
        } else if (this.form.cust_company === null) {
          this.alertSwal('No Company Found!', 'Please select a company to continue', 'error');
        }
      } else {
        this.rangeBarcodeModal = !this.rangeBarcodeModal;
      }
    },
    dataRangeBarcode: function dataRangeBarcode(num, close) {
      this.rangeNumber = num;
      this.rangeBarcodeModal = close;
      this.getRangeBarcode();
    },
    getRangeBarcode: function getRangeBarcode() {
      var _this10 = this;

      this.arrRange.push({
        'range': this.rangeNumber,
        'barcode': this.form.barcode
      });
      axios.post('/api/range-barcode-approval', this.arrRange).then(function (response) {
        // console.log(response.data,'testrange');
        if (_this10.stocks.length !== 0) {
          var dup = '';
          var good = '';

          _this10.stocks.map(function (value) {
            response.data.map(function (result) {
              // console.log(value.barcode,'value');
              // console.log(result.barcode,'result');
              if (value.barcode === result.barcode) {
                _this10.form.barcode = null;
                _this10.form.mat_code = null;
                _this10.form.uom = null;
                dup = 0; // console.log('sulodkadi123');
              } else if (value.barcode !== result.barcode) {
                //good
                // console.log('goodiesulod');
                good = 1;
              }
            });
          });

          if (good === 1 && dup !== 0) {
            // console.log('range success');
            _this10.stocks = response.data;

            _this10.alertSwal('Range Data Success', '', 'success', '');
          } else if (dup === 0) {
            // console.log('range duplicate');
            _this10.alertSwal('Duplicate Data Found', 'Please remove first to continue', 'warning', '');
          }
        } else {
          _this10.stocks = response.data;
        }
      });
    },
    removeElement: function removeElement(index) {
      this.stocks.splice(index, 1);
    },
    getCustCompany: function getCustCompany() {
      var _this11 = this;

      axios.get('/api/get-customer/stock-creation').then(function (response) {
        _this11.company = response.data;
      });
    },
    multiGetBarCode: function multiGetBarCode(newVal) {
      var _this12 = this;

      if (newVal !== null) {
        var formData = new FormData();
        formData.append('MatGroup', newVal);
        axios.post('/api/get-mat-barcode', formData).then(function (response) {
          _this12.multiBarcode = response.data;
        });
      } else {
        this.multiBarcode = [];
      }
    },
    getBarCode: function getBarCode(newVal) {
      var _this13 = this;

      if (newVal !== null) {
        var formData = new FormData();
        formData.append('MatGroup', newVal);
        axios.post('/api/get-mat-barcode', formData).then(function (response) {
          _this13.matBarcode = response.data;
        });
      } else {
        this.matBarcode = [];
      }
    },
    getAllBarCode: function getAllBarCode() {
      var _this14 = this;

      // get all barcode if mat group is null
      axios.get('/api/get-all-barcode').then(function (response) {
        _this14.matBarcode = response.data;
      });
    },
    getMatCode: function getMatCode(newVal) {
      var _this15 = this;

      if (this.form.barcode !== null && this.form.cust_company !== null) {
        axios.get('/api/get-mat-code?barcode=' + this.form.barcode + '&custCompany=' + this.form.cust_company).then(function (response) {
          _this15.form.mat_code = response.data.MATNR;
          _this15.form.mat_des = response.data.MAKTX;
          _this15.form.uom = response.data.MEINH;
          _this15.form.ERDAT = response.data.ERDAT; // last fill up date

          _this15.form.KWMENG = response.data.KWMENG; // last fill up date

          _this15.form.LABST = response.data.LABST;
        });
      } else {}
    },
    fuseSearch: function fuseSearch(options, search) {
      var fuse = new fuse_js__WEBPACK_IMPORTED_MODULE_3__["default"](options, {
        keys: ["WGBEZ"],
        shouldSort: true
      });
      return search.length ? fuse.search(search).map(function (_ref) {
        var item = _ref.item;
        return item;
      }) : fuse.list;
    },
    fuseSearchMatCode: function fuseSearchMatCode(options, search) {
      var fuse = new fuse_js__WEBPACK_IMPORTED_MODULE_3__["default"](options, {
        keys: ["MATNR", "MAKTX"],
        shouldSort: true
      });
      return search.length ? fuse.search(search).map(function (_ref2) {
        var item = _ref2.item;
        return item;
      }) : fuse.list;
    },
    fuseSearchCompany: function fuseSearchCompany(options, search) {
      var fuse = new fuse_js__WEBPACK_IMPORTED_MODULE_3__["default"](options, {
        keys: ["NAME1"],
        shouldSort: true
      });
      return search.length ? fuse.search(search).map(function (_ref3) {
        var item = _ref3.item;
        return item;
      }) : fuse.list;
    },
    fuseSearchBarcode: function fuseSearchBarcode(options, search) {
      var fuse = new fuse_js__WEBPACK_IMPORTED_MODULE_3__["default"](options, {
        keys: ["EAN11", "MAKTX", "MATNR"],
        shouldSort: true
      });
      return search.length ? fuse.search(search).map(function (_ref4) {
        var item = _ref4.item;
        return item;
      }) : fuse.list;
    },
    getMatGroup: function getMatGroup() {
      var _this16 = this;

      axios.get('/api/get-mat-group').then(function (response) {
        _this16.matGroup = response.data;
      });
    },
    alertSwal: function alertSwal(title, text, icon, timer) {
      this.$swal({
        title: title,
        text: text,
        icon: icon,
        timer: timer
      });
    },
    formatNumber: function formatNumber(number) {
      return number === '' ? '' : parseFloat(Math.abs(number)).toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
    }
  }
});

/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/forApprovals/view.vue?vue&type=style&index=0&id=3eb2c6b2&scoped=true&lang=css&":
/*!***********************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/forApprovals/view.vue?vue&type=style&index=0&id=3eb2c6b2&scoped=true&lang=css& ***!
  \***********************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n@media (min-width: 992px) {\n.modal-dialog[data-v-3eb2c6b2] {\n        max-width: 30% !important;\n}\n}\n@media (min-width: 992px) {\n.modal_custom[data-v-3eb2c6b2] {\n        max-width: 50% !important;\n}\n}\n.isRangeTuod1680[data-v-3eb2c6b2]{\n    margin-left: -40px;\n}\n.isRangeTuod1920[data-v-3eb2c6b2]{\n    margin-left: -120px;\n}\n.input-group-text[data-v-3eb2c6b2] {\n    background-color: #007bff;\n    color: white;\n}\n.sampleClass[data-v-3eb2c6b2] {\n    background-color: rgba(0, 0, 0, 0.5) !important;\n    /*opacity: 0.5 !important;*/\n}\n.example-open .modal-backdrop.show[data-v-3eb2c6b2]:nth-of-type(even) {\n    opacity: 5 !important;\n    z-index: 1052 !important;\n}\n.filter-asc[data-v-3eb2c6b2] {\n    border-bottom: 5px solid #ccc;\n    margin-bottom: 1px\n}\n.filter-asc[data-v-3eb2c6b2], .filter-desc[data-v-3eb2c6b2] {\n    width: 0;\n    height: 0;\n    border-left: 5px solid transparent;\n    border-right: 5px solid transparent\n}\n.filter-desc[data-v-3eb2c6b2] {\n    border-top: 5px solid #ccc;\n    margin-top: 1px\n}\n.inline-block[data-v-3eb2c6b2] {\n    display: inline-block\n}\n[data-position='below'] .autocomplete-result-list[data-v-3eb2c6b2] {\n    border-bottom: none;\n    border-radius: 8px 8px 0 0;\n    color: forestgreen;\n    font-weight: bold;\n}\n.wiki-result[data-v-3eb2c6b2] {\n    border-top: 1px solid #eee;\n    padding: 16px;\n    background: transparent;\n}\n.wiki-title[data-v-3eb2c6b2] {\n    font-size: 15px;\n    margin-bottom: 8px;\n    font-weight: 550;\n    color: forestgreen;\n}\n.wiki-snippet[data-v-3eb2c6b2] {\n    font-size: 14px;\n    color: rgba(0, 0, 0, 0.54);\n}\n.wrapAll[data-v-3eb2c6b2] {\n    white-space: nowrap;\n    overflow: hidden;\n}\nth[data-v-3eb2c6b2]:nth-child(1), td[data-v-3eb2c6b2]:nth-child(1) {\n    position: -webkit-sticky;\n    position: sticky;\n    left: 0px;\n    background: white;\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/forApprovals/view.vue?vue&type=style&index=1&lang=css&":
/*!***********************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/forApprovals/view.vue?vue&type=style&index=1&lang=css& ***!
  \***********************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.matGroupHeight .vs__dropdown-toggle {\n    height: 38px;\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/forApprovals/view.vue?vue&type=style&index=0&id=3eb2c6b2&scoped=true&lang=css&":
/*!***************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/forApprovals/view.vue?vue&type=style&index=0&id=3eb2c6b2&scoped=true&lang=css& ***!
  \***************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../node_modules/css-loader??ref--6-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--6-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./view.vue?vue&type=style&index=0&id=3eb2c6b2&scoped=true&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/forApprovals/view.vue?vue&type=style&index=0&id=3eb2c6b2&scoped=true&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/forApprovals/view.vue?vue&type=style&index=1&lang=css&":
/*!***************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/forApprovals/view.vue?vue&type=style&index=1&lang=css& ***!
  \***************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../node_modules/css-loader??ref--6-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--6-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./view.vue?vue&type=style&index=1&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/forApprovals/view.vue?vue&type=style&index=1&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/forApprovals/view.vue?vue&type=template&id=3eb2c6b2&scoped=true&":
/*!********************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/forApprovals/view.vue?vue&type=template&id=3eb2c6b2&scoped=true& ***!
  \********************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c("div", { staticClass: "row" }, [
        _c("div", { staticClass: "col-lg-12" }, [
          _c("div", { staticClass: "card shadow-sm mb-4" }, [
            _vm._m(0),
            _vm._v(" "),
            _c(
              "form",
              {
                directives: [
                  {
                    name: "promise-btn",
                    rawName: "v-promise-btn",
                    value: { action: "submit" },
                    expression: "{action: 'submit'}"
                  }
                ],
                on: {
                  submit: function($event) {
                    $event.preventDefault()
                    return _vm.addItem($event)
                  }
                }
              },
              [
                _c("div", { staticClass: "card-header" }, [
                  _c("div", { staticClass: "row form-group" }, [
                    _c("div", { staticClass: "col-md-3" }, [
                      _c("div", { staticClass: "input-group" }, [
                        _vm._m(1),
                        _vm._v(" "),
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.form.req_date,
                              expression: "form.req_date"
                            }
                          ],
                          staticClass: "form-control",
                          attrs: {
                            type: "date",
                            placeholder: "Request Date",
                            required: ""
                          },
                          domProps: { value: _vm.form.req_date },
                          on: {
                            input: function($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.$set(
                                _vm.form,
                                "req_date",
                                $event.target.value
                              )
                            }
                          }
                        })
                      ])
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "col-md-2" }),
                    _vm._v(" "),
                    _c("div", { staticClass: "col-md-2" }, [
                      _c("div", { staticClass: "input-group" }, [
                        _vm._m(2),
                        _vm._v(" "),
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.form.uom,
                              expression: "form.uom"
                            }
                          ],
                          staticClass: "form-control",
                          attrs: { type: "text", disabled: "" },
                          domProps: { value: _vm.form.uom },
                          on: {
                            input: function($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.$set(_vm.form, "uom", $event.target.value)
                            }
                          }
                        })
                      ])
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "col-md-6" })
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "row form-group" }, [
                    _c("div", { staticClass: "col-md-5" }, [
                      _c(
                        "div",
                        { staticClass: "input-group" },
                        [
                          _vm._m(3),
                          _vm._v(" "),
                          _c("v-select", {
                            class: { matGroupHeight: true },
                            staticStyle: { width: "380px" },
                            attrs: {
                              filter: _vm.fuseSearchCompany,
                              options: _vm.company,
                              getOptionLabel: function(option) {
                                return option.NAME1
                              },
                              reduce: function(company) {
                                return "" + company.NAME1
                              },
                              required: ""
                            },
                            scopedSlots: _vm._u([
                              {
                                key: "option",
                                fn: function(ref) {
                                  var NAME1 = ref.NAME1
                                  return [_c("strong", [_vm._v(_vm._s(NAME1))])]
                                }
                              },
                              {
                                key: "search",
                                fn: function(ref) {
                                  var attributes = ref.attributes
                                  var events = ref.events
                                  return [
                                    _c(
                                      "input",
                                      _vm._g(
                                        _vm._b(
                                          {
                                            staticClass: "vs__search",
                                            attrs: {
                                              required: !_vm.form.cust_company,
                                              name: "cust_company"
                                            }
                                          },
                                          "input",
                                          attributes,
                                          false
                                        ),
                                        events
                                      )
                                    )
                                  ]
                                }
                              }
                            ]),
                            model: {
                              value: _vm.form.cust_company,
                              callback: function($$v) {
                                _vm.$set(_vm.form, "cust_company", $$v)
                              },
                              expression: "form.cust_company"
                            }
                          })
                        ],
                        1
                      )
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "col-md-4" }, [
                      _c("div", { staticClass: "input-group" }, [
                        _vm._m(4),
                        _vm._v(" "),
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.form.mat_code,
                              expression: "form.mat_code"
                            }
                          ],
                          staticClass: "form-control",
                          attrs: { type: "text", disabled: "" },
                          domProps: { value: _vm.form.mat_code },
                          on: {
                            input: function($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.$set(
                                _vm.form,
                                "mat_code",
                                $event.target.value
                              )
                            }
                          }
                        })
                      ])
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "col-md-3" }),
                    _vm._v(" "),
                    _c("div", { staticClass: "col-md-1" })
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "row form-group" }, [
                    _c("div", { staticClass: "col-md-5" }, [
                      _c(
                        "div",
                        { staticClass: "input-group" },
                        [
                          _vm._m(5),
                          _vm._v(" "),
                          _c("v-select", {
                            class: { matGroupHeight: true },
                            staticStyle: { width: "300px" },
                            attrs: {
                              filter: _vm.fuseSearch,
                              options: _vm.matGroup,
                              getOptionLabel: function(option) {
                                return option.WGBEZ
                              },
                              reduce: function(matGroup) {
                                return "" + matGroup.WGBEZ
                              },
                              required: ""
                            },
                            scopedSlots: _vm._u([
                              {
                                key: "option",
                                fn: function(ref) {
                                  var WGBEZ = ref.WGBEZ
                                  return [_c("strong", [_vm._v(_vm._s(WGBEZ))])]
                                }
                              },
                              {
                                key: "search",
                                fn: function(ref) {
                                  var attributes = ref.attributes
                                  var events = ref.events
                                  return [
                                    _c(
                                      "input",
                                      _vm._g(
                                        _vm._b(
                                          {
                                            staticClass: "vs__search",
                                            attrs: { name: "mat_group" }
                                          },
                                          "input",
                                          attributes,
                                          false
                                        ),
                                        events
                                      )
                                    )
                                  ]
                                }
                              }
                            ]),
                            model: {
                              value: _vm.form.mat_group,
                              callback: function($$v) {
                                _vm.$set(_vm.form, "mat_group", $$v)
                              },
                              expression: "form.mat_group"
                            }
                          })
                        ],
                        1
                      )
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "col-md-4" }, [
                      _c(
                        "div",
                        { staticClass: "input-group" },
                        [
                          _vm._m(6),
                          _vm._v(" "),
                          _c("v-select", {
                            class: { matGroupHeight: true },
                            staticStyle: { width: "325px" },
                            attrs: {
                              filter: _vm.fuseSearchBarcode,
                              options: _vm.matBarcode,
                              getOptionLabel: function(option) {
                                return option.EAN11
                              },
                              reduce: function(matBarcode) {
                                return "" + matBarcode.EAN11
                              },
                              required: ""
                            },
                            scopedSlots: _vm._u([
                              {
                                key: "option",
                                fn: function(ref) {
                                  var EAN11 = ref.EAN11
                                  var MAKTX = ref.MAKTX
                                  var MATNR = ref.MATNR
                                  return [
                                    _c("strong", [
                                      _vm._v(
                                        "Material Code: " + _vm._s(MATNR) + " "
                                      )
                                    ]),
                                    _vm._v(" "),
                                    _c("br"),
                                    _vm._v(" "),
                                    _c("strong", [
                                      _vm._v("Barcode: " + _vm._s(EAN11))
                                    ]),
                                    _vm._v(" "),
                                    _c("br"),
                                    _vm._v(" "),
                                    _c("cite", [
                                      _vm._v("Material: " + _vm._s(MAKTX))
                                    ])
                                  ]
                                }
                              },
                              {
                                key: "search",
                                fn: function(ref) {
                                  var attributes = ref.attributes
                                  var events = ref.events
                                  return [
                                    _c(
                                      "input",
                                      _vm._g(
                                        _vm._b(
                                          {
                                            staticClass: "vs__search",
                                            attrs: {
                                              required: !_vm.form.barcode,
                                              name: "barcode"
                                            }
                                          },
                                          "input",
                                          attributes,
                                          false
                                        ),
                                        events
                                      )
                                    )
                                  ]
                                }
                              }
                            ]),
                            model: {
                              value: _vm.form.barcode,
                              callback: function($$v) {
                                _vm.$set(_vm.form, "barcode", $$v)
                              },
                              expression: "form.barcode"
                            }
                          })
                        ],
                        1
                      )
                    ]),
                    _vm._v(" "),
                    _vm.windowWidth === 1680
                      ? _c(
                          "div",
                          {
                            staticClass: "col-md-3",
                            class: [
                              { isRangeTuod1680: _vm.is1680 },
                              { isRangeTuod1920: _vm.is1920 }
                            ]
                          },
                          [
                            _c(
                              "button",
                              {
                                staticClass: "btn btn-warning",
                                attrs: { type: "button" },
                                on: { click: _vm.rangeBarcode }
                              },
                              [
                                _vm._v("Range  "),
                                _c("i", { staticClass: "fas fa-exchange-alt" })
                              ]
                            ),
                            _vm._v(" "),
                            _vm._m(7)
                          ]
                        )
                      : _vm._e(),
                    _vm._v(" "),
                    _vm.windowWidth !== 1680
                      ? _c(
                          "div",
                          {
                            staticClass: "col-md-3",
                            class: [
                              { isRangeTuod1680: _vm.is1680 },
                              { isRangeTuod1920: _vm.is1920 }
                            ]
                          },
                          [
                            _c(
                              "button",
                              {
                                staticClass: "btn btn-warning",
                                attrs: { type: "button" },
                                on: { click: _vm.rangeBarcode }
                              },
                              [
                                _vm._v("Range  "),
                                _c("i", { staticClass: "fas fa-exchange-alt" })
                              ]
                            ),
                            _vm._v(" "),
                            _vm._m(8)
                          ]
                        )
                      : _vm._e()
                  ])
                ])
              ]
            ),
            _vm._v(" "),
            _c("div", { staticClass: "input-group row-md-12 mb-3" }, [
              _c("br"),
              _vm._v(" "),
              _c(
                "button",
                {
                  staticClass: "btn btn-danger",
                  staticStyle: {
                    margin: "0px 20px",
                    top: "11px",
                    position: "absolute"
                  },
                  attrs: { type: "button" },
                  on: {
                    click: function($event) {
                      $event.preventDefault()
                      return _vm.printReleaseForm($event)
                    }
                  }
                },
                [
                  _vm._v(
                    "Print Preview - Release Form  \n                            "
                  ),
                  _c("i", {
                    staticClass: "fas fa-file-pdf",
                    staticStyle: { "font-size": "1.25rem" }
                  })
                ]
              ),
              _vm._v(" "),
              _c(
                "button",
                {
                  staticClass: "btn btn-secondary",
                  staticStyle: {
                    margin: "0px 285px",
                    top: "11px",
                    position: "absolute"
                  },
                  attrs: { type: "submit" },
                  on: {
                    click: function($event) {
                      $event.preventDefault()
                      return _vm.printDeliveryForm($event)
                    }
                  }
                },
                [
                  _vm._v(
                    "\n                            Print Preview - Delivery receipt  \n                            "
                  ),
                  _c("i", {
                    staticClass: "fas fa-file-pdf",
                    staticStyle: { "font-size": "1.25rem" }
                  })
                ]
              )
            ]),
            _vm._v(" "),
            _c(
              "form",
              {
                directives: [
                  {
                    name: "promise-btn",
                    rawName: "v-promise-btn",
                    value: { action: "submit" },
                    expression: "{action: 'submit'}"
                  }
                ],
                on: {
                  submit: function($event) {
                    $event.preventDefault()
                    return _vm.approveRequest($event)
                  }
                }
              },
              [
                _c(
                  "div",
                  { staticClass: "card-body" },
                  [
                    _c("div", { staticClass: "table-responsive" }, [
                      _c(
                        "table",
                        {
                          staticClass:
                            "table align-items-center table-flush table-hover",
                          attrs: {
                            id: "dataTable",
                            width: "100%",
                            cellspacing: "0"
                          }
                        },
                        [
                          _vm._m(9),
                          _vm._v(" "),
                          _c(
                            "tbody",
                            _vm._l(_vm.stocks, function(stock, index) {
                              return _c("tr", [
                                _c(
                                  "td",
                                  {
                                    staticClass: "wrapAll",
                                    staticStyle: { "text-align": "center" }
                                  },
                                  [_vm._v(_vm._s(stock.barcode))]
                                ),
                                _vm._v(" "),
                                _c(
                                  "td",
                                  {
                                    staticClass: "wrapAll",
                                    staticStyle: { "text-align": "center" }
                                  },
                                  [_vm._v(_vm._s(stock.mat_code))]
                                ),
                                _vm._v(" "),
                                _c(
                                  "td",
                                  {
                                    staticClass: "wrapAll",
                                    staticStyle: { "text-align": "center" }
                                  },
                                  [_vm._v(_vm._s(stock.mat_des))]
                                ),
                                _vm._v(" "),
                                _c(
                                  "td",
                                  {
                                    staticClass: "wrapAll",
                                    staticStyle: { "text-align": "center" }
                                  },
                                  [_vm._v(" " + _vm._s(stock.sales_unit))]
                                ),
                                _vm._v(" "),
                                _c(
                                  "td",
                                  {
                                    staticClass: "wrapAll",
                                    staticStyle: { "text-align": "center" }
                                  },
                                  [
                                    _vm._v(
                                      _vm._s(_vm.formatNumber(stock.LABST))
                                    )
                                  ]
                                ),
                                _vm._v(" "),
                                _c(
                                  "td",
                                  {
                                    staticClass: "wrapAll",
                                    staticStyle: { "text-align": "center" }
                                  },
                                  [
                                    _vm._v(
                                      _vm._s(
                                        _vm._f("moment")(
                                          stock.ERDAT,
                                          "MM/DD/YYYY"
                                        )
                                      ) +
                                        "\n                                    "
                                    )
                                  ]
                                ),
                                _vm._v(" "),
                                stock.KWMENG === null
                                  ? _c(
                                      "td",
                                      {
                                        staticClass: "wrapAll",
                                        staticStyle: { "text-align": "center" }
                                      },
                                      [
                                        _vm._v(
                                          _vm._s(stock.KWMENG) +
                                            "\n                                    "
                                        )
                                      ]
                                    )
                                  : _vm._e(),
                                _vm._v(" "),
                                stock.KWMENG !== null
                                  ? _c(
                                      "td",
                                      {
                                        staticClass: "wrapAll",
                                        staticStyle: { "text-align": "center" }
                                      },
                                      [
                                        _vm._v(
                                          _vm._s(
                                            _vm.formatNumber(stock.KWMENG)
                                          ) +
                                            "\n                                    "
                                        )
                                      ]
                                    )
                                  : _vm._e(),
                                _vm._v(" "),
                                _c("td", { staticClass: "wrapAll" }, [
                                  _c("input", {
                                    directives: [
                                      {
                                        name: "model",
                                        rawName: "v-model",
                                        value: stock.stocks_left,
                                        expression: "stock.stocks_left"
                                      }
                                    ],
                                    staticClass: "form-control",
                                    staticStyle: { "text-align": "center" },
                                    attrs: {
                                      type: "number",
                                      disabled: "",
                                      placeholder: ""
                                    },
                                    domProps: { value: stock.stocks_left },
                                    on: {
                                      input: function($event) {
                                        if ($event.target.composing) {
                                          return
                                        }
                                        _vm.$set(
                                          stock,
                                          "stocks_left",
                                          $event.target.value
                                        )
                                      }
                                    }
                                  })
                                ]),
                                _vm._v(" "),
                                _c("td", { staticClass: "wrapAll" }, [
                                  _c("input", {
                                    directives: [
                                      {
                                        name: "model",
                                        rawName: "v-model",
                                        value: stock.order_qty,
                                        expression: "stock.order_qty"
                                      }
                                    ],
                                    staticClass: "form-control",
                                    staticStyle: { "text-align": "center" },
                                    attrs: {
                                      type: "number",
                                      disabled: "",
                                      placeholder: "",
                                      required: ""
                                    },
                                    domProps: { value: stock.order_qty },
                                    on: {
                                      input: function($event) {
                                        if ($event.target.composing) {
                                          return
                                        }
                                        _vm.$set(
                                          stock,
                                          "order_qty",
                                          $event.target.value
                                        )
                                      }
                                    }
                                  })
                                ]),
                                _vm._v(" "),
                                _c("td", { staticClass: "wrapAll" }, [
                                  _c("input", {
                                    directives: [
                                      {
                                        name: "model",
                                        rawName: "v-model",
                                        value: stock.approve_qty,
                                        expression: "stock.approve_qty"
                                      }
                                    ],
                                    staticClass: "form-control",
                                    staticStyle: {
                                      "text-align": "center",
                                      width: "150px"
                                    },
                                    attrs: {
                                      type: "number",
                                      placeholder: "",
                                      disabled: _vm.disApproveQty,
                                      required: ""
                                    },
                                    domProps: { value: stock.approve_qty },
                                    on: {
                                      input: function($event) {
                                        if ($event.target.composing) {
                                          return
                                        }
                                        _vm.$set(
                                          stock,
                                          "approve_qty",
                                          $event.target.value
                                        )
                                      }
                                    }
                                  })
                                ]),
                                _vm._v(" "),
                                _c("td", { staticClass: "wrapAll" }, [
                                  _c("input", {
                                    directives: [
                                      {
                                        name: "model",
                                        rawName: "v-model",
                                        value: stock.box_no,
                                        expression: "stock.box_no"
                                      }
                                    ],
                                    staticClass: "form-control",
                                    staticStyle: {
                                      "text-align": "center",
                                      width: "200px"
                                    },
                                    attrs: {
                                      type: "number",
                                      placeholder: "",
                                      disabled: _vm.disApproveQty
                                    },
                                    domProps: { value: stock.box_no },
                                    on: {
                                      input: function($event) {
                                        if ($event.target.composing) {
                                          return
                                        }
                                        _vm.$set(
                                          stock,
                                          "box_no",
                                          $event.target.value
                                        )
                                      }
                                    }
                                  })
                                ]),
                                _vm._v(" "),
                                _c("td", [
                                  _c(
                                    "a",
                                    {
                                      staticStyle: {
                                        cursor: "pointer",
                                        color: "#E10000"
                                      },
                                      on: {
                                        click: function($event) {
                                          return _vm.removeElement(index)
                                        }
                                      }
                                    },
                                    [_c("b", [_vm._v("Remove")])]
                                  )
                                ])
                              ])
                            }),
                            0
                          )
                        ]
                      )
                    ]),
                    _vm._v(" "),
                    _c(
                      "router-link",
                      {
                        staticClass: "btn btn-danger",
                        attrs: {
                          to: {
                            name: "approval-table",
                            query: {
                              allRequest: "DataTable",
                              id: _vm.currentUser.token
                            }
                          }
                        }
                      },
                      [_vm._v("BACK\n                        ")]
                    ),
                    _vm._v(" "),
                    _c(
                      "button",
                      {
                        directives: [
                          { name: "promise-btn", rawName: "v-promise-btn" }
                        ],
                        staticClass: "btn btn-info",
                        on: {
                          click: function($event) {
                            $event.preventDefault()
                            return _vm.rejectRequest($event)
                          }
                        }
                      },
                      [_vm._v("REJECT\n                        ")]
                    ),
                    _vm._v(" "),
                    _c(
                      "button",
                      {
                        staticClass: "btn btn-success",
                        attrs: { type: "submit" }
                      },
                      [_vm._v("APPROVE\n                        ")]
                    )
                  ],
                  1
                )
              ]
            )
          ])
        ])
      ]),
      _vm._v(" "),
      _vm.rangeBarcodeModal
        ? _c("rangeBarcode", {
            attrs: {
              form: _vm.form,
              rangeBarcodeModal: _vm.rangeBarcodeModal,
              rangeNumber: _vm.rangeNumber
            }
          })
        : _vm._e()
    ],
    1
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "card-header" }, [
      _c("h4", { staticClass: "m-0 font-weight-bold text-primary" }, [
        _vm._v("FOR APPROVAL VIEW DETAILS")
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "input-group-prepend" }, [
      _c(
        "span",
        {
          staticClass: "input-group-text input-group-field",
          attrs: { id: "basic-addon1" }
        },
        [_vm._v("Request Date:")]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "input-group-prepend" }, [
      _c(
        "span",
        {
          staticClass: "input-group-text input-group-field",
          attrs: { id: "basic-addon1" }
        },
        [_vm._v("UOM:")]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "input-group-prepend" }, [
      _c(
        "span",
        {
          staticClass: "input-group-text input-group-field",
          attrs: { id: "basic-addon1" }
        },
        [_vm._v("Company:")]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "input-group-prepend" }, [
      _c(
        "span",
        {
          staticClass: "input-group-text input-group-field",
          attrs: { id: "basic-addon1" }
        },
        [_vm._v("Material Code:")]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "input-group-prepend" }, [
      _c(
        "span",
        {
          staticClass: "input-group-text input-group-field",
          attrs: { id: "basic-addon1" }
        },
        [_vm._v("Material Group:")]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "input-group-prepend" }, [
      _c(
        "span",
        {
          staticClass: "input-group-text input-group-field",
          attrs: { id: "basic-addon1" }
        },
        [_vm._v("Barcode:")]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "button",
      { staticClass: "btn btn-success", attrs: { type: "submit" } },
      [
        _vm._v("Add Item  "),
        _c("i", { staticClass: "fas fa-plus-square fa-lg" })
      ]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "button",
      { staticClass: "btn btn-success", attrs: { type: "submit" } },
      [
        _vm._v("Add Item  "),
        _c("i", { staticClass: "fas fa-plus-square fa-lg" })
      ]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("thead", [
      _c("tr", [
        _c(
          "th",
          {
            staticClass: "wrapAll",
            staticStyle: { "background-color": "#007bff", color: "white" }
          },
          [_vm._v("BARCODE")]
        ),
        _vm._v(" "),
        _c(
          "th",
          {
            staticClass: "wrapAll",
            staticStyle: { "background-color": "#007bff", color: "white" }
          },
          [_vm._v("MATERIAL CODE\n                                    ")]
        ),
        _vm._v(" "),
        _c(
          "th",
          {
            staticClass: "wrapAll",
            staticStyle: { "background-color": "#007bff", color: "white" }
          },
          [
            _vm._v(
              "MATERIAL\n                                        DESCRIPTION\n                                    "
            )
          ]
        ),
        _vm._v(" "),
        _c(
          "th",
          {
            staticClass: "wrapAll",
            staticStyle: { "background-color": "#007bff", color: "white" }
          },
          [_vm._v("UOM")]
        ),
        _vm._v(" "),
        _c(
          "th",
          {
            staticClass: "wrapAll",
            staticStyle: { "background-color": "#007bff", color: "white" }
          },
          [
            _vm._v(
              "SAP UNRESTRICTED\n                                        QTY\n                                    "
            )
          ]
        ),
        _vm._v(" "),
        _c(
          "th",
          {
            staticClass: "wrapAll",
            staticStyle: { "background-color": "#007bff", color: "white" }
          },
          [
            _vm._v(
              "LAST FILL-UP\n                                        DATE\n                                    "
            )
          ]
        ),
        _vm._v(" "),
        _c(
          "th",
          {
            staticClass: "wrapAll",
            staticStyle: { "background-color": "#007bff", color: "white" }
          },
          [
            _vm._v(
              "LAST FILL-UP\n                                        QTY\n                                    "
            )
          ]
        ),
        _vm._v(" "),
        _c(
          "th",
          {
            staticClass: "wrapAll",
            staticStyle: { "background-color": "#007bff", color: "white" }
          },
          [_vm._v("STOCKS LEFT")]
        ),
        _vm._v(" "),
        _c(
          "th",
          {
            staticClass: "wrapAll",
            staticStyle: { "background-color": "#007bff", color: "white" }
          },
          [
            _vm._v(
              "SUGGESTED ORDER\n                                        QTY\n                                    "
            )
          ]
        ),
        _vm._v(" "),
        _c(
          "th",
          {
            staticClass: "wrapAll",
            staticStyle: {
              "background-color": "#007bff",
              color: "white",
              "text-align": "center"
            }
          },
          [_vm._v("APPROVED QTY\n                                    ")]
        ),
        _vm._v(" "),
        _c(
          "th",
          {
            staticClass: "wrapAll",
            staticStyle: {
              "background-color": "#007bff",
              color: "white",
              "text-align": "center"
            }
          },
          [_vm._v("BOX NUMBER")]
        ),
        _vm._v(" "),
        _c(
          "th",
          {
            staticClass: "wrapAll",
            staticStyle: { "background-color": "#007bff", color: "white" }
          },
          [_vm._v("ACTIONS")]
        )
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/components/forApprovals/view.vue":
/*!*******************************************************!*\
  !*** ./resources/js/components/forApprovals/view.vue ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _view_vue_vue_type_template_id_3eb2c6b2_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./view.vue?vue&type=template&id=3eb2c6b2&scoped=true& */ "./resources/js/components/forApprovals/view.vue?vue&type=template&id=3eb2c6b2&scoped=true&");
/* harmony import */ var _view_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./view.vue?vue&type=script&lang=js& */ "./resources/js/components/forApprovals/view.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _view_vue_vue_type_style_index_0_id_3eb2c6b2_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./view.vue?vue&type=style&index=0&id=3eb2c6b2&scoped=true&lang=css& */ "./resources/js/components/forApprovals/view.vue?vue&type=style&index=0&id=3eb2c6b2&scoped=true&lang=css&");
/* harmony import */ var _view_vue_vue_type_style_index_1_lang_css___WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./view.vue?vue&type=style&index=1&lang=css& */ "./resources/js/components/forApprovals/view.vue?vue&type=style&index=1&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");







/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_4__["default"])(
  _view_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _view_vue_vue_type_template_id_3eb2c6b2_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _view_vue_vue_type_template_id_3eb2c6b2_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "3eb2c6b2",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/forApprovals/view.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/forApprovals/view.vue?vue&type=script&lang=js&":
/*!********************************************************************************!*\
  !*** ./resources/js/components/forApprovals/view.vue?vue&type=script&lang=js& ***!
  \********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_view_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./view.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/forApprovals/view.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_view_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/forApprovals/view.vue?vue&type=style&index=0&id=3eb2c6b2&scoped=true&lang=css&":
/*!****************************************************************************************************************!*\
  !*** ./resources/js/components/forApprovals/view.vue?vue&type=style&index=0&id=3eb2c6b2&scoped=true&lang=css& ***!
  \****************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_view_vue_vue_type_style_index_0_id_3eb2c6b2_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/style-loader!../../../../node_modules/css-loader??ref--6-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--6-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./view.vue?vue&type=style&index=0&id=3eb2c6b2&scoped=true&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/forApprovals/view.vue?vue&type=style&index=0&id=3eb2c6b2&scoped=true&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_view_vue_vue_type_style_index_0_id_3eb2c6b2_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_view_vue_vue_type_style_index_0_id_3eb2c6b2_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_view_vue_vue_type_style_index_0_id_3eb2c6b2_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_view_vue_vue_type_style_index_0_id_3eb2c6b2_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ "./resources/js/components/forApprovals/view.vue?vue&type=style&index=1&lang=css&":
/*!****************************************************************************************!*\
  !*** ./resources/js/components/forApprovals/view.vue?vue&type=style&index=1&lang=css& ***!
  \****************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_view_vue_vue_type_style_index_1_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/style-loader!../../../../node_modules/css-loader??ref--6-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--6-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./view.vue?vue&type=style&index=1&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/forApprovals/view.vue?vue&type=style&index=1&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_view_vue_vue_type_style_index_1_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_view_vue_vue_type_style_index_1_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_view_vue_vue_type_style_index_1_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_view_vue_vue_type_style_index_1_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ "./resources/js/components/forApprovals/view.vue?vue&type=template&id=3eb2c6b2&scoped=true&":
/*!**************************************************************************************************!*\
  !*** ./resources/js/components/forApprovals/view.vue?vue&type=template&id=3eb2c6b2&scoped=true& ***!
  \**************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_view_vue_vue_type_template_id_3eb2c6b2_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./view.vue?vue&type=template&id=3eb2c6b2&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/forApprovals/view.vue?vue&type=template&id=3eb2c6b2&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_view_vue_vue_type_template_id_3eb2c6b2_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_view_vue_vue_type_template_id_3eb2c6b2_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);