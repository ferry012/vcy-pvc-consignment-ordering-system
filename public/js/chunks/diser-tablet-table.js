(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["diser-tablet-table"],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/diser_tablet/tabletTable.vue?vue&type=script&lang=js&":
/*!***********************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/diser_tablet/tabletTable.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  name: "manualExportSO",
  data: function data() {
    return {
      logs: {},
      meta: {},
      currentSort: 'created_at',
      currentSortDir: 'desc',
      search: null,
      isSearch: false,
      perPage: ['10', '50', '100'],
      currentPage: '10',
      page: {},
      sortClassActive: {
        'activeDesc': false,
        'activeAsc': true
      }
    };
  },
  methods: {
    index: function index(page) {
      var _this = this;

      if (typeof page === 'undefined') {
        this.page = 1;
      }

      var uri = '/api/get-diser-device-info?page=' + page + '&search=' + this.search + '&sortby=' + this.currentSort + '&sortdir=' + this.currentSortDir + '&currentpage=' + this.currentPage;
      this.uri = uri;
      this.page = page;
      axios.get(uri).then(function (response) {
        _this.logs = response.data;
        _this.meta.total = response.data.total;
        _this.meta.from = response.data.from;
        _this.meta.to = response.data.to;
      });
    },
    selectPageNumber: function selectPageNumber(selected) {
      this.currentPage = selected;
      this.index();
    },
    manualExport: function manualExport(sale) {
      var _this2 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
        var uri;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                uri = '/api/manualExport?saleNo=' + sale;
                _context.next = 3;
                return axios.get(uri).then(function (response) {
                  _this2.$swal({
                    icon: 'success',
                    title: 'Sales order exported manually was successfully!',
                    showConfirmButton: false,
                    timer: 3000,
                    onClose: function onClose() {
                      _this2.index();
                    }
                  });
                });

              case 3:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }))();
    }
  },
  computed: {},
  mounted: function mounted() {
    this.index();
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/diser_tablet/tabletTable.vue?vue&type=template&id=fea88ca4&scoped=true&":
/*!***************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/diser_tablet/tabletTable.vue?vue&type=template&id=fea88ca4&scoped=true& ***!
  \***************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c("div", { staticClass: "row" }, [
      _c("div", { staticClass: "col-lg-12 mb-4" }, [
        _c("div", { staticClass: "card" }, [
          _vm._m(0),
          _vm._v(" "),
          _c("div", { staticClass: "card-body" }, [
            _c("div", { staticClass: "row form-group" }, [
              _c("div", { staticClass: "col-md-1" }, [
                _c("label", [_vm._v("Per Page")]),
                _vm._v(" "),
                _c(
                  "select",
                  {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.currentPage,
                        expression: "currentPage"
                      }
                    ],
                    staticClass: "form-control",
                    on: {
                      change: [
                        function($event) {
                          var $$selectedVal = Array.prototype.filter
                            .call($event.target.options, function(o) {
                              return o.selected
                            })
                            .map(function(o) {
                              var val = "_value" in o ? o._value : o.value
                              return val
                            })
                          _vm.currentPage = $event.target.multiple
                            ? $$selectedVal
                            : $$selectedVal[0]
                        },
                        function($event) {
                          return _vm.selectPageNumber(_vm.currentPage)
                        }
                      ]
                    }
                  },
                  _vm._l(_vm.perPage, function(perPage) {
                    return _c(
                      "option",
                      {
                        domProps: {
                          value: perPage,
                          selected: _vm.currentPage === perPage
                        }
                      },
                      [
                        _vm._v(
                          _vm._s(perPage) + "\n                                "
                        )
                      ]
                    )
                  }),
                  0
                )
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "col-md-8" }),
              _vm._v(" "),
              _c("div", { staticClass: "col-md-3" }, [
                _c(
                  "div",
                  {
                    staticClass: "input-group",
                    staticStyle: { "margin-top": "30px" }
                  },
                  [
                    _vm._m(1),
                    _vm._v(" "),
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.search,
                          expression: "search"
                        }
                      ],
                      staticClass: "form-control",
                      attrs: { placeholder: "Search Here" },
                      domProps: { value: _vm.search },
                      on: {
                        keyup: _vm.index,
                        click: function($event) {
                          _vm.isSearch = true
                        },
                        input: function($event) {
                          if ($event.target.composing) {
                            return
                          }
                          _vm.search = $event.target.value
                        }
                      }
                    })
                  ]
                )
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "table-responsive" }, [
              _c(
                "table",
                {
                  staticClass:
                    "table align-items-center table-flush table-bordered"
                },
                [
                  _vm._m(2),
                  _vm._v(" "),
                  _c(
                    "tbody",
                    [
                      _vm._l(_vm.logs.data, function(log, index) {
                        return _c("tr", { key: log.id }, [
                          _c("td", [_vm._v(" " + _vm._s(index + 1))]),
                          _vm._v(" "),
                          _c("td", [_vm._v(" " + _vm._s(log.name))]),
                          _vm._v(" "),
                          _c("td", [_vm._v(" " + _vm._s(log.device_name))]),
                          _vm._v(" "),
                          _c(
                            "td",
                            { staticStyle: { "text-align": "center" } },
                            [_vm._v(" " + _vm._s(log.device_width))]
                          ),
                          _vm._v(" "),
                          _c(
                            "td",
                            { staticStyle: { "text-align": "center" } },
                            [
                              _vm._v(
                                " " +
                                  _vm._s(
                                    _vm._f("moment")(
                                      log.created_at,
                                      "MM/DD/YYYY, h:mm:ss a"
                                    )
                                  )
                              )
                            ]
                          )
                        ])
                      }),
                      _vm._v(" "),
                      _vm.logs.data && _vm.logs.data.length === 0
                        ? _c("tr", [
                            _c("td", { attrs: { colspan: "4" } }, [
                              _vm._v("No Data")
                            ])
                          ])
                        : _vm._e()
                    ],
                    2
                  )
                ]
              )
            ])
          ]),
          _vm._v(" "),
          _c(
            "div",
            { staticClass: "card-footer" },
            [
              _c(
                "pagination",
                {
                  attrs: { data: _vm.logs, limit: 10 },
                  on: { "pagination-change-page": _vm.index }
                },
                [
                  _c(
                    "span",
                    { attrs: { slot: "prev-nav" }, slot: "prev-nav" },
                    [_vm._v("< Previous")]
                  ),
                  _vm._v(" "),
                  _c(
                    "span",
                    { attrs: { slot: "next-nav" }, slot: "next-nav" },
                    [_vm._v("Next >")]
                  )
                ]
              )
            ],
            1
          )
        ])
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "card-header" }, [
      _c("h4", { staticClass: "m-0 font-weight-bold text-primary" }, [
        _vm._v("Merchandisers Tablet Usage Logs")
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "input-group-prepend" }, [
      _c(
        "span",
        {
          staticClass: "input-group-text input-group-field",
          attrs: { id: "basic-addon1" }
        },
        [_vm._v("Search")]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("thead", { staticClass: "thead-light" }, [
      _c("tr", [
        _c(
          "th",
          { staticStyle: { "background-color": "#007bff", color: "white" } },
          [_vm._v("#")]
        ),
        _vm._v(" "),
        _c(
          "th",
          { staticStyle: { "background-color": "#007bff", color: "white" } },
          [_vm._v("Name")]
        ),
        _vm._v(" "),
        _c(
          "th",
          { staticStyle: { "background-color": "#007bff", color: "white" } },
          [_vm._v("Device Name")]
        ),
        _vm._v(" "),
        _c(
          "th",
          { staticStyle: { "background-color": "#007bff", color: "white" } },
          [_vm._v("Device Width Dimension")]
        ),
        _vm._v(" "),
        _c(
          "th",
          { staticStyle: { "background-color": "#007bff", color: "white" } },
          [_vm._v("Created at")]
        )
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/components/diser_tablet/tabletTable.vue":
/*!**************************************************************!*\
  !*** ./resources/js/components/diser_tablet/tabletTable.vue ***!
  \**************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _tabletTable_vue_vue_type_template_id_fea88ca4_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./tabletTable.vue?vue&type=template&id=fea88ca4&scoped=true& */ "./resources/js/components/diser_tablet/tabletTable.vue?vue&type=template&id=fea88ca4&scoped=true&");
/* harmony import */ var _tabletTable_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./tabletTable.vue?vue&type=script&lang=js& */ "./resources/js/components/diser_tablet/tabletTable.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _tabletTable_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _tabletTable_vue_vue_type_template_id_fea88ca4_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _tabletTable_vue_vue_type_template_id_fea88ca4_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "fea88ca4",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/diser_tablet/tabletTable.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/diser_tablet/tabletTable.vue?vue&type=script&lang=js&":
/*!***************************************************************************************!*\
  !*** ./resources/js/components/diser_tablet/tabletTable.vue?vue&type=script&lang=js& ***!
  \***************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_tabletTable_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./tabletTable.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/diser_tablet/tabletTable.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_tabletTable_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/diser_tablet/tabletTable.vue?vue&type=template&id=fea88ca4&scoped=true&":
/*!*********************************************************************************************************!*\
  !*** ./resources/js/components/diser_tablet/tabletTable.vue?vue&type=template&id=fea88ca4&scoped=true& ***!
  \*********************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_tabletTable_vue_vue_type_template_id_fea88ca4_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./tabletTable.vue?vue&type=template&id=fea88ca4&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/diser_tablet/tabletTable.vue?vue&type=template&id=fea88ca4&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_tabletTable_vue_vue_type_template_id_fea88ca4_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_tabletTable_vue_vue_type_template_id_fea88ca4_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);