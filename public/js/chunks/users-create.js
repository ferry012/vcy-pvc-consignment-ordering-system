(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["users-create"],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/user/Create.vue?vue&type=script&lang=js&":
/*!**********************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/user/Create.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Form__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./_Form */ "./resources/js/components/user/_Form.vue");
/* harmony import */ var _elements_Buttons__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../elements/Buttons */ "./resources/js/components/elements/Buttons.vue");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  name: "Create.vue",
  components: {
    ButtonsElement: _elements_Buttons__WEBPACK_IMPORTED_MODULE_1__["default"],
    UserForm: _Form__WEBPACK_IMPORTED_MODULE_0__["default"]
  },
  data: function data() {
    return {
      user: {
        'name': '',
        'email': '',
        'roles': [],
        'plants': [],
        'custs': [],
        'password': '',
        'password_confirmation': ''
      },
      roles: [],
      plants: [],
      custs: [],
      errors: null
    };
  },
  created: function created() {
    document.querySelector('body').style = '    background-image: none !important;\n';
    this.getRoles();
    this.getPlants();
    this.getCust();
  },
  methods: {
    addUser: function addUser() {
      var _this = this;

      // console.log(this.user);
      axios.post('/api/users', this.user).then(function (response) {
        _this.$swal({
          icon: 'success',
          title: 'New user created',
          showConfirmButton: false,
          timer: 1500
        });

        _this.$router.push({
          name: 'users-table'
        });
      })["catch"](function (error) {
        _this.user.password = '';
        _this.user.password_confirmation = '';

        if (error.response.status === 422) {
          _this.errors = error.response.data.errors;
        }
      });
    },
    getRoles: function getRoles() {
      var _this2 = this;

      axios.get('/api/roles?showAll=true').then(function (response) {
        _this2.roles = response.data;
      })["catch"](function (error) {});
    },
    getPlants: function getPlants() {
      var _this3 = this;

      var uri = '/api/plants';
      this.uri = uri;
      axios.get(uri).then(function (response) {
        _this3.plants = response.data;
      });
    },
    getCust: function getCust() {
      var _this4 = this;

      var uri = '/api/get-customer';
      this.uri = uri;
      axios.get(uri).then(function (response) {
        console.log(response.data);
        _this4.custs = response.data;
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/user/Create.vue?vue&type=template&id=532a0ae5&scoped=true&":
/*!**************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/user/Create.vue?vue&type=template&id=532a0ae5&scoped=true& ***!
  \**************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "card shadow mb-4" }, [
    _c(
      "div",
      { staticClass: "card-body" },
      [
        _vm.errors
          ? _c(
              "div",
              { staticClass: "alert alert-danger", attrs: { role: "alert" } },
              _vm._l(_vm.errors, function(err) {
                return _c("p", [
                  _vm._v(
                    "\n                " +
                      _vm._s(err[0] || err) +
                      "\n            "
                  )
                ])
              }),
              0
            )
          : _vm._e(),
        _vm._v(" "),
        _c("ValidationObserver", {
          ref: "form",
          scopedSlots: _vm._u([
            {
              key: "default",
              fn: function(ref) {
                var handleSubmit = ref.handleSubmit
                return [
                  _c(
                    "form",
                    {
                      on: {
                        submit: function($event) {
                          $event.preventDefault()
                          return handleSubmit(_vm.addUser)
                        }
                      }
                    },
                    [
                      _c("UserForm", {
                        attrs: {
                          user: _vm.user,
                          roles: _vm.roles,
                          plants: _vm.plants,
                          custs: _vm.custs
                        }
                      }),
                      _vm._v(" "),
                      _c("ButtonsElement", {
                        attrs: { submit: true, back: true }
                      })
                    ],
                    1
                  )
                ]
              }
            }
          ])
        })
      ],
      1
    )
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/components/user/Create.vue":
/*!*************************************************!*\
  !*** ./resources/js/components/user/Create.vue ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Create_vue_vue_type_template_id_532a0ae5_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Create.vue?vue&type=template&id=532a0ae5&scoped=true& */ "./resources/js/components/user/Create.vue?vue&type=template&id=532a0ae5&scoped=true&");
/* harmony import */ var _Create_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Create.vue?vue&type=script&lang=js& */ "./resources/js/components/user/Create.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Create_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Create_vue_vue_type_template_id_532a0ae5_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Create_vue_vue_type_template_id_532a0ae5_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "532a0ae5",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/user/Create.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/user/Create.vue?vue&type=script&lang=js&":
/*!**************************************************************************!*\
  !*** ./resources/js/components/user/Create.vue?vue&type=script&lang=js& ***!
  \**************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Create_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./Create.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/user/Create.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Create_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/user/Create.vue?vue&type=template&id=532a0ae5&scoped=true&":
/*!********************************************************************************************!*\
  !*** ./resources/js/components/user/Create.vue?vue&type=template&id=532a0ae5&scoped=true& ***!
  \********************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Create_vue_vue_type_template_id_532a0ae5_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./Create.vue?vue&type=template&id=532a0ae5&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/user/Create.vue?vue&type=template&id=532a0ae5&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Create_vue_vue_type_template_id_532a0ae5_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Create_vue_vue_type_template_id_532a0ae5_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);