(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["stock-req-view"],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/stock_request/view.vue?vue&type=script&lang=js&":
/*!*****************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/stock_request/view.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _trevoreyre_autocomplete_vue_dist_style_css__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @trevoreyre/autocomplete-vue/dist/style.css */ "./node_modules/@trevoreyre/autocomplete-vue/dist/style.css");
/* harmony import */ var _trevoreyre_autocomplete_vue_dist_style_css__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_trevoreyre_autocomplete_vue_dist_style_css__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var fuse_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! fuse.js */ "./node_modules/fuse.js/dist/fuse.esm.js");
/* harmony import */ var jspdf__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! jspdf */ "./node_modules/jspdf/dist/jspdf.es.min.js");
/* harmony import */ var jspdf_autotable__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! jspdf-autotable */ "./node_modules/jspdf-autotable/dist/jspdf.plugin.autotable.js");
/* harmony import */ var jspdf_autotable__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(jspdf_autotable__WEBPACK_IMPORTED_MODULE_5__);


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//TODO IMPORTS





/* harmony default export */ __webpack_exports__["default"] = ({
  name: "stockRequestView",
  data: function data() {
    return {
      form: {
        req_date: '',
        cust_company: null,
        mat_group: null,
        mat_code: null,
        barcode: null,
        mat_des: null,
        uom: null,
        KWMENG: null,
        ERDAT: null
      },
      perPage: ['10', '50', '100'],
      currentPage: '10',
      sortClassActive: {
        'activeDesc': false,
        'activeAsc': true
      },
      meta: {},
      page: {},
      errors: {},
      currentSort: 'stock_req_no',
      currentSortDir: 'desc',
      multiForm: {
        sBarcode: '',
        sMat_code: '',
        sMat_des: '',
        sUom: '',
        sLastFillDate: '',
        sLastFillQty: '',
        sStocksLeft: '',
        sSuggestQty: '',
        sApproveQty: ''
      },
      stocks: [],
      matGroup: [],
      matCode: [],
      matBarcode: [],
      multiBarcode: [],
      sales_unit: [],
      dateTime: '',
      company: [],
      trans_stocks: [],
      stockData: {},
      headerStockNo: '',
      headerReqDate: '',
      headerSalesOrderDate: '',
      headerCompany: '',
      windowWidth: window.innerWidth,
      resWidth: null,
      is1280: false,
      is1680: false,
      is1920: false
    };
  },
  watch: {
    'form.mat_group': function formMat_group(newVal, oldVal) {
      if (newVal !== null) {
        this.getBarCode(newVal);
        this.multiGetBarCode(newVal); // this.getMatCode(newVal);
      }

      if (newVal === null) {
        this.form.mat_code = '';
        this.form.barcode = '';
        this.matCode = [];
        this.matBarcode = [];
      }
    },
    'form.barcode': function formBarcode(newVal, oldVal) {
      this.getMatCode(newVal);
    },
    'stocks': function stocks(newVal, oldVal) {
      console.log(newVal);
    },
    windowWidth: function windowWidth(newWidth, oldWidth) {
      this.resWidth = "it changed to ".concat(newWidth, " from ").concat(oldWidth);
      this.checkWidthToAdjust(newWidth);
    },
    deep: true
  },
  mounted: function mounted() {
    var _this = this;

    window.addEventListener('resize', function () {
      _this.windowWidth = window.innerWidth;
    });
  },
  computed: {
    formatDate: function formatDate() {
      // var formatDate = new Date(this.form.req_date);
      // return formatDate;
      var moment = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js"); // require


      moment().format();
      var today = moment();
      this.dateTime = today.format('YYYY-MM-DD');
      this.form.req_date = this.dateTime;
      return this.dateTime;
    },
    currentUser: function currentUser() {
      return this.$store.getters.currentUser;
    }
  },
  created: function created() {
    Reload.$on('AfterCreated', function () {});
    this.getMatGroup();
    this.getBarCode();
    this.getCustCompany();
    this.getStockHeader();
    this.getViewHeader();
    this.checkWidthToAdjust();
  },
  methods: {
    checkWidthToAdjust: function checkWidthToAdjust(newWidth) {
      console.log(newWidth, this.windowWidth);

      if (newWidth === 1280 || this.windowWidth === 1280) {
        this.is1280 = true;
        this.is1680 = false;
        this.is1920 = false;
      } else if (newWidth === 1680 || this.windowWidth === 1680) {
        this.is1680 = true;
        this.is1920 = false;
        this.is1280 = false;
      } else if (newWidth === 1920 || this.windowWidth === 1920) {
        this.is1920 = true;
        this.is1680 = false;
        this.is1280 = false;
      }
    },
    getStockHeader: function getStockHeader(page) {
      var _this2 = this;

      if (typeof page === 'undefined') {
        this.page = 1;
      }

      var uri = "/api/stock-data/".concat(this.$route.params.id, "/view?page=") + page + "&search=" + this.search + "&sortby=" + this.currentSort + "&sortdir=" + this.currentSortDir + "&currentpage=" + this.currentPage;
      this.uri = uri;
      this.page = page;
      axios.get(uri).then(function (response) {
        _this2.stockData = response.data;
        _this2.meta.total = response.data.meta.total;
        _this2.meta.from = response.data.meta.from;
        _this2.meta.to = response.data.meta.to;
      });
    },
    getViewHeader: function getViewHeader() {
      var _this3 = this;

      axios.get("/api/stock-data/header/".concat(this.$route.params.id)).then(function (response) {
        console.log(response.data[0]);
        _this3.headerStockNo = response.data[0].stock_req_no;
        _this3.headerReqDate = response.data[0].stock_req_date;
        _this3.headerSalesOrderDate = response.data[0].sales_order_date;
        _this3.headerCompany = response.data[0].company;
      });
    },
    selectPageNumber: function selectPageNumber(selected) {
      this.currentPage = selected;
      this.getStockHeader();
    },
    addItem: function addItem() {
      var elem = document.createElement('tr');
      this.stocks.push({
        'req_date': this.form.req_date,
        'cust_company': this.form.cust_company,
        'mat_group': this.form.mat_group,
        'mat_code': this.form.mat_code,
        'mat_des': this.form.mat_des,
        'uom': this.form.uom,
        'barcode': this.form.barcode,
        'last_fill_date': this.form.ERDAT,
        'last_fill_qty': this.form.KWMENG,
        'stocks_left': this.multiForm.sStocksLeft,
        'suggest_qty': this.multiForm.sSuggestQty,
        'approve_qty': this.multiForm.sApproveQty
      });
      this.form.barcode = null;
      this.form.mat_code = null;
      this.form.uom = null;
    },
    stockReqCreate: function stockReqCreate() {
      var _this4 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                if (!(_this4.stocks.length !== 0)) {
                  _context.next = 7;
                  break;
                }

                console.log(_this4.stocks);

                _this4.trans_stocks.push({
                  'stocks': _this4.stocks
                });

                _context.next = 5;
                return axios.post('/api/stocks', _this4.trans_stocks).then(function (response) {
                  Reload.$emit('AfterCreated');
                  Notification.success();
                })["catch"](function (error) {
                  _this4.alertSwal(error, '', 'warning', '');
                });

              case 5:
                _context.next = 8;
                break;

              case 7:
                _this4.alertSwal('Cannot save empty transactions', '', 'warning', '');

              case 8:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }))();
    },
    stockReqAsDraft: function stockReqAsDraft() {
      alert('Under Construction');
    },
    rangeMatCode: function rangeMatCode() {
      alert('Under Construction');
    },
    rangeBarcode: function rangeBarcode() {
      alert('Under Construction');
    },
    removeElement: function removeElement(index) {
      this.stocks.splice(index, 1);
    },
    getCustCompany: function getCustCompany() {
      var _this5 = this;

      axios.get('/api/get-customer/stock-creation').then(function (response) {
        _this5.company = response.data;
      });
    },
    multiGetBarCode: function multiGetBarCode(newVal) {
      var _this6 = this;

      if (newVal !== null) {
        var formData = new FormData();
        formData.append('MatGroup', newVal);
        axios.post('/api/get-mat-barcode', formData).then(function (response) {
          _this6.multiBarcode = response.data;
        });
      } else {
        this.multiBarcode = [];
      }
    },
    getBarCode: function getBarCode(newVal) {
      var _this7 = this;

      if (newVal !== null) {
        var formData = new FormData();
        formData.append('MatGroup', newVal);
        axios.post('/api/get-mat-barcode', formData).then(function (response) {
          _this7.matBarcode = response.data;
        });
      } else {
        this.matBarcode = [];
      }
    },
    getMatCode: function getMatCode(newVal) {
      var _this8 = this;

      if (newVal !== null) {
        var formData = new FormData();
        formData.append('barcode', newVal);
        axios.post('/api/get-mat-code', formData).then(function (response) {
          _this8.form.mat_code = response.data.MATNR;
          _this8.form.mat_des = response.data.MAKTX;
          _this8.sales_unit = response.data.MEINH;
          _this8.form.ERDAT = response.data.ERDAT; // last fill up date

          _this8.form.KWMENG = response.data.KWMENG; // last fill up date
        });
      } else {
        this.form.mat_code = null;
      }
    },
    fuseSearch: function fuseSearch(options, search) {
      var fuse = new fuse_js__WEBPACK_IMPORTED_MODULE_3__["default"](options, {
        keys: ["WGBEZ"],
        shouldSort: true
      });
      return search.length ? fuse.search(search).map(function (_ref) {
        var item = _ref.item;
        return item;
      }) : fuse.list;
    },
    fuseSearchMatCode: function fuseSearchMatCode(options, search) {
      var fuse = new fuse_js__WEBPACK_IMPORTED_MODULE_3__["default"](options, {
        keys: ["MATNR", "MAKTX"],
        shouldSort: true
      });
      return search.length ? fuse.search(search).map(function (_ref2) {
        var item = _ref2.item;
        return item;
      }) : fuse.list;
    },
    fuseSearchCompany: function fuseSearchCompany(options, search) {
      var fuse = new fuse_js__WEBPACK_IMPORTED_MODULE_3__["default"](options, {
        keys: ["NAME1"],
        shouldSort: true
      });
      return search.length ? fuse.search(search).map(function (_ref3) {
        var item = _ref3.item;
        return item;
      }) : fuse.list;
    },
    fuseSearchBarcode: function fuseSearchBarcode(options, search) {
      var fuse = new fuse_js__WEBPACK_IMPORTED_MODULE_3__["default"](options, {
        keys: ["EAN11", "MAKTX", "MATNR"],
        shouldSort: true
      });
      return search.length ? fuse.search(search).map(function (_ref4) {
        var item = _ref4.item;
        return item;
      }) : fuse.list;
    },
    getMatGroup: function getMatGroup() {
      var _this9 = this;

      axios.get('/api/get-mat-group').then(function (response) {
        _this9.matGroup = response.data;
      });
    },
    alertSwal: function alertSwal(title, text, icon, timer) {
      this.$swal({
        title: title,
        text: text,
        icon: icon,
        timer: timer
      });
    },
    formatNumber: function formatNumber(number) {
      return number === '' ? '' : parseFloat(Math.abs(number)).toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
    }
  }
});

/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/stock_request/view.vue?vue&type=style&index=0&id=4b873739&scoped=true&lang=css&":
/*!************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/stock_request/view.vue?vue&type=style&index=0&id=4b873739&scoped=true&lang=css& ***!
  \************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n@media (min-width: 992px) {\n.modal-dialog[data-v-4b873739] {\r\n        max-width: 30% !important;\n}\n}\n@media (min-width: 992px) {\n.modal_custom[data-v-4b873739] {\r\n        max-width: 50% !important;\n}\n}\n.input-group-text[data-v-4b873739] {\r\n    background-color: #007bff;\r\n    color: white;\n}\n.sampleClass[data-v-4b873739] {\r\n    background-color: rgba(0, 0, 0, 0.5) !important;\r\n    /*opacity: 0.5 !important;*/\n}\n.example-open .modal-backdrop.show[data-v-4b873739]:nth-of-type(even) {\r\n    opacity: 5 !important;\r\n    z-index: 1052 !important;\n}\n.filter-asc[data-v-4b873739] {\r\n    border-bottom: 5px solid #ccc;\r\n    margin-bottom: 1px\n}\n.filter-asc[data-v-4b873739], .filter-desc[data-v-4b873739] {\r\n    width: 0;\r\n    height: 0;\r\n    border-left: 5px solid transparent;\r\n    border-right: 5px solid transparent\n}\n.filter-desc[data-v-4b873739] {\r\n    border-top: 5px solid #ccc;\r\n    margin-top: 1px\n}\n.inline-block[data-v-4b873739] {\r\n    display: inline-block\n}\n[data-position='below'] .autocomplete-result-list[data-v-4b873739] {\r\n    border-bottom: none;\r\n    border-radius: 8px 8px 0 0;\r\n    color: forestgreen;\r\n    font-weight: bold;\n}\n.wiki-result[data-v-4b873739] {\r\n    border-top: 1px solid #eee;\r\n    padding: 16px;\r\n    background: transparent;\n}\n.wiki-title[data-v-4b873739] {\r\n    font-size: 15px;\r\n    margin-bottom: 8px;\r\n    font-weight: 550;\r\n    color: forestgreen;\n}\n.wiki-snippet[data-v-4b873739] {\r\n    font-size: 14px;\r\n    color: rgba(0, 0, 0, 0.54);\n}\n.wrapAll[data-v-4b873739] {\r\n    white-space: nowrap;\r\n    overflow: hidden;\n}\r\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/stock_request/view.vue?vue&type=style&index=1&lang=css&":
/*!************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/stock_request/view.vue?vue&type=style&index=1&lang=css& ***!
  \************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.matGroupHeight .vs__dropdown-toggle {\r\n    height: 38px;\n}\r\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/moment/locale sync recursive ^\\.\\/.*$":
/*!**************************************************!*\
  !*** ./node_modules/moment/locale sync ^\.\/.*$ ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": "./node_modules/moment/locale/af.js",
	"./af.js": "./node_modules/moment/locale/af.js",
	"./ar": "./node_modules/moment/locale/ar.js",
	"./ar-dz": "./node_modules/moment/locale/ar-dz.js",
	"./ar-dz.js": "./node_modules/moment/locale/ar-dz.js",
	"./ar-kw": "./node_modules/moment/locale/ar-kw.js",
	"./ar-kw.js": "./node_modules/moment/locale/ar-kw.js",
	"./ar-ly": "./node_modules/moment/locale/ar-ly.js",
	"./ar-ly.js": "./node_modules/moment/locale/ar-ly.js",
	"./ar-ma": "./node_modules/moment/locale/ar-ma.js",
	"./ar-ma.js": "./node_modules/moment/locale/ar-ma.js",
	"./ar-sa": "./node_modules/moment/locale/ar-sa.js",
	"./ar-sa.js": "./node_modules/moment/locale/ar-sa.js",
	"./ar-tn": "./node_modules/moment/locale/ar-tn.js",
	"./ar-tn.js": "./node_modules/moment/locale/ar-tn.js",
	"./ar.js": "./node_modules/moment/locale/ar.js",
	"./az": "./node_modules/moment/locale/az.js",
	"./az.js": "./node_modules/moment/locale/az.js",
	"./be": "./node_modules/moment/locale/be.js",
	"./be.js": "./node_modules/moment/locale/be.js",
	"./bg": "./node_modules/moment/locale/bg.js",
	"./bg.js": "./node_modules/moment/locale/bg.js",
	"./bm": "./node_modules/moment/locale/bm.js",
	"./bm.js": "./node_modules/moment/locale/bm.js",
	"./bn": "./node_modules/moment/locale/bn.js",
	"./bn-bd": "./node_modules/moment/locale/bn-bd.js",
	"./bn-bd.js": "./node_modules/moment/locale/bn-bd.js",
	"./bn.js": "./node_modules/moment/locale/bn.js",
	"./bo": "./node_modules/moment/locale/bo.js",
	"./bo.js": "./node_modules/moment/locale/bo.js",
	"./br": "./node_modules/moment/locale/br.js",
	"./br.js": "./node_modules/moment/locale/br.js",
	"./bs": "./node_modules/moment/locale/bs.js",
	"./bs.js": "./node_modules/moment/locale/bs.js",
	"./ca": "./node_modules/moment/locale/ca.js",
	"./ca.js": "./node_modules/moment/locale/ca.js",
	"./cs": "./node_modules/moment/locale/cs.js",
	"./cs.js": "./node_modules/moment/locale/cs.js",
	"./cv": "./node_modules/moment/locale/cv.js",
	"./cv.js": "./node_modules/moment/locale/cv.js",
	"./cy": "./node_modules/moment/locale/cy.js",
	"./cy.js": "./node_modules/moment/locale/cy.js",
	"./da": "./node_modules/moment/locale/da.js",
	"./da.js": "./node_modules/moment/locale/da.js",
	"./de": "./node_modules/moment/locale/de.js",
	"./de-at": "./node_modules/moment/locale/de-at.js",
	"./de-at.js": "./node_modules/moment/locale/de-at.js",
	"./de-ch": "./node_modules/moment/locale/de-ch.js",
	"./de-ch.js": "./node_modules/moment/locale/de-ch.js",
	"./de.js": "./node_modules/moment/locale/de.js",
	"./dv": "./node_modules/moment/locale/dv.js",
	"./dv.js": "./node_modules/moment/locale/dv.js",
	"./el": "./node_modules/moment/locale/el.js",
	"./el.js": "./node_modules/moment/locale/el.js",
	"./en-au": "./node_modules/moment/locale/en-au.js",
	"./en-au.js": "./node_modules/moment/locale/en-au.js",
	"./en-ca": "./node_modules/moment/locale/en-ca.js",
	"./en-ca.js": "./node_modules/moment/locale/en-ca.js",
	"./en-gb": "./node_modules/moment/locale/en-gb.js",
	"./en-gb.js": "./node_modules/moment/locale/en-gb.js",
	"./en-ie": "./node_modules/moment/locale/en-ie.js",
	"./en-ie.js": "./node_modules/moment/locale/en-ie.js",
	"./en-il": "./node_modules/moment/locale/en-il.js",
	"./en-il.js": "./node_modules/moment/locale/en-il.js",
	"./en-in": "./node_modules/moment/locale/en-in.js",
	"./en-in.js": "./node_modules/moment/locale/en-in.js",
	"./en-nz": "./node_modules/moment/locale/en-nz.js",
	"./en-nz.js": "./node_modules/moment/locale/en-nz.js",
	"./en-sg": "./node_modules/moment/locale/en-sg.js",
	"./en-sg.js": "./node_modules/moment/locale/en-sg.js",
	"./eo": "./node_modules/moment/locale/eo.js",
	"./eo.js": "./node_modules/moment/locale/eo.js",
	"./es": "./node_modules/moment/locale/es.js",
	"./es-do": "./node_modules/moment/locale/es-do.js",
	"./es-do.js": "./node_modules/moment/locale/es-do.js",
	"./es-mx": "./node_modules/moment/locale/es-mx.js",
	"./es-mx.js": "./node_modules/moment/locale/es-mx.js",
	"./es-us": "./node_modules/moment/locale/es-us.js",
	"./es-us.js": "./node_modules/moment/locale/es-us.js",
	"./es.js": "./node_modules/moment/locale/es.js",
	"./et": "./node_modules/moment/locale/et.js",
	"./et.js": "./node_modules/moment/locale/et.js",
	"./eu": "./node_modules/moment/locale/eu.js",
	"./eu.js": "./node_modules/moment/locale/eu.js",
	"./fa": "./node_modules/moment/locale/fa.js",
	"./fa.js": "./node_modules/moment/locale/fa.js",
	"./fi": "./node_modules/moment/locale/fi.js",
	"./fi.js": "./node_modules/moment/locale/fi.js",
	"./fil": "./node_modules/moment/locale/fil.js",
	"./fil.js": "./node_modules/moment/locale/fil.js",
	"./fo": "./node_modules/moment/locale/fo.js",
	"./fo.js": "./node_modules/moment/locale/fo.js",
	"./fr": "./node_modules/moment/locale/fr.js",
	"./fr-ca": "./node_modules/moment/locale/fr-ca.js",
	"./fr-ca.js": "./node_modules/moment/locale/fr-ca.js",
	"./fr-ch": "./node_modules/moment/locale/fr-ch.js",
	"./fr-ch.js": "./node_modules/moment/locale/fr-ch.js",
	"./fr.js": "./node_modules/moment/locale/fr.js",
	"./fy": "./node_modules/moment/locale/fy.js",
	"./fy.js": "./node_modules/moment/locale/fy.js",
	"./ga": "./node_modules/moment/locale/ga.js",
	"./ga.js": "./node_modules/moment/locale/ga.js",
	"./gd": "./node_modules/moment/locale/gd.js",
	"./gd.js": "./node_modules/moment/locale/gd.js",
	"./gl": "./node_modules/moment/locale/gl.js",
	"./gl.js": "./node_modules/moment/locale/gl.js",
	"./gom-deva": "./node_modules/moment/locale/gom-deva.js",
	"./gom-deva.js": "./node_modules/moment/locale/gom-deva.js",
	"./gom-latn": "./node_modules/moment/locale/gom-latn.js",
	"./gom-latn.js": "./node_modules/moment/locale/gom-latn.js",
	"./gu": "./node_modules/moment/locale/gu.js",
	"./gu.js": "./node_modules/moment/locale/gu.js",
	"./he": "./node_modules/moment/locale/he.js",
	"./he.js": "./node_modules/moment/locale/he.js",
	"./hi": "./node_modules/moment/locale/hi.js",
	"./hi.js": "./node_modules/moment/locale/hi.js",
	"./hr": "./node_modules/moment/locale/hr.js",
	"./hr.js": "./node_modules/moment/locale/hr.js",
	"./hu": "./node_modules/moment/locale/hu.js",
	"./hu.js": "./node_modules/moment/locale/hu.js",
	"./hy-am": "./node_modules/moment/locale/hy-am.js",
	"./hy-am.js": "./node_modules/moment/locale/hy-am.js",
	"./id": "./node_modules/moment/locale/id.js",
	"./id.js": "./node_modules/moment/locale/id.js",
	"./is": "./node_modules/moment/locale/is.js",
	"./is.js": "./node_modules/moment/locale/is.js",
	"./it": "./node_modules/moment/locale/it.js",
	"./it-ch": "./node_modules/moment/locale/it-ch.js",
	"./it-ch.js": "./node_modules/moment/locale/it-ch.js",
	"./it.js": "./node_modules/moment/locale/it.js",
	"./ja": "./node_modules/moment/locale/ja.js",
	"./ja.js": "./node_modules/moment/locale/ja.js",
	"./jv": "./node_modules/moment/locale/jv.js",
	"./jv.js": "./node_modules/moment/locale/jv.js",
	"./ka": "./node_modules/moment/locale/ka.js",
	"./ka.js": "./node_modules/moment/locale/ka.js",
	"./kk": "./node_modules/moment/locale/kk.js",
	"./kk.js": "./node_modules/moment/locale/kk.js",
	"./km": "./node_modules/moment/locale/km.js",
	"./km.js": "./node_modules/moment/locale/km.js",
	"./kn": "./node_modules/moment/locale/kn.js",
	"./kn.js": "./node_modules/moment/locale/kn.js",
	"./ko": "./node_modules/moment/locale/ko.js",
	"./ko.js": "./node_modules/moment/locale/ko.js",
	"./ku": "./node_modules/moment/locale/ku.js",
	"./ku.js": "./node_modules/moment/locale/ku.js",
	"./ky": "./node_modules/moment/locale/ky.js",
	"./ky.js": "./node_modules/moment/locale/ky.js",
	"./lb": "./node_modules/moment/locale/lb.js",
	"./lb.js": "./node_modules/moment/locale/lb.js",
	"./lo": "./node_modules/moment/locale/lo.js",
	"./lo.js": "./node_modules/moment/locale/lo.js",
	"./lt": "./node_modules/moment/locale/lt.js",
	"./lt.js": "./node_modules/moment/locale/lt.js",
	"./lv": "./node_modules/moment/locale/lv.js",
	"./lv.js": "./node_modules/moment/locale/lv.js",
	"./me": "./node_modules/moment/locale/me.js",
	"./me.js": "./node_modules/moment/locale/me.js",
	"./mi": "./node_modules/moment/locale/mi.js",
	"./mi.js": "./node_modules/moment/locale/mi.js",
	"./mk": "./node_modules/moment/locale/mk.js",
	"./mk.js": "./node_modules/moment/locale/mk.js",
	"./ml": "./node_modules/moment/locale/ml.js",
	"./ml.js": "./node_modules/moment/locale/ml.js",
	"./mn": "./node_modules/moment/locale/mn.js",
	"./mn.js": "./node_modules/moment/locale/mn.js",
	"./mr": "./node_modules/moment/locale/mr.js",
	"./mr.js": "./node_modules/moment/locale/mr.js",
	"./ms": "./node_modules/moment/locale/ms.js",
	"./ms-my": "./node_modules/moment/locale/ms-my.js",
	"./ms-my.js": "./node_modules/moment/locale/ms-my.js",
	"./ms.js": "./node_modules/moment/locale/ms.js",
	"./mt": "./node_modules/moment/locale/mt.js",
	"./mt.js": "./node_modules/moment/locale/mt.js",
	"./my": "./node_modules/moment/locale/my.js",
	"./my.js": "./node_modules/moment/locale/my.js",
	"./nb": "./node_modules/moment/locale/nb.js",
	"./nb.js": "./node_modules/moment/locale/nb.js",
	"./ne": "./node_modules/moment/locale/ne.js",
	"./ne.js": "./node_modules/moment/locale/ne.js",
	"./nl": "./node_modules/moment/locale/nl.js",
	"./nl-be": "./node_modules/moment/locale/nl-be.js",
	"./nl-be.js": "./node_modules/moment/locale/nl-be.js",
	"./nl.js": "./node_modules/moment/locale/nl.js",
	"./nn": "./node_modules/moment/locale/nn.js",
	"./nn.js": "./node_modules/moment/locale/nn.js",
	"./oc-lnc": "./node_modules/moment/locale/oc-lnc.js",
	"./oc-lnc.js": "./node_modules/moment/locale/oc-lnc.js",
	"./pa-in": "./node_modules/moment/locale/pa-in.js",
	"./pa-in.js": "./node_modules/moment/locale/pa-in.js",
	"./pl": "./node_modules/moment/locale/pl.js",
	"./pl.js": "./node_modules/moment/locale/pl.js",
	"./pt": "./node_modules/moment/locale/pt.js",
	"./pt-br": "./node_modules/moment/locale/pt-br.js",
	"./pt-br.js": "./node_modules/moment/locale/pt-br.js",
	"./pt.js": "./node_modules/moment/locale/pt.js",
	"./ro": "./node_modules/moment/locale/ro.js",
	"./ro.js": "./node_modules/moment/locale/ro.js",
	"./ru": "./node_modules/moment/locale/ru.js",
	"./ru.js": "./node_modules/moment/locale/ru.js",
	"./sd": "./node_modules/moment/locale/sd.js",
	"./sd.js": "./node_modules/moment/locale/sd.js",
	"./se": "./node_modules/moment/locale/se.js",
	"./se.js": "./node_modules/moment/locale/se.js",
	"./si": "./node_modules/moment/locale/si.js",
	"./si.js": "./node_modules/moment/locale/si.js",
	"./sk": "./node_modules/moment/locale/sk.js",
	"./sk.js": "./node_modules/moment/locale/sk.js",
	"./sl": "./node_modules/moment/locale/sl.js",
	"./sl.js": "./node_modules/moment/locale/sl.js",
	"./sq": "./node_modules/moment/locale/sq.js",
	"./sq.js": "./node_modules/moment/locale/sq.js",
	"./sr": "./node_modules/moment/locale/sr.js",
	"./sr-cyrl": "./node_modules/moment/locale/sr-cyrl.js",
	"./sr-cyrl.js": "./node_modules/moment/locale/sr-cyrl.js",
	"./sr.js": "./node_modules/moment/locale/sr.js",
	"./ss": "./node_modules/moment/locale/ss.js",
	"./ss.js": "./node_modules/moment/locale/ss.js",
	"./sv": "./node_modules/moment/locale/sv.js",
	"./sv.js": "./node_modules/moment/locale/sv.js",
	"./sw": "./node_modules/moment/locale/sw.js",
	"./sw.js": "./node_modules/moment/locale/sw.js",
	"./ta": "./node_modules/moment/locale/ta.js",
	"./ta.js": "./node_modules/moment/locale/ta.js",
	"./te": "./node_modules/moment/locale/te.js",
	"./te.js": "./node_modules/moment/locale/te.js",
	"./tet": "./node_modules/moment/locale/tet.js",
	"./tet.js": "./node_modules/moment/locale/tet.js",
	"./tg": "./node_modules/moment/locale/tg.js",
	"./tg.js": "./node_modules/moment/locale/tg.js",
	"./th": "./node_modules/moment/locale/th.js",
	"./th.js": "./node_modules/moment/locale/th.js",
	"./tk": "./node_modules/moment/locale/tk.js",
	"./tk.js": "./node_modules/moment/locale/tk.js",
	"./tl-ph": "./node_modules/moment/locale/tl-ph.js",
	"./tl-ph.js": "./node_modules/moment/locale/tl-ph.js",
	"./tlh": "./node_modules/moment/locale/tlh.js",
	"./tlh.js": "./node_modules/moment/locale/tlh.js",
	"./tr": "./node_modules/moment/locale/tr.js",
	"./tr.js": "./node_modules/moment/locale/tr.js",
	"./tzl": "./node_modules/moment/locale/tzl.js",
	"./tzl.js": "./node_modules/moment/locale/tzl.js",
	"./tzm": "./node_modules/moment/locale/tzm.js",
	"./tzm-latn": "./node_modules/moment/locale/tzm-latn.js",
	"./tzm-latn.js": "./node_modules/moment/locale/tzm-latn.js",
	"./tzm.js": "./node_modules/moment/locale/tzm.js",
	"./ug-cn": "./node_modules/moment/locale/ug-cn.js",
	"./ug-cn.js": "./node_modules/moment/locale/ug-cn.js",
	"./uk": "./node_modules/moment/locale/uk.js",
	"./uk.js": "./node_modules/moment/locale/uk.js",
	"./ur": "./node_modules/moment/locale/ur.js",
	"./ur.js": "./node_modules/moment/locale/ur.js",
	"./uz": "./node_modules/moment/locale/uz.js",
	"./uz-latn": "./node_modules/moment/locale/uz-latn.js",
	"./uz-latn.js": "./node_modules/moment/locale/uz-latn.js",
	"./uz.js": "./node_modules/moment/locale/uz.js",
	"./vi": "./node_modules/moment/locale/vi.js",
	"./vi.js": "./node_modules/moment/locale/vi.js",
	"./x-pseudo": "./node_modules/moment/locale/x-pseudo.js",
	"./x-pseudo.js": "./node_modules/moment/locale/x-pseudo.js",
	"./yo": "./node_modules/moment/locale/yo.js",
	"./yo.js": "./node_modules/moment/locale/yo.js",
	"./zh-cn": "./node_modules/moment/locale/zh-cn.js",
	"./zh-cn.js": "./node_modules/moment/locale/zh-cn.js",
	"./zh-hk": "./node_modules/moment/locale/zh-hk.js",
	"./zh-hk.js": "./node_modules/moment/locale/zh-hk.js",
	"./zh-mo": "./node_modules/moment/locale/zh-mo.js",
	"./zh-mo.js": "./node_modules/moment/locale/zh-mo.js",
	"./zh-tw": "./node_modules/moment/locale/zh-tw.js",
	"./zh-tw.js": "./node_modules/moment/locale/zh-tw.js"
};


function webpackContext(req) {
	var id = webpackContextResolve(req);
	return __webpack_require__(id);
}
function webpackContextResolve(req) {
	if(!__webpack_require__.o(map, req)) {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	}
	return map[req];
}
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = "./node_modules/moment/locale sync recursive ^\\.\\/.*$";

/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/stock_request/view.vue?vue&type=style&index=0&id=4b873739&scoped=true&lang=css&":
/*!****************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/stock_request/view.vue?vue&type=style&index=0&id=4b873739&scoped=true&lang=css& ***!
  \****************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../node_modules/css-loader??ref--6-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--6-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./view.vue?vue&type=style&index=0&id=4b873739&scoped=true&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/stock_request/view.vue?vue&type=style&index=0&id=4b873739&scoped=true&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/stock_request/view.vue?vue&type=style&index=1&lang=css&":
/*!****************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/stock_request/view.vue?vue&type=style&index=1&lang=css& ***!
  \****************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../node_modules/css-loader??ref--6-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--6-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./view.vue?vue&type=style&index=1&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/stock_request/view.vue?vue&type=style&index=1&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/stock_request/view.vue?vue&type=template&id=4b873739&scoped=true&":
/*!*********************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/stock_request/view.vue?vue&type=template&id=4b873739&scoped=true& ***!
  \*********************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c("div", { staticClass: "row" }, [
      _c("div", { staticClass: "col-lg-12" }, [
        _c("div", { staticClass: "card shadow-sm mb-4" }, [
          _vm._m(0),
          _vm._v(" "),
          _c("div", { staticClass: "card-header" }, [
            _c("div", { staticClass: "row form-group" }, [
              _vm.windowWidth === 1280 && _vm.windowWidth !== 962
                ? _c("div", { staticClass: "col-md-4" }, [
                    _c("div", { staticClass: "input-group" }, [
                      _vm._m(1),
                      _vm._v(" "),
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: _vm.headerStockNo,
                            expression: "headerStockNo"
                          }
                        ],
                        staticClass: "form-control",
                        staticStyle: { "text-align": "right" },
                        attrs: { type: "text", disabled: "" },
                        domProps: { value: _vm.headerStockNo },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.headerStockNo = $event.target.value
                          }
                        }
                      })
                    ])
                  ])
                : _vm._e(),
              _vm._v(" "),
              _vm.windowWidth !== 1280 && _vm.windowWidth === 962
                ? _c("div", { staticClass: "col-md-5" }, [
                    _c("div", { staticClass: "input-group" }, [
                      _vm._m(2),
                      _vm._v(" "),
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: _vm.headerStockNo,
                            expression: "headerStockNo"
                          }
                        ],
                        staticClass: "form-control",
                        staticStyle: { "text-align": "right" },
                        attrs: { type: "text", disabled: "" },
                        domProps: { value: _vm.headerStockNo },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.headerStockNo = $event.target.value
                          }
                        }
                      })
                    ])
                  ])
                : _vm._e(),
              _vm._v(" "),
              _vm.windowWidth !== 1280 && _vm.windowWidth !== 962
                ? _c("div", { staticClass: "col-md-3" }, [
                    _c("div", { staticClass: "input-group" }, [
                      _vm._m(3),
                      _vm._v(" "),
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: _vm.headerStockNo,
                            expression: "headerStockNo"
                          }
                        ],
                        staticClass: "form-control",
                        staticStyle: { "text-align": "right" },
                        attrs: { type: "text", disabled: "" },
                        domProps: { value: _vm.headerStockNo },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.headerStockNo = $event.target.value
                          }
                        }
                      })
                    ])
                  ])
                : _vm._e(),
              _vm._v(" "),
              _vm.windowWidth !== 1280 && _vm.windowWidth !== 962
                ? _c("div", { staticClass: "col-md-1" })
                : _vm._e(),
              _vm._v(" "),
              _vm.windowWidth === 1280 && _vm.windowWidth !== 962
                ? _c("div", { staticClass: "col-md-4" }, [
                    _c("div", { staticClass: "input-group" }, [
                      _vm._m(4),
                      _vm._v(" "),
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: _vm.headerSalesOrderDate,
                            expression: "headerSalesOrderDate"
                          }
                        ],
                        staticClass: "form-control",
                        staticStyle: { "text-align": "right" },
                        attrs: { type: "text", disabled: "" },
                        domProps: { value: _vm.headerSalesOrderDate },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.headerSalesOrderDate = $event.target.value
                          }
                        }
                      })
                    ])
                  ])
                : _vm._e(),
              _vm._v(" "),
              _vm.windowWidth !== 1280 && _vm.windowWidth === 962
                ? _c("div", { staticClass: "col-md-5" }, [
                    _c("div", { staticClass: "input-group" }, [
                      _vm._m(5),
                      _vm._v(" "),
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: _vm.headerSalesOrderDate,
                            expression: "headerSalesOrderDate"
                          }
                        ],
                        staticClass: "form-control",
                        staticStyle: { "text-align": "right" },
                        attrs: { type: "text", disabled: "" },
                        domProps: { value: _vm.headerSalesOrderDate },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.headerSalesOrderDate = $event.target.value
                          }
                        }
                      })
                    ])
                  ])
                : _vm._e(),
              _vm._v(" "),
              _vm.windowWidth !== 1280 && _vm.windowWidth !== 962
                ? _c("div", { staticClass: "col-md-3" }, [
                    _c("div", { staticClass: "input-group" }, [
                      _vm._m(6),
                      _vm._v(" "),
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: _vm.headerSalesOrderDate,
                            expression: "headerSalesOrderDate"
                          }
                        ],
                        staticClass: "form-control",
                        staticStyle: { "text-align": "right" },
                        attrs: { type: "text", disabled: "" },
                        domProps: { value: _vm.headerSalesOrderDate },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.headerSalesOrderDate = $event.target.value
                          }
                        }
                      })
                    ])
                  ])
                : _vm._e()
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "row form-group" }, [
              _vm.windowWidth === 1280 && _vm.windowWidth !== 962
                ? _c("div", { staticClass: "col-md-4" }, [
                    _c("div", { staticClass: "input-group" }, [
                      _vm._m(7),
                      _vm._v(" "),
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: _vm.headerReqDate,
                            expression: "headerReqDate"
                          }
                        ],
                        staticClass: "form-control",
                        staticStyle: { "text-align": "right" },
                        attrs: { type: "text", disabled: "" },
                        domProps: { value: _vm.headerReqDate },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.headerReqDate = $event.target.value
                          }
                        }
                      })
                    ])
                  ])
                : _vm._e(),
              _vm._v(" "),
              _vm.windowWidth !== 1280 && _vm.windowWidth === 962
                ? _c("div", { staticClass: "col-md-5" }, [
                    _c("div", { staticClass: "input-group" }, [
                      _vm._m(8),
                      _vm._v(" "),
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: _vm.headerReqDate,
                            expression: "headerReqDate"
                          }
                        ],
                        staticClass: "form-control",
                        staticStyle: { "text-align": "right" },
                        attrs: { type: "text", disabled: "" },
                        domProps: { value: _vm.headerReqDate },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.headerReqDate = $event.target.value
                          }
                        }
                      })
                    ])
                  ])
                : _vm._e(),
              _vm._v(" "),
              _vm.windowWidth !== 1280 && _vm.windowWidth !== 962
                ? _c("div", { staticClass: "col-md-3" }, [
                    _c("div", { staticClass: "input-group" }, [
                      _vm._m(9),
                      _vm._v(" "),
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: _vm.headerReqDate,
                            expression: "headerReqDate"
                          }
                        ],
                        staticClass: "form-control",
                        staticStyle: { "text-align": "right" },
                        attrs: { type: "text", disabled: "" },
                        domProps: { value: _vm.headerReqDate },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.headerReqDate = $event.target.value
                          }
                        }
                      })
                    ])
                  ])
                : _vm._e(),
              _vm._v(" "),
              _vm.windowWidth !== 1280 && _vm.windowWidth !== 962
                ? _c("div", { staticClass: "col-md-1" })
                : _vm._e(),
              _vm._v(" "),
              _vm.windowWidth === 1280 && _vm.windowWidth !== 962
                ? _c("div", { staticClass: "col-md-6" }, [
                    _c("div", { staticClass: "input-group" }, [
                      _vm._m(10),
                      _vm._v(" "),
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: _vm.headerCompany,
                            expression: "headerCompany"
                          }
                        ],
                        staticClass: "form-control",
                        attrs: { type: "text", disabled: "" },
                        domProps: { value: _vm.headerCompany },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.headerCompany = $event.target.value
                          }
                        }
                      })
                    ])
                  ])
                : _vm._e(),
              _vm._v(" "),
              _vm.windowWidth !== 1280 && _vm.windowWidth === 962
                ? _c("div", { staticClass: "col-md-7" }, [
                    _c("div", { staticClass: "input-group" }, [
                      _vm._m(11),
                      _vm._v(" "),
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: _vm.headerCompany,
                            expression: "headerCompany"
                          }
                        ],
                        staticClass: "form-control",
                        attrs: { type: "text", disabled: "" },
                        domProps: { value: _vm.headerCompany },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.headerCompany = $event.target.value
                          }
                        }
                      })
                    ])
                  ])
                : _vm._e(),
              _vm._v(" "),
              _vm.windowWidth !== 1280 && _vm.windowWidth !== 962
                ? _c("div", { staticClass: "col-md-4" }, [
                    _c("div", { staticClass: "input-group" }, [
                      _vm._m(12),
                      _vm._v(" "),
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: _vm.headerCompany,
                            expression: "headerCompany"
                          }
                        ],
                        staticClass: "form-control",
                        attrs: { type: "text", disabled: "" },
                        domProps: { value: _vm.headerCompany },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.headerCompany = $event.target.value
                          }
                        }
                      })
                    ])
                  ])
                : _vm._e(),
              _vm._v(" "),
              _c("div", { staticClass: "col-md-3" })
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "input-group row-md-12 mb-4" }, [
            _c("br"),
            _vm._v(" "),
            _c("div", { staticClass: "col-md-1" }, [
              _c(
                "label",
                { staticStyle: { "margin-left": "9px", "margin-top": "3px" } },
                [_vm._v("Per Page")]
              ),
              _vm._v(" "),
              _c(
                "select",
                {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.currentPage,
                      expression: "currentPage"
                    }
                  ],
                  staticClass: "form-control",
                  staticStyle: {
                    margin: "11px 6px 0px 6px",
                    top: "20px",
                    position: "absolute"
                  },
                  on: {
                    change: [
                      function($event) {
                        var $$selectedVal = Array.prototype.filter
                          .call($event.target.options, function(o) {
                            return o.selected
                          })
                          .map(function(o) {
                            var val = "_value" in o ? o._value : o.value
                            return val
                          })
                        _vm.currentPage = $event.target.multiple
                          ? $$selectedVal
                          : $$selectedVal[0]
                      },
                      function($event) {
                        return _vm.selectPageNumber(_vm.currentPage)
                      }
                    ]
                  }
                },
                _vm._l(_vm.perPage, function(perPage) {
                  return _c(
                    "option",
                    {
                      domProps: {
                        value: perPage,
                        selected: _vm.currentPage === perPage
                      }
                    },
                    [_vm._v(_vm._s(perPage) + "\n                            ")]
                  )
                }),
                0
              )
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "col-md-2" })
          ]),
          _vm._v(" "),
          _c(
            "div",
            { staticClass: "card-body" },
            [
              _c("div", { staticClass: "table-responsive" }, [
                _c(
                  "table",
                  {
                    staticClass:
                      "table align-items-center table-bordered table-flush table-hover",
                    attrs: { id: "dataTable", width: "100%", cellspacing: "0" }
                  },
                  [
                    _vm._m(13),
                    _vm._v(" "),
                    _c(
                      "tbody",
                      _vm._l(_vm.stockData.data, function(stock, index) {
                        return _c(
                          "tr",
                          { key: stock.id, attrs: { index: index } },
                          [
                            _c("td", { staticClass: "wrapAll" }, [
                              _vm._v(_vm._s(stock.barcode))
                            ]),
                            _vm._v(" "),
                            _c("td", { staticClass: "wrapAll" }, [
                              _vm._v(_vm._s(stock.mat_code))
                            ]),
                            _vm._v(" "),
                            _c("td", { staticClass: "wrapAll" }, [
                              _vm._v(_vm._s(stock.mat_des))
                            ]),
                            _vm._v(" "),
                            _c("td", { staticClass: "wrapAll" }, [
                              _vm._v(" " + _vm._s(stock.sales_unit))
                            ]),
                            _vm._v(" "),
                            _c(
                              "td",
                              {
                                staticClass: "wrapAll",
                                staticStyle: { "text-align": "center" }
                              },
                              [
                                _vm._v(
                                  _vm._s(
                                    _vm._f("moment")(stock.ERDAT, "MM/DD/YYYY")
                                  ) + "\n                                "
                                )
                              ]
                            ),
                            _vm._v(" "),
                            _c(
                              "td",
                              {
                                staticClass: "wrapAll",
                                staticStyle: { "text-align": "right" }
                              },
                              [
                                _vm._v(
                                  " " + _vm._s(_vm.formatNumber(stock.KWMENG))
                                )
                              ]
                            ),
                            _vm._v(" "),
                            _c(
                              "td",
                              {
                                staticClass: "wrapAll",
                                staticStyle: { "text-align": "right" }
                              },
                              [
                                _vm._v(
                                  _vm._s(_vm.formatNumber(stock.stocks_left)) +
                                    "\n                                "
                                )
                              ]
                            ),
                            _vm._v(" "),
                            _c(
                              "td",
                              {
                                staticClass: "wrapAll",
                                staticStyle: { "text-align": "right" }
                              },
                              [
                                _vm._v(
                                  _vm._s(_vm.formatNumber(stock.order_qty)) +
                                    "\n                                "
                                )
                              ]
                            ),
                            _vm._v(" "),
                            _c(
                              "td",
                              {
                                staticClass: "wrapAll",
                                staticStyle: { "text-align": "right" }
                              },
                              [
                                _vm._v(
                                  _vm._s(_vm.formatNumber(stock.approve_qty)) +
                                    "\n                                "
                                )
                              ]
                            )
                          ]
                        )
                      }),
                      0
                    )
                  ]
                ),
                _vm._v(" "),
                _c("p", { staticClass: "float-left" }, [
                  _vm._v(
                    "Showing " +
                      _vm._s(_vm.meta.from || 0) +
                      " to " +
                      _vm._s(_vm.meta.to || 0) +
                      " of " +
                      _vm._s(_vm.meta.total || 0) +
                      " entries"
                  )
                ])
              ]),
              _vm._v(" "),
              _c(
                "router-link",
                {
                  staticClass: "btn btn-danger",
                  staticStyle: { "margin-top": "-15px" },
                  attrs: {
                    to: {
                      name: "stock-request-table",
                      query: {
                        stockRequest: "DataTable",
                        id: _vm.currentUser.token
                      }
                    }
                  }
                },
                [_vm._v("Back")]
              ),
              _vm._v(" "),
              _c("pagination", {
                staticClass: "float-right",
                attrs: { data: _vm.stockData, limit: 5 },
                on: { "pagination-change-page": _vm.getStockHeader }
              })
            ],
            1
          )
        ])
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "card-header" }, [
      _c("h4", { staticClass: "m-0 font-weight-bold text-primary" }, [
        _vm._v("STOCK REQUEST VIEW DETAILS")
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "input-group-prepend" }, [
      _c(
        "span",
        {
          staticClass: "input-group-text input-group-field",
          attrs: { id: "basic-addon1" }
        },
        [_vm._v("Stock Request Number:")]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "input-group-prepend" }, [
      _c(
        "span",
        {
          staticClass: "input-group-text input-group-field",
          attrs: { id: "basic-addon1" }
        },
        [_vm._v("Stock Request Number:")]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "input-group-prepend" }, [
      _c(
        "span",
        {
          staticClass: "input-group-text input-group-field",
          attrs: { id: "basic-addon1" }
        },
        [_vm._v("Stock Request Number:")]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "input-group-prepend" }, [
      _c(
        "span",
        {
          staticClass: "input-group-text input-group-field",
          attrs: { id: "basic-addon1" }
        },
        [_vm._v("Sales Order Date:")]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "input-group-prepend" }, [
      _c(
        "span",
        {
          staticClass: "input-group-text input-group-field",
          attrs: { id: "basic-addon1" }
        },
        [_vm._v("Sales Order Date:")]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "input-group-prepend" }, [
      _c(
        "span",
        {
          staticClass: "input-group-text input-group-field",
          attrs: { id: "basic-addon1" }
        },
        [_vm._v("Sales Order Date:")]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "input-group-prepend" }, [
      _c(
        "span",
        {
          staticClass: "input-group-text input-group-field",
          attrs: { id: "basic-addon1" }
        },
        [_vm._v("Request Date:")]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "input-group-prepend" }, [
      _c(
        "span",
        {
          staticClass: "input-group-text input-group-field",
          attrs: { id: "basic-addon1" }
        },
        [_vm._v("Request Date:")]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "input-group-prepend" }, [
      _c(
        "span",
        {
          staticClass: "input-group-text input-group-field",
          attrs: { id: "basic-addon1" }
        },
        [_vm._v("Request Date:")]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "input-group-prepend" }, [
      _c(
        "span",
        {
          staticClass: "input-group-text input-group-field",
          attrs: { id: "basic-addon1" }
        },
        [_vm._v("Company:")]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "input-group-prepend" }, [
      _c(
        "span",
        {
          staticClass: "input-group-text input-group-field",
          attrs: { id: "basic-addon1" }
        },
        [_vm._v("Company:")]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "input-group-prepend" }, [
      _c(
        "span",
        {
          staticClass: "input-group-text input-group-field",
          attrs: { id: "basic-addon1" }
        },
        [_vm._v("Company:")]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("thead", [
      _c("tr", { staticClass: "bg-primary" }, [
        _c("th", { staticClass: "wrapAll", staticStyle: { color: "white" } }, [
          _vm._v("BARCODE")
        ]),
        _vm._v(" "),
        _c("th", { staticClass: "wrapAll", staticStyle: { color: "white" } }, [
          _vm._v("MATERIAL CODE")
        ]),
        _vm._v(" "),
        _c("th", { staticClass: "wrapAll", staticStyle: { color: "white" } }, [
          _vm._v("MATERIAL DESCRIPTION")
        ]),
        _vm._v(" "),
        _c("th", { staticClass: "wrapAll", staticStyle: { color: "white" } }, [
          _vm._v("UOM")
        ]),
        _vm._v(" "),
        _c(
          "th",
          {
            staticClass: "wrapAll",
            staticStyle: { color: "white", "text-align": "center" }
          },
          [_vm._v("LAST FILL-UP DATE")]
        ),
        _vm._v(" "),
        _c(
          "th",
          {
            staticClass: "wrapAll",
            staticStyle: { color: "white", "text-align": "right" }
          },
          [_vm._v("LAST FILL-UP QTY")]
        ),
        _vm._v(" "),
        _c(
          "th",
          {
            staticClass: "wrapAll",
            staticStyle: { color: "white", "text-align": "right" }
          },
          [_vm._v("STOCKS LEFT")]
        ),
        _vm._v(" "),
        _c(
          "th",
          {
            staticClass: "wrapAll",
            staticStyle: { color: "white", "text-align": "right" }
          },
          [_vm._v("SUGGESTED ORDER QTY")]
        ),
        _vm._v(" "),
        _c(
          "th",
          {
            staticClass: "wrapAll",
            staticStyle: { color: "white", "text-align": "right" }
          },
          [_vm._v("APPROVED QTY")]
        )
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/components/stock_request/view.vue":
/*!********************************************************!*\
  !*** ./resources/js/components/stock_request/view.vue ***!
  \********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _view_vue_vue_type_template_id_4b873739_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./view.vue?vue&type=template&id=4b873739&scoped=true& */ "./resources/js/components/stock_request/view.vue?vue&type=template&id=4b873739&scoped=true&");
/* harmony import */ var _view_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./view.vue?vue&type=script&lang=js& */ "./resources/js/components/stock_request/view.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _view_vue_vue_type_style_index_0_id_4b873739_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./view.vue?vue&type=style&index=0&id=4b873739&scoped=true&lang=css& */ "./resources/js/components/stock_request/view.vue?vue&type=style&index=0&id=4b873739&scoped=true&lang=css&");
/* harmony import */ var _view_vue_vue_type_style_index_1_lang_css___WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./view.vue?vue&type=style&index=1&lang=css& */ "./resources/js/components/stock_request/view.vue?vue&type=style&index=1&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");







/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_4__["default"])(
  _view_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _view_vue_vue_type_template_id_4b873739_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _view_vue_vue_type_template_id_4b873739_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "4b873739",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/stock_request/view.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/stock_request/view.vue?vue&type=script&lang=js&":
/*!*********************************************************************************!*\
  !*** ./resources/js/components/stock_request/view.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_view_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./view.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/stock_request/view.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_view_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/stock_request/view.vue?vue&type=style&index=0&id=4b873739&scoped=true&lang=css&":
/*!*****************************************************************************************************************!*\
  !*** ./resources/js/components/stock_request/view.vue?vue&type=style&index=0&id=4b873739&scoped=true&lang=css& ***!
  \*****************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_view_vue_vue_type_style_index_0_id_4b873739_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/style-loader!../../../../node_modules/css-loader??ref--6-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--6-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./view.vue?vue&type=style&index=0&id=4b873739&scoped=true&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/stock_request/view.vue?vue&type=style&index=0&id=4b873739&scoped=true&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_view_vue_vue_type_style_index_0_id_4b873739_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_view_vue_vue_type_style_index_0_id_4b873739_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_view_vue_vue_type_style_index_0_id_4b873739_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_view_vue_vue_type_style_index_0_id_4b873739_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ "./resources/js/components/stock_request/view.vue?vue&type=style&index=1&lang=css&":
/*!*****************************************************************************************!*\
  !*** ./resources/js/components/stock_request/view.vue?vue&type=style&index=1&lang=css& ***!
  \*****************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_view_vue_vue_type_style_index_1_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/style-loader!../../../../node_modules/css-loader??ref--6-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--6-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./view.vue?vue&type=style&index=1&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/stock_request/view.vue?vue&type=style&index=1&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_view_vue_vue_type_style_index_1_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_view_vue_vue_type_style_index_1_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_view_vue_vue_type_style_index_1_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_view_vue_vue_type_style_index_1_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ "./resources/js/components/stock_request/view.vue?vue&type=template&id=4b873739&scoped=true&":
/*!***************************************************************************************************!*\
  !*** ./resources/js/components/stock_request/view.vue?vue&type=template&id=4b873739&scoped=true& ***!
  \***************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_view_vue_vue_type_template_id_4b873739_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./view.vue?vue&type=template&id=4b873739&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/stock_request/view.vue?vue&type=template&id=4b873739&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_view_vue_vue_type_template_id_4b873739_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_view_vue_vue_type_template_id_4b873739_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);