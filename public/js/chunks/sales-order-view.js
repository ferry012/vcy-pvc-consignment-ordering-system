(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["sales-order-view"],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/sales_order/view.vue?vue&type=script&lang=js&":
/*!***************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/sales_order/view.vue?vue&type=script&lang=js& ***!
  \***************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _trevoreyre_autocomplete_vue_dist_style_css__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @trevoreyre/autocomplete-vue/dist/style.css */ "./node_modules/@trevoreyre/autocomplete-vue/dist/style.css");
/* harmony import */ var _trevoreyre_autocomplete_vue_dist_style_css__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_trevoreyre_autocomplete_vue_dist_style_css__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var fuse_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! fuse.js */ "./node_modules/fuse.js/dist/fuse.esm.js");
/* harmony import */ var jspdf__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! jspdf */ "./node_modules/jspdf/dist/jspdf.es.min.js");
/* harmony import */ var jspdf_autotable__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! jspdf-autotable */ "./node_modules/jspdf-autotable/dist/jspdf.plugin.autotable.js");
/* harmony import */ var jspdf_autotable__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(jspdf_autotable__WEBPACK_IMPORTED_MODULE_5__);


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//TODO IMPORTS





/* harmony default export */ __webpack_exports__["default"] = ({
  name: "salesOrderView",
  data: function data() {
    return {
      form: {
        req_date: '',
        cust_company: null,
        mat_group: null,
        mat_code: null,
        barcode: null,
        mat_des: null,
        uom: null,
        KWMENG: null,
        ERDAT: null
      },
      perPage: ['10', '50', '100'],
      currentPage: '10',
      sortClassActive: {
        'activeDesc': false,
        'activeAsc': true
      },
      meta: {},
      page: {},
      errors: null,
      currentSort: 'stock_req_no',
      currentSortDir: 'desc',
      multiForm: {
        sBarcode: '',
        sMat_code: '',
        sMat_des: '',
        sUom: '',
        sLastFillDate: '',
        sLastFillQty: '',
        sStocksLeft: '',
        sSuggestQty: '',
        sApproveQty: ''
      },
      stocks: [],
      matGroup: [],
      matCode: [],
      matBarcode: [],
      multiBarcode: [],
      sales_unit: [],
      dateTime: '',
      company: [],
      trans_stocks: [],
      trans_header: [],
      stockData: {},
      headerStockNo: '',
      headerReqDate: '',
      headerSalesOrderDate: '',
      headerCompany: '',
      statusOptions: [{
        name: "<span style=\"padding: 4px; background: #28a745; border-radius: 0.25rem; color: white;\">POSTED</span>"
      }, {
        name: "<span style=\"padding: 4px; background: #6f42c1; border-radius: 0.25rem; color: white;\">PENDING</span>"
      }],
      stockStatus: '',
      netVal: 0,
      poDate: '',
      poNumber: '',
      reqPoDate: true,
      reqPoNumber: true,
      pohHeader: [],
      netValPost: 0,
      poForm: {
        poDate: '',
        poNumber: ''
      },
      postedButton: false,
      requestBy: null,
      headerReqDateField: false,
      poDateField: false,
      poNumberField: false
    };
  },
  watch: {
    'form.mat_group': function formMat_group(newVal, oldVal) {
      if (newVal !== null) {
        this.getBarCode(newVal);
        this.multiGetBarCode(newVal); // this.getMatCode(newVal);
      }

      if (newVal === null) {
        this.form.mat_code = '';
        this.form.barcode = '';
        this.matCode = [];
        this.matBarcode = [];
      }
    },
    'form.barcode': function formBarcode(newVal, oldVal) {
      this.getMatCode(newVal);
    },
    'stocks': function stocks(newVal, oldVal) {
      console.log(newVal);
    },
    'stockData.data': function stockDataData(newVal, oldVal) {
      var addCost = 0;
      newVal.forEach(function (value, index) {
        addCost += value.total_cost;
      }); // console.log(addCost,'total');

      this.netValPost = addCost;
      this.netVal = this.formatNumber(addCost);
    },
    'headerReqDate': function headerReqDate(newVal, oldVal) {
      var req_date = [];
      this.trans_header.forEach(function (obj) {
        // console.log(this.trans_header[0],'test123');
        // console.log(obj.req_date,'test222');
        // console.log(req_date,'test555');
        req_date.push(obj.req_date = newVal);
      }); // console.log(req_date,'test');

      this.trans_header.push({
        'req_date': req_date
      });
    },
    'poDate': function poDate(newVal, oldVal) {
      console.log(newVal, 'test');
    },
    deep: true
  },
  computed: {
    currentUser: function currentUser() {
      return this.$store.getters.currentUser;
    }
  },
  created: function created() {
    Reload.$on('AfterCreated', function () {});
    this.getMatGroup();
    this.getBarCode();
    this.getCustCompany();
    this.getStockHeader();
    this.getViewHeader();
    this.formatDate();
    this.postOnlyOnce();
  },
  mounted: function mounted() {
    this.postOnlyOnce();
  },
  methods: {
    postOnlyOnce: function postOnlyOnce() {
      var _this = this;

      axios.get("/api/".concat(this.$route.params.id, "/error-post")).then(function (response) {
        console.log(response.data.error);

        if (response.data.error === 'Not Found') {
          _this.postedButton = false;
        } else if (response.data.error === 'Found') {
          _this.postedButton = true;
        }
      });
    },
    postSalesOrder: function postSalesOrder() {
      var _this2 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
        var format;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _this2.pohHeader.push({
                  'po_number': _this2.poNumber,
                  'po_date': _this2.poDate
                });

                format = _this2.string_to_amount(_this2.netVal);

                _this2.trans_stocks.push({
                  'sales_order': _this2.stockData,
                  'header': _this2.trans_header,
                  // 'po_header': this.pohHeader,
                  'po_number': _this2.poForm.poNumber,
                  'po_date': _this2.poForm.poDate,
                  'net_value': format
                });

                console.log(_this2.trans_stocks);
                _context.next = 6;
                return axios.post("/api/sales-data/".concat(_this2.$route.params.id, "/post-sap"), _this2.trans_stocks).then(function (response) {
                  if (response.data.error === 'Found') {
                    _this2.alertSwal('Duplicate Posting Found!', '', 'error', 3000);

                    _this2.$router.push({
                      name: 'sales-order-table',
                      query: {
                        approvedRequest: 'DataTable',
                        id: _this2.currentUser.token
                      }
                    });
                  } else {
                    Reload.$emit('AfterCreated');
                    Notification.success();

                    _this2.$router.push({
                      name: 'sales-order-table',
                      query: {
                        approvedRequest: 'DataTable',
                        id: _this2.currentUser.token
                      }
                    });
                  }
                })["catch"](function (error) {
                  if (error.response.status === 422) {
                    _this2.errors = error.response.data.errors;
                    console.log(_this2.errors);
                  } else {
                    _this2.alertSwal(error, '', 'warning', '');
                  }
                });

              case 6:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }))();
    },
    getStockHeader: function getStockHeader(page) {
      var _this3 = this;

      if (typeof page === 'undefined') {
        this.page = 1;
      }

      var uri = "/api/sales-data/".concat(this.$route.params.id, "/view?page=") + page + "&search=" + this.search + "&sortby=" + this.currentSort + "&sortdir=" + this.currentSortDir + "&currentpage=" + this.currentPage;
      this.uri = uri;
      this.page = page;
      axios.get(uri).then(function (response) {
        _this3.stockData = response.data; // this.meta.total = response.data.meta.total;
        // this.meta.from = response.data.meta.from;
        // this.meta.to = response.data.meta.to;
      });
    },
    getViewHeader: function getViewHeader() {
      var _this4 = this;

      axios.get("/api/sales-data/header/".concat(this.$route.params.id)).then(function (response) {
        console.log(response.data[0]);
        _this4.stockStatus = response.data[0].status;
        _this4.headerStockNo = response.data[0].stock_req_no;
        _this4.headerSalesOrderDate = response.data[0].sales_order_date;
        _this4.headerCompany = response.data[0].company;
        _this4.requestBy = response.data[0].request_by;

        _this4.trans_header.push({
          'company': _this4.headerCompany,
          'stock_req_no': _this4.headerStockNo,
          'req_date': _this4.headerReqDate,
          'request_by': _this4.requestBy
        });

        if (_this4.stockStatus === 'PENDING') {
          _this4.postedButton = false;
          _this4.headerReqDateField = false;
          _this4.poDateField = false;
          _this4.poNumberField = false;
        } else if (_this4.stockStatus === 'POSTED') {
          _this4.postedButton = true;
          _this4.poForm.poNumber = response.data[0].po_number;
          _this4.poForm.poDate = response.data[0].po_date;
          _this4.headerReqDateField = true;
          _this4.poDateField = true;
          _this4.poNumberField = true;
        }
      });
    },
    formatDate: function formatDate() {
      // var formatDate = new Date(this.form.req_date);
      // return formatDate;
      var moment = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js"); // require


      moment().format();
      var today = moment();
      this.headerReqDate = today.format('YYYY-MM-DD'); // return this.headerReqDate;
    },
    selectPageNumber: function selectPageNumber(selected) {
      this.currentPage = selected;
      this.getStockHeader();
    },
    addItem: function addItem() {
      var elem = document.createElement('tr');
      this.stocks.push({
        'req_date': this.form.req_date,
        'cust_company': this.form.cust_company,
        'mat_group': this.form.mat_group,
        'mat_code': this.form.mat_code,
        'mat_des': this.form.mat_des,
        'uom': this.form.uom,
        'barcode': this.form.barcode,
        'last_fill_date': this.form.ERDAT,
        'last_fill_qty': this.form.KWMENG,
        'stocks_left': this.multiForm.sStocksLeft,
        'suggest_qty': this.multiForm.sSuggestQty,
        'approve_qty': this.multiForm.sApproveQty
      });
      this.form.barcode = null;
      this.form.mat_code = null;
      this.form.uom = null;
    },
    stockReqCreate: function stockReqCreate() {
      var _this5 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2() {
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                if (!(_this5.stocks.length !== 0)) {
                  _context2.next = 7;
                  break;
                }

                console.log(_this5.stocks);

                _this5.trans_stocks.push({
                  'stocks': _this5.stocks
                });

                _context2.next = 5;
                return axios.post('/api/stocks', _this5.trans_stocks).then(function (response) {
                  Reload.$emit('AfterCreated');
                  Notification.success();
                })["catch"](function (error) {
                  _this5.alertSwal(error, '', 'warning', '');
                });

              case 5:
                _context2.next = 8;
                break;

              case 7:
                _this5.alertSwal('Cannot save empty transactions', '', 'warning', '');

              case 8:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2);
      }))();
    },
    stockReqAsDraft: function stockReqAsDraft() {
      alert('Under Construction');
    },
    rangeMatCode: function rangeMatCode() {
      alert('Under Construction');
    },
    rangeBarcode: function rangeBarcode() {
      alert('Under Construction');
    },
    removeElement: function removeElement(index) {
      this.stocks.splice(index, 1);
    },
    getCustCompany: function getCustCompany() {
      var _this6 = this;

      axios.get('/api/get-customer/stock-creation').then(function (response) {
        _this6.company = response.data;
      });
    },
    multiGetBarCode: function multiGetBarCode(newVal) {
      var _this7 = this;

      if (newVal !== null) {
        var formData = new FormData();
        formData.append('MatGroup', newVal);
        axios.post('/api/get-mat-barcode', formData).then(function (response) {
          _this7.multiBarcode = response.data;
        });
      } else {
        this.multiBarcode = [];
      }
    },
    getBarCode: function getBarCode(newVal) {
      var _this8 = this;

      if (newVal !== null) {
        var formData = new FormData();
        formData.append('MatGroup', newVal);
        axios.post('/api/get-mat-barcode', formData).then(function (response) {
          _this8.matBarcode = response.data;
        });
      } else {
        this.matBarcode = [];
      }
    },
    getMatCode: function getMatCode(newVal) {
      var _this9 = this;

      if (newVal !== null) {
        var formData = new FormData();
        formData.append('barcode', newVal);
        axios.post('/api/get-mat-code', formData).then(function (response) {
          _this9.form.mat_code = response.data.MATNR;
          _this9.form.mat_des = response.data.MAKTX;
          _this9.sales_unit = response.data.MEINH;
          _this9.form.ERDAT = response.data.ERDAT; // last fill up date

          _this9.form.KWMENG = response.data.KWMENG; // last fill up date
        });
      } else {
        this.form.mat_code = null;
      }
    },
    fuseSearch: function fuseSearch(options, search) {
      var fuse = new fuse_js__WEBPACK_IMPORTED_MODULE_3__["default"](options, {
        keys: ["WGBEZ"],
        shouldSort: true
      });
      return search.length ? fuse.search(search).map(function (_ref) {
        var item = _ref.item;
        return item;
      }) : fuse.list;
    },
    fuseSearchMatCode: function fuseSearchMatCode(options, search) {
      var fuse = new fuse_js__WEBPACK_IMPORTED_MODULE_3__["default"](options, {
        keys: ["MATNR", "MAKTX"],
        shouldSort: true
      });
      return search.length ? fuse.search(search).map(function (_ref2) {
        var item = _ref2.item;
        return item;
      }) : fuse.list;
    },
    fuseSearchCompany: function fuseSearchCompany(options, search) {
      var fuse = new fuse_js__WEBPACK_IMPORTED_MODULE_3__["default"](options, {
        keys: ["NAME1"],
        shouldSort: true
      });
      return search.length ? fuse.search(search).map(function (_ref3) {
        var item = _ref3.item;
        return item;
      }) : fuse.list;
    },
    fuseSearchBarcode: function fuseSearchBarcode(options, search) {
      var fuse = new fuse_js__WEBPACK_IMPORTED_MODULE_3__["default"](options, {
        keys: ["EAN11", "MAKTX", "MATNR"],
        shouldSort: true
      });
      return search.length ? fuse.search(search).map(function (_ref4) {
        var item = _ref4.item;
        return item;
      }) : fuse.list;
    },
    getMatGroup: function getMatGroup() {
      var _this10 = this;

      axios.get('/api/get-mat-group').then(function (response) {
        _this10.matGroup = response.data;
      });
    },
    alertSwal: function alertSwal(title, text, icon, timer) {
      this.$swal({
        title: title,
        text: text,
        icon: icon,
        timer: timer
      });
    },
    formatNumber: function formatNumber(number) {
      return number === '' ? '' : parseFloat(Math.abs(number)).toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
    },
    string_to_amount: function string_to_amount(string_amount) {
      return string_amount.toString().replace(/,/g, '') * 1;
    }
  }
});

/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/sales_order/view.vue?vue&type=style&index=0&id=687948b8&scoped=true&lang=css&":
/*!**********************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/sales_order/view.vue?vue&type=style&index=0&id=687948b8&scoped=true&lang=css& ***!
  \**********************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n@media (min-width: 992px) {\n.modal-dialog[data-v-687948b8] {\r\n        max-width: 30% !important;\n}\n}\n@media (min-width: 992px) {\n.modal_custom[data-v-687948b8] {\r\n        max-width: 50% !important;\n}\n}\n.input-group-text[data-v-687948b8] {\r\n    background-color: #007bff;\r\n    color: white;\n}\n.sampleClass[data-v-687948b8] {\r\n    background-color: rgba(0, 0, 0, 0.5) !important;\r\n    /*opacity: 0.5 !important;*/\n}\n.example-open .modal-backdrop.show[data-v-687948b8]:nth-of-type(even) {\r\n    opacity: 5 !important;\r\n    z-index: 1052 !important;\n}\n.filter-asc[data-v-687948b8] {\r\n    border-bottom: 5px solid #ccc;\r\n    margin-bottom: 1px\n}\n.filter-asc[data-v-687948b8], .filter-desc[data-v-687948b8] {\r\n    width: 0;\r\n    height: 0;\r\n    border-left: 5px solid transparent;\r\n    border-right: 5px solid transparent\n}\n.filter-desc[data-v-687948b8] {\r\n    border-top: 5px solid #ccc;\r\n    margin-top: 1px\n}\n.inline-block[data-v-687948b8] {\r\n    display: inline-block\n}\n[data-position='below'] .autocomplete-result-list[data-v-687948b8] {\r\n    border-bottom: none;\r\n    border-radius: 8px 8px 0 0;\r\n    color: forestgreen;\r\n    font-weight: bold;\n}\n.wiki-result[data-v-687948b8] {\r\n    border-top: 1px solid #eee;\r\n    padding: 16px;\r\n    background: transparent;\n}\n.wiki-title[data-v-687948b8] {\r\n    font-size: 15px;\r\n    margin-bottom: 8px;\r\n    font-weight: 550;\r\n    color: forestgreen;\n}\n.wiki-snippet[data-v-687948b8] {\r\n    font-size: 14px;\r\n    color: rgba(0, 0, 0, 0.54);\n}\n.wrapAll[data-v-687948b8] {\r\n    white-space: nowrap;\r\n    overflow: hidden;\n}\r\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/sales_order/view.vue?vue&type=style&index=1&lang=css&":
/*!**********************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/sales_order/view.vue?vue&type=style&index=1&lang=css& ***!
  \**********************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.matGroupHeight .vs__dropdown-toggle {\r\n    height: 38px;\n}\r\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/moment/locale sync recursive ^\\.\\/.*$":
/*!**************************************************!*\
  !*** ./node_modules/moment/locale sync ^\.\/.*$ ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": "./node_modules/moment/locale/af.js",
	"./af.js": "./node_modules/moment/locale/af.js",
	"./ar": "./node_modules/moment/locale/ar.js",
	"./ar-dz": "./node_modules/moment/locale/ar-dz.js",
	"./ar-dz.js": "./node_modules/moment/locale/ar-dz.js",
	"./ar-kw": "./node_modules/moment/locale/ar-kw.js",
	"./ar-kw.js": "./node_modules/moment/locale/ar-kw.js",
	"./ar-ly": "./node_modules/moment/locale/ar-ly.js",
	"./ar-ly.js": "./node_modules/moment/locale/ar-ly.js",
	"./ar-ma": "./node_modules/moment/locale/ar-ma.js",
	"./ar-ma.js": "./node_modules/moment/locale/ar-ma.js",
	"./ar-sa": "./node_modules/moment/locale/ar-sa.js",
	"./ar-sa.js": "./node_modules/moment/locale/ar-sa.js",
	"./ar-tn": "./node_modules/moment/locale/ar-tn.js",
	"./ar-tn.js": "./node_modules/moment/locale/ar-tn.js",
	"./ar.js": "./node_modules/moment/locale/ar.js",
	"./az": "./node_modules/moment/locale/az.js",
	"./az.js": "./node_modules/moment/locale/az.js",
	"./be": "./node_modules/moment/locale/be.js",
	"./be.js": "./node_modules/moment/locale/be.js",
	"./bg": "./node_modules/moment/locale/bg.js",
	"./bg.js": "./node_modules/moment/locale/bg.js",
	"./bm": "./node_modules/moment/locale/bm.js",
	"./bm.js": "./node_modules/moment/locale/bm.js",
	"./bn": "./node_modules/moment/locale/bn.js",
	"./bn-bd": "./node_modules/moment/locale/bn-bd.js",
	"./bn-bd.js": "./node_modules/moment/locale/bn-bd.js",
	"./bn.js": "./node_modules/moment/locale/bn.js",
	"./bo": "./node_modules/moment/locale/bo.js",
	"./bo.js": "./node_modules/moment/locale/bo.js",
	"./br": "./node_modules/moment/locale/br.js",
	"./br.js": "./node_modules/moment/locale/br.js",
	"./bs": "./node_modules/moment/locale/bs.js",
	"./bs.js": "./node_modules/moment/locale/bs.js",
	"./ca": "./node_modules/moment/locale/ca.js",
	"./ca.js": "./node_modules/moment/locale/ca.js",
	"./cs": "./node_modules/moment/locale/cs.js",
	"./cs.js": "./node_modules/moment/locale/cs.js",
	"./cv": "./node_modules/moment/locale/cv.js",
	"./cv.js": "./node_modules/moment/locale/cv.js",
	"./cy": "./node_modules/moment/locale/cy.js",
	"./cy.js": "./node_modules/moment/locale/cy.js",
	"./da": "./node_modules/moment/locale/da.js",
	"./da.js": "./node_modules/moment/locale/da.js",
	"./de": "./node_modules/moment/locale/de.js",
	"./de-at": "./node_modules/moment/locale/de-at.js",
	"./de-at.js": "./node_modules/moment/locale/de-at.js",
	"./de-ch": "./node_modules/moment/locale/de-ch.js",
	"./de-ch.js": "./node_modules/moment/locale/de-ch.js",
	"./de.js": "./node_modules/moment/locale/de.js",
	"./dv": "./node_modules/moment/locale/dv.js",
	"./dv.js": "./node_modules/moment/locale/dv.js",
	"./el": "./node_modules/moment/locale/el.js",
	"./el.js": "./node_modules/moment/locale/el.js",
	"./en-au": "./node_modules/moment/locale/en-au.js",
	"./en-au.js": "./node_modules/moment/locale/en-au.js",
	"./en-ca": "./node_modules/moment/locale/en-ca.js",
	"./en-ca.js": "./node_modules/moment/locale/en-ca.js",
	"./en-gb": "./node_modules/moment/locale/en-gb.js",
	"./en-gb.js": "./node_modules/moment/locale/en-gb.js",
	"./en-ie": "./node_modules/moment/locale/en-ie.js",
	"./en-ie.js": "./node_modules/moment/locale/en-ie.js",
	"./en-il": "./node_modules/moment/locale/en-il.js",
	"./en-il.js": "./node_modules/moment/locale/en-il.js",
	"./en-in": "./node_modules/moment/locale/en-in.js",
	"./en-in.js": "./node_modules/moment/locale/en-in.js",
	"./en-nz": "./node_modules/moment/locale/en-nz.js",
	"./en-nz.js": "./node_modules/moment/locale/en-nz.js",
	"./en-sg": "./node_modules/moment/locale/en-sg.js",
	"./en-sg.js": "./node_modules/moment/locale/en-sg.js",
	"./eo": "./node_modules/moment/locale/eo.js",
	"./eo.js": "./node_modules/moment/locale/eo.js",
	"./es": "./node_modules/moment/locale/es.js",
	"./es-do": "./node_modules/moment/locale/es-do.js",
	"./es-do.js": "./node_modules/moment/locale/es-do.js",
	"./es-mx": "./node_modules/moment/locale/es-mx.js",
	"./es-mx.js": "./node_modules/moment/locale/es-mx.js",
	"./es-us": "./node_modules/moment/locale/es-us.js",
	"./es-us.js": "./node_modules/moment/locale/es-us.js",
	"./es.js": "./node_modules/moment/locale/es.js",
	"./et": "./node_modules/moment/locale/et.js",
	"./et.js": "./node_modules/moment/locale/et.js",
	"./eu": "./node_modules/moment/locale/eu.js",
	"./eu.js": "./node_modules/moment/locale/eu.js",
	"./fa": "./node_modules/moment/locale/fa.js",
	"./fa.js": "./node_modules/moment/locale/fa.js",
	"./fi": "./node_modules/moment/locale/fi.js",
	"./fi.js": "./node_modules/moment/locale/fi.js",
	"./fil": "./node_modules/moment/locale/fil.js",
	"./fil.js": "./node_modules/moment/locale/fil.js",
	"./fo": "./node_modules/moment/locale/fo.js",
	"./fo.js": "./node_modules/moment/locale/fo.js",
	"./fr": "./node_modules/moment/locale/fr.js",
	"./fr-ca": "./node_modules/moment/locale/fr-ca.js",
	"./fr-ca.js": "./node_modules/moment/locale/fr-ca.js",
	"./fr-ch": "./node_modules/moment/locale/fr-ch.js",
	"./fr-ch.js": "./node_modules/moment/locale/fr-ch.js",
	"./fr.js": "./node_modules/moment/locale/fr.js",
	"./fy": "./node_modules/moment/locale/fy.js",
	"./fy.js": "./node_modules/moment/locale/fy.js",
	"./ga": "./node_modules/moment/locale/ga.js",
	"./ga.js": "./node_modules/moment/locale/ga.js",
	"./gd": "./node_modules/moment/locale/gd.js",
	"./gd.js": "./node_modules/moment/locale/gd.js",
	"./gl": "./node_modules/moment/locale/gl.js",
	"./gl.js": "./node_modules/moment/locale/gl.js",
	"./gom-deva": "./node_modules/moment/locale/gom-deva.js",
	"./gom-deva.js": "./node_modules/moment/locale/gom-deva.js",
	"./gom-latn": "./node_modules/moment/locale/gom-latn.js",
	"./gom-latn.js": "./node_modules/moment/locale/gom-latn.js",
	"./gu": "./node_modules/moment/locale/gu.js",
	"./gu.js": "./node_modules/moment/locale/gu.js",
	"./he": "./node_modules/moment/locale/he.js",
	"./he.js": "./node_modules/moment/locale/he.js",
	"./hi": "./node_modules/moment/locale/hi.js",
	"./hi.js": "./node_modules/moment/locale/hi.js",
	"./hr": "./node_modules/moment/locale/hr.js",
	"./hr.js": "./node_modules/moment/locale/hr.js",
	"./hu": "./node_modules/moment/locale/hu.js",
	"./hu.js": "./node_modules/moment/locale/hu.js",
	"./hy-am": "./node_modules/moment/locale/hy-am.js",
	"./hy-am.js": "./node_modules/moment/locale/hy-am.js",
	"./id": "./node_modules/moment/locale/id.js",
	"./id.js": "./node_modules/moment/locale/id.js",
	"./is": "./node_modules/moment/locale/is.js",
	"./is.js": "./node_modules/moment/locale/is.js",
	"./it": "./node_modules/moment/locale/it.js",
	"./it-ch": "./node_modules/moment/locale/it-ch.js",
	"./it-ch.js": "./node_modules/moment/locale/it-ch.js",
	"./it.js": "./node_modules/moment/locale/it.js",
	"./ja": "./node_modules/moment/locale/ja.js",
	"./ja.js": "./node_modules/moment/locale/ja.js",
	"./jv": "./node_modules/moment/locale/jv.js",
	"./jv.js": "./node_modules/moment/locale/jv.js",
	"./ka": "./node_modules/moment/locale/ka.js",
	"./ka.js": "./node_modules/moment/locale/ka.js",
	"./kk": "./node_modules/moment/locale/kk.js",
	"./kk.js": "./node_modules/moment/locale/kk.js",
	"./km": "./node_modules/moment/locale/km.js",
	"./km.js": "./node_modules/moment/locale/km.js",
	"./kn": "./node_modules/moment/locale/kn.js",
	"./kn.js": "./node_modules/moment/locale/kn.js",
	"./ko": "./node_modules/moment/locale/ko.js",
	"./ko.js": "./node_modules/moment/locale/ko.js",
	"./ku": "./node_modules/moment/locale/ku.js",
	"./ku.js": "./node_modules/moment/locale/ku.js",
	"./ky": "./node_modules/moment/locale/ky.js",
	"./ky.js": "./node_modules/moment/locale/ky.js",
	"./lb": "./node_modules/moment/locale/lb.js",
	"./lb.js": "./node_modules/moment/locale/lb.js",
	"./lo": "./node_modules/moment/locale/lo.js",
	"./lo.js": "./node_modules/moment/locale/lo.js",
	"./lt": "./node_modules/moment/locale/lt.js",
	"./lt.js": "./node_modules/moment/locale/lt.js",
	"./lv": "./node_modules/moment/locale/lv.js",
	"./lv.js": "./node_modules/moment/locale/lv.js",
	"./me": "./node_modules/moment/locale/me.js",
	"./me.js": "./node_modules/moment/locale/me.js",
	"./mi": "./node_modules/moment/locale/mi.js",
	"./mi.js": "./node_modules/moment/locale/mi.js",
	"./mk": "./node_modules/moment/locale/mk.js",
	"./mk.js": "./node_modules/moment/locale/mk.js",
	"./ml": "./node_modules/moment/locale/ml.js",
	"./ml.js": "./node_modules/moment/locale/ml.js",
	"./mn": "./node_modules/moment/locale/mn.js",
	"./mn.js": "./node_modules/moment/locale/mn.js",
	"./mr": "./node_modules/moment/locale/mr.js",
	"./mr.js": "./node_modules/moment/locale/mr.js",
	"./ms": "./node_modules/moment/locale/ms.js",
	"./ms-my": "./node_modules/moment/locale/ms-my.js",
	"./ms-my.js": "./node_modules/moment/locale/ms-my.js",
	"./ms.js": "./node_modules/moment/locale/ms.js",
	"./mt": "./node_modules/moment/locale/mt.js",
	"./mt.js": "./node_modules/moment/locale/mt.js",
	"./my": "./node_modules/moment/locale/my.js",
	"./my.js": "./node_modules/moment/locale/my.js",
	"./nb": "./node_modules/moment/locale/nb.js",
	"./nb.js": "./node_modules/moment/locale/nb.js",
	"./ne": "./node_modules/moment/locale/ne.js",
	"./ne.js": "./node_modules/moment/locale/ne.js",
	"./nl": "./node_modules/moment/locale/nl.js",
	"./nl-be": "./node_modules/moment/locale/nl-be.js",
	"./nl-be.js": "./node_modules/moment/locale/nl-be.js",
	"./nl.js": "./node_modules/moment/locale/nl.js",
	"./nn": "./node_modules/moment/locale/nn.js",
	"./nn.js": "./node_modules/moment/locale/nn.js",
	"./oc-lnc": "./node_modules/moment/locale/oc-lnc.js",
	"./oc-lnc.js": "./node_modules/moment/locale/oc-lnc.js",
	"./pa-in": "./node_modules/moment/locale/pa-in.js",
	"./pa-in.js": "./node_modules/moment/locale/pa-in.js",
	"./pl": "./node_modules/moment/locale/pl.js",
	"./pl.js": "./node_modules/moment/locale/pl.js",
	"./pt": "./node_modules/moment/locale/pt.js",
	"./pt-br": "./node_modules/moment/locale/pt-br.js",
	"./pt-br.js": "./node_modules/moment/locale/pt-br.js",
	"./pt.js": "./node_modules/moment/locale/pt.js",
	"./ro": "./node_modules/moment/locale/ro.js",
	"./ro.js": "./node_modules/moment/locale/ro.js",
	"./ru": "./node_modules/moment/locale/ru.js",
	"./ru.js": "./node_modules/moment/locale/ru.js",
	"./sd": "./node_modules/moment/locale/sd.js",
	"./sd.js": "./node_modules/moment/locale/sd.js",
	"./se": "./node_modules/moment/locale/se.js",
	"./se.js": "./node_modules/moment/locale/se.js",
	"./si": "./node_modules/moment/locale/si.js",
	"./si.js": "./node_modules/moment/locale/si.js",
	"./sk": "./node_modules/moment/locale/sk.js",
	"./sk.js": "./node_modules/moment/locale/sk.js",
	"./sl": "./node_modules/moment/locale/sl.js",
	"./sl.js": "./node_modules/moment/locale/sl.js",
	"./sq": "./node_modules/moment/locale/sq.js",
	"./sq.js": "./node_modules/moment/locale/sq.js",
	"./sr": "./node_modules/moment/locale/sr.js",
	"./sr-cyrl": "./node_modules/moment/locale/sr-cyrl.js",
	"./sr-cyrl.js": "./node_modules/moment/locale/sr-cyrl.js",
	"./sr.js": "./node_modules/moment/locale/sr.js",
	"./ss": "./node_modules/moment/locale/ss.js",
	"./ss.js": "./node_modules/moment/locale/ss.js",
	"./sv": "./node_modules/moment/locale/sv.js",
	"./sv.js": "./node_modules/moment/locale/sv.js",
	"./sw": "./node_modules/moment/locale/sw.js",
	"./sw.js": "./node_modules/moment/locale/sw.js",
	"./ta": "./node_modules/moment/locale/ta.js",
	"./ta.js": "./node_modules/moment/locale/ta.js",
	"./te": "./node_modules/moment/locale/te.js",
	"./te.js": "./node_modules/moment/locale/te.js",
	"./tet": "./node_modules/moment/locale/tet.js",
	"./tet.js": "./node_modules/moment/locale/tet.js",
	"./tg": "./node_modules/moment/locale/tg.js",
	"./tg.js": "./node_modules/moment/locale/tg.js",
	"./th": "./node_modules/moment/locale/th.js",
	"./th.js": "./node_modules/moment/locale/th.js",
	"./tk": "./node_modules/moment/locale/tk.js",
	"./tk.js": "./node_modules/moment/locale/tk.js",
	"./tl-ph": "./node_modules/moment/locale/tl-ph.js",
	"./tl-ph.js": "./node_modules/moment/locale/tl-ph.js",
	"./tlh": "./node_modules/moment/locale/tlh.js",
	"./tlh.js": "./node_modules/moment/locale/tlh.js",
	"./tr": "./node_modules/moment/locale/tr.js",
	"./tr.js": "./node_modules/moment/locale/tr.js",
	"./tzl": "./node_modules/moment/locale/tzl.js",
	"./tzl.js": "./node_modules/moment/locale/tzl.js",
	"./tzm": "./node_modules/moment/locale/tzm.js",
	"./tzm-latn": "./node_modules/moment/locale/tzm-latn.js",
	"./tzm-latn.js": "./node_modules/moment/locale/tzm-latn.js",
	"./tzm.js": "./node_modules/moment/locale/tzm.js",
	"./ug-cn": "./node_modules/moment/locale/ug-cn.js",
	"./ug-cn.js": "./node_modules/moment/locale/ug-cn.js",
	"./uk": "./node_modules/moment/locale/uk.js",
	"./uk.js": "./node_modules/moment/locale/uk.js",
	"./ur": "./node_modules/moment/locale/ur.js",
	"./ur.js": "./node_modules/moment/locale/ur.js",
	"./uz": "./node_modules/moment/locale/uz.js",
	"./uz-latn": "./node_modules/moment/locale/uz-latn.js",
	"./uz-latn.js": "./node_modules/moment/locale/uz-latn.js",
	"./uz.js": "./node_modules/moment/locale/uz.js",
	"./vi": "./node_modules/moment/locale/vi.js",
	"./vi.js": "./node_modules/moment/locale/vi.js",
	"./x-pseudo": "./node_modules/moment/locale/x-pseudo.js",
	"./x-pseudo.js": "./node_modules/moment/locale/x-pseudo.js",
	"./yo": "./node_modules/moment/locale/yo.js",
	"./yo.js": "./node_modules/moment/locale/yo.js",
	"./zh-cn": "./node_modules/moment/locale/zh-cn.js",
	"./zh-cn.js": "./node_modules/moment/locale/zh-cn.js",
	"./zh-hk": "./node_modules/moment/locale/zh-hk.js",
	"./zh-hk.js": "./node_modules/moment/locale/zh-hk.js",
	"./zh-mo": "./node_modules/moment/locale/zh-mo.js",
	"./zh-mo.js": "./node_modules/moment/locale/zh-mo.js",
	"./zh-tw": "./node_modules/moment/locale/zh-tw.js",
	"./zh-tw.js": "./node_modules/moment/locale/zh-tw.js"
};


function webpackContext(req) {
	var id = webpackContextResolve(req);
	return __webpack_require__(id);
}
function webpackContextResolve(req) {
	if(!__webpack_require__.o(map, req)) {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	}
	return map[req];
}
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = "./node_modules/moment/locale sync recursive ^\\.\\/.*$";

/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/sales_order/view.vue?vue&type=style&index=0&id=687948b8&scoped=true&lang=css&":
/*!**************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/sales_order/view.vue?vue&type=style&index=0&id=687948b8&scoped=true&lang=css& ***!
  \**************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../node_modules/css-loader??ref--6-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--6-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./view.vue?vue&type=style&index=0&id=687948b8&scoped=true&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/sales_order/view.vue?vue&type=style&index=0&id=687948b8&scoped=true&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/sales_order/view.vue?vue&type=style&index=1&lang=css&":
/*!**************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/sales_order/view.vue?vue&type=style&index=1&lang=css& ***!
  \**************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../node_modules/css-loader??ref--6-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--6-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./view.vue?vue&type=style&index=1&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/sales_order/view.vue?vue&type=style&index=1&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/sales_order/view.vue?vue&type=template&id=687948b8&scoped=true&":
/*!*******************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/sales_order/view.vue?vue&type=template&id=687948b8&scoped=true& ***!
  \*******************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c("div", { staticClass: "row" }, [
      _c("div", { staticClass: "col-lg-12" }, [
        _c(
          "div",
          { staticClass: "card shadow-sm mb-4" },
          [
            _c("ValidationObserver", {
              ref: "form",
              scopedSlots: _vm._u([
                {
                  key: "default",
                  fn: function(ref) {
                    var handleSubmit = ref.handleSubmit
                    return [
                      _c(
                        "form",
                        {
                          directives: [
                            {
                              name: "promise-btn",
                              rawName: "v-promise-btn",
                              value: { action: "submit" },
                              expression: "{action: 'submit'}"
                            }
                          ],
                          on: {
                            submit: function($event) {
                              $event.preventDefault()
                              return handleSubmit(_vm.postSalesOrder)
                            }
                          }
                        },
                        [
                          _c("div", { staticClass: "card-header" }, [
                            _c(
                              "h4",
                              {
                                staticClass: "m-0 font-weight-bold text-primary"
                              },
                              [_vm._v("SALES ORDER VIEW DETAILS")]
                            )
                          ]),
                          _vm._v(" "),
                          _c("div", { staticClass: "card-header" }, [
                            _c("div", { staticClass: "row form-group" }, [
                              _c("div", { staticClass: "col-md-4" }, [
                                _c("div", { staticClass: "input-group" }, [
                                  _c(
                                    "div",
                                    { staticClass: "input-group-prepend" },
                                    [
                                      _c(
                                        "span",
                                        {
                                          staticClass:
                                            "input-group-text input-group-field",
                                          attrs: { id: "basic-addon1" }
                                        },
                                        [_vm._v("Company:")]
                                      )
                                    ]
                                  ),
                                  _vm._v(" "),
                                  _c("input", {
                                    directives: [
                                      {
                                        name: "model",
                                        rawName: "v-model",
                                        value: _vm.headerCompany,
                                        expression: "headerCompany"
                                      }
                                    ],
                                    staticClass: "form-control",
                                    attrs: { type: "text", disabled: "" },
                                    domProps: { value: _vm.headerCompany },
                                    on: {
                                      input: function($event) {
                                        if ($event.target.composing) {
                                          return
                                        }
                                        _vm.headerCompany = $event.target.value
                                      }
                                    }
                                  })
                                ])
                              ]),
                              _vm._v(" "),
                              _c("div", { staticClass: "col-md-1" }),
                              _vm._v(" "),
                              _c(
                                "div",
                                { staticClass: "col-md-3" },
                                [
                                  _c("ValidationProvider", {
                                    attrs: {
                                      name: "PO Number",
                                      rules: "required"
                                    },
                                    scopedSlots: _vm._u(
                                      [
                                        {
                                          key: "default",
                                          fn: function(ref) {
                                            var errors = ref.errors
                                            return [
                                              _c(
                                                "div",
                                                { staticClass: "input-group" },
                                                [
                                                  _c(
                                                    "div",
                                                    {
                                                      staticClass:
                                                        "input-group-prepend"
                                                    },
                                                    [
                                                      _c(
                                                        "span",
                                                        {
                                                          staticClass:
                                                            "input-group-text input-group-field",
                                                          attrs: {
                                                            id: "basic-addon1"
                                                          }
                                                        },
                                                        [_vm._v("PO Number:")]
                                                      )
                                                    ]
                                                  ),
                                                  _vm._v(" "),
                                                  _c("input", {
                                                    directives: [
                                                      {
                                                        name: "model",
                                                        rawName: "v-model",
                                                        value:
                                                          _vm.poForm.poNumber,
                                                        expression:
                                                          "poForm.poNumber"
                                                      }
                                                    ],
                                                    staticClass: "form-control",
                                                    class: {
                                                      "is-invalid": errors[0]
                                                    },
                                                    attrs: {
                                                      type: "number",
                                                      name: "PO Number",
                                                      placeholder: "PO Number",
                                                      disabled:
                                                        _vm.poNumberField
                                                    },
                                                    domProps: {
                                                      value: _vm.poForm.poNumber
                                                    },
                                                    on: {
                                                      input: function($event) {
                                                        if (
                                                          $event.target
                                                            .composing
                                                        ) {
                                                          return
                                                        }
                                                        _vm.$set(
                                                          _vm.poForm,
                                                          "poNumber",
                                                          $event.target.value
                                                        )
                                                      }
                                                    }
                                                  })
                                                ]
                                              ),
                                              _vm._v(" "),
                                              _c(
                                                "span",
                                                { staticClass: "text-danger" },
                                                [_vm._v(_vm._s(errors[0]))]
                                              )
                                            ]
                                          }
                                        }
                                      ],
                                      null,
                                      true
                                    )
                                  })
                                ],
                                1
                              )
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "row form-group" }, [
                              _c("div", { staticClass: "col-md-3" }, [
                                _c("div", { staticClass: "input-group" }, [
                                  _c(
                                    "div",
                                    { staticClass: "input-group-prepend" },
                                    [
                                      _c(
                                        "span",
                                        {
                                          staticClass:
                                            "input-group-text input-group-field",
                                          attrs: { id: "basic-addon1" }
                                        },
                                        [_vm._v("Status:")]
                                      )
                                    ]
                                  ),
                                  _vm._v(" "),
                                  _vm.stockStatus === "PENDING"
                                    ? _c(
                                        "span",
                                        {
                                          staticStyle: {
                                            padding: "4px",
                                            background: "#6f42c1",
                                            "border-radius": "0.25rem",
                                            color: "white",
                                            "margin-left": "2px"
                                          }
                                        },
                                        [
                                          _vm._v(
                                            "\n                                        " +
                                              _vm._s(_vm.stockStatus) +
                                              "\n                                    "
                                          )
                                        ]
                                      )
                                    : _vm._e(),
                                  _vm._v(" "),
                                  _vm.stockStatus === "POSTED"
                                    ? _c(
                                        "span",
                                        {
                                          staticStyle: {
                                            padding: "4px",
                                            background: "#28a745",
                                            "border-radius": "0.25rem",
                                            color: "white",
                                            "margin-left": "2px"
                                          }
                                        },
                                        [
                                          _vm._v(
                                            "\n                                        " +
                                              _vm._s(_vm.stockStatus) +
                                              "\n                                    "
                                          )
                                        ]
                                      )
                                    : _vm._e()
                                ])
                              ]),
                              _vm._v(" "),
                              _c("div", { staticClass: "col-md-2" }),
                              _vm._v(" "),
                              _c(
                                "div",
                                { staticClass: "col-md-3" },
                                [
                                  _c("ValidationProvider", {
                                    attrs: {
                                      name: "PO Date",
                                      rules: "required"
                                    },
                                    scopedSlots: _vm._u(
                                      [
                                        {
                                          key: "default",
                                          fn: function(ref) {
                                            var errors = ref.errors
                                            return [
                                              _c(
                                                "div",
                                                { staticClass: "input-group" },
                                                [
                                                  _c(
                                                    "div",
                                                    {
                                                      staticClass:
                                                        "input-group-prepend"
                                                    },
                                                    [
                                                      _c(
                                                        "span",
                                                        {
                                                          staticClass:
                                                            "input-group-text input-group-field",
                                                          attrs: {
                                                            id: "basic-addon1"
                                                          }
                                                        },
                                                        [_vm._v("PO Date:")]
                                                      )
                                                    ]
                                                  ),
                                                  _vm._v(" "),
                                                  _c("input", {
                                                    directives: [
                                                      {
                                                        name: "model",
                                                        rawName: "v-model",
                                                        value:
                                                          _vm.poForm.poDate,
                                                        expression:
                                                          "poForm.poDate"
                                                      }
                                                    ],
                                                    staticClass: "form-control",
                                                    class: {
                                                      "is-invalid": errors[0]
                                                    },
                                                    attrs: {
                                                      type: "date",
                                                      disabled: _vm.poDateField,
                                                      name: "name",
                                                      placeholder:
                                                        "Permission name"
                                                    },
                                                    domProps: {
                                                      value: _vm.poForm.poDate
                                                    },
                                                    on: {
                                                      input: function($event) {
                                                        if (
                                                          $event.target
                                                            .composing
                                                        ) {
                                                          return
                                                        }
                                                        _vm.$set(
                                                          _vm.poForm,
                                                          "poDate",
                                                          $event.target.value
                                                        )
                                                      }
                                                    }
                                                  })
                                                ]
                                              ),
                                              _vm._v(" "),
                                              _c(
                                                "span",
                                                { staticClass: "text-danger" },
                                                [_vm._v(_vm._s(errors[0]))]
                                              )
                                            ]
                                          }
                                        }
                                      ],
                                      null,
                                      true
                                    )
                                  })
                                ],
                                1
                              ),
                              _vm._v(" "),
                              _c("div", { staticClass: "col-md-3" })
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "row form-group" }, [
                              _c("div", { staticClass: "col-md-4" }, [
                                _c("div", { staticClass: "input-group" }, [
                                  _c(
                                    "div",
                                    { staticClass: "input-group-prepend" },
                                    [
                                      _c(
                                        "span",
                                        {
                                          staticClass:
                                            "input-group-text input-group-field",
                                          attrs: { id: "basic-addon1" }
                                        },
                                        [_vm._v("Required delivery date:")]
                                      )
                                    ]
                                  ),
                                  _vm._v(" "),
                                  _c("input", {
                                    directives: [
                                      {
                                        name: "model",
                                        rawName: "v-model",
                                        value: _vm.headerReqDate,
                                        expression: "headerReqDate"
                                      }
                                    ],
                                    staticClass: "form-control",
                                    attrs: {
                                      type: "date",
                                      disabled: _vm.headerReqDateField
                                    },
                                    domProps: { value: _vm.headerReqDate },
                                    on: {
                                      input: function($event) {
                                        if ($event.target.composing) {
                                          return
                                        }
                                        _vm.headerReqDate = $event.target.value
                                      }
                                    }
                                  })
                                ])
                              ]),
                              _vm._v(" "),
                              _c("div", { staticClass: "col-md-1" }),
                              _vm._v(" "),
                              _c("div", { staticClass: "col-md-3" }, [
                                _c("div", { staticClass: "input-group" }, [
                                  _c(
                                    "div",
                                    { staticClass: "input-group-prepend" },
                                    [
                                      _c(
                                        "span",
                                        {
                                          staticClass:
                                            "input-group-text input-group-field",
                                          attrs: { id: "basic-addon1" }
                                        },
                                        [_vm._v("Net Value:")]
                                      )
                                    ]
                                  ),
                                  _vm._v(" "),
                                  _c("input", {
                                    directives: [
                                      {
                                        name: "model",
                                        rawName: "v-model.number",
                                        value: _vm.netVal,
                                        expression: "netVal",
                                        modifiers: { number: true }
                                      }
                                    ],
                                    staticClass: "form-control",
                                    staticStyle: { "text-align": "right" },
                                    attrs: { type: "text", disabled: "" },
                                    domProps: { value: _vm.netVal },
                                    on: {
                                      input: function($event) {
                                        if ($event.target.composing) {
                                          return
                                        }
                                        _vm.netVal = _vm._n($event.target.value)
                                      },
                                      blur: function($event) {
                                        return _vm.$forceUpdate()
                                      }
                                    }
                                  })
                                ])
                              ]),
                              _vm._v(" "),
                              _c("div", { staticClass: "col-md-3" })
                            ])
                          ]),
                          _vm._v(" "),
                          _c("div", { staticClass: "card-body" }, [
                            _c(
                              "div",
                              { staticClass: "table-responsive" },
                              [
                                _c(
                                  "table",
                                  {
                                    staticClass:
                                      "table align-items-center table-bordered table-flush table-hover",
                                    attrs: {
                                      id: "dataTable",
                                      width: "100%",
                                      cellspacing: "0"
                                    }
                                  },
                                  [
                                    _c("thead", [
                                      _c("tr", { staticClass: "bg-primary" }, [
                                        _c(
                                          "th",
                                          {
                                            staticClass: "wrapAll",
                                            staticStyle: {
                                              color: "white",
                                              "text-align": "center"
                                            }
                                          },
                                          [
                                            _vm._v(
                                              "MATERIAL CODE\n                                            "
                                            )
                                          ]
                                        ),
                                        _vm._v(" "),
                                        _c(
                                          "th",
                                          {
                                            staticClass: "wrapAll",
                                            staticStyle: {
                                              color: "white",
                                              "text-align": "center"
                                            }
                                          },
                                          [
                                            _vm._v(
                                              "MATERIAL\n                                                DESCRIPTION\n                                            "
                                            )
                                          ]
                                        ),
                                        _vm._v(" "),
                                        _c(
                                          "th",
                                          {
                                            staticClass: "wrapAll",
                                            staticStyle: {
                                              color: "white",
                                              "text-align": "center"
                                            }
                                          },
                                          [
                                            _vm._v(
                                              "APPROVED QTY\n                                            "
                                            )
                                          ]
                                        ),
                                        _vm._v(" "),
                                        _c(
                                          "th",
                                          {
                                            staticClass: "wrapAll",
                                            staticStyle: {
                                              color: "white",
                                              "text-align": "right"
                                            }
                                          },
                                          [_vm._v("COST")]
                                        ),
                                        _vm._v(" "),
                                        _c(
                                          "th",
                                          {
                                            staticClass: "wrapAll",
                                            staticStyle: {
                                              color: "white",
                                              "text-align": "right"
                                            }
                                          },
                                          [_vm._v("TOTAL COST")]
                                        )
                                      ])
                                    ]),
                                    _vm._v(" "),
                                    _c(
                                      "tbody",
                                      _vm._l(_vm.stockData.data, function(
                                        stock,
                                        index
                                      ) {
                                        return _c(
                                          "tr",
                                          {
                                            key: stock.id,
                                            attrs: { index: index }
                                          },
                                          [
                                            _c(
                                              "td",
                                              {
                                                staticClass: "wrapAll",
                                                staticStyle: {
                                                  "text-align": "center"
                                                }
                                              },
                                              [_vm._v(_vm._s(stock.MATNR))]
                                            ),
                                            _vm._v(" "),
                                            _c(
                                              "td",
                                              {
                                                staticClass: "wrapAll",
                                                staticStyle: {
                                                  "text-align": "center"
                                                }
                                              },
                                              [_vm._v(_vm._s(stock.MAKTX))]
                                            ),
                                            _vm._v(" "),
                                            _c(
                                              "td",
                                              {
                                                staticClass: "wrapAll",
                                                staticStyle: {
                                                  "text-align": "center"
                                                }
                                              },
                                              [
                                                _vm._v(
                                                  _vm._s(stock.approved_qty) +
                                                    "\n                                            "
                                                )
                                              ]
                                            ),
                                            _vm._v(" "),
                                            _c(
                                              "td",
                                              {
                                                staticClass: "wrapAll",
                                                staticStyle: {
                                                  "text-align": "right"
                                                }
                                              },
                                              [
                                                _vm._v(
                                                  "\n                                                " +
                                                    _vm._s(
                                                      _vm.formatNumber(
                                                        stock.VERPR
                                                      )
                                                    ) +
                                                    "\n                                            "
                                                )
                                              ]
                                            ),
                                            _vm._v(" "),
                                            _c(
                                              "td",
                                              {
                                                staticClass: "wrapAll",
                                                staticStyle: {
                                                  "text-align": "right"
                                                }
                                              },
                                              [
                                                _vm._v(
                                                  "\n                                                " +
                                                    _vm._s(
                                                      _vm.formatNumber(
                                                        stock.total_cost
                                                      )
                                                    ) +
                                                    "\n                                            "
                                                )
                                              ]
                                            )
                                          ]
                                        )
                                      }),
                                      0
                                    )
                                  ]
                                ),
                                _vm._v(" "),
                                _c("br"),
                                _vm._v(" "),
                                _c(
                                  "router-link",
                                  {
                                    staticClass: "btn btn-danger",
                                    attrs: {
                                      to: {
                                        name: "sales-order-table",
                                        query: {
                                          approvedRequest: "DataTable",
                                          id: _vm.currentUser.token
                                        }
                                      }
                                    }
                                  },
                                  [
                                    _vm._v(
                                      "\n                                        Back\n                                    "
                                    )
                                  ]
                                ),
                                _vm._v(" "),
                                _vm.$can("sales_order_post_to_sap")
                                  ? _c(
                                      "button",
                                      {
                                        staticClass: "btn btn-success",
                                        attrs: {
                                          disabled: _vm.postedButton,
                                          type: "submit"
                                        }
                                      },
                                      [
                                        _vm._v(
                                          "\n                                        Post\n                                    "
                                        )
                                      ]
                                    )
                                  : _vm._e()
                              ],
                              1
                            )
                          ])
                        ]
                      )
                    ]
                  }
                }
              ])
            })
          ],
          1
        )
      ])
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/components/sales_order/view.vue":
/*!******************************************************!*\
  !*** ./resources/js/components/sales_order/view.vue ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _view_vue_vue_type_template_id_687948b8_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./view.vue?vue&type=template&id=687948b8&scoped=true& */ "./resources/js/components/sales_order/view.vue?vue&type=template&id=687948b8&scoped=true&");
/* harmony import */ var _view_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./view.vue?vue&type=script&lang=js& */ "./resources/js/components/sales_order/view.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _view_vue_vue_type_style_index_0_id_687948b8_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./view.vue?vue&type=style&index=0&id=687948b8&scoped=true&lang=css& */ "./resources/js/components/sales_order/view.vue?vue&type=style&index=0&id=687948b8&scoped=true&lang=css&");
/* harmony import */ var _view_vue_vue_type_style_index_1_lang_css___WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./view.vue?vue&type=style&index=1&lang=css& */ "./resources/js/components/sales_order/view.vue?vue&type=style&index=1&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");







/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_4__["default"])(
  _view_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _view_vue_vue_type_template_id_687948b8_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _view_vue_vue_type_template_id_687948b8_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "687948b8",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/sales_order/view.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/sales_order/view.vue?vue&type=script&lang=js&":
/*!*******************************************************************************!*\
  !*** ./resources/js/components/sales_order/view.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_view_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./view.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/sales_order/view.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_view_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/sales_order/view.vue?vue&type=style&index=0&id=687948b8&scoped=true&lang=css&":
/*!***************************************************************************************************************!*\
  !*** ./resources/js/components/sales_order/view.vue?vue&type=style&index=0&id=687948b8&scoped=true&lang=css& ***!
  \***************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_view_vue_vue_type_style_index_0_id_687948b8_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/style-loader!../../../../node_modules/css-loader??ref--6-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--6-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./view.vue?vue&type=style&index=0&id=687948b8&scoped=true&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/sales_order/view.vue?vue&type=style&index=0&id=687948b8&scoped=true&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_view_vue_vue_type_style_index_0_id_687948b8_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_view_vue_vue_type_style_index_0_id_687948b8_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_view_vue_vue_type_style_index_0_id_687948b8_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_view_vue_vue_type_style_index_0_id_687948b8_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ "./resources/js/components/sales_order/view.vue?vue&type=style&index=1&lang=css&":
/*!***************************************************************************************!*\
  !*** ./resources/js/components/sales_order/view.vue?vue&type=style&index=1&lang=css& ***!
  \***************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_view_vue_vue_type_style_index_1_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/style-loader!../../../../node_modules/css-loader??ref--6-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--6-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./view.vue?vue&type=style&index=1&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/sales_order/view.vue?vue&type=style&index=1&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_view_vue_vue_type_style_index_1_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_view_vue_vue_type_style_index_1_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_view_vue_vue_type_style_index_1_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_view_vue_vue_type_style_index_1_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ "./resources/js/components/sales_order/view.vue?vue&type=template&id=687948b8&scoped=true&":
/*!*************************************************************************************************!*\
  !*** ./resources/js/components/sales_order/view.vue?vue&type=template&id=687948b8&scoped=true& ***!
  \*************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_view_vue_vue_type_template_id_687948b8_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./view.vue?vue&type=template&id=687948b8&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/sales_order/view.vue?vue&type=template&id=687948b8&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_view_vue_vue_type_template_id_687948b8_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_view_vue_vue_type_template_id_687948b8_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);