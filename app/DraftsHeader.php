<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DraftsHeader extends Model
{
    protected $table = 'drafts_header';
    protected $fillable = ['drafts_req_no','drafts_req_date','company','creator','save_as'];
}
