<?php

namespace App\Http\Controllers;

use App\DiserTablet;
use Illuminate\Http\Request;

class DiserTabletController extends Controller
{
    public function diserDeviceInfo(Request $request){

//        $mobile = strpos($_SERVER['HTTP_USER_AGENT'],"IPhone");
        $mobile = $_SERVER['HTTP_USER_AGENT'];
        DiserTablet::create([
            'name' =>  $request[0]['name'],
            'device_width' => $request[0]['device_width'],
            'device_name' => $mobile,
        ]);
        return 'Success';
    }
    public function getDiserDeviceInfo(Request $request){
        $searchValue = $request->search;
        $orderBy = $request->sortby;
        $orderByDir = $request->sortdir;
        $perPage = $request->currentpage;

        $query = DiserTablet::when($searchValue !== 'null', function ($query) use ($searchValue) {
            $query->where('name', 'LIKE', "%$searchValue%")
                ->orwhere('device_name', 'LIKE',"%$searchValue%")
                ->orwhere('device_width', 'LIKE', "%$searchValue%");
        })
            ->orderBy($orderBy, $orderByDir)
            ->paginate($perPage);

        return response()->json($query);
    }
}
