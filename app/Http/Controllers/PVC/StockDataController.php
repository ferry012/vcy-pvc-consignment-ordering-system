<?php

namespace App\Http\Controllers\PVC;

use App\ApprovalMappingRequest;
use App\CustomerMaster;
use App\Http\Controllers\Controller;
use App\Http\Resources\StockDataCollection;
use App\KNA1;
use App\StockHeader;
use App\Stocks;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use DateTime;
use Illuminate\Support\Facades\Gate;

class StockDataController extends Controller
{
    public function getStockData(Request $request)
    {
        if ($request->showAll) {
            $query = StockHeader::orderBy('stock_req_no', 'desc')->get();
        } else {
            $name = auth()->user()->name;
            $searchValue = $request->search;
            $orderBy = $request->sortby;
            $orderByDir = $request->sortdir;
            $perPage = $request->currentpage;
            $searchStockNumber = $request->stockReqNumber;
            $creator = $request->creator;
            $status = $request->status;
            $company = $request->company;
            $userCompany = $request->userCompany;
            if ($creator === "null") {
                $creator = null;
            }
            if ($status === "null") {
                $status = null;
            }
            if ($company === "null") {
                $company = null;
            }

            $from = null;
            $to = null;

            if ($request->dateFromAndTo !== null && $request->dateFromAndTo !== "") {
                $split = explode(",", $request->dateFromAndTo);
                $from_x = $split[0];
                $to_x = $split[1];
                if ($to_x === '' || $from_x === '') {
                    $from = null;
                    $to = null;
                } else {
                    $from = DateTime::createFromFormat('d/m/Y', $from_x)->format('Y/m/d');
                    $to = DateTime::createFromFormat('d/m/Y', $to_x)->format('Y/m/d');
                }
            } else if ($request->dateFromAndTo === null && $request->dateFromAndTo === "") {
                $from = null;
                $to = null;
            }
            $level1 = User::permission('stock_level_one_view')->first();
            $level2 = User::permission('stock_level_two_view')->first();
            if ($from !== null && $to !== null) {
                if($name === $level1->name || $name === $level2->name){
                    if($name === $level1->name){
                        $query = StockHeader::join('approval_mapping as a','stocks_header.stock_req_no','=','a.stock_no')
//                            ->where(function ($query)  {
//                                $query->whereNull('a.level_one')
//                                    ->whereNull('a.level_two');
//                            })
                            ->whereBetween('stocks_header.stock_req_date', [Carbon::parse($from)->startOfDay(), Carbon::parse($to)->endOfDay()])
                            ->where(function ($query) use ($searchStockNumber) {
                                $query->orwhere('stocks_header.stock_req_no', 'LIKE', "%$searchStockNumber%");
                            })
                            ->where(function ($query) use ($company) {
                                $query->orwhere('stocks_header.company', 'LIKE', "%$company%");
                            })
                            ->where(function ($query) use ($status) {
                                $query->orwhere('stocks_header.status', 'LIKE', "%$status%");
                            })
                            ->where(function ($query) use ($creator) {
                                $query->orwhere('stocks_header.creator', 'LIKE', "%$creator%");
                            })
                            ->where(function ($query) use ($userCompany) {
                                $query->orwhere('stocks_header.company', 'LIKE', "%$userCompany%");
                            })
                            ->select('stocks_header.*')
                            ->orderBy($orderBy, $orderByDir)->paginate($perPage);
                    }
                    if($name === $level2->name){
                        $query = StockHeader::join('approval_mapping as a','stocks_header.stock_req_no','=','a.stock_no')
//                            ->where(function ($query)  {
//                                $query->where('a.level_one','=','APPROVED')
//                                    ->orwhere('a.level_one','!=','REJECTED');
//                            })
                            ->whereBetween('stocks_header.stock_req_date', [Carbon::parse($from)->startOfDay(), Carbon::parse($to)->endOfDay()])
                            ->where(function ($query) use ($searchStockNumber) {
                                $query->orwhere('stocks_header.stock_req_no', 'LIKE', "%$searchStockNumber%");
                            })
                            ->where(function ($query) use ($company) {
                                $query->orwhere('stocks_header.company', 'LIKE', "%$company%");
                            })
                            ->where(function ($query) use ($status) {
                                $query->orwhere('stocks_header.status', 'LIKE', "%$status%");
                            })
                            ->where(function ($query) use ($creator) {
                                $query->orwhere('stocks_header.creator', 'LIKE', "%$creator%");
                            })
                            ->where(function ($query) use ($userCompany) {
                                $query->orwhere('stocks_header.company', 'LIKE', "%$userCompany%");
                            })
                            ->select('stocks_header.*')
                            ->orderBy($orderBy, $orderByDir)->paginate($perPage);
                    }
                }else{
                    $query = StockHeader::where('creator','=',$name)
                        ->whereBetween('stock_req_date', [Carbon::parse($from)->startOfDay(), Carbon::parse($to)->endOfDay()])
                        ->where(function ($query) use ($searchStockNumber) {
                            $query->orwhere('stock_req_no', 'LIKE', "%$searchStockNumber%");
                        })
                        ->where(function ($query) use ($company) {
                            $query->orwhere('company', 'LIKE', "%$company%");
                        })
                        ->where(function ($query) use ($status) {
                            $query->orwhere('status', 'LIKE', "%$status%");
                        })
                        ->where(function ($query) use ($creator) {
                            $query->orwhere('creator', 'LIKE', "%$creator%");
                        })
                        ->where(function ($query) use ($userCompany) {
                            $query->orwhere('company', 'LIKE', "%$userCompany%");
                        })
                        ->select('stocks_header.*')
                        ->orderBy($orderBy, $orderByDir)->paginate($perPage);
                }
                return new StockDataCollection($query);
            } else if ($from === null && $to === null) {
                if($name === $level1->name || $name === $level2->name){
                    if($name === $level1->name){
                        $query = StockHeader::join('approval_mapping as a','stocks_header.stock_req_no','=','a.stock_no')
//                            ->where(function ($query)  {
//                                $query->whereNull('a.level_one')
//                                    ->whereNull('a.level_two');
//                            })
                            ->where(function ($query) use ($searchStockNumber) {
                                $query->orwhere('stocks_header.stock_req_no', 'LIKE', "%$searchStockNumber%");
                            })
                            ->where(function ($query) use ($company) {
                                $query->orwhere('stocks_header.company', 'LIKE', "%$company%");
                            })
                            ->where(function ($query) use ($status) {
                                $query->orwhere('stocks_header.status', 'LIKE', "%$status%");
                            })
                            ->where(function ($query) use ($creator) {
                                $query->orwhere('stocks_header.creator', 'LIKE', "%$creator%");
                            })
                            ->where(function ($query) use ($userCompany) {
                                $query->orwhere('stocks_header.company', 'LIKE', "%$userCompany%");
                            })
                            ->select('stocks_header.*')
                            ->orderBy($orderBy, $orderByDir)->paginate($perPage);
                    }
                    if($name === $level2->name){
                        $query = StockHeader::join('approval_mapping as a','stocks_header.stock_req_no','=','a.stock_no')
//                            ->where(function ($query)  {
//                                $query->where('a.level_one','=','APPROVED')
//                                    ->orwhere('a.level_one','!=','REJECTED');
//                            })
//                            ->where(function ($query)  {
//                                $query->whereNull('a.level_two');
//                            })
                            ->where(function ($query) use ($searchStockNumber) {
                                $query->orwhere('stocks_header.stock_req_no', 'LIKE', "%$searchStockNumber%");
                            })
                            ->where(function ($query) use ($company) {
                                $query->orwhere('stocks_header.company', 'LIKE', "%$company%");
                            })
                            ->where(function ($query) use ($status) {
                                $query->orwhere('stocks_header.status', 'LIKE', "%$status%");
                            })
                            ->where(function ($query) use ($creator) {
                                $query->orwhere('stocks_header.creator', 'LIKE', "%$creator%");
                            })
                            ->where(function ($query) use ($userCompany) {
                                $query->orwhere('stocks_header.company', 'LIKE', "%$userCompany%");
                            })
                            ->select('stocks_header.*')
                            ->orderBy($orderBy, $orderByDir)->paginate($perPage);
                    }
                }
                else {
                    $query = StockHeader::where('creator','=',$name)
                        ->where(function ($query) use ($searchStockNumber) {
                            $query->orwhere('stock_req_no', 'LIKE', "%$searchStockNumber%");
                        })
                        ->where(function ($query) use ($company) {
                            $query->orwhere('company', 'LIKE', "%$company%");
                        })
                        ->where(function ($query) use ($status) {
                            $query->orwhere('status', 'LIKE', "%$status%");
                        })
                        ->where(function ($query) use ($creator) {
                            $query->orwhere('creator', 'LIKE', "%$creator%");
                        })
                        ->where(function ($query) use ($userCompany) {
                            $query->orwhere('company', 'LIKE', "%$userCompany%");
                        })
                        ->select('stocks_header.*')
                        ->orderBy($orderBy, $orderByDir)->paginate($perPage);
                }
                return new StockDataCollection($query);
            }
        } // end

//        return new StockDataCollection($query);
    }
    public function getApprovalData(Request $request)
    {
//        if (!Gate::allows('stock_level_one_view') || !Gate::allows('stock_level_two_view') || !Gate::allows('permission_view')) {
//            return abort(403);
//        }
        if ($request->showAll) {
            $query = StockHeader::orderBy('stock_req_no', 'desc')->get();
        } else {
            $name = auth()->user()->name;
            $searchValue = $request->search;
            $orderBy = $request->sortby;
            $orderByDir = $request->sortdir;
            $perPage = $request->currentpage;
            $searchStockNumber = $request->stockReqNumber;
            $creator = $request->creator;
            $status = $request->status;
            $company = $request->company;
            $userCompany = $request->userCompany;
            if ($creator === "null") {
                $creator = null;
            }
            if ($status === "null") {
                $status = null;
            }
            if ($company === "null") {
                $company = null;
            }

            $from = null;
            $to = null;

            if ($request->dateFromAndTo !== null && $request->dateFromAndTo !== "") {
                $split = explode(",", $request->dateFromAndTo);
                $from_x = $split[0];
                $to_x = $split[1];
                if ($to_x === '' || $from_x === '') {
                    $from = null;
                    $to = null;
                } else {
                    $from = DateTime::createFromFormat('d/m/Y', $from_x)->format('Y/m/d');
                    $to = DateTime::createFromFormat('d/m/Y', $to_x)->format('Y/m/d');
                }
            } else if ($request->dateFromAndTo === null && $request->dateFromAndTo === "") {
                $from = null;
                $to = null;
            }
            $level1 = User::permission('stock_level_one_view')->first();
            $level2 = User::permission('stock_level_two_view')->first();
            if ($from !== null && $to !== null) {
                if($name === $level1->name || $name === $level2->name){
                    if($name === $level1->name){
                        $query = StockHeader::join('approval_mapping as a','stocks_header.stock_req_no','=','a.stock_no')
                            ->where(function ($query)  {
                                $query->whereNull('a.level_one')
                                    ->whereNull('a.level_two');
                            })
                            ->whereBetween('stocks_header.stock_req_date', [Carbon::parse($from)->startOfDay(), Carbon::parse($to)->endOfDay()])
                            ->where(function ($query) use ($searchStockNumber) {
                                $query->orwhere('stocks_header.stock_req_no', 'LIKE', "%$searchStockNumber%");
                            })
                            ->where(function ($query) use ($company) {
                                $query->orwhere('stocks_header.company', 'LIKE', "%$company%");
                            })
                            ->where(function ($query) use ($status) {
                                $query->orwhere('stocks_header.status', 'LIKE', "%$status%");
                            })
                            ->where(function ($query) use ($creator) {
                                $query->orwhere('stocks_header.creator', 'LIKE', "%$creator%");
                            })
                            ->where(function ($query) use ($userCompany) {
                                $query->orwhere('stocks_header.company', 'LIKE', "%$userCompany%");
                            })
                            ->select('stocks_header.*')
                            ->orderBy($orderBy, $orderByDir)->paginate($perPage);
                    }
                    if($name === $level2->name){
                        $query = StockHeader::join('approval_mapping as a','stocks_header.stock_req_no','=','a.stock_no')
                            ->where(function ($query)  {
                                $query->where('a.level_one','=','APPROVED')
                                    ->orwhere('a.level_one','!=','REJECTED');
                            })
                            ->whereBetween('stocks_header.stock_req_date', [Carbon::parse($from)->startOfDay(), Carbon::parse($to)->endOfDay()])
                            ->where(function ($query) use ($searchStockNumber) {
                                $query->orwhere('stocks_header.stock_req_no', 'LIKE', "%$searchStockNumber%");
                            })
                            ->where(function ($query) use ($company) {
                                $query->orwhere('stocks_header.company', 'LIKE', "%$company%");
                            })
                            ->where(function ($query) use ($status) {
                                $query->orwhere('stocks_header.status', 'LIKE', "%$status%");
                            })
                            ->where(function ($query) use ($creator) {
                                $query->orwhere('stocks_header.creator', 'LIKE', "%$creator%");
                            })
                            ->where(function ($query) use ($userCompany) {
                                $query->orwhere('stocks_header.company', 'LIKE', "%$userCompany%");
                            })
                            ->select('stocks_header.*')
                            ->orderBy($orderBy, $orderByDir)->paginate($perPage);
                    }
                }else{
                    $query = StockHeader::where('creator','=',$name)
                        ->whereBetween('stock_req_date', [Carbon::parse($from)->startOfDay(), Carbon::parse($to)->endOfDay()])
                        ->where(function ($query) use ($searchStockNumber) {
                            $query->orwhere('stock_req_no', 'LIKE', "%$searchStockNumber%");
                        })
                        ->where(function ($query) use ($company) {
                            $query->orwhere('company', 'LIKE', "%$company%");
                        })
                        ->where(function ($query) use ($status) {
                            $query->orwhere('status', 'LIKE', "%$status%");
                        })
                        ->where(function ($query) use ($creator) {
                            $query->orwhere('creator', 'LIKE', "%$creator%");
                        })
                        ->where(function ($query) use ($userCompany) {
                            $query->orwhere('company', 'LIKE', "%$userCompany%");
                        })
                        ->select('stocks_header.*')
                        ->orderBy($orderBy, $orderByDir)->paginate($perPage);
                }
                return new StockDataCollection($query);
            } else if ($from === null && $to === null) {
                if($name === $level1->name || $name === $level2->name){
                    if($name === $level1->name){
                        $query = StockHeader::join('approval_mapping as a','stocks_header.stock_req_no','=','a.stock_no')
                            ->where(function ($query)  {
                                $query->whereNull('a.level_one')
                                    ->whereNull('a.level_two');
                            })
                            ->where(function ($query) use ($searchStockNumber) {
                                $query->orwhere('stocks_header.stock_req_no', 'LIKE', "%$searchStockNumber%");
                            })
                            ->where(function ($query) use ($company) {
                                $query->orwhere('stocks_header.company', 'LIKE', "%$company%");
                            })
                            ->where(function ($query) use ($status) {
                                $query->orwhere('stocks_header.status', 'LIKE', "%$status%");
                            })
                            ->where(function ($query) use ($creator) {
                                $query->orwhere('stocks_header.creator', 'LIKE', "%$creator%");
                            })
                            ->where(function ($query) use ($userCompany) {
                                $query->orwhere('stocks_header.company', 'LIKE', "%$userCompany%");
                            })
                            ->select('stocks_header.*')
                            ->orderBy($orderBy, $orderByDir)->paginate($perPage);
                    }
                    if($name === $level2->name){
                        $query = StockHeader::join('approval_mapping as a','stocks_header.stock_req_no','=','a.stock_no')
                            ->where(function ($query)  {
                                $query->where('a.level_one','=','APPROVED')
                                    ->orwhere('a.level_one','!=','REJECTED');
                            })
                            ->where(function ($query)  {
                                $query->whereNull('a.level_two');
                            })
                            ->where(function ($query) use ($searchStockNumber) {
                                $query->orwhere('stocks_header.stock_req_no', 'LIKE', "%$searchStockNumber%");
                            })
                            ->where(function ($query) use ($company) {
                                $query->orwhere('stocks_header.company', 'LIKE', "%$company%");
                            })
                            ->where(function ($query) use ($status) {
                                $query->orwhere('stocks_header.status', 'LIKE', "%$status%");
                            })
                            ->where(function ($query) use ($creator) {
                                $query->orwhere('stocks_header.creator', 'LIKE', "%$creator%");
                            })
                            ->where(function ($query) use ($userCompany) {
                                $query->orwhere('stocks_header.company', 'LIKE', "%$userCompany%");
                            })
                            ->select('stocks_header.*')
                            ->orderBy($orderBy, $orderByDir)->paginate($perPage);
                    }
                }
                else {
                    $query = StockHeader::where('creator','=',$name)
                        ->where(function ($query) use ($searchStockNumber) {
                            $query->orwhere('stock_req_no', 'LIKE', "%$searchStockNumber%");
                        })
                        ->where(function ($query) use ($company) {
                            $query->orwhere('company', 'LIKE', "%$company%");
                        })
                        ->where(function ($query) use ($status) {
                            $query->orwhere('status', 'LIKE', "%$status%");
                        })
                        ->where(function ($query) use ($creator) {
                            $query->orwhere('creator', 'LIKE', "%$creator%");
                        })
                        ->where(function ($query) use ($userCompany) {
                            $query->orwhere('company', 'LIKE', "%$userCompany%");
                        })
                        ->select('stocks_header.*')
                        ->orderBy($orderBy, $orderByDir)->paginate($perPage);
                }
                return new StockDataCollection($query);
            }

        }

//        return new StockDataCollection($query);
    }
    public function getApprovalDataForPrinting(Request $request)
    {
//        if (!Gate::allows('stock_level_one_view') || !Gate::allows('stock_level_two_view') || !Gate::allows('permission_view')) {
//            return abort(403);
//        }
        if ($request->showAll) {
            $query = StockHeader::orderBy('stock_req_no', 'desc')->get();
        } else {
            $name = auth()->user()->name;
            $searchValue = $request->search;
            $orderBy = $request->sortby;
            $orderByDir = $request->sortdir;
            $perPage = $request->currentpage;
            $searchStockNumber = $request->stockReqNumber;
            $creator = $request->creator;
            $status = $request->status;
            $company = $request->company;
            $userCompany = $request->userCompany;
            if ($creator === "null") {
                $creator = null;
            }
            if ($status === "null") {
                $status = null;
            }
            if ($company === "null") {
                $company = null;
            }

            $from = null;
            $to = null;

            if ($request->dateFromAndTo !== null && $request->dateFromAndTo !== "") {
                $split = explode(",", $request->dateFromAndTo);
                $from_x = $split[0];
                $to_x = $split[1];
                if ($to_x === '' || $from_x === '') {
                    $from = null;
                    $to = null;
                } else {
                    $from = DateTime::createFromFormat('d/m/Y', $from_x)->format('Y/m/d');
                    $to = DateTime::createFromFormat('d/m/Y', $to_x)->format('Y/m/d');
                }
            } else if ($request->dateFromAndTo === null && $request->dateFromAndTo === "") {
                $from = null;
                $to = null;
            }
            $level1 = User::permission('stock_level_one_view')->first();
            $level2 = User::permission('stock_level_two_view')->first();
            if ($from !== null && $to !== null) {
                if($name === $level1->name || $name === $level2->name){
                    if($name === $level1->name){
                        $query = StockHeader::join('approval_mapping as a','stocks_header.stock_req_no','=','a.stock_no')
                            ->where(function ($query)  {
                                $query->where(function ($query)  {
                                    $query->whereNull('a.level_one')
                                        ->whereNull('a.level_two');
                                })->orwhere(function ($query)  {
                                    $query->where('a.level_one','=','APPROVED')
                                        ->orwhere('a.level_two','=','REJECTED')
                                        ->orwhere('a.level_two','=','APPROVED');
                                });
                            })
                            ->whereBetween('stocks_header.stock_req_date', [Carbon::parse($from)->startOfDay(), Carbon::parse($to)->endOfDay()])
                            ->where(function ($query) use ($searchStockNumber) {
                                $query->orwhere('stocks_header.stock_req_no', 'LIKE', "%$searchStockNumber%");
                            })
                            ->where(function ($query) use ($company) {
                                $query->orwhere('stocks_header.company', 'LIKE', "%$company%");
                            })
                            ->where(function ($query) use ($status) {
                                $query->orwhere('stocks_header.status', 'LIKE', "%$status%");
                            })
                            ->where(function ($query) use ($creator) {
                                $query->orwhere('stocks_header.creator', 'LIKE', "%$creator%");
                            })
                            ->where(function ($query) use ($userCompany) {
                                $query->orwhere('stocks_header.company', 'LIKE', "%$userCompany%");
                            })
                            ->select('stocks_header.*')
                            ->orderBy($orderBy, $orderByDir)->paginate($perPage);
                    }
                    if($name === $level2->name){
                        $query = StockHeader::join('approval_mapping as a','stocks_header.stock_req_no','=','a.stock_no')
                            ->where(function ($query)  {
                                $query->where(function ($query)  {
                                    $query->where('a.level_one','=','APPROVED')
                                        ->where('a.level_one','!=','REJECTED');
                                })->orwhere(function ($query)  {
                                    $query->where('a.level_two','!=','REJECTED')
                                        ->where('a.level_two','=','APPROVED');
                                });
                            })
                            ->whereBetween('stocks_header.stock_req_date', [Carbon::parse($from)->startOfDay(), Carbon::parse($to)->endOfDay()])
                            ->where(function ($query) use ($searchStockNumber) {
                                $query->orwhere('stocks_header.stock_req_no', 'LIKE', "%$searchStockNumber%");
                            })
                            ->where(function ($query) use ($company) {
                                $query->orwhere('stocks_header.company', 'LIKE', "%$company%");
                            })
                            ->where(function ($query) use ($status) {
                                $query->orwhere('stocks_header.status', 'LIKE', "%$status%");
                            })
                            ->where(function ($query) use ($creator) {
                                $query->orwhere('stocks_header.creator', 'LIKE', "%$creator%");
                            })
                            ->where(function ($query) use ($userCompany) {
                                $query->orwhere('stocks_header.company', 'LIKE', "%$userCompany%");
                            })
                            ->select('stocks_header.*')
                            ->orderBy($orderBy, $orderByDir)->paginate($perPage);
                    }
                }else{
                    $query = StockHeader::where('creator','=',$name)
                        ->whereBetween('stock_req_date', [Carbon::parse($from)->startOfDay(), Carbon::parse($to)->endOfDay()])
                        ->where(function ($query) use ($searchStockNumber) {
                            $query->orwhere('stock_req_no', 'LIKE', "%$searchStockNumber%");
                        })
                        ->where(function ($query) use ($company) {
                            $query->orwhere('company', 'LIKE', "%$company%");
                        })
                        ->where(function ($query) use ($status) {
                            $query->orwhere('status', 'LIKE', "%$status%");
                        })
                        ->where(function ($query) use ($creator) {
                            $query->orwhere('creator', 'LIKE', "%$creator%");
                        })
                        ->where(function ($query) use ($userCompany) {
                            $query->orwhere('company', 'LIKE', "%$userCompany%");
                        })
                        ->select('stocks_header.*')
                        ->orderBy($orderBy, $orderByDir)->paginate($perPage);
                }
                return new StockDataCollection($query);
            } else if ($from === null && $to === null) {
                if($name === $level1->name || $name === $level2->name){
                    if($name === $level1->name){
                        $query = StockHeader::join('approval_mapping as a','stocks_header.stock_req_no','=','a.stock_no')
                            ->where(function ($query)  {
                                $query->where(function ($query)  {
                                    $query->whereNull('a.level_one')
                                        ->whereNull('a.level_two');
                                })->orwhere(function ($query)  {
                                    $query->where('a.level_one','=','APPROVED')
                                        ->orwhere('a.level_two','=','REJECTED')
                                        ->orwhere('a.level_two','=','APPROVED');
                                });
                            })
                            ->where(function ($query) use ($searchStockNumber) {
                                $query->orwhere('stocks_header.stock_req_no', 'LIKE', "%$searchStockNumber%");
                            })
                            ->where(function ($query) use ($company) {
                                $query->orwhere('stocks_header.company', 'LIKE', "%$company%");
                            })
                            ->where(function ($query) use ($status) {
                                $query->orwhere('stocks_header.status', 'LIKE', "%$status%");
                            })
                            ->where(function ($query) use ($creator) {
                                $query->orwhere('stocks_header.creator', 'LIKE', "%$creator%");
                            })
                            ->where(function ($query) use ($userCompany) {
                                $query->orwhere('stocks_header.company', 'LIKE', "%$userCompany%");
                            })
                            ->select('stocks_header.*')
                            ->orderBy($orderBy, $orderByDir)->paginate($perPage);
                    }
                    if($name === $level2->name){
                        $query = StockHeader::join('approval_mapping as a','stocks_header.stock_req_no','=','a.stock_no')
                            ->where(function ($query)  {
                                $query->where('a.level_one','=','APPROVED')
                                    ->where('a.level_one','!=','REJECTED')
                                    ->where('a.level_two','!=','REJECTED');
                            })
//                            ->where(function ($query)  {
//                                $query->whereNull('a.level_two');
//                            })
                            ->where(function ($query) use ($searchStockNumber) {
                                $query->orwhere('stocks_header.stock_req_no', 'LIKE', "%$searchStockNumber%");
                            })
                            ->where(function ($query) use ($company) {
                                $query->orwhere('stocks_header.company', 'LIKE', "%$company%");
                            })
                            ->where(function ($query) use ($status) {
                                $query->orwhere('stocks_header.status', 'LIKE', "%$status%");
                            })
                            ->where(function ($query) use ($creator) {
                                $query->orwhere('stocks_header.creator', 'LIKE', "%$creator%");
                            })
                            ->where(function ($query) use ($userCompany) {
                                $query->orwhere('stocks_header.company', 'LIKE', "%$userCompany%");
                            })
                            ->select('stocks_header.*')
                            ->orderBy($orderBy, $orderByDir)->paginate($perPage);
                    }
                }
                else {
                    $query = StockHeader::where('creator','=',$name)
                        ->where(function ($query) use ($searchStockNumber) {
                            $query->orwhere('stock_req_no', 'LIKE', "%$searchStockNumber%");
                        })
                        ->where(function ($query) use ($company) {
                            $query->orwhere('company', 'LIKE', "%$company%");
                        })
                        ->where(function ($query) use ($status) {
                            $query->orwhere('status', 'LIKE', "%$status%");
                        })
                        ->where(function ($query) use ($creator) {
                            $query->orwhere('creator', 'LIKE', "%$creator%");
                        })
                        ->where(function ($query) use ($userCompany) {
                            $query->orwhere('company', 'LIKE', "%$userCompany%");
                        })
                        ->select('stocks_header.*')
                        ->orderBy($orderBy, $orderByDir)->paginate($perPage);
                }
                return new StockDataCollection($query);
            }

        }

//        return new StockDataCollection($query);
    }
    public function viewStockData(Request $request, $id)
    {
        if ($request->showAll) {
            $query = StockHeader::orderBy('stock_req_no', 'desc')->get();
        } else {
            $searchValue = $request->search;
            $orderBy = $request->sortby;
            $orderByDir = $request->sortdir;
            $perPage = $request->currentpage;

            $query = StockHeader::join('stock_no as a', 'stocks_header.stock_req_no', '=', 'a.stock_req_no')
                ->join('stocks as b', 'a.stocks_id', '=', 'b.id')
                ->where('stocks_header.stock_req_no', '=', $id)
                ->select('b.*', 'stocks_header.stock_req_no')
                ->paginate($perPage);
        }
        return new StockDataCollection($query);
    }
    public function headerStockData(Request $request, $id)
    {
        $header = StockHeader::where('stock_req_no', '=', $id)
            ->select('stocks_header.*')
            ->get();

        return response()->json($header);
    }


    public function creatorUser()
    {

        $name = auth()->user()->name;
        $checkIfCreator = User::permission('merchandiser_creator_stock_request_data')->get();

        if ($checkIfCreator->toArray() !== []) {
            foreach ($checkIfCreator as $key => $user) {
                if ($name === $user['name']) {
                    $query = StockHeader::where('creator', '=', $name)
                        ->select('creator')
                        ->distinct()
                        ->first();
                } else if ($name !== $user['name']) {
                    $query = null;
                }
            }
        } else {
            $query = null;
        }

        return response()->json($query);
    }

    public function allCompanyUser()
    {
        $databaseName = Config::get('database.connections');
//        $query = CustomerMaster::join($databaseName['sqlsrv2']['database'] . '.dbo.KNVV as a', 'CUSTOMER_MASTER.KUNNR', '=', 'a.KUNNR')
//            ->where('a.VKORG', '=', '1902')
//            ->select('a.VKORG', 'CUSTOMER_MASTER.NAME1', 'CUSTOMER_MASTER.id')
//            ->get();

        $query = KNA1::join($databaseName['sqlsrv2']['database'] . '.dbo.KNVV as a', 'KNA1.KUNNR', '=', 'a.KUNNR')
            ->where('a.VKORG', '=', '1902')
            ->select('a.VKORG', 'KNA1.NAME1')
            ->get();


        return response()->json($query);
    }

    public function allCreatorUser()
    {

        $query = StockHeader::select('creator')->distinct()->get();
        return response()->json($query);
    }
    public function divertDratftToStocks(Request $request,$id)
    {
        $query = Stocks::divertDraftsToStocks($request,$id);
        return response()->json($query);
    }
}
