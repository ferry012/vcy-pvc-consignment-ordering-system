<?php

namespace App\Http\Controllers\PVC;

use App\Drafts;
use App\DraftsHeader;
use App\DraftsNo;
use App\Http\Controllers\Controller;
use App\Http\Resources\DraftsCollection;
use App\Http\Resources\StockDataCollection;
use App\StockHeader;
use Carbon\Carbon;
use Illuminate\Http\Request;
use DateTime;

class DraftsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->showAll) {
            $query = DraftsHeader::orderBy('drafts_req_no', 'desc')->get();
        } else {
            $name = auth()->user()->name;
            $searchValue = $request->search;
            $orderBy = $request->sortby;
            $orderByDir = $request->sortdir;
            $perPage = $request->currentpage;

            $draftNumber = $request->draftNumber;
            $company = $request->company;


            $from = null;
            $to = null;

            if ($request->dateFromAndTo !== null && $request->dateFromAndTo !== "") {
                $split = explode(",", $request->dateFromAndTo);
                $from_x = $split[0];
                $to_x = $split[1];
                if ($to_x === '' || $from_x === '') {
                    $from = null;
                    $to = null;
                } else {
                    $from = DateTime::createFromFormat('d/m/Y', $from_x)->format('Y/m/d');
                    $to = DateTime::createFromFormat('d/m/Y', $to_x)->format('Y/m/d');
                }
            } else if ($request->dateFromAndTo === null && $request->dateFromAndTo === "") {
                $from = null;
                $to = null;
            }

            if ($from !== null && $to !== null) {
                $query = DraftsHeader::where('creator', '=', $name)
                    ->whereNotNull('save_as')
                    ->whereBetween('drafts_req_date', [Carbon::parse($from)->startOfDay(), Carbon::parse($to)->endOfDay()])
                    ->where(function ($query) use ($draftNumber) {
                        $query->orwhere('drafts_req_no', 'LIKE', "%$draftNumber%");
                    })
                    ->where(function ($query) use ($company) {
                        $query->orwhere('company', 'LIKE', "%$company%");
                    })
                    ->orderBy($orderBy, $orderByDir)->paginate($perPage);
                return new DraftsCollection($query);
            } else if ($from === null && $to === null) {
                $query = DraftsHeader::where('creator', '=', $name)
                    ->whereNotNull('save_as')
                    ->where(function ($query) use ($draftNumber) {
                        $query->orwhere('drafts_req_no', 'LIKE', "%$draftNumber%");
                    })
                    ->where(function ($query) use ($company) {
                        $query->orwhere('company', 'LIKE', "%$company%");
                    })
                    ->orderBy($orderBy, $orderByDir)->paginate($perPage);
                return new DraftsCollection($query);
            }

        }

//        return new StockDataCollection($query);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $query = Drafts::CreateDrafts($request);
        return response()->json($query);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {

        if ($request->showAll) {
            $query = DraftsHeader::orderBy('drafts_req_no', 'desc')->get();
        } else {
            $searchValue = $request->search;
            $orderBy = $request->sortby;
            $orderByDir = $request->sortdir;
            $perPage = $request->currentpage;

            $query = DraftsHeader::join('drafts_no as a', 'drafts_header.drafts_req_no', '=', 'a.drafts_req_no')
                ->join('drafts as b', 'a.drafts_id', '=', 'b.id')
                ->where('drafts_header.drafts_req_no', '=', $id)
                ->select('b.*','drafts_header.drafts_req_no')
                ->get();
        }
//        return response()->json($query);
        return new DraftsCollection($query);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $Drafts = Drafts::join('drafts_no as a', 'drafts.id', '=', 'a.drafts_id')
            ->where('a.drafts_req_no', '=', $id)
            ->select('drafts.id as id')
            ->get();
        $arr = [];
        foreach ($Drafts as $key => $code){
            $query = Drafts::findOrFail($code['id']);
            $query->delete();
        }
        $DraftsHeader = DraftsHeader::where('drafts_req_no', $id)->delete();
//        $DraftsNo = DraftsNo::where('drafts_req_no', '=', $id)->delete();

        $host = gethostname();
        $nameUser = auth()->user()->name;
        $clientIP = $_SERVER['HTTP_CLIENT_IP']
            ?? $_SERVER["HTTP_CF_CONNECTING_IP"] # when behind cloudflare
            ?? $_SERVER['HTTP_X_FORWARDED']
            ?? $_SERVER['HTTP_X_FORWARDED_FOR']
            ?? $_SERVER['HTTP_FORWARDED']
            ?? $_SERVER['HTTP_FORWARDED_FOR']
            ?? $_SERVER['REMOTE_ADDR']
            ?? '0.0.0.0';

        $clientIP = '0.0.0.0';

        if (isset($_SERVER['HTTP_CLIENT_IP'])) {
            $clientIP = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (isset($_SERVER['HTTP_CF_CONNECTING_IP'])) {
            $clientIP = $_SERVER['HTTP_CF_CONNECTING_IP'];
        } elseif (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $clientIP = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } elseif (isset($_SERVER['HTTP_X_FORWARDED'])) {
            $clientIP = $_SERVER['HTTP_X_FORWARDED'];
        } elseif (isset($_SERVER['HTTP_FORWARDED_FOR'])) {
            $clientIP = $_SERVER['HTTP_FORWARDED_FOR'];
        } elseif (isset($_SERVER['HTTP_FORWARDED'])) {
            $clientIP = $_SERVER['HTTP_FORWARDED'];
        } elseif (isset($_SERVER['REMOTE_ADDR'])) {
            $clientIP = $_SERVER['REMOTE_ADDR'];
        }

        activity('Drafts - Delete')
            ->log('[WEB]' . ' ' . $nameUser . ' ' . 'has deleted a draft #' . ' ' .$id .' '
                . 'using' . ' ' . $host . ' ' . 'computer'. ' '. ' with an ip address of ' . ' ' .$clientIP);
    }

    public function getDraftsHeader(Request $request, $id)
    {
        $header = DraftsHeader::where('drafts_header.drafts_req_no', '=', $id)
            ->select('drafts_header.*')
            ->get();

        return response()->json($header);
    }

    public function updateDrafts(Request $request, $id)
    {
        $query = Drafts::updateDrafts($request,$id);
        return response()->json($query);
    }
}
