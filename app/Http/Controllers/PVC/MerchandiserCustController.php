<?php

namespace App\Http\Controllers\PVC;

use App\CustomerMaster;
use App\Http\Controllers\Controller;
use App\KNA1;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;

class MerchandiserCustController extends Controller
{
    public function getCust(Request $request){
        $databaseName = Config::get('database.connections');

//        $query = CustomerMaster::join($databaseName['sqlsrv2']['database'] . '.dbo.KNVV as a','CUSTOMER_MASTER.KUNNR','=','a.KUNNR')
//            ->where('a.VKORG','=','1902')
//            ->select('a.VKORG','CUSTOMER_MASTER.NAME1','CUSTOMER_MASTER.id')
//            ->get();
//        return response()->json($query);

        $query = KNA1::join($databaseName['sqlsrv2']['database'] . '.dbo.KNVV as a','KNA1.KUNNR','=','a.KUNNR')
            ->where('a.VKORG','=','1902')
            ->select('a.VKORG','KNA1.NAME1')
            ->get();
        return response()->json($query);
    }
    public function getCustStockCreation(){
        $databaseName = Config::get('database.connections');
        $userId = auth()->user()->id;

        $query = User::join('merchandiser_cust as a','users.id','=','a.users_id')
            ->where('users.id','=',$userId)
            ->select('a.NAME1','a.*')
            ->get();

        return response()->json($query);
    }
}
