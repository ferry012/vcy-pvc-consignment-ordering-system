<?php

namespace App\Http\Controllers\PVC;

use App\Http\Controllers\Controller;
use App\INVENTORY;
use App\MaterialMaster;
use App\MVKE;
use App\StockHeader;
use App\Stocks;
use App\VBAP;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

class StockRequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $query = Stocks::CreateStocks($request);
        return response()->json($query);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getMatGroup()
    {
        $databaseName = Config::get('database.connections');
        $getmatgroup = MaterialMaster::select('WGBEZ')
            ->distinct()
            ->get();
        return response()->json($getmatgroup);
    }
    public function getAllBarcode(Request $request){

        $databaseName = Config::get('database.connections');
        $query = MaterialMaster::join('MARC as a','MATERIAL_MASTER.MATNR','=','a.MATNR')
            ->join('INVENTORY as b','MATERIAL_MASTER.MATNR','=','b.MATNR')
            ->where(function ($query) {
               $query->where('MATERIAL_MASTER.EAN11','!=','')
                   ->where('MATERIAL_MASTER.MATNR','!=','')
                    ->where('a.WERKS','=','9021')
                    ->where('b.WERKS','=','9021')
                   ->where('b.LGORT','!=','9099');
                })
               ->select('MATERIAL_MASTER.EAN11', 'MATERIAL_MASTER.MAKTX', 'MATERIAL_MASTER.MATNR')
               ->get();

        return response()->json($query);

    }
    public function getMatCode(Request $request)
    {
        $databaseName = Config::get('database.connections');

        $getmatcode = MaterialMaster::where('MATERIAL_MASTER.EAN11', '=', $request->barcode)
            ->select('MATERIAL_MASTER.MATNR', 'MATERIAL_MASTER.MAKTX')
            ->first();


        $sales_unit = '';
        $getMVKE = MaterialMaster::leftjoin($databaseName['sqlsrv2']['database'] . '.dbo.MVKE as a', function ($join) {
                $join->on('MATERIAL_MASTER.MATNR', '=', 'a.MATNR');
                $join->on('a.VKORG', '=', DB::raw('1902'));
            })
            ->where(function ($query) use ($request) {
                $query->where('MATERIAL_MASTER.EAN11', '=', $request->barcode)
                        ->where('a.VKORG','=','1902');
            })
            ->select('MATERIAL_MASTER.MEINS','a.VRKME')
            ->first();

        if($getMVKE !== null){
            $string =  preg_replace('/\s+/', '', $getMVKE->VRKME);

            if($getMVKE->VRKME === null || $string !== ''){
                $sales_unit = $getMVKE->VRKME;
                $errorMVKE = null;
            }
            else{
                $sales_unit = $getMVKE->MEINS;
                $errorMVKE = null;
            }
        }
        else{
            $baseUnit = MaterialMaster::where('EAN11','=',$request->barcode)
                ->select('MEINS')
                ->first();
            $sales_unit = $baseUnit->MEINS;
        }


        $getDateAndQty = VBAP::join($databaseName['sqlsrv2']['database'] . '.dbo.VBAK as a', 'VBAP.VBELN', '=', 'a.VBELN')
            ->join($databaseName['sqlsrv2']['database'] . '.dbo.KNA1 as b', 'a.KUNNR', '=', 'b.KUNNR')
            ->where(function ($query) use ($getmatcode,$request) {
                $query->where('a.AUART', '=', 'ZKB')
                    ->where('VBAP.MATNR', '=', $getmatcode->MATNR)
                    ->where('b.NAME1','=',$request->custCompany)
                    ->where('VBAP.KWMENG', '!=', 0);
            })
            ->select('VBAP.ERDAT', 'VBAP.KWMENG')
            ->orderBy('VBAP.ERDAT', 'desc')
            ->first();


        if($getDateAndQty !== null){
//            $erroVBAP = null;
            $valERDAT = $getDateAndQty->ERDAT;
            $valKWMENG = $getDateAndQty->KWMENG;
        }
        else{
            $valERDAT = null;
            $valKWMENG = null;
//            $erroVBAP = 'No History Found';
        }

        $SAPUNRESTRICTEDQTY = INVENTORY::join($databaseName['sqlsrv2']['database'] . '.dbo.MATERIAL_MASTER as a', 'INVENTORY.MATNR', '=', 'a.MATNR')
            ->where(function ($query) {
                $query->where('INVENTORY.WERKS', '=', '9021')
                    ->where('INVENTORY.LGORT', '!=', '9099');
            })
            ->where('a.EAN11', '=', $request->barcode)
            ->select('INVENTORY.LABST')
            ->first();

        if($SAPUNRESTRICTEDQTY === null){
            $sapQTY = 'No Inventory Found';
            $sapLABST =  null;
        }
        else{
            $sapQTY = null;
            $sapLABST = $SAPUNRESTRICTEDQTY->LABST;
        }

        return response()->json([
            'MATNR' => $getmatcode->MATNR,
            'MAKTX' => $getmatcode->MAKTX,
            'MEINH' => $sales_unit,
            'ERDAT' => $valERDAT, //last fill up date
            'KWMENG' => $valKWMENG, // last fill up qty
//            'ERRORVBAP' => $erroVBAP,
            'ERRORSAPQTY' => $sapQTY,
            'LABST' => $sapLABST,
        ]);
    }

    public function getBarcode(Request $request)
    {

        $databaseName = Config::get('database.connections');
        $query = MaterialMaster::join('MARC as a','MATERIAL_MASTER.MATNR','=','a.MATNR')
            ->join('INVENTORY as b','MATERIAL_MASTER.MATNR','=','b.MATNR')
            ->where('MATERIAL_MASTER.WGBEZ', '=', $request->MatGroup)
            ->where(function ($query) {
                $query->where('MATERIAL_MASTER.EAN11','!=','')
                        ->where('MATERIAL_MASTER.MATNR','!=','')
                        ->where('a.WERKS','=','9021')
                        ->where('b.WERKS','=','9021')
                        ->where('b.LGORT','!=','9099');
            })
            ->select('MATERIAL_MASTER.EAN11', 'MATERIAL_MASTER.MAKTX', 'MATERIAL_MASTER.MATNR')
            ->get();

        return response()->json($query);
    }
}
