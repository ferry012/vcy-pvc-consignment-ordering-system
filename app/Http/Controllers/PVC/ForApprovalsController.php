<?php

namespace App\Http\Controllers\PVC;

use App\ApprovalMappingRequest;
use App\Http\Controllers\Controller;
use App\Http\Resources\ForApprovalsCollection;
use App\Jobs\PVCStockApproveJob;
use App\Jobs\PVCStockApproveToMerchJob;
use App\Jobs\PVCStockRejectJob;
use App\Jobs\PVCStockRejectMerchJob;
use App\Jobs\PVCStockRejectToSecApproverJob;
use App\Mail\PVCStockApproveToMerchMail;
use App\SalesOrderDetails;
use App\SalesOrderHeader;
use App\StockHeader;
use App\StockNo;
use App\Stocks;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;

class ForApprovalsController extends Controller
{
    public function approvalDetails(Request $request, $id)
    {
        if ($request->showAll) {
            $query = StockHeader::orderBy('stock_req_no', 'desc')->get();
        } else {
            $searchValue = $request->search;
            $orderBy = $request->sortby;
            $orderByDir = $request->sortdir;
            $perPage = $request->currentpage;
            $name = auth()->user()->name;
            $databaseName = Config::get('database.connections');

            $mapping = ApprovalMappingRequest::where('stock_no', '=', $id)
                ->select('level_one', 'level_two')
                ->first();

            $level1 = User::permission('stock_level_one_view')->first();
            $level2 = User::permission('stock_level_two_view')->first();

            if ($name === $level1->name) {
                if ($mapping['level_one'] !== null) {
                    if ($mapping['level_one'] === 'APPROVED' || $mapping['level_one'] === 'REJECTED') {
                        $query = [];
                    }
                } else {
                    $query = StockHeader::join('stock_no as a', 'stocks_header.stock_req_no', '=', 'a.stock_req_no')
                        ->join('stocks as b', 'a.stocks_id', '=', 'b.id')
                        ->join($databaseName['sqlsrv2']['database'] . '.dbo.INVENTORY as c', 'b.mat_code', '=', 'c.MATNR')
                        ->where('stocks_header.stock_req_no', '=', $id)
                        ->where(function ($query) {
                            $query->where('c.WERKS', '=', '9021')
                                ->where('c.LGORT', '!=', '9099');
                        })
                        ->select('b.*', 'stocks_header.stock_req_no', 'c.LABST')
                        ->get();
                }
            }
            if ($name === $level2->name) {
                if ($mapping['level_one'] !== null) {

                    if ($mapping['level_one'] === 'APPROVED') {
                        $query = StockHeader::join('stock_no as a', 'stocks_header.stock_req_no', '=', 'a.stock_req_no')
                            ->join('stocks as b', 'a.stocks_id', '=', 'b.id')
                            ->join($databaseName['sqlsrv2']['database'] . '.dbo.INVENTORY as c', 'b.mat_code', '=', 'c.MATNR')
                            ->where(function ($query) {
                                $query->where('c.WERKS', '=', '9021')
                                    ->where('c.LGORT', '!=', '9099');
                            })
                            ->where('stocks_header.stock_req_no', '=', $id)
                            ->select('b.*', 'stocks_header.stock_req_no', 'c.LABST')
                            ->get();
                    }
                    if ($mapping['level_one'] === 'REJECTED') {
                        $query = [];
                    }
                } else {
                    $query = [];
                }
            } else if ($name !== $level1->name && $name !== $level2->name) {
                $query = StockHeader::join('stock_no as a', 'stocks_header.stock_req_no', '=', 'a.stock_req_no')
                    ->join('stocks as b', 'a.stocks_id', '=', 'b.id')
                    ->join($databaseName['sqlsrv2']['database'] . '.dbo.INVENTORY as c', 'b.mat_code', '=', 'c.MATNR')
                    ->where(function ($query) {
                        $query->where('c.WERKS', '=', '9021')
                            ->where('c.LGORT', '!=', '9099');
                    })
                    ->where('stocks_header.stock_req_no', '=', $id)
                    ->select('b.*', 'stocks_header.stock_req_no', 'c.LABST')
                    ->get();
            }
        }
        return new ForApprovalsCollection($query);
    }
    public function approvalDetailsPrinting(Request $request, $id)
    {
        if ($request->showAll) {
            $query = StockHeader::orderBy('stock_req_no', 'desc')->get();
        } else {
            $searchValue = $request->search;
            $orderBy = $request->sortby;
            $orderByDir = $request->sortdir;
            $perPage = $request->currentpage;
            $name = auth()->user()->name;
            $databaseName = Config::get('database.connections');

            $mapping = ApprovalMappingRequest::where('stock_no', '=', $id)
                ->select('level_one', 'level_two')
                ->first();

            $level1 = User::permission('stock_level_one_view')->first();
            $level2 = User::permission('stock_level_two_view')->first();

            if ($name === $level1->name) {
                if ($mapping['level_one'] !== null) {
                    if ($mapping['level_one'] === 'APPROVED' || $mapping['level_one'] === 'REJECTED') {
                        $query = StockHeader::join('stock_no as a', 'stocks_header.stock_req_no', '=', 'a.stock_req_no')
                            ->join('stocks as b', 'a.stocks_id', '=', 'b.id')
                            ->join($databaseName['sqlsrv2']['database'] . '.dbo.INVENTORY as c', 'b.mat_code', '=', 'c.MATNR')
                            ->where(function ($query) {
                                $query->where('c.WERKS', '=', '9021')
                                    ->where('c.LGORT', '!=', '9099');
                            })
                            ->where('stocks_header.stock_req_no', '=', $id)
                            ->select('b.*', 'stocks_header.stock_req_no', 'c.LABST')
                            ->get();
                    }
                } else {
                    $query = StockHeader::join('stock_no as a', 'stocks_header.stock_req_no', '=', 'a.stock_req_no')
                        ->join('stocks as b', 'a.stocks_id', '=', 'b.id')
                        ->join($databaseName['sqlsrv2']['database'] . '.dbo.INVENTORY as c', 'b.mat_code', '=', 'c.MATNR')
                        ->where(function ($query) {
                            $query->where('c.WERKS', '=', '9021')
                                ->where('c.LGORT', '!=', '9099');
                        })
                        ->where('stocks_header.stock_req_no', '=', $id)
                        ->select('b.*', 'stocks_header.stock_req_no', 'c.LABST')
                        ->get();
                }
            }
            if ($name === $level2->name) {
                if ($mapping['level_one'] !== null) {

                    if ($mapping['level_one'] === 'APPROVED') {
                        $query = StockHeader::join('stock_no as a', 'stocks_header.stock_req_no', '=', 'a.stock_req_no')
                            ->join('stocks as b', 'a.stocks_id', '=', 'b.id')
                            ->join($databaseName['sqlsrv2']['database'] . '.dbo.INVENTORY as c', 'b.mat_code', '=', 'c.MATNR')
                            ->where(function ($query) {
                                $query->where('c.WERKS', '=', '9021')
                                    ->where('c.LGORT', '!=', '9099');
                            })
                            ->where('stocks_header.stock_req_no', '=', $id)
                            ->select('b.*', 'stocks_header.stock_req_no', 'c.LABST')
                            ->get();
                    }
                    if ($mapping['level_one'] === 'REJECTED') {
                        $query = StockHeader::join('stock_no as a', 'stocks_header.stock_req_no', '=', 'a.stock_req_no')
                            ->join('stocks as b', 'a.stocks_id', '=', 'b.id')
                            ->join($databaseName['sqlsrv2']['database'] . '.dbo.INVENTORY as c', 'b.mat_code', '=', 'c.MATNR')
                            ->where(function ($query) {
                                $query->where('c.WERKS', '=', '9021')
                                    ->where('c.LGORT', '!=', '9099');
                            })
                            ->where('stocks_header.stock_req_no', '=', $id)
                            ->select('b.*', 'stocks_header.stock_req_no', 'c.LABST')
                            ->get();
                    }
                } else {
                    $query = StockHeader::join('stock_no as a', 'stocks_header.stock_req_no', '=', 'a.stock_req_no')
                        ->join('stocks as b', 'a.stocks_id', '=', 'b.id')
                        ->join($databaseName['sqlsrv2']['database'] . '.dbo.INVENTORY as c', 'b.mat_code', '=', 'c.MATNR')
                        ->where(function ($query) {
                            $query->where('c.WERKS', '=', '9021')
                                ->where('c.LGORT', '!=', '9099');
                        })
                        ->where('stocks_header.stock_req_no', '=', $id)
                        ->select('b.*', 'stocks_header.stock_req_no', 'c.LABST')
                        ->get();
                }
            } else if ($name !== $level1->name && $name !== $level2->name) {
                $query = StockHeader::join('stock_no as a', 'stocks_header.stock_req_no', '=', 'a.stock_req_no')
                    ->join('stocks as b', 'a.stocks_id', '=', 'b.id')
                    ->join($databaseName['sqlsrv2']['database'] . '.dbo.INVENTORY as c', 'b.mat_code', '=', 'c.MATNR')
                    ->where(function ($query) {
                        $query->where('c.WERKS', '=', '9021')
                            ->where('c.LGORT', '!=', '9099');
                    })
                    ->where('stocks_header.stock_req_no', '=', $id)
                    ->select('b.*', 'stocks_header.stock_req_no', 'c.LABST')
                    ->get();
            }
        }
        return new ForApprovalsCollection($query);
    }
    public function approvalHeader(Request $request, $id)
    {
        $header = StockHeader::where('stock_req_no', '=', $id)
            ->select('stocks_header.*')
            ->get();

        return response()->json($header);
    }

    public function approveRequest(Request $request, $id)
    {
        $name = auth()->user()->name;
        $level1 = User::permission('stock_level_one_view')->first();
        $level2 = User::permission('stock_level_two_view')->first();

        if ($name === $level1->name) {  // level one

            $storeCreator = StockHeader::where('stock_req_no', '=', $id)
                ->select('creator')
                ->first();

            $stocks = Stocks::join('stock_no as a', 'stocks.id', '=', 'a.stocks_id')
                ->where('a.stock_req_no', '=', $id)
                ->select('stocks.id as id')
                ->get();

            $arr = [];

            foreach ($stocks as $key => $code) {
                $query_Stocks = Stocks::findOrFail($code['id']);
                $query_Stocks->delete();
            }
            $DraftsHeader = StockHeader::where('stock_req_no', $id)->delete();
            $DraftsNo = StockNo::where('stock_req_no', '=', $id)->delete();
            $arrID = [];

            foreach ($request[0]['stocks'] as $key => $stocks) {
                $headerStockID = StockNo::where('stock_req_no', '=', $id)
                    ->select('stocks_id')
                    ->first();

                $data = array();
                $data['barcode'] = $stocks['barcode'];
                $data['mat_code'] = $stocks['mat_code'];
                $data['mat_des'] = $stocks['mat_des'];
                $data['sales_unit'] = $stocks['sales_unit'];
                $data['ERDAT'] = $stocks['ERDAT']; // date
                $data['KWMENG'] = $stocks['KWMENG'];
                if(!isset($stocks['stocks_left'])){ // false
                    $data['stocks_left'] = null;
                }
                elseif(isset($stocks['stocks_left'])){ //TRUE
                    $data['stocks_left'] = $stocks['stocks_left'];
                }

                if(!isset($stocks['order_qty'])){
                    $data['order_qty'] =  null;
                }
                elseif (isset($stocks['order_qty'])){
                    $data['order_qty'] =  $stocks['order_qty']; // suggest_order_qty
                }

                if(!isset($stocks['approve_qty'])){
                    $data['approve_qty'] = null;
                }
                elseif (isset($stocks['approve_qty'])){
                    $data['approve_qty'] = $stocks['approve_qty'];
                }
                if(!isset($stocks['box_no'])){
                    $data['box_no'] = null;
                }
                elseif(isset($stocks['box_no'])){
                    $data['box_no'] = $stocks['box_no'];
                }
//                $data['remarks'] = $stocks['remarks'];

                $query = Stocks::create($data);
                $getID = Stocks::select('id')->latest()->first();
                $arrID[] = [
                    'id' => $getID->id,
                ];
            }
            foreach ($arrID as $key => $stocks_id) {
                $query_stockNo = StockNo::create([
                    'stocks_id' => $stocks_id['id'],
                    'stock_req_no' => (integer)$id, // draft stock no
                ]);
            }
            $stock_header = StockHeader::create([
                'stock_req_no' => (integer)$id,  // draft stock no
                'stock_req_date' => $request[0]['header'][0]['req_date'],
                'company' => $request[0]['header'][0]['cust_company'],
                'creator' => $storeCreator->creator,
                'status' => 'PENDING',
            ]);
            $query = ApprovalMappingRequest::where('stock_no', '=', $id)
                ->update([
                    'level_one' => 'APPROVED',
                ]);

            $notifyUsersLevel2 = User::permission('email_approver_level_2')->get();
            $get_level_two = User::permission('stock_level_two_view')->first();
            $approver = User::permission('stock_level_one_view')->first();
            $usersToAdmins = User::permission('email_admin')->get();

            $getHeader = StockHeader::where('stocks_header.stock_req_no','=',(integer)$id)
                ->select('stocks_header.*')
                ->get();

            foreach ($usersToAdmins as $key => $recipient){

                $arrData = [
                    'recipient' => $get_level_two->name,
                    'header' => $getHeader,
                    'approver' => $approver->name,
                    'creator' => null,
                ];
                PVCStockApproveJob::dispatch($arrData,$recipient->email); //done
            }

            foreach ($notifyUsersLevel2 as $key => $recipient){

                $arrData = [
                    'recipient' => $get_level_two->name,
                    'header' => $getHeader,
                    'approver' => $approver->name,
                    'creator' => null,
                ];
                PVCStockApproveJob::dispatch($arrData,$recipient->email); //done
            }


            $host = gethostname();
            $nameUser = auth()->user()->name;
            $clientIP = $_SERVER['HTTP_CLIENT_IP']
                ?? $_SERVER["HTTP_CF_CONNECTING_IP"] # when behind cloudflare
                ?? $_SERVER['HTTP_X_FORWARDED']
                ?? $_SERVER['HTTP_X_FORWARDED_FOR']
                ?? $_SERVER['HTTP_FORWARDED']
                ?? $_SERVER['HTTP_FORWARDED_FOR']
                ?? $_SERVER['REMOTE_ADDR']
                ?? '0.0.0.0';

            $clientIP = '0.0.0.0';

            if (isset($_SERVER['HTTP_CLIENT_IP'])) {
                $clientIP = $_SERVER['HTTP_CLIENT_IP'];
            } elseif (isset($_SERVER['HTTP_CF_CONNECTING_IP'])) {
                $clientIP = $_SERVER['HTTP_CF_CONNECTING_IP'];
            } elseif (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
                $clientIP = $_SERVER['HTTP_X_FORWARDED_FOR'];
            } elseif (isset($_SERVER['HTTP_X_FORWARDED'])) {
                $clientIP = $_SERVER['HTTP_X_FORWARDED'];
            } elseif (isset($_SERVER['HTTP_FORWARDED_FOR'])) {
                $clientIP = $_SERVER['HTTP_FORWARDED_FOR'];
            } elseif (isset($_SERVER['HTTP_FORWARDED'])) {
                $clientIP = $_SERVER['HTTP_FORWARDED'];
            } elseif (isset($_SERVER['REMOTE_ADDR'])) {
                $clientIP = $_SERVER['REMOTE_ADDR'];
            }

            activity('For Approval - APPROVE')
                ->log('[WEB]' . ' ' . $nameUser . ' ' . 'has approved a stock request #' . ' ' .(integer)$id .' '
                    . 'using' . ' ' . $host . ' ' . 'computer'. ' '. ' with an ip address of ' . ' ' .$clientIP);

        }
        if ($name === $level2->name) {  // level two


            $storeCreator = StockHeader::where('stock_req_no', '=', $id)
                ->select('creator')
                ->first();

            $stocks = Stocks::join('stock_no as a', 'stocks.id', '=', 'a.stocks_id')
                ->where('a.stock_req_no', '=', $id)
                ->select('stocks.id as id')
                ->get();

            $arr = [];

            foreach ($stocks as $key => $code) {
                $query_Stocks = Stocks::findOrFail($code['id']);
                $query_Stocks->delete();
            }
            $DraftsHeader = StockHeader::where('stock_req_no', $id)->delete();
            $DraftsNo = StockNo::where('stock_req_no', '=', $id)->delete();
            $arrID = [];

            foreach ($request[0]['stocks'] as $key => $stocks) {

                $headerStockID = StockNo::where('stock_req_no', '=', $id)
                    ->select('stocks_id')
                    ->first();

                $data = array();
                $data['barcode'] = $stocks['barcode'];
                $data['mat_code'] = $stocks['mat_code'];
                $data['mat_des'] = $stocks['mat_des'];
                $data['sales_unit'] = $stocks['sales_unit'];
                $data['ERDAT'] = $stocks['ERDAT']; // date
                $data['KWMENG'] = $stocks['KWMENG'];
                if(!isset($stocks['stocks_left'])){ // false
                    $data['stocks_left'] = null;
                }
                elseif(isset($stocks['stocks_left'])){ //TRUE
                    $data['stocks_left'] = $stocks['stocks_left'];
                }

                if(!isset($stocks['order_qty'])){
                    $data['order_qty'] =  null;
                }
                elseif (isset($stocks['order_qty'])){
                    $data['order_qty'] =  $stocks['order_qty']; // suggest_order_qty
                }

                if(!isset($stocks['approve_qty'])){
                    $data['approve_qty'] = null;
                }
                elseif (isset($stocks['approve_qty'])){
                    $data['approve_qty'] = $stocks['approve_qty'];
                }
                if(!isset($stocks['box_no'])){
                    $data['box_no'] = null;
                }
                elseif(isset($stocks['box_no'])){
                    $data['box_no'] = $stocks['box_no'];
                }
//                $data['remarks'] = $stocks['remarks'];

                $query = Stocks::create($data);
                $getID = Stocks::select('id')->latest()->first();
                $arrID[] = [
                    'id' => $getID->id,
                ];
            }

            foreach ($arrID as $key => $stocks_id) {
                $query_stockNo = StockNo::create([
                    'stocks_id' => $stocks_id['id'],
                    'stock_req_no' => (integer)$id, // draft stock no
                ]);
            }

            $stock_header = StockHeader::create([
                'stock_req_no' => (integer)$id,  // draft stock no
                'stock_req_date' => $request[0]['header'][0]['req_date'],
                'company' => $request[0]['header'][0]['cust_company'],
                'creator' => $storeCreator->creator,
                'status' => 'APPROVED',
            ]);

            $query = ApprovalMappingRequest::where('stock_no', '=', $id)
                ->update([
                    'level_two' => 'APPROVED',
                ]);

            $query = SalesOrderHeader::create([
                'stock_req_no' => (integer)$id,
                'sales_order_date' => $request[0]['header'][0]['req_date'],
                'status' => 'PENDING',
                'company' => $request[0]['header'][0]['cust_company'],
                'request_by' => $storeCreator->creator,
                'approved_by' => $level2->name,
            ]);

            $query = SalesOrderDetails::CreateSalesDetails($request,$id);

            $usersToNotify = User::permission('email_approver_level_1')->get();
            $get_level_two = User::permission('stock_level_two_view')->first();
            $approver = User::permission('stock_level_one_view')->first();
            $approverLevelTwo = User::permission('stock_level_two_view')->first();
            $usersToAdmins = User::permission('email_admin')->get();
            $onlyAdmin = User::permission('notify_admin_second_approver_response')->first();

            $getHeader = StockHeader::where('stocks_header.stock_req_no','=',(integer)$id)
                ->select('stocks_header.*')
                ->get();

            $creator = StockHeader::where('stocks_header.stock_req_no','=',(integer)$id)
                ->select('stocks_header.*')
                ->first();

            $notifyToMerchandiser = User::where('name','=',$creator['creator'])
                ->where('name','!=',$get_level_two->name)
                ->select('name')
                ->first();

            $notifyToMerchandiserMail = User::where('name','=',$creator['creator'])
                ->where('name','!=',$get_level_two->name)
                ->get();

                if($notifyToMerchandiser !== null){
                    $creatorMerchandiser = $notifyToMerchandiser->name;
                }
                elseif($notifyToMerchandiser === null){
                    $creatorMerchandiser = '';
                }

            foreach ($notifyToMerchandiserMail as $key => $recipient){ //done 08/07/2021

                $arrData = [
                    'recipient' => $get_level_two->name,
                    'header' => $getHeader,
                    'approver' => $approverLevelTwo->name,
                    'creator' => $creatorMerchandiser,
                ];

                PVCStockApproveToMerchJob::dispatch($arrData,$recipient->email); //done
            }

            foreach ($usersToAdmins as $key => $recipient){

                $arrData = [
                    'recipient' => $onlyAdmin->name,
                    'header' => $getHeader,
                    'approver' => $approverLevelTwo->name,
                    'creator' => $creatorMerchandiser,
                ];
                PVCStockApproveJob::dispatch($arrData,$recipient->email); //done
                PVCStockApproveToMerchJob::dispatch($arrData,$recipient->email); //done
            }

            $host = gethostname();
            $nameUser = auth()->user()->name;
            $clientIP = $_SERVER['HTTP_CLIENT_IP']
                ?? $_SERVER["HTTP_CF_CONNECTING_IP"] # when behind cloudflare
                ?? $_SERVER['HTTP_X_FORWARDED']
                ?? $_SERVER['HTTP_X_FORWARDED_FOR']
                ?? $_SERVER['HTTP_FORWARDED']
                ?? $_SERVER['HTTP_FORWARDED_FOR']
                ?? $_SERVER['REMOTE_ADDR']
                ?? '0.0.0.0';

            $clientIP = '0.0.0.0';

            if (isset($_SERVER['HTTP_CLIENT_IP'])) {
                $clientIP = $_SERVER['HTTP_CLIENT_IP'];
            } elseif (isset($_SERVER['HTTP_CF_CONNECTING_IP'])) {
                $clientIP = $_SERVER['HTTP_CF_CONNECTING_IP'];
            } elseif (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
                $clientIP = $_SERVER['HTTP_X_FORWARDED_FOR'];
            } elseif (isset($_SERVER['HTTP_X_FORWARDED'])) {
                $clientIP = $_SERVER['HTTP_X_FORWARDED'];
            } elseif (isset($_SERVER['HTTP_FORWARDED_FOR'])) {
                $clientIP = $_SERVER['HTTP_FORWARDED_FOR'];
            } elseif (isset($_SERVER['HTTP_FORWARDED'])) {
                $clientIP = $_SERVER['HTTP_FORWARDED'];
            } elseif (isset($_SERVER['REMOTE_ADDR'])) {
                $clientIP = $_SERVER['REMOTE_ADDR'];
            }

            activity('FOR APPROVAL - APPROVE')
                ->log('[WEB]' . ' ' . $nameUser . ' ' . 'has approved a stock request #' . ' ' .(integer)$id .' '
                    . 'using' . ' ' . $host . ' ' . 'computer'. ' '. ' with an ip address of ' . ' ' .$clientIP);
        }
    }

    public function rejectRequest(Request $request, $id)
    {
        $name = auth()->user()->name;
        $level1 = User::permission('stock_level_one_view')->first();
        $level2 = User::permission('stock_level_two_view')->first();

        if ($name === $level1->name) {  // level one

            $storeCreator = StockHeader::where('stock_req_no', '=', $id)
                ->select('creator')
                ->first();
//            $storeItCreator = $storeCreator->creator;
            $stocks = Stocks::join('stock_no as a', 'stocks.id', '=', 'a.stocks_id')
                ->where('a.stock_req_no', '=', $id)
                ->select('stocks.id as id')
                ->get();

            $arr = [];

            foreach ($stocks as $key => $code) {
                $query_Stocks = Stocks::findOrFail($code['id']);
                $query_Stocks->delete();
            }
            StockHeader::where('stock_req_no', $id)->delete();
            StockNo::where('stock_req_no', '=', $id)->delete();
            $arrID = [];
            foreach ($request[0]['stocks'] as $key => $stocks) {

                $headerStockID = StockNo::where('stock_req_no', '=', $id)
                    ->select('stocks_id')
                    ->first();

                $data = array();
                $data['barcode'] = $stocks['barcode'];
                $data['mat_code'] = $stocks['mat_code'];
                $data['mat_des'] = $stocks['mat_des'];
                $data['sales_unit'] = $stocks['sales_unit'];
                $data['ERDAT'] = $stocks['ERDAT']; // date
                $data['KWMENG'] = $stocks['KWMENG'];

                if(!isset($stocks['stocks_left'])){ // false
                    $data['stocks_left'] = null;
                }
                elseif(isset($stocks['stocks_left'])){ //TRUE
                    $data['stocks_left'] = $stocks['stocks_left'];
                }

                if(!isset($stocks['order_qty'])){
                    $data['order_qty'] =  null;
                }
                elseif (isset($stocks['order_qty'])){
                    $data['order_qty'] =  $stocks['order_qty']; // suggest_order_qty
                }

                if(!isset($stocks['approve_qty'])){
                    $data['approve_qty'] = null;
                }
                elseif (isset($stocks['approve_qty'])){
                    $data['approve_qty'] = $stocks['approve_qty'];
                }
                if(!isset($stocks['box_no'])){
                    $data['box_no'] = null;
                }
                elseif(isset($stocks['box_no'])){
                    $data['box_no'] = $stocks['box_no'];
                }


//                $data['remarks'] = $stocks['remarks'];

                $query = Stocks::create($data);
                $getID = Stocks::select('id')->latest()->first();
                $arrID[] = [
                    'id' => $getID->id,
                ];
            }
            foreach ($arrID as $key => $stocks_id) {
                $query_stockNo = StockNo::create([
                    'stocks_id' => $stocks_id['id'],
                    'stock_req_no' => (integer)$id, // draft stock no
                ]);
            }

            $stock_header = StockHeader::create([
                'stock_req_no' => (integer)$id,  // draft stock no
                'stock_req_date' => $request[0]['header'][0]['req_date'],
                'company' => $request[0]['header'][0]['cust_company'],
                'creator' => $storeCreator->creator,
                'status' => 'REJECTED',
            ]);

            $query = ApprovalMappingRequest::where('stock_no', '=', $id)
                ->update([
                    'level_one' => 'REJECTED',
                ]);

            $usersToNotify = User::permission('email_approver_level_1')->get();
            $get_level_two = User::permission('stock_level_two_view')->first();
            $approver = User::permission('stock_level_one_view')->first();
            $usersToAdmins = User::permission('email_admin')->get();
            $notifyUserLevel2 = User::permission('email_approver_level_2')->get();

            $getHeader = StockHeader::where('stocks_header.stock_req_no','=',(integer)$id)
                ->select('stocks_header.*')
                ->get();

            $creator = StockHeader::where('stocks_header.stock_req_no','=',(integer)$id)
                ->select('stocks_header.*')
                ->first();

            $notifyToMerchandiserMail = User::where('name','=',$creator['creator'])
                ->where('name','!=',$get_level_two->name)
                ->get();

            foreach ($usersToAdmins as $key => $recipient){
                $arrData = [
                    'recipient' => $get_level_two->name,
                    'header' => $getHeader,
                    'approver' => $approver->name,
                    'creator' => null,
                ];

                PVCStockRejectJob::dispatch($arrData,$recipient->email); //done
            }

            foreach ($notifyUserLevel2 as $key => $recipient){
                $arrData = [
                    'recipient' => $get_level_two->name,
                    'header' => $getHeader,
                    'approver' => $approver->name,
                    'creator' => null,
                ];

                PVCStockRejectJob::dispatch($arrData,$recipient->email); //done
            }

            foreach ($notifyToMerchandiserMail as $key => $recipient){

                $arrData = [
                    'recipient' => $get_level_two->name,
                    'header' => $getHeader,
                    'approver' => $approver->name,
                    'creator' => null,
                ];
                PVCStockRejectMerchJob::dispatch($arrData,$recipient->email); //done
            }


            $host = gethostname();
            $nameUser = auth()->user()->name;
            $clientIP = $_SERVER['HTTP_CLIENT_IP']
                ?? $_SERVER["HTTP_CF_CONNECTING_IP"] # when behind cloudflare
                ?? $_SERVER['HTTP_X_FORWARDED']
                ?? $_SERVER['HTTP_X_FORWARDED_FOR']
                ?? $_SERVER['HTTP_FORWARDED']
                ?? $_SERVER['HTTP_FORWARDED_FOR']
                ?? $_SERVER['REMOTE_ADDR']
                ?? '0.0.0.0';

            $clientIP = '0.0.0.0';

            if (isset($_SERVER['HTTP_CLIENT_IP'])) {
                $clientIP = $_SERVER['HTTP_CLIENT_IP'];
            } elseif (isset($_SERVER['HTTP_CF_CONNECTING_IP'])) {
                $clientIP = $_SERVER['HTTP_CF_CONNECTING_IP'];
            } elseif (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
                $clientIP = $_SERVER['HTTP_X_FORWARDED_FOR'];
            } elseif (isset($_SERVER['HTTP_X_FORWARDED'])) {
                $clientIP = $_SERVER['HTTP_X_FORWARDED'];
            } elseif (isset($_SERVER['HTTP_FORWARDED_FOR'])) {
                $clientIP = $_SERVER['HTTP_FORWARDED_FOR'];
            } elseif (isset($_SERVER['HTTP_FORWARDED'])) {
                $clientIP = $_SERVER['HTTP_FORWARDED'];
            } elseif (isset($_SERVER['REMOTE_ADDR'])) {
                $clientIP = $_SERVER['REMOTE_ADDR'];
            }

            activity('For Approval - REJECT')
                ->log('[WEB]' . ' ' . $nameUser . ' ' . 'has rejected a stock request #' . ' ' .(integer)$id .' '
                    . 'using' . ' ' . $host . ' ' . 'computer'. ' '. ' with an ip address of ' . ' ' .$clientIP);
        }
        if ($name === $level2->name) {  // level two

            $storeCreator = StockHeader::where('stock_req_no', '=', $id)
                ->select('creator')
                ->first();

            $stocks = Stocks::join('stock_no as a', 'stocks.id', '=', 'a.stocks_id')
                ->where('a.stock_req_no', '=', $id)
                ->select('stocks.id as id')
                ->get();

            $arr = [];

            foreach ($stocks as $key => $code) {
                $query_Stocks = Stocks::findOrFail($code['id']);
                $query_Stocks->delete();
            }
            $DraftsHeader = StockHeader::where('stock_req_no', $id)->delete();
            $DraftsNo = StockNo::where('stock_req_no', '=', $id)->delete();
            $arrID = [];
            foreach ($request[0]['stocks'] as $key => $stocks) {

                $headerStockID = StockNo::where('stock_req_no', '=', $id)
                    ->select('stocks_id')
                    ->first();

                $data = array();
                $data['barcode'] = $stocks['barcode'];
                $data['mat_code'] = $stocks['mat_code'];
                $data['mat_des'] = $stocks['mat_des'];
                $data['sales_unit'] = $stocks['sales_unit'];
                $data['ERDAT'] = $stocks['ERDAT']; // date
                $data['KWMENG'] = $stocks['KWMENG'];
                if(!isset($stocks['stocks_left'])){ // false
                    $data['stocks_left'] = null;
                }
                elseif(isset($stocks['stocks_left'])){ //TRUE
                    $data['stocks_left'] = $stocks['stocks_left'];
                }

                if(!isset($stocks['order_qty'])){
                    $data['order_qty'] =  null;
                }
                elseif (isset($stocks['order_qty'])){
                    $data['order_qty'] =  $stocks['order_qty']; // suggest_order_qty
                }

                if(!isset($stocks['approve_qty'])){
                    $data['approve_qty'] = null;
                }
                elseif (isset($stocks['approve_qty'])){
                    $data['approve_qty'] = $stocks['approve_qty'];
                }
                if(!isset($stocks['box_no'])){
                    $data['box_no'] = null;
                }
                elseif(isset($stocks['box_no'])){
                    $data['box_no'] = $stocks['box_no'];
                }
//                $data['remarks'] = $stocks['remarks'];

                $query = Stocks::create($data);
                $getID = Stocks::select('id')->latest()->first();
                $arrID[] = [
                    'id' => $getID->id,
                ];
            }
            foreach ($arrID as $key => $stocks_id) {
                $query_stockNo = StockNo::create([
                    'stocks_id' => $stocks_id['id'],
                    'stock_req_no' => (integer)$id, // draft stock no
                ]);
            }
            $stock_header = StockHeader::create([
                'stock_req_no' => (integer)$id,  // draft stock no
                'stock_req_date' => $request[0]['header'][0]['req_date'],
                'company' => $request[0]['header'][0]['cust_company'],
                'creator' => $storeCreator->creator,
                'status' => 'REJECTED',
            ]);
            $query = ApprovalMappingRequest::where('stock_no', '=', $id)
                ->update([
                    'level_two' => 'REJECTED',
                ]);

            $usersToNotify = User::permission('email_approver_level_1')->get();
            $get_level_two = User::permission('stock_level_two_view')->first();
            $approver = User::permission('stock_level_two_view')->first();
            $usersToAdmins = User::permission('email_admin')->get();
            $onlyAdmin = User::permission('notify_admin_second_approver_response')->first();

            $getHeader = StockHeader::where('stocks_header.stock_req_no','=',(integer)$id)
                ->select('stocks_header.*')
                ->get();

            $creator = StockHeader::where('stocks_header.stock_req_no','=',(integer)$id)
                ->select('stocks_header.*')
                ->first();

            $notifyToMerchandiserMail = User::where('name','=',$creator['creator'])
                ->where('name','!=',$get_level_two->name)
                ->get();

            foreach ($usersToAdmins as $key => $recipient){
                $arrData = [
                    'recipient' => $onlyAdmin->name,
                    'header' => $getHeader,
                    'approver' => $approver->name,
                    'creator' => null,
                ];

                PVCStockRejectToSecApproverJob::dispatch($arrData,$recipient->email);
            }

            foreach ($notifyToMerchandiserMail as $key => $recipient){

                $arrData = [
                    'recipient' => $get_level_two->name,
                    'header' => $getHeader,
                    'approver' => $approver->name,
                    'creator' => null,
                ];
                PVCStockRejectMerchJob::dispatch($arrData,$recipient->email);
            }
            $host = gethostname();
            $nameUser = auth()->user()->name;
            $clientIP = $_SERVER['HTTP_CLIENT_IP']
                ?? $_SERVER["HTTP_CF_CONNECTING_IP"] # when behind cloudflare
                ?? $_SERVER['HTTP_X_FORWARDED']
                ?? $_SERVER['HTTP_X_FORWARDED_FOR']
                ?? $_SERVER['HTTP_FORWARDED']
                ?? $_SERVER['HTTP_FORWARDED_FOR']
                ?? $_SERVER['REMOTE_ADDR']
                ?? '0.0.0.0';

            $clientIP = '0.0.0.0';

            if (isset($_SERVER['HTTP_CLIENT_IP'])) {
                $clientIP = $_SERVER['HTTP_CLIENT_IP'];
            } elseif (isset($_SERVER['HTTP_CF_CONNECTING_IP'])) {
                $clientIP = $_SERVER['HTTP_CF_CONNECTING_IP'];
            } elseif (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
                $clientIP = $_SERVER['HTTP_X_FORWARDED_FOR'];
            } elseif (isset($_SERVER['HTTP_X_FORWARDED'])) {
                $clientIP = $_SERVER['HTTP_X_FORWARDED'];
            } elseif (isset($_SERVER['HTTP_FORWARDED_FOR'])) {
                $clientIP = $_SERVER['HTTP_FORWARDED_FOR'];
            } elseif (isset($_SERVER['HTTP_FORWARDED'])) {
                $clientIP = $_SERVER['HTTP_FORWARDED'];
            } elseif (isset($_SERVER['REMOTE_ADDR'])) {
                $clientIP = $_SERVER['REMOTE_ADDR'];
            }

            activity('For Approval - REJECT')
                ->log('[WEB]' . ' ' . $nameUser . ' ' . 'has rejected a stock request #' . ' ' .(integer)$id .' '
                    . 'using' . ' ' . $host . ' ' . 'computer'. ' '. ' with an ip address of ' . ' ' .$clientIP);
        }
    }

}
