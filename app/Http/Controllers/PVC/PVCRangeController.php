<?php

namespace App\Http\Controllers\PVC;

use App\Http\Controllers\Controller;
use App\INVENTORY;
use App\MaterialMaster;
use App\VBAP;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;

class PVCRangeController extends Controller
{
    public function rangeBarcode(Request $request){

        $databaseName = Config::get('database.connections');
        $valBar = (integer)$request[0]['barcode'] + (integer)$request[0]['range'];
        $range = MaterialMaster::join($databaseName['sqlsrv2']['database'] . '.dbo.MVKE as a', 'MATERIAL_MASTER.MATNR', '=', 'a.MATNR')
        ->where(function ($query) use ($request,$valBar) {
            $query->whereBetween('MATERIAL_MASTER.EAN11',[$request[0]['barcode'],(string)$valBar])
//                ->where('MATERIAL_MASTER.EAN11', '>=', $request[0]['barcode'])
//                    ->where('MATERIAL_MASTER.EAN11','<=',(string)$valBar)
                        ->where('a.VKORG','=','1902');
        })
        ->select('MATERIAL_MASTER.EAN11','MATERIAL_MASTER.MATNR','MATERIAL_MASTER.MAKTX')
        ->get();
        $arrRange = [];
        foreach ($range as $perItem =>$value){

            $getMVKE = MaterialMaster::join($databaseName['sqlsrv2']['database'] . '.dbo.MVKE as a', 'MATERIAL_MASTER.MATNR', '=', 'a.MATNR')
                ->where(function ($query) use ($value) {
                    $query->where('MATERIAL_MASTER.EAN11', '=', $value['EAN11'])
                        ->where('a.VKORG','=','1902');
                })
                ->select('MATERIAL_MASTER.MEINS','a.VRKME')
                ->first();
            $string =  preg_replace('/\s+/', '', $getMVKE['VRKME']);

            if($getMVKE->VRKME === null || $string !== ''){
                $sales_unit = $getMVKE->VRKME;
            }
            else{
                $sales_unit = $getMVKE->MEINS;
            }

            $getDateAndQty = VBAP::join($databaseName['sqlsrv2']['database'] . '.dbo.VBAK as a', 'VBAP.VBELN', '=', 'a.VBELN')
                ->where(function ($query) use ($value) {
                    $query->where('a.AUART', '=', 'ZKB')
                        ->where('VBAP.MATNR', '=', $value['MATNR'])
                        ->where('VBAP.KWMENG', '!=', 0);
                })
                ->select('VBAP.ERDAT', 'VBAP.KWMENG')
                ->orderBy('VBAP.ERDAT', 'desc')
                ->first();

            if($getDateAndQty !== null){
                $valERDAT = $getDateAndQty->ERDAT;
                $valKWMENG = $getDateAndQty->KWMENG;
            }elseif($getDateAndQty === null){
                $valERDAT = null;
                $valKWMENG = null;
            }
            $arrRange[] = [
                'barcode' => $value['EAN11'],
                'mat_code' => $value['MATNR'],
                'mat_des' => $value['MAKTX'],
                'uom' => $sales_unit,
                'last_fill_date' => $valERDAT, //last fill up date
                'last_fill_qty' => $valKWMENG, // last fill up qty
            ];
        }

        return response()->json($arrRange);
    }
    public function rangeBarcodeApproval(Request $request){ //for approvals

        $databaseName = Config::get('database.connections');
        $valBar = (integer)$request[0]['barcode'] + (integer)$request[0]['range'];
        $range = MaterialMaster::join($databaseName['sqlsrv2']['database'] . '.dbo.MVKE as a', 'MATERIAL_MASTER.MATNR', '=', 'a.MATNR')
            ->where(function ($query) use ($request,$valBar) {
                $query->whereBetween('MATERIAL_MASTER.EAN11',[$request[0]['barcode'],(string)$valBar])
                    ->where('a.VKORG','=','1902');
            })
            ->select('MATERIAL_MASTER.EAN11','MATERIAL_MASTER.MATNR','MATERIAL_MASTER.MAKTX')
            ->get();
        $arrRange = [];
        foreach ($range as $perItem =>$value){

            $getMVKE = MaterialMaster::join($databaseName['sqlsrv2']['database'] . '.dbo.MVKE as a', 'MATERIAL_MASTER.MATNR', '=', 'a.MATNR')
                ->where(function ($query) use ($value) {
                    $query->where('MATERIAL_MASTER.EAN11', '=', $value['EAN11'])
                        ->where('a.VKORG','=','1902');
                })
                ->select('MATERIAL_MASTER.MEINS','a.VRKME')
                ->first();
            $string =  preg_replace('/\s+/', '', $getMVKE['VRKME']);

            if($getMVKE->VRKME === null || $string !== ''){
                $sales_unit = $getMVKE->VRKME;
            }
            else{
                $sales_unit = $getMVKE->MEINS;
            }

            $getDateAndQty = VBAP::join($databaseName['sqlsrv2']['database'] . '.dbo.VBAK as a', 'VBAP.VBELN', '=', 'a.VBELN')
                ->where(function ($query) use ($value) {
                    $query->where('a.AUART', '=', 'ZKB')
                        ->where('VBAP.MATNR', '=', $value['MATNR'])
                        ->where('VBAP.KWMENG', '!=', 0);
                })
                ->select('VBAP.ERDAT', 'VBAP.KWMENG')
                ->orderBy('VBAP.ERDAT', 'desc')
                ->first();
            if($getDateAndQty !== null){
                $valERDAT = $getDateAndQty->ERDAT;
                $valKWMENG = $getDateAndQty->KWMENG;
            }elseif($getDateAndQty === null){
                $valERDAT = null;
                $valKWMENG = null;
            }
            $SAPUNRESTRICTEDQTY = INVENTORY::join($databaseName['sqlsrv2']['database'] . '.dbo.MATERIAL_MASTER as a', 'INVENTORY.MATNR', '=', 'a.MATNR')
                ->where(function ($query) {
                    $query->where('INVENTORY.WERKS', '=', '9021')
                        ->where('INVENTORY.LGORT', '!=', '9099');
                })
                ->where('a.EAN11', '=', $value['EAN11'])
                ->select('INVENTORY.LABST')
                ->first();

            $arrRange[] = [
                'barcode' => $value['EAN11'],
                'mat_code' => $value['MATNR'],
                'mat_des' => $value['MAKTX'],
                'sales_unit' => $sales_unit,
                'ERDAT' => $valERDAT, //last fill up date
                'KWMENG' => $valKWMENG, // last fill up qty
                'LABST' => $SAPUNRESTRICTEDQTY->LABST,
            ];
        }

        return response()->json($arrRange);
    }
    public function rangeBarcodeDrafts(Request $request){ //drafts

        $databaseName = Config::get('database.connections');
        $valBar = (integer)$request[0]['barcode'] + (integer)$request[0]['range'];
        $range = MaterialMaster::join($databaseName['sqlsrv2']['database'] . '.dbo.MVKE as a', 'MATERIAL_MASTER.MATNR', '=', 'a.MATNR')
            ->where(function ($query) use ($request,$valBar) {
                $query->whereBetween('MATERIAL_MASTER.EAN11',[$request[0]['barcode'],(string)$valBar])
                    ->where('a.VKORG','=','1902');
            })
            ->select('MATERIAL_MASTER.EAN11','MATERIAL_MASTER.MATNR','MATERIAL_MASTER.MAKTX')
            ->get();
        $arrRange = [];
        foreach ($range as $perItem =>$value){

            $getMVKE = MaterialMaster::join($databaseName['sqlsrv2']['database'] . '.dbo.MVKE as a', 'MATERIAL_MASTER.MATNR', '=', 'a.MATNR')
                ->where(function ($query) use ($value) {
                    $query->where('MATERIAL_MASTER.EAN11', '=', $value['EAN11'])
                        ->where('a.VKORG','=','1902');
                })
                ->select('MATERIAL_MASTER.MEINS','a.VRKME')
                ->first();
            $string =  preg_replace('/\s+/', '', $getMVKE['VRKME']);

            if($getMVKE->VRKME === null || $string !== ''){
                $sales_unit = $getMVKE->VRKME;
            }
            else{
                $sales_unit = $getMVKE->MEINS;
            }

            $getDateAndQty = VBAP::join($databaseName['sqlsrv2']['database'] . '.dbo.VBAK as a', 'VBAP.VBELN', '=', 'a.VBELN')
                ->where(function ($query) use ($value) {
                    $query->where('a.AUART', '=', 'ZKB')
                        ->where('VBAP.MATNR', '=', $value['MATNR'])
                        ->where('VBAP.KWMENG', '!=', 0);
                })
                ->select('VBAP.ERDAT', 'VBAP.KWMENG')
                ->orderBy('VBAP.ERDAT', 'desc')
                ->first();

            $SAPUNRESTRICTEDQTY = INVENTORY::join($databaseName['sqlsrv2']['database'] . '.dbo.MATERIAL_MASTER as a', 'INVENTORY.MATNR', '=', 'a.MATNR')
                ->where(function ($query) {
                    $query->where('INVENTORY.WERKS', '=', '9021')
                        ->where('INVENTORY.LGORT', '!=', '9099');
                })
                ->where('a.EAN11', '=', $value['EAN11'])
                ->select('INVENTORY.LABST')
                ->first();
            if($getDateAndQty !== null){
                $valERDAT = $getDateAndQty->ERDAT;
                $valKWMENG = $getDateAndQty->KWMENG;
            }elseif($getDateAndQty === null){
                $valERDAT = null;
                $valKWMENG = null;
            }
            $arrRange[] = [
                'barcode' => $value['EAN11'],
                'mat_code' => $value['MATNR'],
                'mat_des' => $value['MAKTX'],
                'sales_unit' => $sales_unit,
                'ERDAT' => $valERDAT, //last fill up date
                'KWMENG' => $valKWMENG, // last fill up qty
                'LABST' => $SAPUNRESTRICTEDQTY->LABST,
            ];
        }

        return response()->json($arrRange);
    }
}
