<?php

namespace App\Http\Controllers\PVC;

use App\ActivityLog;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ActivityLogsController extends Controller
{
    public function actLogs(Request $request){

        $searchValue = $request->search;
        $orderBy = $request->sortby;
        $orderByDir = $request->sortdir;
        $perPage = $request->currentpage;

        $query = ActivityLog::when($searchValue !== 'null', function ($query) use ($searchValue) {
            $query->where('description', 'LIKE', '%' . $searchValue . '%')
                    ->where('log_name', 'LIKE', '%' . $searchValue . '%');
            })
            ->orderBy($orderBy, $orderByDir)
            ->paginate($perPage);

        return response()->json($query);
    }
}
