<?php

namespace App\Http\Controllers\PVC;

use App\ApprovalMappingRequest;
use App\ForPrinting;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ForPrintingController extends Controller
{
    public function showCurrentPrintingNo()
    {
        $query = ForPrinting::orderBy('created_at', 'desc')->select('release_form as RELEASE', 'delivery_receipt as DELIVERY')->first();

        if ($query === null) {
            $release = null;
            $delivery = null;
        } else {
            if (ForPrinting::where(function ($queryWhere) use ($query) {
                $queryWhere->where('release_form', '=', $query->RELEASE)
                    ->where('delivery_receipt', '=', $query->DELIVERY);
                })->exists()) {
                $getStockNo = ForPrinting::where(function ($queryWhere) use ($query) {
                    $queryWhere->where('release_form', '=', $query->RELEASE)
                        ->where('delivery_receipt', '=', $query->DELIVERY);
                })
                    ->select('stock_no')
                    ->first();

                $approval_mapping = ApprovalMappingRequest::where('stock_no',$getStockNo->stock_no)
                    ->select('level_one','level_two')
                    ->first();

                $checkApproved = 'APPROVED';
                $checkReject = 'REJECTED';
                if($approval_mapping->level_one === $checkApproved || $approval_mapping->level_two === $checkApproved
                    ||$approval_mapping->level_two === null){
                    if($approval_mapping->level_one === $checkApproved){
                        $total_rel = $query->RELEASE + 1;
                        $test_rel =  str_pad((string)$total_rel,6,"0",STR_PAD_LEFT);

                        $total_del = $query->DELIVERY + 1;
                        $test_del =  str_pad((string)$total_del,6,"0",STR_PAD_LEFT);

                        $release = $test_rel; // 00001
                        $delivery = $test_del;
                    }
                    elseif($approval_mapping->level_two === $checkApproved || $approval_mapping->level_two === null){
                        $total_rel = $query->RELEASE + 1;
                        $test_rel =  str_pad((string)$total_rel,6,"0",STR_PAD_LEFT);

                        $total_del = $query->DELIVERY + 1;
                        $test_del =  str_pad((string)$total_del,6,"0",STR_PAD_LEFT);

                        $release = $test_rel; // 00001
                        $delivery = $test_del;
                    }

                }
                else{
                    if($approval_mapping->level_one === $checkReject || $approval_mapping->level_two === $checkReject){
                        $release = $query->RELEASE; // 00001
                        $delivery = $query->DELIVERY;
                    }
                }
            } else {

            }
//            $release = $query->RELEASE; // 00001
//            $delivery = $query->DELIVERY;
        }
        return response()->json([
            'RELEASE' => $release,
            'DELIVERY' => $delivery,
        ]);
    }

    public function getPrintNo(Request $request)
    {
        $query = ForPrinting::where('stock_no',$request->printID)->select('release_form as RELEASE', 'delivery_receipt as DELIVERY')->first();

        return response()->json([
            'RELEASE' => $query->RELEASE,
            'DELIVERY' => $query->DELIVERY,
        ]);
    }
}
