<?php

namespace App\Http\Controllers\PVC;

use App\FtpPath;
use App\Http\Controllers\Controller;
use App\KNVV;
use App\SalesOrderDetails;
use App\SalesOrderHeader;
use App\SalesOrderSapMessages;
use App\SalesOrderSapMessagesDetails;
use App\Stocks;
use Carbon\Carbon;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Storage;
use SimpleXMLElement;
use Spatie\ArrayToXml\ArrayToXml;

class PostToSapController extends Controller
{
    public function __construct()
    {
        $environment = '';
        if (App::environment('local', 'dev')) {
            $environment = 'SO_DIY';
        }
        if (App::environment('staging', 'production')) {
            $environment = 'SO_DIY';
        }
        $this->environment = $environment;
    }
    public function postOnlyOnce(Request $request,$id){
        if (SalesOrderSapMessages::where('sales_order_number', '=', (integer)$id)->exists()) {
            return response()->json([
                'error' => 'Found',
            ]);
        } else {
            return response()->json([
               'error' => 'Not Found',
            ]);
        }
    }
    public function postToSap(Request $request, $id)
    {
        if (SalesOrderSapMessages::where('sales_order_number', '=', (integer)$id)->exists()) {
            return response()->json([
                'error' => 'Found',
            ]);
        } else {
            SalesOrderSapMessages::create([
                'sales_order_number' => $id,
            ]);

            $ftp_path = new FtpPath;
            $filePath = $ftp_path->salesOrderPVC();
            $databaseName = Config::get('database.connections');
            $hdr = [];
            $details = [];


//        dd(count($request[0]['sales_order']['data']) * 10 / 10);
            foreach ($request[0]['sales_order']['data'] as $key => $sales) {

                $query = KNVV::join($databaseName['sqlsrv2']['database'] . '.dbo.KNA1 as a', 'KNVV.KUNNR', '=', 'a.KUNNR')
                    ->where(function ($query) use ($request) {
                        $query->where('a.NAME1', '=', $request[0]['header'][1]['company'])
                            ->where('KNVV.VKORG', '=', '1902');
                    })
                    ->select('a.KUNNR')
                    ->first();

                $unit = Stocks::join('stock_no as a', 'stocks.id', '=', 'a.stocks_id')
                    ->where('a.stock_req_no', '=', $sales['sales_order_no'])
                    ->where(function ($query) use ($sales) {
                        $query->where('stocks.mat_code', '=', $sales['MATNR'])
                            ->where('stocks.mat_des', '=', $sales['MAKTX']);
                    })
                    ->select('stocks.sales_unit')
                    ->first();
                $date = new DateTime($request[0]['po_date']);
                $format = $date->format('Y-m-d');
                $split = explode("-", $format);
                $combine = implode("", $split);

                $hdr = [
                    'CUSTOMER' => $query->KUNNR,
                    'PONUMBER' => $request[0]['po_number'],
                    'PODATE' => $combine,
                    'STOCKREQNO' => $id,
                    'CREATED_BY' => $request[0]['header'][1]['request_by'],
                ];

                $details[$key] = [
                    'INDEX' => ($key + 1) * 10,
                    'MATNR' => $sales['MATNR'],
                    'QTY' => $sales['approved_qty'],
                    'UNIT' => $unit->sales_unit,
                ];

                $sales_details = SalesOrderDetails::where('sales_order_no', '=', $id)
                    ->where(function ($query) use ($sales) {
                        $query->where('MAKTX', '=', $sales['MAKTX'])
                            ->where('MATNR', '=', $sales['MATNR'])
                            ->where('id', '=', $sales['id']);
                    })
                    ->update([
                        'cost' => $sales['VERPR'],
                        'total_cost' => $sales['total_cost'],
                    ]);

            }
            $sales_header = SalesOrderHeader::where('stock_req_no', '=', $id)
                ->update([
                    'po_number' => $request[0]['po_number'],
                    'po_date' => $request[0]['po_date'],
                    'net_value' => $request[0]['net_value'],
                ]);

            $xml = new SimpleXMLElement('<root/>');
            $xmlData = $xml->addChild('data');
            $header = $xmlData->addChild('HEADER');
            foreach ($hdr as $key => $value) {
                $header->addChild($key, $value);
            }
            $detailsXml = $xmlData->addChild('ITEMS');
            foreach ($details as $value) {
//            $parentItem = $detailsXml->addChild('ITEM');
                foreach ($value as $lineitem => $item) {
                    $detailsXml->addChild($lineitem, $item);
                }
            }
//        $detailsXml = $xmlData->addChild('ITEMS');
//        foreach ($details as $value){
//            $parentItem = $detailsXml->addChild('ITEM');
//            foreach ($value as $lineitem => $item){
//                $parentItem->addChild($lineitem, $item);
//            }
//        }

            $xml_output = $xml->asXML();
            $unix = round(microtime(true) * 1000);
            $get10 = substr($unix, 3);
//        $file_name = Carbon::now()->format('Ymds') . ".xml";
            $file_name = $get10 . ".xml";
            $path = $filePath . $file_name;
            Storage::disk('ftp')->put($path, $xml_output);
        }
    }

    public function showPostSO(Request $request)
    {
        $searchValue = $request->search;
        $orderBy = $request->sortby;
        $orderByDir = $request->sortdir;
        $perPage = $request->currentpage;

        $query = SalesOrderSapMessages::with('details')
            ->where('sap_message', '!=', null)
            ->where('sap_message', '!=', '')
            ->when($searchValue !== 'null', function ($query) use ($searchValue) {
                $query->where('sales_order_number', 'LIKE', '%' . $searchValue . '%');
            })
            ->orderBy($orderBy, $orderByDir)
            ->paginate($perPage);

        return response()->json($query);
    }

    public function manualExport(Request $request)
    {

        $ftp_path = new FtpPath;
        $filePath = $ftp_path->salesOrderPVC();

        $db = Config::get('database.connections')['sqlsrv2']['database'] . '.dbo.';
        $databaseName = Config::get('database.connections');
        $hdr = [];
        $details = [];
        $sales_details = SalesOrderDetails::where('sales_order_no', '=', (integer)$request->saleNo)->get();
        foreach ($sales_details as $key => $sales) {

            $sales_header = SalesOrderHeader::where('stock_req_no', '=', $request->saleNo)->first();

            $query = KNVV::join($databaseName['sqlsrv2']['database'] . '.dbo.KNA1 as a', 'KNVV.KUNNR', '=', 'a.KUNNR')
                ->where(function ($query) use ($sales_header) {
                    $query->where('a.NAME1', '=', $sales_header->company)
                        ->where('KNVV.VKORG', '=', '1902');
                })
                ->select('a.KUNNR')
                ->first();

            $unit = Stocks::join('stock_no as a', 'stocks.id', '=', 'a.stocks_id')
                ->where('a.stock_req_no', '=', $sales['sales_order_no'])
                ->where(function ($query) use ($sales) {
                    $query->where('stocks.mat_code', '=', $sales['MATNR'])
                        ->where('stocks.mat_des', '=', $sales['MAKTX']);
                })
                ->select('stocks.sales_unit')
                ->first();

            $date = new DateTime($sales_header['po_date']);
            $format = $date->format('Y-m-d');
            $split = explode("-", $format);
            $combine = implode("", $split);

            $hdr = [
                'CUSTOMER' => $query->KUNNR,
                'PONUMBER' => $sales_header['po_number'],
                'PODATE' => $combine,
                'STOCKREQNO' => $sales_header['stock_req_no'],
                'CREATED_BY' => $sales_header['request_by'],
            ];

            $details[] = [
                'INDEX' => ($key + 1) * 10,
                'MATNR' => $sales['MATNR'],
                'QTY' => $sales['approved_qty'], //VERPR
                'UNIT' => $unit->sales_unit,
            ];
        }

        //clear sap message
        $item = SalesOrderSapMessages::where('sales_order_number', '=', (integer)$request->saleNo)->first();
        $item->sap_message = null;
        $item->save();

        //clear sap messages details
        $errorMsg = SalesOrderSapMessagesDetails::where('transaction','=',(integer)$request->saleNo)->select('id as id')->get();
        foreach ($errorMsg as $key => $value){
            $errorFromSap = SalesOrderSapMessagesDetails::findOrFail($value['id']);
            $errorFromSap->delete();
        }

        $xml = new SimpleXMLElement('<root/>');
        $xmlData = $xml->addChild('data');
        $header = $xmlData->addChild('HEADER');
        foreach ($hdr as $key => $value) {
            $header->addChild($key, $value);
        }
        $detailsXml = $xmlData->addChild('ITEMS');
        foreach ($details as $value) {
//            $parentItem = $detailsXml->addChild('ITEM');
            foreach ($value as $lineitem => $item) {
                $detailsXml->addChild($lineitem, $item);
            }
        }
//        foreach ($details as $value){
//            $parentItem = $detailsXml->addChild('ITEM');
//            foreach ($value as $lineitem => $item){
//                $parentItem->addChild($lineitem, $item);
//            }
//        }
        $xml_output = $xml->asXML();
        $unix = round(microtime(true) * 1000);
        $get10 = substr($unix, 3);
//        $file_name = Carbon::now()->format('Ymds') . ".xml";
        $file_name = $get10 . ".xml";
        $path = $filePath . $file_name;
        Storage::disk('ftp')->put($path, $xml_output);
    }
}
