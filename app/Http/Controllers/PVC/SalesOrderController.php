<?php

namespace App\Http\Controllers\PVC;

use App\Http\Controllers\Controller;
use App\Http\Resources\SalesOrderCollection;
use App\SalesOrderHeader;
use App\StockHeader;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use DateTime;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Gate;

class SalesOrderController extends Controller
{
    public function salesOrder(Request $request){
//        if (!Gate::allows('stock_level_two_view') || !Gate::allows('permission_view')) {
//            return abort(403);
//        }
        if ($request->showAll) {
            $query = SalesOrderHeader::orderBy('stock_req_no', 'desc')->get();
        }
        else{
            $name = auth()->user()->name;
            $searchValue = $request->search;
            $orderBy = $request->sortby;
            $orderByDir = $request->sortdir;
            $perPage = $request->currentpage;
            $searchStockNumber = $request->stockReqNumber;
            $creator = $request->creator;
            $status = $request->status;
            $company = $request->company;
            $userCompany = $request->userCompany;
            $approver = $request->approver;
            if ($creator === "null") {
                $creator = null;
            }
            if ($status === "null") {
                $status = null;
            }
            if ($company === "null") {
                $company = null;
            }

            $from = null;
            $to = null;

            if ($request->dateFromAndTo !== null && $request->dateFromAndTo !== "") {
                $split = explode(",", $request->dateFromAndTo);
                $from_x = $split[0];
                $to_x = $split[1];
                if ($to_x === '' || $from_x === '') {
                    $from = null;
                    $to = null;
                } else {
                    $from = DateTime::createFromFormat('d/m/Y', $from_x)->format('Y/m/d');
                    $to = DateTime::createFromFormat('d/m/Y', $to_x)->format('Y/m/d');
                }
            } else if ($request->dateFromAndTo === null && $request->dateFromAndTo === "") {
                $from = null;
                $to = null;
            }
            $level1 = User::permission('stock_level_one_view')->first();
            $level2 = User::permission('stock_level_two_view')->first();
            if ($from !== null && $to !== null) {
                if($name === $level1->name || $name === $level2->name){
                    if($name === $level1->name){
                        $query = SalesOrderHeader::join('approval_mapping as a','sales_order_header.stock_req_no','=','a.stock_no')
                            ->where(function ($query)  {
                                $query->where('a.level_one','=','APPROVED')
                                    ->where('a.level_two','=','APPROVED');
                            })
                            ->whereBetween('sales_order_header.sales_order_date', [Carbon::parse($from)->startOfDay(), Carbon::parse($to)->endOfDay()])
                            ->where(function ($query) use ($searchStockNumber) {
                                $query->orwhere('sales_order_header.stock_req_no', 'LIKE', "%$searchStockNumber%");
                            })
                            ->where(function ($query) use ($company) {
                                $query->orwhere('sales_order_header.company', 'LIKE', "%$company%");
                            })
                            ->where(function ($query) use ($status) {
                                $query->orwhere('sales_order_header.status', 'LIKE', "%$status%");
                            })
                            ->where(function ($query) use ($creator) {
                                $query->orwhere('sales_order_header.request_by', 'LIKE', "%$creator%");
                            })
                            ->where(function ($query) use ($approver) {
                                $query->orwhere('sales_order_header.approved_by', 'LIKE', "%$approver%");
                            })
                            ->select('sales_order_header.*')
                            ->orderBy($orderBy, $orderByDir)->paginate($perPage);
                    }
                    if($name === $level2->name){
                        $query = SalesOrderHeader::join('approval_mapping as a','sales_order_header.stock_req_no','=','a.stock_no')
                            ->where(function ($query)  {
                                $query->where('a.level_one','=','APPROVED')
                                    ->orwhere('a.level_two','=','APPROVED');
                            })
                            ->whereBetween('sales_order_header.sales_order_date', [Carbon::parse($from)->startOfDay(), Carbon::parse($to)->endOfDay()])
                            ->where(function ($query) use ($searchStockNumber) {
                                $query->orwhere('sales_order_header.stock_req_no', 'LIKE', "%$searchStockNumber%");
                            })
                            ->where(function ($query) use ($company) {
                                $query->orwhere('sales_order_header.company', 'LIKE', "%$company%");
                            })
                            ->where(function ($query) use ($status) {
                                $query->orwhere('sales_order_header.status', 'LIKE', "%$status%");
                            })
                            ->where(function ($query) use ($creator) {
                                $query->orwhere('sales_order_header.request_by', 'LIKE', "%$creator%");
                            })
                            ->where(function ($query) use ($approver) {
                                $query->orwhere('sales_order_header.approved_by', 'LIKE', "%$approver%");
                            })
                            ->select('sales_order_header.*')
                            ->orderBy($orderBy, $orderByDir)->paginate($perPage);
                    }
                }else{
                    $query = SalesOrderHeader::whereBetween('sales_order_date', [Carbon::parse($from)->startOfDay(), Carbon::parse($to)->endOfDay()])
                        ->where(function ($query) use ($searchStockNumber) {
                            $query->orwhere('stock_req_no', 'LIKE', "%$searchStockNumber%");
                        })
                        ->where(function ($query) use ($company) {
                            $query->orwhere('company', 'LIKE', "%$company%");
                        })
                        ->where(function ($query) use ($status) {
                            $query->orwhere('status', 'LIKE', "%$status%");
                        })
                        ->where(function ($query) use ($creator) {
                            $query->orwhere('request_by', 'LIKE', "%$creator%");
                        })
                        ->where(function ($query) use ($approver) {
                            $query->orwhere('approved_by', 'LIKE', "%$approver%");
                        })
                        ->select('sales_order_header.*')
                        ->orderBy($orderBy, $orderByDir)->paginate($perPage);
                }
                return new SalesOrderCollection($query);
            }
            else if ($from === null && $to === null) {
                if($name === $level1->name || $name === $level2->name){
                    if($name === $level1->name){
                        $query = SalesOrderHeader::join('approval_mapping as a','sales_order_header.stock_req_no','=','a.stock_no')
                            ->where(function ($query)  {
                                $query->where('a.level_one','=','APPROVED')
                                    ->where('a.level_two','=','APPROVED');
                            })
                            ->where(function ($query) use ($searchStockNumber) {
                                $query->orwhere('sales_order_header.stock_req_no', 'LIKE', "%$searchStockNumber%");
                            })
                            ->where(function ($query) use ($company) {
                                $query->orwhere('sales_order_header.company', 'LIKE', "%$company%");
                            })
                            ->where(function ($query) use ($status) {
                                $query->orwhere('sales_order_header.status', 'LIKE', "%$status%");
                            })
                            ->where(function ($query) use ($creator) {
                                $query->orwhere('sales_order_header.request_by', 'LIKE', "%$creator%");
                            })
                            ->where(function ($query) use ($approver) {
                                $query->orwhere('sales_order_header.approved_by', 'LIKE', "%$approver%");
                            })
                            ->select('sales_order_header.*')
                            ->orderBy($orderBy, $orderByDir)->paginate($perPage);
                    }
                    if($name === $level2->name){

                        $query = SalesOrderHeader::join('approval_mapping as a','sales_order_header.stock_req_no','=','a.stock_no')
                            ->where(function ($query)  {
                                $query->where('a.level_one','=','APPROVED')
                                    ->where('a.level_two','=','APPROVED');
                            })
                            ->where(function ($query) use ($searchStockNumber) {
                                $query->orwhere('sales_order_header.stock_req_no', 'LIKE', "%$searchStockNumber%");
                            })
                            ->where(function ($query) use ($company) {
                                $query->orwhere('sales_order_header.company', 'LIKE', "%$company%");
                            })
                            ->where(function ($query) use ($status) {
                                $query->orwhere('sales_order_header.status', 'LIKE', "%$status%");
                            })
                            ->where(function ($query) use ($creator) {
                                $query->orwhere('sales_order_header.request_by', 'LIKE', "%$creator%");
                            })
                            ->where(function ($query) use ($approver) {
                                $query->orwhere('sales_order_header.approved_by', '=', $approver);
                            })
                            ->select('sales_order_header.*')
                            ->orderBy($orderBy, $orderByDir)->paginate($perPage);
                    }
                }
                else {
                    $query = SalesOrderHeader::join('approval_mapping as a','sales_order_header.stock_req_no','=','a.stock_no')
                        ->where(function ($query)  {
                            $query->where('a.level_one','=','APPROVED')
                                ->where('a.level_two','=','APPROVED');
                        })
                        ->where(function ($query) use ($searchStockNumber) {
                            $query->orwhere('sales_order_header.stock_req_no', 'LIKE', "%$searchStockNumber%");
                        })
                        ->where(function ($query) use ($company) {
                            $query->orwhere('sales_order_header.company', 'LIKE', "%$company%");
                        })
                        ->where(function ($query) use ($status) {
                            $query->orwhere('sales_order_header.status', 'LIKE', "%$status%");
                        })
                        ->where(function ($query) use ($creator) {
                            $query->orwhere('sales_order_header.request_by', 'LIKE', "%$creator%");
                        })
                        ->where(function ($query) use ($approver) {
                            $query->orwhere('sales_order_header.approved_by', 'LIKE', "%$approver%");
                        })
                        ->select('sales_order_header.*')
                        ->orderBy($orderBy, $orderByDir)->paginate($perPage);
                }
                return new SalesOrderCollection($query);
            }

        }

    }
    public function headerSalesData(Request $request,$id){
        $query = SalesOrderHeader::where('stock_req_no','=',$id)
            ->select('sales_order_header.*')
            ->get();
        return response()->json($query);
    }
    public function viewSalesData(Request $request,$id){
        if ($request->showAll) {
            $query = SalesOrderHeader::orderBy('stock_req_no', 'desc')->get();
        } else {
            $searchValue = $request->search;
            $orderBy = $request->sortby;
            $orderByDir = $request->sortdir;
            $perPage = $request->currentpage;
            $databaseName = Config::get('database.connections');

            $query = SalesOrderHeader::join('sales_order_details as a', 'sales_order_header.stock_req_no', '=', 'a.sales_order_no')
                ->join($databaseName['sqlsrv2']['database'] . '.dbo.MBEW as b','a.MATNR','=','b.MATNR')
                ->where('b.BWKEY','=','9021')
                ->where('sales_order_header.stock_req_no', '=', $id)
                ->select('a.*','sales_order_header.stock_req_no','b.VERPR')
                ->get();
            foreach ($query as $key => $code){
                $query[$key]['total_cost'] = (float)number_format((integer)$code['approved_qty'] * (float)$code['VERPR'],2,'.','');
                $query[$key]['VERPR'] = (float)$code['VERPR'];
            }
        }
        return new SalesOrderCollection($query);
    }
}
