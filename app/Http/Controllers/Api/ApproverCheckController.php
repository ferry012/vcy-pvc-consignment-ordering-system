<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;

class ApproverCheckController extends Controller
{
    public function checkUser()
    {
        $name = auth()->user()->name;
        $user = User::permission('approve_qty')->get();
        $level1 = User::permission('stock_level_one_view')->get();
        $level2 = User::permission('stock_level_two_view')->get();

        foreach ($user as $user_id => $user_name) {
            foreach ($level1 as $level1_id => $level1_name) {
                foreach ($level2 as $level2_id => $level2_name) {

                    if ($user_name['name'] === $level1_name['name'] || $user_name['name'] === $level2_name['name']) {

                        if ($user_name['name'] === $level1_name['name'] && $level1_name['name'] === $name) {
                            $check = 1;
                        } else if ($user_name['name'] === $level2_name['name'] && $level2_name['name'] === $name) {
                            $check = 1;
                        } else if ($user_name['name'] === $level1_name['name'] && $level1_name['name'] !== $name && $level2_name['name'] !== $name) {
                            $check = 0;
                        }

                    }
                }
            }
        }

        return response()->json([
            'check' => $check,
        ]);
    }

    public function checkUserView()
    {

        $name = auth()->user()->name;
        $user = User::permission('approve_qty')->get();
        $level1 = User::permission('stock_level_one_view')->get();
        $level2 = User::permission('stock_level_two_view')->get();

        foreach ($user as $user_id => $user_name) {
            foreach ($level1 as $level1_id => $level1_name) {
                foreach ($level2 as $level2_id => $level2_name) {

                    if ($user_name['name'] === $level1_name['name'] || $user_name['name'] === $level2_name['name']) {

                        if ($user_name['name'] === $level1_name['name'] && $level1_name['name'] === $name) {
                            $check = 1;
                        } else if ($user_name['name'] === $level2_name['name'] && $level2_name['name'] === $name) {
                            $check = 1;
                        } else if ($user_name['name'] === $level1_name['name'] && $level1_name['name'] !== $name && $level2_name['name'] !== $name) {
                            $check = 0;
                        }
                    }
                }
            }
        }

        return response()->json([
            'check' => $check,
        ]);
    }
}
