<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PlantsController extends Controller
{
    public function index(Request $request)
    {
    if (! Gate::allows('role_view')) {
        return abort(403);
    }

    if($request->showAll) {
        $query = Role::with('permissions')->orderBy('name','asc')->get();
    }else{
        $db = DB::connection('sqlsrv2')->getDatabaseName();
        $searchValue = $request->search;
        $orderBy = $request->sortby;
        $orderByDir = $request->sortdir;
        $perPage = $request->currentpage;

        // $query = Role::with('permissions')->where('name', 'LIKE', "%$searchValue%")
        //     ->orderBy($orderBy, $orderByDir)->paginate($perPage);
        $query = User::with('permissions')
        ->select('pb.NAME1','pb.WERKS','users.*')
        ->join('plantbranch','users.id','=','plantbranch.users_id')
        ->join($db.'.dbo.PLANT_BRANCHES as pb','plantbranch.WERKS','=','pb.WERKS')
        ->where('users.name', 'LIKE', "%$searchValue%")
        ->orwhere('users.email', 'LIKE', "%$searchValue%")
        ->orwhereDate('users.created_at', 'LIKE', "%$searchValue%")
        ->orderBy($orderBy, $orderByDir)->paginate($perPage);
    }

    return new RoleCollection($query);
}
}
