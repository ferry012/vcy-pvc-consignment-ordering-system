<?php

namespace App;

use App\Jobs\PVCStocksJob;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Validator;
use phpDocumentor\Reflection\Location;

class Stocks extends Model
{
    protected $table = 'stocks';
    protected $fillable = ['barcode', 'mat_code', 'mat_des', 'sales_unit', 'barcode', 'ERDAT', 'KWMENG', 'stocks_left', 'order_qty', 'approve_qty'
        ,'box_no','remarks'];

    public function stocks()
    {
        return $this->hasMany('App\StockNo', 'stocks_id');
    }

    public static function NotifyUsers($stocksData,$stock_header){

        $notifyUsersLevel1 = User::permission('email_approver_level_1')->get();
        $notifyUsersLevel2 = User::permission('email_approver_level_1')->get();
        $get_level_one = User::permission('email_approver_level_1')->first();
        $usersToAdmins = User::permission('email_admin')->get();

        $arrData = [];

        foreach ($usersToAdmins as $key => $recipient){
            $arrData = [
                'stocks' => $stocksData,
                'recipient' => $get_level_one->name,
                'header' => $stock_header,
            ];
            PVCStocksJob::dispatch($arrData,$recipient->email); //done
        }

        foreach ($notifyUsersLevel1 as $key => $recipient){
            $arrData = [
                'stocks' => $stocksData,
                'recipient' => $get_level_one->name,
                'header' => $stock_header,
            ];
            PVCStocksJob::dispatch($arrData,$recipient->email); //done
        }

    }

    public static function CreateStocks($request)
    {
        $name = auth()->user()->name; //$name === 'Joaquin Pigar Jr.' || $name === 'Kara Katrina Caceres'
        $date = Carbon::now()->format('d/m/Y');
        $stockNo = null;
        $arrID = [];
        $query = '';
        $arrPush = [];
        foreach ($request[0]['stocks'] as $key => $stocks) {

            $data = array();
            $data['barcode'] = $stocks['barcode'];
            $data['mat_code'] = $stocks['mat_code'];
            $data['mat_des'] = $stocks['mat_des'];
            $data['sales_unit'] = $stocks['uom'];
            $data['ERDAT'] = $stocks['last_fill_date']; // date
            $data['KWMENG'] = $stocks['last_fill_qty'];

            if(!isset($stocks['stocks_left'])){ // false
                $data['stocks_left'] = null;
            }
            elseif(isset($stocks['stocks_left'])){ //TRUE
                $data['stocks_left'] = $stocks['stocks_left'];
            }

            if(!isset($stocks['suggest_qty'])){
                $data['order_qty'] =  null;
            }
            elseif (isset($stocks['suggest_qty'])){
                $data['order_qty'] =  $stocks['suggest_qty']; // suggest_order_qty
            }

            if(!isset($stocks['approve_qty'])){
                $data['approve_qty'] = null;
            }
            elseif (isset($stocks['approve_qty'])){
                $data['approve_qty'] = $stocks['approve_qty'];
            }

            $query = Stocks::create($data);
            $arrPush[$key] = [
                $data
            ];
            $getID = Stocks::select('id')->latest()->first();
            $arrID[] = [
                'id' => $getID->id,
            ];
        }
//        StockNo::select('stock_req_no')->latest()->first();
        $getStockNo = StockNo::max('stock_req_no');
        if ($getStockNo === null) {
            $format = 100001; // 4 zeros
            $stockNo = $format;
        } else {
            $stockNo = $getStockNo + 1;
        }
        foreach ($arrID as $key => $id) {
            $query_stockNo = StockNo::create([
                'stocks_id' => $id['id'],
                'stock_req_no' => $stockNo,
            ]);
        }

        $stock_header = StockHeader::create([
            'stock_req_no' => $stockNo,
            'stock_req_date' => $request[0]['stocks'][0]['stockDate'],
            'company' => $request[0]['stocks'][0]['cust_company'],
            'creator' => $name,
            'status' => 'PENDING',

        ]);

        $getHeader = StockHeader::where('stocks_header.stock_req_no','=',$stockNo)
            ->select('stocks_header.*')
            ->get();

        ApprovalMappingRequest::create([
            'stock_no' => $stockNo,
        ]);

        $release_print = ForPrinting::max('release_form');
        $delivery_print = ForPrinting::max('delivery_receipt');

        if($release_print === null || $delivery_print === null){
            $forPrinting = ForPrinting::create([
                'stock_no' => $stockNo,
                'release_form' => '000001',
                'delivery_receipt' => '000001',
            ]);
        }
        else{
            $total_rel = $release_print + 1;
            $test_rel =  str_pad((string)$total_rel,6,"0",STR_PAD_LEFT);

            $total_del = $delivery_print + 1;
            $test_del =  str_pad((string)$total_del,6,"0",STR_PAD_LEFT);

            $forPrinting = ForPrinting::create([
                'stock_no' => $stockNo,
                'release_form' => $test_rel,
                'delivery_receipt' => $test_del,
            ]);
        }


        $host = gethostname();
        $nameUser = auth()->user()->name;
        $clientIP = $_SERVER['HTTP_CLIENT_IP']
            ?? $_SERVER["HTTP_CF_CONNECTING_IP"] # when behind cloudflare
            ?? $_SERVER['HTTP_X_FORWARDED']
            ?? $_SERVER['HTTP_X_FORWARDED_FOR']
            ?? $_SERVER['HTTP_FORWARDED']
            ?? $_SERVER['HTTP_FORWARDED_FOR']
            ?? $_SERVER['REMOTE_ADDR']
            ?? '0.0.0.0';

        $clientIP = '0.0.0.0';

        if (isset($_SERVER['HTTP_CLIENT_IP'])) {
            $clientIP = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (isset($_SERVER['HTTP_CF_CONNECTING_IP'])) {
            $clientIP = $_SERVER['HTTP_CF_CONNECTING_IP'];
        } elseif (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $clientIP = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } elseif (isset($_SERVER['HTTP_X_FORWARDED'])) {
            $clientIP = $_SERVER['HTTP_X_FORWARDED'];
        } elseif (isset($_SERVER['HTTP_FORWARDED_FOR'])) {
            $clientIP = $_SERVER['HTTP_FORWARDED_FOR'];
        } elseif (isset($_SERVER['HTTP_FORWARDED'])) {
            $clientIP = $_SERVER['HTTP_FORWARDED'];
        } elseif (isset($_SERVER['REMOTE_ADDR'])) {
            $clientIP = $_SERVER['REMOTE_ADDR'];
        }

        activity('Stock Request - Create')
            ->log('[WEB]' . ' ' . $nameUser . ' ' . 'has created a stock request #'. ' '. $stockNo . ' '
                . 'using' . ' ' . $host . ' ' . 'computer'. ' '. ' with an ip address of ' . ' ' .$clientIP);

        self::NotifyUsers($arrPush,$getHeader);

        return $query;
    }
    public static function divertDraftsToStocks($request,$id)
    {
        $name = auth()->user()->name; //$name === 'Joaquin Pigar Jr.' || $name === 'Kara Katrina Caceres'
        $date = Carbon::now()->format('d/m/Y');
        $stockNo = null;
        $arrID = [];
        $arrPush = [];
//        $Drafts = Drafts::join('drafts_no as a', 'drafts.id', '=', 'a.drafts_id')
//            ->where('a.drafts_req_no', '=', $id)
//            ->select('drafts.id as id')
//            ->get();
//        $arr = [];
//        foreach ($Drafts as $key => $code){
//            $query_Drafts = Drafts::findOrFail($code['id']);
//            $query_Drafts->delete();
//        }
//        $DraftsNo = DraftsNo::where('drafts_req_no', '=', $id)->delete();
        DraftsHeader::where('drafts_req_no', $id)
            ->update([
                'save_as' => null,
            ]);
        foreach ($request[0]['stocks'] as $key => $stocks) {

            $data = array();
            $data['barcode'] = $stocks['barcode'];
            $data['mat_code'] = $stocks['mat_code'];
            $data['mat_des'] = $stocks['mat_des'];
            $data['sales_unit'] = $stocks['sales_unit'];
            $data['ERDAT'] = $stocks['ERDAT']; // date
            $data['KWMENG'] = $stocks['KWMENG'];
            if(!isset($stocks['stocks_left'])){ // false
                $data['stocks_left'] = null;
            }
            elseif(isset($stocks['stocks_left'])){ //TRUE
                $data['stocks_left'] = $stocks['stocks_left'];
            }

            if(!isset($stocks['order_qty'])){
                $data['order_qty'] =  null;
            }
            elseif (isset($stocks['order_qty'])){
                $data['order_qty'] =  $stocks['order_qty']; // suggest_order_qty
            }

            if(!isset($stocks['approve_qty'])){
                $data['approve_qty'] = null;
            }
            elseif (isset($stocks['approve_qty'])){
                $data['approve_qty'] = $stocks['approve_qty'];
            }

            $query = Stocks::create($data);
            $getID = Stocks::select('id')->latest()->first();
            $arrPush[$key] = [
                $data
            ];
            $arrID[] = [
                'id' => $getID->id,
            ];
        }

        DraftsHeader::where('drafts_req_no','=',$id)
            ->update([
            'save_as' => null,
        ]);
        $getStockNo = StockNo::max('stock_req_no');
        if ($getStockNo === null) {
            $format = 100001; // 4 zeros
            $stockNo = $format;
        } else {
            $stockNo = $getStockNo + 1;
        }

        foreach ($arrID as $key => $id) {
            $query_stockNo = StockNo::create([
                'stocks_id' => $id['id'],
                'stock_req_no' => $stockNo,
            ]);
        }

        $stock_header = StockHeader::create([
            'stock_req_no' => $stockNo,
            'stock_req_date' => $request[0]['header'][0]['req_date'],
            'company' => $request[0]['header'][0]['cust_company'],
            'creator' => $name,
            'status' => 'PENDING',
        ]);

         ApprovalMappingRequest::create([
            'stock_no' => $stockNo,
        ]);

        $release_print = ForPrinting::max('release_form');
        $delivery_print = ForPrinting::max('delivery_receipt');

        if($release_print === null || $delivery_print === null){
            $forPrinting = ForPrinting::create([
                'stock_no' => $stockNo,
                'release_form' => '000001',
                'delivery_receipt' => '000001',
            ]);
        }
        else{
            $total_rel = $release_print + 1;
            $test_rel =  str_pad((string)$total_rel,6,"0",STR_PAD_LEFT);

            $total_del = $delivery_print + 1;
            $test_del =  str_pad((string)$total_del,6,"0",STR_PAD_LEFT);

            $forPrinting = ForPrinting::create([
                'stock_no' => $stockNo,
                'release_form' => $test_rel,
                'delivery_receipt' => $test_del,
            ]);
        }
        $getHeader = StockHeader::where('stocks_header.stock_req_no','=',$stockNo)
            ->select('stocks_header.*')
            ->get();
        $host = gethostname();
        $nameUser = auth()->user()->name;
        $clientIP = $_SERVER['HTTP_CLIENT_IP']
            ?? $_SERVER["HTTP_CF_CONNECTING_IP"] # when behind cloudflare
            ?? $_SERVER['HTTP_X_FORWARDED']
            ?? $_SERVER['HTTP_X_FORWARDED_FOR']
            ?? $_SERVER['HTTP_FORWARDED']
            ?? $_SERVER['HTTP_FORWARDED_FOR']
            ?? $_SERVER['REMOTE_ADDR']
            ?? '0.0.0.0';

        $clientIP = '0.0.0.0';

        if (isset($_SERVER['HTTP_CLIENT_IP'])) {
            $clientIP = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (isset($_SERVER['HTTP_CF_CONNECTING_IP'])) {
            $clientIP = $_SERVER['HTTP_CF_CONNECTING_IP'];
        } elseif (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $clientIP = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } elseif (isset($_SERVER['HTTP_X_FORWARDED'])) {
            $clientIP = $_SERVER['HTTP_X_FORWARDED'];
        } elseif (isset($_SERVER['HTTP_FORWARDED_FOR'])) {
            $clientIP = $_SERVER['HTTP_FORWARDED_FOR'];
        } elseif (isset($_SERVER['HTTP_FORWARDED'])) {
            $clientIP = $_SERVER['HTTP_FORWARDED'];
        } elseif (isset($_SERVER['REMOTE_ADDR'])) {
            $clientIP = $_SERVER['REMOTE_ADDR'];
        }

        activity('Drafts To Stock Request- Create')
            ->log('[WEB]' . ' ' . $nameUser . ' ' . 'has created a stock request #' . ' ' .$stockNo .' '
                . 'using' . ' ' . $host . ' ' . 'computer'. ' '. ' with an ip address of ' . ' ' .$clientIP);

        self::NotifyUsers($arrPush,$getHeader);

        return $query;
    }
}
