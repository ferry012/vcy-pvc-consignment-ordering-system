<?php

namespace App;

use App\Model\Inventory;
use App\Model\Invoice;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class InvoiceSapMessages extends Model
{
    protected $table = 'invoice_sap_messages';

    protected $fillable = [
        'invoice_number', 'sap_message'
    ];

    public static function importXMLposted()
    {
//        set_time_limit(0);
//        ini_set('memory_limit', '-1');
//
//        $path = env('FTP_ROOT') . "/ERROR/";
//        $url = Storage::disk('ftp')->allFiles($path);
//
//        foreach ($url as $key => $files) {
//            $xml = Storage::disk('ftp')->get($files);
//            $file = simplexml_load_string($xml, null, LIBXML_NOCDATA);
//            foreach ($file as $row){
//
//                $invoice = (string)$row->INVOICE;
//                $message = (string)$row->LINE;
//
//                // if not deficit add 1
//                // if deficit 0
//                $item = InvoiceSapMessages::where('invoice_number', '=', $invoice)->first();
//
//                $detail = Invoice::where('invoice_number','=',$invoice)->first();
//                $marketingInv = Inventory::where('INVENTORY.MATNR', '=', $detail->invoice_MATNR)
//                    ->where('INVENTORY.WERKS', '=', $detail->invoice_plant)
//                    ->first();
//
//                if(Str::contains($message,'Deficit')){
//                    $marketingInv->LABST = 0;
//                    $marketingInv->save();
//
//                }else{
//                    $marketingInv->LABST = $marketingInv->LABST + 1;
//                    $marketingInv->save();
//                }
//
//                $item->sap_message = $message;
//                $item->save();
//            }
//
//
//            Storage::disk('ftp')->delete($files);
//        }

    }
}
