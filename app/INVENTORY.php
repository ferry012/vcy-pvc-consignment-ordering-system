<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class INVENTORY extends Model
{
    protected $connection = 'sqlsrv2';
    protected $table = 'INVENTORY';
}
