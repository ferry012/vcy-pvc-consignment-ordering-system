<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalesOrderHeader extends Model
{
    protected $table = 'sales_order_header';
    protected $fillable = ['stock_req_no','sales_order_date','sales_order_number','status','request_by','approved_by','po_number','po_date','net_value','company'];
}
