<?php

namespace App;


use Illuminate\Database\Eloquent\Model;

class Plant extends Model
{
    protected $connection = 'sqlsrv2';
    protected $table = 'PLANT_BRANCHES';
    protected $fillable = ['WERKS','NAME1'];
}
