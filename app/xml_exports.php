<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class xml_exports extends Model
{
    protected $table='xml_exports';
    protected $connection ='sqlsrv';
    protected $fillable = [
        'WERKS', 'cost_center', 'gl_account'
    ];
}
