<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomerMaster extends Model
{
    protected $connection = 'sqlsrv2';
    protected $table = 'CUSTOMER_MASTER';
}
