<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MaterialMaster extends Model
{
    protected $connection = 'sqlsrv2';
    protected $table = 'MATERIAL_MASTER';
}
