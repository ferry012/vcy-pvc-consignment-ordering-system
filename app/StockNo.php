<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StockNo extends Model
{
    protected $table = 'stock_no';
    protected $fillable = ['stocks_id','stock_req_no'];


}
