<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Drafts extends Model
{
    protected $table = 'drafts';
    protected $fillable = ['barcode', 'mat_code', 'mat_des', 'sales_unit', 'barcode', 'ERDAT', 'KWMENG', 'stocks_left', 'order_qty', 'approve_qty'];

    public function draftNo()
    {
        return $this->hasMany('App\DraftsNo', 'drafts_id');
    }

    public static function CreateDrafts($request){
        $name = auth()->user()->name; //$name === 'Joaquin Pigar Jr.' || $name === 'Kara Katrina Caceres'
        $date = Carbon::now()->format('d/m/Y');
        $stockNo = null;
        $arrID = [];

        foreach ($request[0]['stocks'] as $key => $stocks) {

            $data = array();
            $data['barcode'] = $stocks['barcode'];
            $data['mat_code'] = $stocks['mat_code'];
            $data['mat_des'] = $stocks['mat_des'];
            $data['sales_unit'] = $stocks['uom'];
            $data['ERDAT'] = $stocks['last_fill_date']; // date
            $data['KWMENG'] = $stocks['last_fill_qty'];
            if(!isset($stocks['stocks_left'])){ // false
                $data['stocks_left'] = null;
            }
            elseif(isset($stocks['stocks_left'])){ //TRUE
                $data['stocks_left'] = $stocks['stocks_left'];
            }
            if(!isset($stocks['suggest_qty'])){
                $data['order_qty'] =  null;
            }
            elseif (isset($stocks['suggest_qty'])){
                $data['order_qty'] =  $stocks['suggest_qty']; // suggest_order_qty
            }
            if(!isset($stocks['approve_qty'])){
                $data['approve_qty'] = null;
            }
            elseif (isset($stocks['approve_qty'])){
                $data['approve_qty'] = $stocks['approve_qty'];
            }

            $query = Drafts::create($data);
            $getID = Drafts::select('id')->latest()->first();
            $arrID[] = [
                'id' => $getID->id,
            ];
        }
        $getStockNo = DraftsNo::max('drafts_req_no');
        if ($getStockNo === null) {
            $format = 900001; // 4 zeros
            $stockNo = $format;
        } else {
            $stockNo = $getStockNo + 1;
        }
        foreach ($arrID as $key => $id) {
            $query_stockNo = DraftsNo::create([
                'drafts_id' => $id['id'],
                'drafts_req_no' => $stockNo,
            ]);
        }
//        dd($request[0]['header'][0]);
        if(!isset($request[0]['stocks'][0]['stockDate']) || !isset($request[0]['stocks'][0]['cust_company'])){ //false
            $stock_header = DraftsHeader::create([
                'drafts_req_no' => $stockNo,
                'drafts_req_date' => $request[0]['header'][0]['stockDate'],
                'company' => $request[0]['header'][0]['cust_company'],
                'creator' => $name,
                'save_as' =>'ACTIVE',
            ]);
        }
        else{
            $stock_header = DraftsHeader::create([
                'drafts_req_no' => $stockNo,
                'drafts_req_date' => $request[0]['stocks'][0]['stockDate'],
                'company' => $request[0]['stocks'][0]['cust_company'],
                'creator' => $name,
                'save_as' =>'ACTIVE',
            ]);
        }

        return $query;
    }

    public static function updateDrafts($request,$id){

        $name = auth()->user()->name; //$name === 'Joaquin Pigar Jr.' || $name === 'Kara Katrina Caceres'
        $date = Carbon::now()->format('d/m/Y');
        $stockNo = null;
        $arrID = [];

        $Drafts = Drafts::join('drafts_no as a', 'drafts.id', '=', 'a.drafts_id')
            ->where('a.drafts_req_no', '=', $id)
            ->select('drafts.id as id')
            ->get();
        $arr = [];
        foreach ($Drafts as $key => $code){
            $query_Drafts = Drafts::findOrFail($code['id']);
            $query_Drafts->delete();
        }
        $DraftsHeader = DraftsHeader::where('drafts_req_no', $id)->delete();
        $DraftsNo = DraftsNo::where('drafts_req_no', '=', $id)->delete();

        foreach ($request[0]['stocks'] as $key => $stocks) {
//            dd(isset($stocks['stocks_left']));
            $data = array();
            $data['barcode'] = $stocks['barcode'];
            $data['mat_code'] = $stocks['mat_code'];
            $data['mat_des'] = $stocks['mat_des'];
            $data['sales_unit'] = $stocks['sales_unit'];
            $data['ERDAT'] = $stocks['ERDAT']; // date
            $data['KWMENG'] = $stocks['KWMENG'];

            if(!isset($stocks['stocks_left'])){ // false
                $data['stocks_left'] = null;
            }
            elseif(isset($stocks['stocks_left'])){ //TRUE
                $data['stocks_left'] = $stocks['stocks_left'];
            }
            if(!isset($stocks['order_qty'])){
                $data['order_qty'] =  null;
            }
            elseif (isset($stocks['order_qty'])){
                $data['order_qty'] =  $stocks['order_qty']; // suggest_order_qty
            }
            if(!isset($stocks['approve_qty'])){
                $data['approve_qty'] = null;
            }
            elseif (isset($stocks['approve_qty'])){
                $data['approve_qty'] = $stocks['approve_qty'];
            }


            $query = Drafts::create($data);
            $getID = Drafts::select('id')->latest()->first();
            $arrID[] = [
                'id' => $getID->id,
            ];
        }
        foreach ($arrID as $key => $drafts_id) {
            $query_stockNo = DraftsNo::create([
                'drafts_id' => $drafts_id['id'],
                'drafts_req_no' => (integer)$id, // draft stock no
            ]);
        }
        $stock_header = DraftsHeader::create([
            'drafts_req_no' => (integer)$id,  // draft stock no
            'drafts_req_date' => $request[0]['header'][0]['req_date'],
            'company' => $request[0]['header'][0]['cust_company'],
            'creator' => $name,
            'save_as' =>'ACTIVE',
        ]);

        $host = gethostname();
        $nameUser = auth()->user()->name;
        $clientIP = $_SERVER['HTTP_CLIENT_IP']
            ?? $_SERVER["HTTP_CF_CONNECTING_IP"] # when behind cloudflare
            ?? $_SERVER['HTTP_X_FORWARDED']
            ?? $_SERVER['HTTP_X_FORWARDED_FOR']
            ?? $_SERVER['HTTP_FORWARDED']
            ?? $_SERVER['HTTP_FORWARDED_FOR']
            ?? $_SERVER['REMOTE_ADDR']
            ?? '0.0.0.0';

        $clientIP = '0.0.0.0';

        if (isset($_SERVER['HTTP_CLIENT_IP'])) {
            $clientIP = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (isset($_SERVER['HTTP_CF_CONNECTING_IP'])) {
            $clientIP = $_SERVER['HTTP_CF_CONNECTING_IP'];
        } elseif (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $clientIP = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } elseif (isset($_SERVER['HTTP_X_FORWARDED'])) {
            $clientIP = $_SERVER['HTTP_X_FORWARDED'];
        } elseif (isset($_SERVER['HTTP_FORWARDED_FOR'])) {
            $clientIP = $_SERVER['HTTP_FORWARDED_FOR'];
        } elseif (isset($_SERVER['HTTP_FORWARDED'])) {
            $clientIP = $_SERVER['HTTP_FORWARDED'];
        } elseif (isset($_SERVER['REMOTE_ADDR'])) {
            $clientIP = $_SERVER['REMOTE_ADDR'];
        }

        activity('Drafts - Update')
            ->log('[WEB]' . ' ' . $nameUser . ' ' . 'has updated a draft #' . ' ' .$id .' '
                . 'using' . ' ' . $host . ' ' . 'computer'. ' '. ' with an ip address of ' . ' ' .$clientIP);

        return $query;
    }
}
