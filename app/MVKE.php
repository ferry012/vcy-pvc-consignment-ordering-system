<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MVKE extends Model
{
    protected $connection = 'sqlsrv2';
    protected $table = 'MVKE';
}
