<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KNA1 extends Model
{
    protected $connection = 'sqlsrv2';
    protected $table = 'KNA1';
}
