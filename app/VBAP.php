<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VBAP extends Model
{
    protected $connection = 'sqlsrv2';
    protected $table = 'VBAP';
}
