<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MerchandiserCust extends Model
{
    protected $table = 'merchandiser_cust';
    protected $fillable = [
        'users_id','VKORG','NAME1'
    ];
}
