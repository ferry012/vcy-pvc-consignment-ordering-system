<?php

namespace App\Exports;

use App\Model\Invoice;
use App\Model\MarketingInventory;
use Carbon\Carbon;
use DateTime;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Concerns\WithEvents;

class ExcelExport implements FromCollection, WithHeadings, ShouldAutoSize,WithStyles,WithEvents
{
    /**
     * @return \Illuminate\Support\Collection
     *
     */
    protected $done;

    public function __construct($from,$to)
    {

//        $this->done = $done;
        $this->from = $from;
        $this->to = $to;

    }

    public function collection()
    {
//        dd($this->from , $this->to);
        $db = DB::connection('sqlsrv2')->getDatabaseName();
        return Invoice::join($db.'.dbo.MBEW as mb', function ($join) {
            $join->on('mb.MATNR', '=', 'invoices.invoice_MATNR');
            $join->on('mb.BWKEY', '=', 'invoices.invoice_plant');
        })
//            ->where('invoices.invoice_date', '=', $this->done)
            ->whereBetween('invoices.created_at', [Carbon::parse($this->from)->startOfDay(), Carbon::parse($this->to)->endOfDay()])
            ->latest()
            ->get(['invoices.invoice_plant',
                'invoices.invoice_date',
                'invoices.invoice_number',
                'invoices.total_amount',
                'invoices.invoice_market_MAKTX',
                'mb.VERPR',
                'invoices.cost_invoice_amount',]);
//        dd($this->done);
    }

    public function headings(): array
    {
        return [
            'Plant / Branch' ,
            'Date of Redemption',
            'Invoice Number',
            'Invoice Amount',
            'Freebie Redeemed',
            'Cost of Freebie',
            'Cost / Invoice Amount',
        ];
    }

    public function styles(Worksheet $sheet)
    {
        $sheet->getStyle(1)->getFont()->setBold(true);
//        $sheet->setAlignment('center');

    }
    public function registerEvents(): array
    {
        return [
            AfterSheet::class=> function(AfterSheet $event) {
                $event->sheet->styleCells(
                    'A2:A*',
                    'B2:B*',
                    'C2:C*',
                    'D2:D*',
                    'E2:E*',
                    'F2:F*',
                    'G2:G*',
                    [
                        'alignment' => [
                            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,

//                            $spreadsheet->getActiveSheet()->getStyle('B2')
//                                ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
//                            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT,
                        ],
                    ]
                );
            },
        ];
    }

}
