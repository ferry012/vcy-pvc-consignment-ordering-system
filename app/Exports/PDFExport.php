<?php

namespace App\Exports;

use App\Model\Invoice;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithColumnWidths;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class PDFExport implements FromCollection, WithHeadings, WithColumnWidths,WithStyles
{
    /**
    * @return \Illuminate\Support\Collection
    */

        public function __construct($from,$to)
    {
        $this->from = $from;
        $this->to = $to;

    }
    public function collection()
    {

        $db = DB::connection('sqlsrv2')->getDatabaseName();
        return Invoice::join($db.'.dbo.MBEW as mb', function ($join) {
            $join->on('mb.MATNR', '=', 'invoices.invoice_MATNR');
            $join->on('mb.BWKEY', '=', 'invoices.invoice_plant');
        })
            ->whereBetween('invoices.created_at', [Carbon::parse($this->from)->startOfDay(), Carbon::parse($this->to)->endOfDay()])
            ->latest()
            ->get([
                'invoices.invoice_plant',
                'invoices.invoice_date',
                'invoices.invoice_number',
                'invoices.total_amount',
                'invoices.invoice_market_MAKTX',
                'mb.VERPR',
                'invoices.cost_invoice_amount',]);

    }

    public function headings(): array
    {
        return [
            'Plant / Branch' ,
            'Date of Redemption',
            'Invoice Number',
            'Invoice Amount',
            'Freebie Redeemed',
            'Cost of Freebie',
            'Cost / Invoice Amount',
        ];
    }

    public function columnWidths(): array
    {
        return [
            'A' => 13,
            'B' => 15,
            'C' => 15,
            'D' => 15,
            'E' => 15,
            'F' => 15,
            'G' => 15,
        ];
    }
    public function styles(Worksheet $sheet)
    {
        $sheet->getStyle(1)->getFont()->setBold(true);
//        return [
//            // Style the first row as bold text.
//            1    => ['font' => ['bold' => true]],
//
//            // Styling a specific cell by coordinate.
//            'B2' => ['font' => ['italic' => true]],
//
//            // Styling an entire column.
//            'C'  => ['font' => ['size' => 16]],
//        ];
    }

}
