<?php

namespace App\Exports;

use App\Model\Invoice;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class InventoryReportExport implements WithHeadings, ShouldAutoSize, WithStyles, WithEvents, FromArray
{
    /**
     * @return \Illuminate\Support\Collection
     */
    protected $done;

    public function __construct($from,$to)
    {
        $this->from = $from;
        $this->to = $to;
    }
    public function array(): array
    {
        $db = DB::connection('sqlsrv2')->getDatabaseName();
        $query = DB::table('invoices as a')
            ->join($db . '.dbo.INVENTORY as b', function ($join) {
                $join->on('b.MATNR', '=', 'a.invoice_MATNR');
                $join->on('b.WERKS', '=', 'a.invoice_plant');
            })
            ->whereBetween('a.created_at', [Carbon::parse($this->from)->startOfDay(), Carbon::parse($this->to)->endOfDay()])
//            ->where('a.invoice_date', '=', $this->done)
            ->select('b.MATNR', 'a.invoice_market_MAKTX',
                DB::raw("'2311_total_qty'=(SELECT SUM(d.qty) FROM invoices AS d
		            WHERE b.MATNR = d.invoice_MATNR AND d.invoice_plant = 2311)"),
                DB::raw("'2311_total_LABST'=(SELECT SUM(LABST) FROM VCY820.dbo.INVENTORY AS c
		            WHERE b.MATNR = c.MATNR AND c.WERKS = 2311)"),

                DB::raw("'2321_total_qty'=(SELECT SUM(d.qty) FROM invoices AS d
		            WHERE b.MATNR = d.invoice_MATNR AND d.invoice_plant = 2321)"),
                DB::raw("'2321_total_LABST'=(SELECT SUM(LABST) FROM VCY820.dbo.INVENTORY AS c
		            WHERE b.MATNR = c.MATNR AND c.WERKS = 2321)"),

                DB::raw("'2331_total_qty'=(SELECT SUM(d.qty) FROM invoices AS d
		            WHERE b.MATNR = d.invoice_MATNR AND d.invoice_plant = 2331)"),
                DB::raw("'2331_total_LABST'=(SELECT SUM(LABST) FROM VCY820.dbo.INVENTORY AS c
		            WHERE b.MATNR = c.MATNR AND c.WERKS = 2331)"),

                DB::raw("'2341_total_qty'=(SELECT SUM(d.qty) FROM invoices AS d
		            WHERE b.MATNR = d.invoice_MATNR AND d.invoice_plant = 2341)"),
                DB::raw("'2341_total_LABST'=(SELECT SUM(LABST) FROM VCY820.dbo.INVENTORY AS c
		            WHERE b.MATNR = c.MATNR AND c.WERKS = 2341)"),

                DB::raw("'2351_total_qty'=(SELECT SUM(d.qty) FROM invoices AS d
		            WHERE b.MATNR = d.invoice_MATNR AND d.invoice_plant = 2351)"),
                DB::raw("'2351_total_LABST'=(SELECT SUM(LABST) FROM VCY820.dbo.INVENTORY AS c
		            WHERE b.MATNR = c.MATNR AND c.WERKS = 2351)"),

                DB::raw("'2361_total_qty'=(SELECT SUM(d.qty) FROM invoices AS d
		            WHERE b.MATNR = d.invoice_MATNR AND d.invoice_plant = 2361)"),
                DB::raw("'2361_total_LABST'=(SELECT SUM(LABST) FROM VCY820.dbo.INVENTORY AS c
		            WHERE b.MATNR = c.MATNR AND c.WERKS = 2361)")
            )
            ->groupBy('b.MATNR', 'a.invoice_market_MAKTX')
            ->get()
            ->toArray();

//        dd($query);
        return $query;


    }

    public function map($query): array
    {
        return [
            $query->MATNR,
            $query->invoice_market_MAKTX,
            $query['2311_total_qty'],
            $query['2311_total_LABST'],
            $query['2321_total_qty'],
            $query['2321_total_LABST'],
            $query['2331_total_qty'],
            $query['2331_total_LABST'],
            $query['2341_total_qty'],
            $query['2341_total_LABST'],
            $query['2351_total_qty'],
            $query['2351_total_LABST'],
            $query['2361_total_qty'],
            $query['2361_total_LABST'],


        ];
    }

    public function headings(): array
    {
        return [
            'Material #',
            'Material Description',
            '2311',
            'Onhand',
            '2321',
            'Onhand',
            '2331',
            'Onhand',
            '2341',
            'Onhand',
            '2351',
            'Onhand',
            '2361',
            'Onhand',
        ];
    }

    public function styles(Worksheet $sheet)
    {
        $sheet->getStyle(1)->getFont()->setBold(true);
//        $sheet->setAlignment('center');

    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function (AfterSheet $event) {
                $event->sheet->styleCells(
                    'A2:A*',
                    'B2:B*',
                    'C2:C*',
                    'D2:D*',
                    'E2:E*',
                    'F2:F*',
                    'G2:G*',
                    [
                        'alignment' => [
                            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,

//                            $spreadsheet->getActiveSheet()->getStyle('B2')
//                                ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
//                            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT,
                        ],
                    ]
                );
            },
        ];
    }
}
