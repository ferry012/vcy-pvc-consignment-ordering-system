<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalesOrderDetails extends Model
{
    protected $connection = 'sqlsrv';
    protected $table = 'sales_order_details';
    protected $fillable = ['sales_order_no', 'MATNR', 'MAKTX', 'approved_qty', 'cost', 'total_cost'];

    public static function CreateSalesDetails($request,$id)
    {
        foreach ($request[0]['stocks'] as $key => $stocks) {
            $salesDetails = array();
            $salesDetails['sales_order_no'] = (integer)$id;
            $salesDetails['MATNR'] = $stocks['mat_code'];
            $salesDetails['MAKTX'] = $stocks['mat_des'];
            if($stocks['approve_qty'] === null){
                $salesDetails['approved_qty'] = 0;
            }
            else if($stocks['approve_qty'] !== null){
                $salesDetails['approved_qty'] = $stocks['approve_qty'];
            }
            $query = SalesOrderDetails::create($salesDetails);
        }
    }
}
