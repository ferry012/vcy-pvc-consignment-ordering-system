<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KNVV extends Model
{
    protected $connection = 'sqlsrv2';
    protected $table = 'KNVV';
}
