<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ApprovalMappingRequest extends Model
{
    protected $table = 'approval_mapping';
    protected $fillable = ['stock_no', 'level_one', 'level_two'];
}
