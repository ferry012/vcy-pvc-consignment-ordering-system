<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalesOrderSapMessagesDetails extends Model
{
    protected $table = 'sales_order_sap_messages_details';
    protected $fillable = ['stock_no','transaction','ref_id','lineError','dateError'];

    public function sub_details()
    {
        return $this->belongsTo('App\SalesOrderSapMessages','stock_no');
    }
}
