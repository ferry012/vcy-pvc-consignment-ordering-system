<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class SalesOrderSapMessages extends Model
{
    protected $table = 'sales_order_sap_messages';
    protected $fillable = ['sales_order_number','sap_message'];

    public function details() {
        return $this->hasMany('App\SalesOrderSapMessagesDetails','stock_no');
    }

    public static function importXMLError(){
//        dd('test');
        $ftp_path = new FtpPath;
        $filePath = $ftp_path->salesOrderPVCError();

        set_time_limit(0);
        ini_set('memory_limit', '-1');

        $url = Storage::disk('ftp')->allFiles($filePath);
        $stock_no = null;
        foreach ($url as $key => $files) {
            $xml = Storage::disk('ftp')->get($files);
            $file = simplexml_load_string($xml, null, LIBXML_NOCDATA);
            $arr = [];

            foreach ($file as $row){

                $stock_no = $row->TRANSACTION;
                $transaction = (string)$row->TRANSACTION;
                $ref_id = (string)$row->REFERENCE_ID;
                $dateError = (string)$row->DATE;
                $message = (string)$row->LINE;


                $sap_id = SalesOrderSapMessages::where('sales_order_number','=',$stock_no)->first();

                SalesOrderSapMessagesDetails::updateOrCreate([
                      'stock_no' => $sap_id->id,
                      'transaction' => $transaction,
                      'ref_id' => $ref_id,
                      'lineError' => $message,
                      'dateError' =>  $dateError,
                ]);
               SalesOrderSapMessages::where('sales_order_number', '=', $stock_no)
                    ->update([
                        'sap_message' => 'ERROR',
                    ]);

               SalesOrderHeader::where('stock_req_no','=',$stock_no)
                    ->update([
                       'status' => 'PENDING',
                    ]);
            }
            Storage::disk('ftp')->delete($files);
        }
//        dd($arr);
        if($stock_no !== null){
            $host = gethostname();
            $nameUser = SalesOrderHeader::where('stock_req_no','=',$stock_no)->first();

            activity('For Approval - POSTED ERROR')
                ->log('[WEB]' . ' ' . $nameUser->approved_by . ' ' . 'has posted a sales order number of' . ' ' .$stock_no
                    .' '. 'was error'. ' '. 'using' . ' ' . $host . ' ' . 'computer');
        }
    }
    public static function importXMLPosted(){

        $ftp_path = new FtpPath;
        $filePath = $ftp_path->salesOrderPVCPosted();

        set_time_limit(0);
        ini_set('memory_limit', '-1');

        $url = Storage::disk('ftp')->allFiles($filePath);
        $stock_no = null;
        foreach ($url as $key => $files) {
            $xml = Storage::disk('ftp')->get($files);
            $file = simplexml_load_string($xml, null, LIBXML_NOCDATA);
            foreach ($file as $row){

                $stock_no = (string)$row->TRANSACTION; //my stock id
                $sales_order_number = (string)$row->REFERENCE_ID;
                $sales_order_date = (string)$row->DATE;

                SalesOrderHeader::where('stock_req_no','=',$stock_no)
                    ->update([
                       'sales_order_number' => $sales_order_number,
                       'sales_order_date' => $sales_order_date,
                       'status' => 'POSTED',
                    ]);
                StockHeader::where('stock_req_no','=',$stock_no)
                    ->update([
                        'sales_order_number' => $sales_order_number,
                        'sales_order_date' => $sales_order_date,

                    ]);
            }
            Storage::disk('ftp')->delete($files);
        }

        if($stock_no !== null){
            $host = gethostname();
            $nameUser = SalesOrderHeader::where('stock_req_no','=',$stock_no)->first();

            activity('For Approval - POSTED SUCCESS')
                ->log('[WEB]' . ' ' . $nameUser->approved_by . ' ' . 'has posted a sales order number of' . ' ' .$sales_order_number
                    .' '. 'was success'. ' '. 'using' . ' ' . $host . ' ' . 'computer');
        }

    }
}
