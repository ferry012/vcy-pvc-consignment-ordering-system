<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DiserTablet extends Model
{
    protected $table = 'diser_tablets';
    protected $fillable = ['name','device_name','device_width'];
}
