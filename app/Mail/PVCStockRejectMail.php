<?php

namespace App\Mail;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class PVCStockRejectMail extends Mailable
{
    use Queueable, SerializesModels;
    protected $object;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($object)
    {
        $this->object = $object;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $varObj = $this->object;
        return $this->from(env('MAIL_USERNAME'), 'PVC Consignment Ordering System')
            ->subject('PVC Consignment Stock Request Automated Email')
            ->markdown('mails.pvc_notification_reject_mail',compact('varObj'))
            ->with([
                'name' => 'PVC Stock Request',
            ]);
    }
}
