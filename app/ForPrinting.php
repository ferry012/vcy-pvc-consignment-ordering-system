<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ForPrinting extends Model
{
    protected $table = 'for_printing';
    protected $fillable = ['stock_no','release_form','delivery_receipt'];
}
