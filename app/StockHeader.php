<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StockHeader extends Model
{
    protected $table = 'stocks_header';
    protected $fillable = ['stock_req_no','stock_req_date','company','status','creator','sales_order_date'];

    public function stockNo()
    {
        return $this->hasMany('App\stockNo', 'stock_req_no');
    }
}
