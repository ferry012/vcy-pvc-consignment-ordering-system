<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DraftsNo extends Model
{
    protected $table = 'drafts_no';
    protected $fillable = ['drafts_id','drafts_req_no'];
}
