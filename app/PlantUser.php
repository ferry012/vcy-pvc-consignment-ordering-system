<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlantUser extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'plantbranch';
    protected $fillable = [
        'WERKS','users_id','NAME1'
    ];
}
