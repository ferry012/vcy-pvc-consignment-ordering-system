<?php

namespace App\Jobs;

use App\Mail\PVCStockApproveToMerchMail;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class PVCStockApproveToMerchJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $object;
    protected $email;
    protected $contents;
    protected $tries = 5;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($object,$email)
    {
        $this->object = $object; // array data
        $this->email = $email; // receiver
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $data = new PVCStockApproveToMerchMail($this->object);
        Mail::to($this->email)->send($data);
    }
}
