<?php

namespace App\Jobs;

use App\Mail\PVCStockApproveMail;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class PVCStockApproveJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $object;
    protected $email;
    protected $contents;
    protected $tries = 5;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($object,$email)
    {
        $this->object = $object; // array data
        $this->email = $email; // receiver
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $data = new PVCStockApproveMail($this->object);
        Mail::to($this->email)->send($data);
    }
}
