<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FtpPath extends Model
{
    public $path = 'SO_DIY';
    public function salesOrderPVC(){
        return $this->path . '/SALES_ORDERS/';
    }public function salesOrderPVCError(){
        return $this->path . '/ERRORS/';
    }public function salesOrderPVCPosted(){
        return $this->path . '/POSTED/';
    }
}
