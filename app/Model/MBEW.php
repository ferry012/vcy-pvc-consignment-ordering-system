<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class MBEW extends Model
{
    protected $table='MBEW';
    protected $connection ='sqlsrv2';
    protected $fillable = [
        'VERPR','MATNR','BWKEY',
    ];
    protected $primaryKey = 'ID';
    public $timestamps = false;
}
